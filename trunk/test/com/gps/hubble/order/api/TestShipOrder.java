package com.gps.hubble.order.api;

import org.junit.Assert;
import org.junit.Test;

import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;

public class TestShipOrder extends TestBase{

	@Override
	protected void beforeTestCase() {
		super.beforeTestCase();
		manageCommonCode("ORDER_VALIDATION", "COST_CENTER", "N");
	}

	private YFCDocument getOrdDoc() {
		return YFCDocument.getDocumentFor("<Order EnterpriseCode='TELSTRA_SCLSA' Division='CXS041' OrderName='' InterfaceNo='INT_ODR_1' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS'  BillToID='TESTCUSTOMER' ShipToID='DAC'   OrderType='MATERIAL_RESERVATION' > " +
				"<PersonInfoBillTo FirstName='Keerthi' AddressLine2='' AddressLine3='' AddressLine4='' City='Bangalore' ZipCode='' State='' DayPhone='' DayFaxNo=''/><OrderLines/></Order>");
	}

	@Test
	public void mandatoryFieldsLineLevel() throws Exception{
		String errorString = "TEL_ERR_006_001";
		try{
			invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='TC001' >" +
					"<ShipmentLines>" +
					"<ShipmentLine Action='' PrimeLineNo='' SubLineNo='' ItemID='' StatusQuantity=''/>" +
					"</ShipmentLines>" +
					"</Shipment>"));

		}catch(Exception e){
			System.out.println(e.toString());
			if(e.toString().contains(errorString)){
				Assert.assertEquals("Pass", "Pass");
			}else{        
				Assert.assertEquals("Pass", "Fail");
			}
		} 
	}


	@Test
	public void mandatoryFieldsOrderName() throws Exception{
		String errorString = "TEL_ERR_006_001";
		try{
			invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='' >" +
					"<ShipmentLines>" +
					"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
					"</ShipmentLines>" +
					"</Shipment>"));
		}catch(Exception e){
			if(e.toString().contains(errorString)){
				Assert.assertEquals("Pass", "Pass");
			}else{
				Assert.assertEquals("Pass", "Fail");
			}
		} 
	}

	@Test
	public void orderHoldValidation() throws Exception{
		String errorStr = "TEL_ERR_006_002";

		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T1");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		invokeYantraApi("createOrder", ordDoc);

		ordDoc = getOrdDoc();   
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T2");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		invokeYantraApi("createOrder", ordDoc);

		try{
			invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T2' >" +
					"<ShipmentLines>" +
					"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
					"</ShipmentLines>" +
					"</Shipment>"));
		}catch (Exception e){
			if(e.toString().contains(errorStr)){
				System.out.println("Exception : " + e.toString());
				Assert.assertEquals("Pass", "Pass");        
			}else{
				Assert.assertEquals("Pass", "Fail");
			}
		}

	}

	@Test
	public void orderLineVsCEVMsgLineValidation() throws Exception{
		String errorStr = "TEL_ERR_006_006";

		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T3");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		invokeYantraApi("createOrder", ordDoc);

		try{
			invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T3' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE' >" +
					"<ShipmentLines>" +
					"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
					"<ShipmentLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
					"</ShipmentLines>" +
					"</Shipment>"));
		}catch (Exception e){
			if(e.toString().contains(errorStr)){
				System.out.println("Exception : " + e.toString());
				Assert.assertEquals("Pass", "Pass");        
			}else{
				Assert.assertEquals("Pass", "Fail");
			}
		}
	}

	@Test
	public void backOrderForAllOrdLineQts() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T4");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T4' >" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='0'/>" +
				"</ShipmentLines>" +
				"</Shipment>"));   
		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
		String expectedTotalNumberOfRecords="0";
		String resultTotalNumberOfRecords=Integer.toString(shipmentDoc.getElementsByTagName("Shipment").getLength());
		System.out.println("shipmentDoc : " + shipmentDoc.toString());
		Assert.assertEquals(expectedTotalNumberOfRecords, resultTotalNumberOfRecords);
	}

	@Test
	public void createShipmentForPartialShip() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T5");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T5");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

		invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T5' >" +
				"<OrderLines>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</OrderLines>" +
				"</OrderRelease>"));
		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T5' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE'>" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</ShipmentLines>" +
				"</Shipment>"));
		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
		String expectedTotalNumberOfRecords="1";
		String resultTotalNumberOfRecords=Integer.toString(shipmentDoc.getElementsByTagName("Shipment").getLength());
		System.out.println("shipmentDoc : " + shipmentDoc.toString());
		Assert.assertEquals(expectedTotalNumberOfRecords, resultTotalNumberOfRecords);
	}

	@Test
	public void createShipmentForFullPickMultiline() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T6");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T6");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).createChild("OrderLine");
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "3");
		orderLine.setAttribute("PrimeLineNo", "2");
		orderLine.setAttribute("ChangeStatus", "");
		itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());

		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

		invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T6' >" +
				"<OrderLines>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='17'/>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</OrderLines>" +
				"</OrderRelease>"));   

		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T6' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE'>" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='17'/>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</ShipmentLines>" +
				"</Shipment>")); 

		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"),YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status=''><ShipmentLines ><ShipmentLine ShipmentLineKey='' />" +
				"</ShipmentLines></Shipment></Shipments>"));
		String expectedShipmentStatus="1400";
		String resultShipmentStatus=XPathUtil.getXpathAttribute(shipmentDoc, "//Shipment/@Status");
		System.out.println("shipmentDoc : " + shipmentDoc.toString());
		Assert.assertEquals(expectedShipmentStatus, resultShipmentStatus);
	}

	@Test
	public void createShipmentWithTwoOrderReleaseKeys() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T6");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T6");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).createChild("OrderLine");
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "3");
		orderLine.setAttribute("PrimeLineNo", "2");
		orderLine.setAttribute("ChangeStatus", "");
		itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());    
		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);   
		YFCDocument sInDoc= YFCDocument.getDocumentFor("<ScheduleOrder OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"'  ScheduleAndRelease='Y' CheckInventory='N' />");
		System.out.println("sInDoc : " + sInDoc.toString());
		try{
			invokeYantraApi("scheduleOrder", sInDoc);
		}catch (Exception e){
			System.out.println("e : " + e.toString());
		}

		ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T6");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T6");
		System.out.println("Order constant values doc : " + ordDoc.toString());    
		orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "5");
		orderLine.setAttribute("PrimeLineNo", "3");
		orderLine.setAttribute("ChangeStatus", "");
		itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		invokeYantraService("GpsManageOrder", ordDoc);

		invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T6' >" +
				"<OrderLines>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='17'/>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='3' SubLineNo='1' ItemID='PenStand' StatusQuantity='5'/>" +
				"</OrderLines>" +
				"</OrderRelease>"));  
		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T6' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE' >" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='17'/>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='3' SubLineNo='1' ItemID='PenStand' StatusQuantity='5'/>" +
				"</ShipmentLines>" +
				"</Shipment>")); 

		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"),YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey='' Status=''><ShipmentLines ><ShipmentLine ShipmentLineKey='' />" +
				"</ShipmentLines></Shipment></Shipments>"));
		String expectedShipmentStatus="1400";
		String resultShipmentStatus=XPathUtil.getXpathAttribute(shipmentDoc, "//Shipment/@Status");
		System.out.println("shipmentDoc : " + shipmentDoc.toString());
		Assert.assertEquals(expectedShipmentStatus, resultShipmentStatus);
	}

	@Test
	public void orderAvailableValidation() throws Exception{
		String errorString = "TEL_ERR_006_007-1";
		try{
			invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='TC001000' >" +
					"<OrderLines>" +
					"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='1111' StatusQuantity='1'/>" +
					"</OrderLines>" +
					"</OrderRelease>"));
		}catch(Exception e){
			if(!e.toString().contains(errorString)){
				Assert.fail("This error string should come "+errorString);
			}
		} 
	}


	@Test
	public void createShipmentVectorOrd() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T5");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T5");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

		invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T5' >" +
				"<OrderLines>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</OrderLines>" +
				"</OrderRelease>"));
		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T5' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE'>" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</ShipmentLines>" +
				"</Shipment>"));

		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T5' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE'>" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</ShipmentLines>" +
				"</Shipment>"));

		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
		YFCNodeList<YFCElement> oEle = shipmentDoc.getDocumentElement().getElementsByTagName("Shipment");
		for (YFCElement yfcElement : oEle) {
			if(!yfcElement.getString().contains("Status=\"1400\"")){
				Assert.fail();
			}
		}
		System.out.println("shipmentDoc : " + shipmentDoc.toString());

	}

	@Test
	public void createShipmentVectorOrdPicked() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T5");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T5");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

		invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T5' >" +
				"<OrderLines>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				"</OrderLines>" +
				"</OrderRelease>"));
		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T5' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE'>" +
				"<ShipmentLines>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='14'/>" +
				"</ShipmentLines>" +
				"</Shipment>"));

		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
		System.out.println("shipmentDoc : " + shipmentDoc.toString());		
		if(!shipmentDoc.getString().contains("Status=\"1400\"")){
			Assert.fail();
		}
	}

	@Test
	public void createShipmentVectorOrdPickedTwoLines() throws Exception{
		YFCDocument ordDoc = getOrdDoc();       
		ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T5");
		ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T5");
		System.out.println("Order constant values doc : " + ordDoc.toString());
		YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
		orderLine.setAttribute("ShipNode", "TestOrg5");
		orderLine.setAttribute("OrderedQty", "17");
		orderLine.setAttribute("PrimeLineNo", "1");
		orderLine.setAttribute("ChangeStatus", "");
		YFCElement itemEle = orderLine.getChildElement("Item", true);
		itemEle.setAttribute("ItemID", "PenStand");
		itemEle.setAttribute("UnitOfMeasure", "Each");
		YFCElement orderLine1 = ordDoc.getDocumentElement().getChildElement("OrderLines").createChild("OrderLine");
		orderLine1.setAttribute("ShipNode", "TestOrg5");
		orderLine1.setAttribute("OrderedQty", "7");
		orderLine1.setAttribute("PrimeLineNo", "2");
		orderLine1.setAttribute("ChangeStatus", "");
		YFCElement itemEle1 = orderLine1.getChildElement("Item", true);
		itemEle1.setAttribute("ItemID", "PenStand");
		itemEle1.setAttribute("UnitOfMeasure", "Each");
		System.out.println("Order doc with Order Line : " + ordDoc.toString());
		YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

		
		invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T5' >" +
				"<OrderLines>" +
				"<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
				//"<OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='0'/>" +
				"</OrderLines>" +
				"</OrderRelease>"));
		
		invokeYantraService("GpsShipOrRejectOrder", YFCDocument.getDocumentFor("<Shipment OrderName='JUNIT-T5' ActualShipmentDate='2016-07-24T00:00:00-04:00' SCAC='ROYAL' TrackingNo='100001' CarrierServiceCode='AIE'>" +
				"<ShipmentLines>" +
				//"<ShipmentLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='0'/>" +
				"<ShipmentLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='7'/>" +
				"</ShipmentLines>" +
				"</Shipment>"));

		YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
		System.out.println("shipmentDoc : " + shipmentDoc.toString());		
		if(!shipmentDoc.getString().contains("Status=\"1400\"")){
			Assert.fail();
		}
	}

	private void manageCommonCode(String codeType,String codeValue, String shortDescription){
		YFCDocument manageCommonCodeInput = YFCDocument.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\""+shortDescription+"\" CodeValue=\""+codeValue+"\" CodeType='"+codeType+"' />");
		invokeYantraApi("manageCommonCode", manageCommonCodeInput);
	}

}

