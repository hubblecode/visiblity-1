package com.gps.hubble.order.api;

import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfs.japi.YFSException;

public class TestManageOrder extends TestBase {

	static int i=1;
	
	@Test
	public void testCreateOrder1(){

		try {
			/*
			Scenario:-
				InterfaceNo=INT_ODR_1
				OrderType=Material Reservation
				EntryType=Integral Plus
				BillToID=ext-id present 
				node type DC/LC/LS
			Outcome:-
				DocumentType=0006
				DepartmentCode=Inventory Materials
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "LC";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");

			System.out.println("******************* Order Creation Test Case 1 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder2(){

		try {
			/*
			Scenario:-			
				InterfaceNo=INT_ODR_2
				OrderType=TRANSPORT_ORDER
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id present
				NodeType=WLVS

			Outcome:-			
				DocumentType=0006
				DepartmentCode=Wireless
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "WLVS";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"WIRELESS");

			System.out.println("******************* Order Creation Test Case 2 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder3(){

		try {
			/*
			Scenario:-			
				InterfaceNo=INT_ODR_1
				OrderType=Transport Order 
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id is present
				NodeType=WBVS

			Outcome:-
				DocumentType=0006
				DepartmentCode=WIDEBAND
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "WBVS";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"WIDEBAND");

			System.out.println("******************* Order Creation Test Case 3 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}


	@Test
	public void testCreateOrder4(){

		try {
			/*
			Scenario:-			
                Scenario:-
Int -INT_ODR_1
Order Type- Material Reservation
Entry Type -Integral Plus

Expected Outcome:-
Receiving Node- CT Node
Ship Node- DC Node
DepartmentCode- Inventory Materials
DocumentType-0006
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "CT";
			String sNodeTypeShip = "DC";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");


			System.out.println("******************* Order Creation Test Case 4 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder5(){

		try {
			/*
			Scenario:-			
                Scenario:-
Int -INT_ODR_2
Order Type- Material Reservation
Entry Type -Integral Plus

Expected Outcome:-
Receiving Node- CT Node
Ship Node- DC Node
DepartmentCode- Inventory Materials
DocumentType-0006
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "CT";
			String sNodeTypeShip = "DC";



			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");


			System.out.println("******************* Order Creation Test Case 5 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder6(){

		try {
			/*
			Scenario:-			
                Scenario:-
Scenario:-
Int INT_ODR_1
Order Type -Transport Order
Entry Type -Integral Plus

Expected Outcome:-
Receiving Node- DC Node
Ship Node -DC Node
DepartmentCode- Inventory Materials
DocumentType-0006

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test

			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");


			System.out.println("******************* Order Creation Test Case 6 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder7(){

		try {
			/*
			Scenario:-			
                Scenario:-
Scenario:-
Int INT_ODR_2
Order Type -Transport Order
Entry Type -Integral Plus

Expected Outcome:-
Receiving Node- DC Node
Ship Node -DC Node
DepartmentCode- Inventory Materials
DocumentType-0006

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");


			System.out.println("******************* Order Creation Test Case 7 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder8(){

		try {

			/*	Scenario:-			
				InterfaceNo=INT_ODR_4
				OrderType=PURCHASE_ORDER
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id present
				SellerOrganizationCode=Vendor
			Outcome:-			
				DocumentType=0005
				DepartmentCode=Inventory Materials
				Receiving Node=DC
				ShipNode=Vendors Node
			 */

			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sNodeType = "DC";
			String sChangeStatusOrderLine1 = "B";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", sChangeStatusOrderLine1);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeType);

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);




			Assert.assertEquals("Document Type Check = ",eleOutputOrder.getAttribute("DocumentType"),"0005");
			Assert.assertEquals("Department Code Check = ",eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"Vendor1_N1");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			//	Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Backordered");

			System.out.println("******************* Order Creation Test Case 8 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder9(){

		try {
			/*
			Scenario:-			
                Scenario:-
Int -INT_ODR_1
Order Type- Material Reservation
Entry Type -Integral Plus

Expected Outcome:-
Receiving Node- null
Ship Node- DC Node
DepartmentCode- PMC
DocumentType-0001
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "3664050";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";
			String sShipNode="TestOrg10";
			//String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			//createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			//setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"PMC");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			//Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"");


			System.out.println("******************* Order Creation Test Case 9 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder10(){

		try {

			/*	Scenario:-			
				InterfaceNo=INT_ODR_3
				OrderType=PURCHASE_ORDER
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id present
				SellerOrganizationCode=Vendor
			Outcome:-			
				DocumentType=0005
				DepartmentCode=Inventory Materials
				Receiving Node=""
				ShipNode=Vendors Node
			 */

			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";
			String sNodeType = "";
			String sChangeStatusOrderLine1 = "B";
			String sDepartmentCode="INVENTORY_MATERIAL";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", sChangeStatusOrderLine1);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			//setNodeType(sExternalCustomerID,sNodeType);

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);




			Assert.assertEquals("Document Type Check = ",eleOutputOrder.getAttribute("DocumentType"),"0005");
			Assert.assertEquals("Department Code Check = ",eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"Vendor1_N1");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),null);
			//	Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Backordered");

			System.out.println("******************* Order Creation Test Case 10 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder11(){

		try {

			/*	Scenario:-			
			InterfaceNo=INT_ODR_4
			OrderType=PURCHASE_ORDER
			EntryType=INTEGRAL_PLUS
			BillToID=not present
			SellerOrganizationCode=Vendor
		Outcome:-			
			DocumentType=0005
			DepartmentCode=Inventory Materials
			Receiving Node=""
			ShipNode=Vendors Node
			 */

			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";
			String sNodeType = "";
			String sChangeStatusOrderLine1 = "B";
			String sDepartmentCode="INVENTORY_MATERIAL";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", sChangeStatusOrderLine1);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			//setNodeType(sExternalCustomerID,sNodeType);

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);




			Assert.assertEquals("Document Type Check = ",eleOutputOrder.getAttribute("DocumentType"),"0005");
			Assert.assertEquals("Department Code Check = ",eleOutputOrder.getAttribute("DepartmentCode"),"INVENTORY_MATERIAL");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"Vendor1_N1");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),null);
			//	Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Backordered");

			System.out.println("******************* Order Creation Test Case 11 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}


	@Test
	public void testCreateOrder12(){

		try {
			/*
			Scenario:-			
                Scenario:-
Scenario:-
Int -INT_ODR_1
Order Type- Material Reservation
Entry Type -Integral Plus
DepartmentCode - ISGM


Expected Outcome:-
Receiving Node- CT Node
Ship Node- DC Node
DepartmentCode- ISGM
DocumentType-0006

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "CT";
			String sNodeTypeShip = "DC";
			String sDepartmentCode= "ISGM";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode",sDepartmentCode );
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"ISGM");


			System.out.println("******************* Order Creation Test Case 12 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder13(){

		try {
			/*
			Scenario:-			
                Scenario:-
Scenario:-
Int -INT_ODR_2
Order Type- Material Reservation
Entry Type -Integral Plus
DepartmentCode - ISGM


Expected Outcome:-
Receiving Node- CT Node
Ship Node- DC Node
DepartmentCode- ISGM
DocumentType-0006

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "CT";
			String sNodeTypeShip = "DC";
			String sDepartmentCode= "ISGM";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode",sDepartmentCode );
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);


			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"ISGM");


			System.out.println("******************* Order Creation Test Case 13 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder14(){

		try {
			/*
			Scenario:-			
				InterfaceNo=INT_ODR_1
				OrderType=Transport Order 
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id is present
				NodeType=WBVS

			Outcome:-
			Receiving Node- CT Node
			Ship Node- DC Node
				DocumentType=0006
				DepartmentCode=WIDEBAND
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_1";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "WBVS";
			String sNodeTypeShip = "DC";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"WIDEBAND");

			System.out.println("******************* Order Creation Test Case 14 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder15(){

		try {
			/*
			Scenario:-			
				InterfaceNo=INT_ODR_2
				OrderType=Transport Order 
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id is present
				NodeType=WBVS

			Outcome:-
			Receiving Node- CT Node
			Ship Node- DC Node
				DocumentType=0006
				DepartmentCode=WIDEBAND
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "WBVS";
			String sNodeTypeShip = "DC";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0006");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"WIDEBAND");

			System.out.println("******************* Order Creation Test Case 15 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder16(){

		try {
			/*
			Scenario:-			
				InterfaceNo=INT_ODR_4
				OrderType=Purchase Order 
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id is present
				NodeType=WBVS
							DepartmentCode=WIDEBAND

			Outcome:-
			Receiving Node- CT Node
			Ship Node- Vendors Node
				DocumentType=0005
				DepartmentCode=WIDEBAND
			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			//		String sShipNode="TestOrg10";
			String sNodeTypeRecv = "WLVS";
			//		String sNodeTypeShip = "DC";
			String sDepartmentCode="WIRELESS";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			//	input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			//setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);

			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"Vendor1_N1");
			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"WIRELESS");

			System.out.println("******************* Order Creation Test Case 16 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder17(){

		try {
			/*
			Scenario:-
Int -INT_ODR_3
Order Type- Purchase Order
Entry Type -Integral Plus

Expected Outcome:-
Receiving Node-DC Node
Ship Node- NBN Node
DepartmentCode- NBN
DocumentType-0005

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";
			String sDepartmentCode="NBN";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"NBN");

			System.out.println("******************* Order Creation Test Case 17 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testCreateOrder18(){

		try {
			/*
			Scenario:-
Invalid Item

Expected Outcome:-
Order created
Receiving Node-DC Node
Ship Node- NBN Node
DepartmentCode- NBN
DocumentType-0005

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLineInvalidItem();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "VAARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";
			String sDepartmentCode="NBN";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			//createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","Y");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());


			YFCDocument getOrgListInput = YFCDocument.getDocumentFor("<Organization OrganizationCode='"+sBillToID+"'/>");
			YFCDocument getOrgListtempInput = YFCDocument.getDocumentFor("<OrganizationList><Organization OrganizationCode='' /></OrganizationList>");
			YFCDocument outputOrgXml = invokeYantraApi("getOrganizationList", getOrgListInput,getOrgListtempInput);
			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ShipNode"),"TestOrg10");
			Assert.assertEquals(eleOutputOrderLine.getAttribute("ReceivingNode"),"TestOrg5");
			Assert.assertEquals(eleOutputOrder.getAttribute("DepartmentCode"),"NBN");
			Assert.assertEquals("Creater DAC Check = ",outputOrgXml.getDocumentElement().getElementsByTagName("Organization").item(0).getAttribute("OrganizationCode"),sBillToID);

			System.out.println("******************* Order Creation Test Case 18 *******************");

		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}
	/*	
	public void testCreateOrder4(){

		try {

			Scenario:-			
				InterfaceNo=INT_ODR_4
				OrderType=PURCHASE_ORDER
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id present
				SellerOrganizationCode=Vendor
			    orderline change status=B				
			Outcome:-			
				DocumentType=0001
				DepartmentCode=CISCO_INT
				Line backordered

			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";
			String sNodeType = "DC";
			String sChangeStatusOrderLine1 = "B";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", sChangeStatusOrderLine1);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			//setNodeType(sExternalCustomerID,sNodeType);

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Document Type Check = ",eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Department Code Check = ",eleOutputOrder.getAttribute("DepartmentCode"),"CISCO_INT");
			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Backordered");

			System.out.println("******************* Order Creation Test Case 4 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}*/

	/*	
	public void testCreateOrder5(){

		try {

			Scenario:-			
				InterfaceNo=INT_ODR_4
				OrderType=PURCHASE_ORDER
				EntryType=INTEGRAL_PLUS
				BillToID=ext-id present
				SellerOrganizationCode=Vendor
			    orderline change status=B				
			Outcome:-			
				DocumentType=0001
				DepartmentCode=CISCO_INT
				Line backordered

			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";
			String sNodeType = "DC";
			//String sChangeStatusOrderLine1 = "B";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			//input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", sChangeStatusOrderLine1);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			//setNodeType(sExternalCustomerID,sNodeType);

			//setting RequiresChainedOrder=Y for Seller
			setRequiresChainedOrderForSeller(sSellerOrganizationCode);

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//call schedule order api to move order to Awaiting Chained Order Creation
			invokeYantraApi("scheduleOrder", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>"));


			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Document Type Check = ",eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Seller Org Check = ",eleOutputOrder.getAttribute("SellerOrganizationCode"),"TELSTRA");
			Assert.assertEquals("Department Code Check = ",eleOutputOrder.getAttribute("DepartmentCode"),"CISCO_INT");
			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Awaiting Chained Order Creation");
			Assert.assertEquals("Order Line ShipNode Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("ShipNode"),sSellerOrganizationCode+"_N1");

			System.out.println("******************* Order Creation Test Case 5 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}*/

	@Test
	public void testChangeOrder1(){

		try {
			/*
			Scenario:-	
				Sales Order already exist with 2 lines
				Order update messaged received
					AddLine new line
					Cancel Existing OrderLine1 by passing ChangeStatus='D'
					Change quantity to Existing OrderLine2 by passing Quantity=<NEW QUANTITY>
			Outcome:-			
				A line must be added and status is Backorder
				Existing OrderLine1 should get cancelled
				Existing OrderLine2 should have new quantity
			 */


			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "Vendor1";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";

			//get Default Order XML with 2 lines
			YFCDocument input = getMultiLineOrder(2, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			//input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", sChangeStatusOrderLine1);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);


			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//Order is created now again call GpsManageOrder for Order Modification testing
			YFCDocument input1 = getMultiLineOrder(3, sEnterpriseCode);
			input1.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input1.getDocumentElement().setAttribute("OrderName", sOrderName);
			input1.getDocumentElement().setAttribute("EntryType", sEntryType);
			input1.getDocumentElement().setAttribute("OrderType", sOrderType);
			input1.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input1.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input1.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input1.getDocumentElement().setAttribute("BillToID", sBillToID);
			input1.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input1.getElementsByTagName("OrderLine").item(0).setAttribute("OrderedQty", "20");
			input1.getElementsByTagName("OrderLine").item(1).setAttribute("ChangeStatus", "D");
			//input1.getElementsByTagName("OrderLine").item(2).setAttribute("ChangeStatus", "B");

			//call ManageOrder Service to change order
			invokeYantraService("GpsManageOrder", input1);
			System.out.println("<--------- Order Modified = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());


			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Order Line Quantity Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("OrderedQty"),"20.00");
			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(1).getAttribute("Status"),"Cancelled");
			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(2).getAttribute("Status"),"Created");

			System.out.println("******************* Order Change Test Case 1 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testChangeOrder2(){

		try {

			/*Scenario:-	
				Sales Order already exist with 2 lines, chained Order is created and shipped
				Order update messaged received for status as Delivered
			Outcome:-			
				Both Lines should move Delivered status
*/

			//setting Test Case Related Data
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "MATERIAL_RESERVATION";
			String sInterfaceNo = "INT_ODR_2";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "3664050";
			String sShipToID = "000021844";
			String sExternalCustomerID = "";
			String sShipNode="TestOrg10";
			//String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";




			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			//createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			//setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);
			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create1 = >" + input.toString());

	

			//call Schedule Order api to move order to Awaiting Chained Order Creation
			//invokeYantraApi("scheduleOrder", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>"));

			//call createChainedOrder api to create Drop Ship PO.
			//YFCDocument docChainedOrder = invokeYantraApi("createChainedOrder", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>"), chainedOrderListTemplate());

			//System.out.println("Chained Order Output = "+docChainedOrder.toString());
			//create and confirm Shipment for Drop Ship PO
			//YFCDocument docChainedShipment = createAndConfirmChainedOrderShipment(docChainedOrder);
			//System.out.println("Chained Shipment Output = "+docChainedShipment.toString());
			//change Drop Ship to InTransit status
			//invokeYantraApi("changeShipmentStatus", YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.10000' ShipmentKey='"+docChainedShipment.getDocumentElement().getAttribute("ShipmentKey")+"' TransactionId='GPS_IN_TRANSIT.0005.ex'/>"));
//			YFCDocument docPickxml=invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='"+sOrderName+"' ShipNode='"+sShipNode+"' ><OrderLines><OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' StatusQuantity='10' ItemID='TestItem1' UnitOfMeasure='EA' ></OrderLine></OrderLines></OrderRelease>"));
			//System.out.println("<--------- Order Create2 = >" + docPickxml.toString());
			
			

			//Order is created now again call GpsManageOrder for Order Modification testing


//			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
//			input.getDocumentElement().setAttribute("OrderName", sOrderName);
//			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
//			input.getDocumentElement().setAttribute("EntryType", sEntryType);
//			input.getDocumentElement().setAttribute("OrderType", sOrderType);
//			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
//			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
//			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
//			input.getDocumentElement().setAttribute("BillToID", sBillToID);
//			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
//			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", "D");
			//input1.getElementsByTagName("OrderLine").item(1).setAttribute("ChangeStatus", "C");
			//input1.getElementsByTagName("OrderLine").item(2).setAttribute("ChangeStatus", "B");

			//call ManageOrder Service to change order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Modified = "+sOrderName+" --------->");


			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 
			YFCElement eleOutputOrderLine=eleOutputOrder.getElementsByTagName("OrderLine").item(0);
          
			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			String sDocumentType=(eleOutputOrder.getAttribute("DocumentType"));
			System.out.println("<--------- Order sDocumentType = >" + sDocumentType);
			
			//get details of newly created Order
//			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
	//		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

		//	YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			//Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Order Delivered");
			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Cancelled");

			System.out.println("******************* Order Change Test Case 2 *******************");


		 }
		
	catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

/*	@Test
	public void testChangeOrder2(){

		try {
			
			Scenario:-	
				Sales Order already exist with 2 lines, Order / Shipment is in Intransit status
				Order update messaged received for status as Delivered
			Outcome:-			
				Both Lines should move Delivered status
			 			

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sShipNode = "TestOrg5";
			String sExternalCustomerID = "";

			//get Default Order XML with 2 lines
			YFCDocument input = getMultiLineOrder(2, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);
			input.getElementsByTagName("OrderLine").item(1).setAttribute("ShipNode", sShipNode);

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);	

			//setInventoryTrackNoForShipNode(sShipNode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//call Schedule Order api to move order to Awaiting Chained Order Creation
			YFCDocument test=null;
			test =invokeYantraApi("scheduleOrder", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"' ScheduleAndRelease='Y'/>"));

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
			System.out.println("<--------- Order List  = "+output.toString());


			//create and confirm Shipment for Drop Ship PO
			YFCDocument docChainedShipment = createAndConfirmSalesOrderShipment(output);
			
			

			//change Drop Ship to InTransit status
			invokeYantraApi("changeShipmentStatus", YFCDocument.getDocumentFor("<Shipment BaseDropStatus='1400.10000' ShipmentKey='"
					+docChainedShipment.getDocumentElement().getAttribute("ShipmentKey")+"' TransactionId='GPS_IN_TRANSIT.0001.ex'/>"));

			//Order is created now again call GpsManageOrder for Order Modification testing
			YFCDocument input1 = getMultiLineOrder(3, sEnterpriseCode);
			input1.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input1.getDocumentElement().setAttribute("OrderName", sOrderName);
			input1.getDocumentElement().setAttribute("EntryType", sEntryType);
			input1.getDocumentElement().setAttribute("OrderType", sOrderType);
			input1.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input1.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input1.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input1.getDocumentElement().setAttribute("BillToID", sBillToID);
			input1.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input1.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", "C");
			input1.getElementsByTagName("OrderLine").item(1).setAttribute("ChangeStatus", "C");
			//input1.getElementsByTagName("OrderLine").item(2).setAttribute("ChangeStatus", "B");

			//call ManageOrder Service to change order
			invokeYantraService("GpsManageOrder", input1);
			System.out.println("<--------- Order Modified = "+sOrderName+" --------->");			

			//get details of newly created Order
			getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("Status"),"Order Delivered");
			Assert.assertEquals("Order Line Status Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(1).getAttribute("Status"),"Order Delivered");
			Assert.assertEquals("Order Line ShipNode Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(2).getAttribute("ShipNode"),sShipNode);

			System.out.println("******************* Order Change Test Case 3 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}
*/
	
	@Test
	public void testOrderValidationZipCode1(){

		try {
			/*
			Scenario:-	
				while creating order ZipCode is Incorrect
			Outcome:-			
				ErrorCode is validated
			 */			

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sShipNode = "TestOrg5";
			String sExternalCustomerID = "";

			//YFCDocument docGetZipCodeLocationList = YFCDocument.getDocumentFor("<ZipCodeLocationList><ZipCodeLocation ZipCode='1234' Country='AU' City='Melbourne' State='VIC'/></ZipCodeLocationList>");
			//invokeEntityApi("createZipCodeLocation", YFCDocument.getDocumentFor("<ZipCodeLocation ZipCode='1234' Country='AU' City='Melbourne' State='VIC'/>"), null);
			//invokeEntityApi("createZipCodeLocation", YFCDocument.getDocumentFor("<ZipCodeLocation ZipCode='1234' Country='AU' City='Melbourne' State='NSW'/>"), null);

			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(1, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", "");
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("City", "");
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("Country", "");
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("State", "VIC");
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("ZipCode", "1234");


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);	

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","Y");

			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");


			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Bill To City Check = ",eleOutputOrder.getElementsByTagName("PersonInfoBillTo").item(0).getAttribute("City"),"Melbourne");
			Assert.assertEquals("Bill To Country Check = ",eleOutputOrder.getElementsByTagName("PersonInfoBillTo").item(0).getAttribute("Country"),"AU");
			Assert.assertEquals("Bill To State Check = ",eleOutputOrder.getElementsByTagName("PersonInfoBillTo").item(0).getAttribute("State"),"VIC");

			System.out.println("******************* ZipCode Validation Test Case 1 *******************");


		} catch (Exception e) {
			if(e.toString().contains("TEL_ERR_1001_004")){
				System.out.println("inside error");
				Assert.assertEquals("Pass", "Pass");
			}else{
				Assert.assertEquals("Pass", "Fail");
			}
		}

	}

	@Test
	public void testOrderValidationZipCode2(){

		try {
			/*
			Scenario:-	
				while creating order ZipCode is wrong but City And Status is correct
			Outcome:-			
				ZipCode should be updated
			 */			

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sShipNode = "TestOrg5";
			String sExternalCustomerID = "";

			//YFCDocument docGetZipCodeLocationList = YFCDocument.getDocumentFor("<ZipCodeLocationList><ZipCodeLocation ZipCode='1234' Country='AU' City='Melbourne' State='VIC'/></ZipCodeLocationList>");
			invokeEntityApi("createZipCodeLocation", YFCDocument.getDocumentFor("<ZipCodeLocation ZipCode='1234' Country='AU' City='Melbourne' State='VIC'/>"), null);
			invokeEntityApi("createZipCodeLocation", YFCDocument.getDocumentFor("<ZipCodeLocation ZipCode='1234' Country='AU' City='Melbourne' State='NSW'/>"), null);

			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(1, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", "");
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("City", "");
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("Country", "");
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("State", "VIC");
			input.getElementsByTagName("PersonInfoBillTo").item(0).setAttribute("ZipCode", "1234");


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);	

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","Y");

			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");


			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("Bill To City Check = ",eleOutputOrder.getElementsByTagName("PersonInfoBillTo").item(0).getAttribute("City"),"Melbourne");
			Assert.assertEquals("Bill To Country Check = ",eleOutputOrder.getElementsByTagName("PersonInfoBillTo").item(0).getAttribute("Country"),"AU");
			Assert.assertEquals("Bill To State Check = ",eleOutputOrder.getElementsByTagName("PersonInfoBillTo").item(0).getAttribute("State"),"VIC");

			System.out.println("******************* ZipCode Validation Test Case 2 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}



	@Test
	public void testOrderValidationItemRound1(){

		try {
			/*
			Scenario:-	
				Run quantity exist 
			Outcome:-			
				Quantity should be updated accordingly
			 */			

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sShipNode = "TestOrg5";
			String sExternalCustomerID = "";

			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(2, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", "");
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);
			input.getElementsByTagName("OrderLine").item(1).setAttribute("ShipNode", sShipNode);
			input.getElementsByTagName("OrderLine").item(0).setAttribute("OrderedQty", "80");
			input.getElementsByTagName("OrderLine").item(1).setAttribute("OrderedQty", "110");

			// update Node Item Defination
			updateNodeItemDefn("TestItem1","EA", sShipNode, sEnterpriseCode, "100");
			updateNodeItemDefn("TestItem2","EA", sShipNode, sEnterpriseCode, "100");

			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);	

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","Y");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");


			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());

			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 

			Assert.assertEquals("OL#1 Ordered Qty Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(0).getAttribute("OrderedQty"),"100.00");
			Assert.assertEquals("OL#2 Ordered Qty Check = ",eleOutputOrder.getElementsByTagName("OrderLine").item(1).getAttribute("OrderedQty"),"200.00");

			System.out.println("******************* Order Validation Item Round Test Case 1 *******************");


		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}

	@Test
	public void testOrderValidationCostCenter1(){

		try {
			/*
			Scenario:-	
				Division exist in Order but not in cost center 
			Outcome:-			
				Error with error code = TEL_ERR_1001_002 should be thrown
			 */			

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "PURCHASE_ORDER";
			String sInterfaceNo = "INT_ODR_4";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "AARON";
			String sShipToID = "000021844";
			String sShipNode = "TestOrg5";
			String sExternalCustomerID = "";

			//get Default Order XML with 1 lines
			YFCDocument input = getMultiLineOrder(2, sEnterpriseCode);

			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", "");
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("Division", "ABCD");


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);	

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","Y");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","N");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//call ManageOrder Service to create order
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");


			System.out.println("******************* Order Validation Cost Center Test Case 1 *******************");


		}catch(Exception e){
			if(e.toString().contains("TEL_ERR_1001_002")){
				System.out.println("inside error");
				Assert.assertEquals("Pass", "Pass");
			}else{
				Assert.assertEquals("Pass", "Fail");
			}
		} 
	}

	protected void createItem(String sItemId, String sUOM, String sOrganizationCode) {

		String ItemXml = "<ItemList><Item CanUseAsServiceTool='N' GlobalItemID='' ItemGroupCode='PROD' ItemID='' OrganizationCode='' "
				+ " UnitOfMeasure=''><PrimaryInformation AllowGiftWrap='N' AssumeInfiniteInventory='N' BundleFulfillmentMode='01' "
				+ "BundlePricingStrategy='PARENT' CapacityQuantityStrategy='' CostCurrency='AUD' CountryOfOrigin='' CreditWOReceipt='N'"
				+ " DefaultProductClass='' Description='' ExtendedDescription='' ExtendedDisplayDescription='Test Item' "
				+ "FixedCapacityQtyPerLine='0.00' FixedPricingQtyPerLine='0.00' InvoiceBasedOnActuals='N' IsAirShippingAllowed='Y' "
				+ "IsDeliveryAllowed='N' IsEligibleForShippingDiscount='Y' IsFreezerRequired='N' IsHazmat='N' IsParcelShippingAllowed='Y'"
				+ " IsPickupAllowed='N' IsProcurementAllowed='Y' IsReturnService='N' IsReturnable='Y' IsShippingAllowed='Y' "
				+ "IsStandaloneService='' IsSubOnOrderAllowed='' ItemType='' KitCode='' ManufacturerItem='' ManufacturerItemDesc='' "
				+ "ManufacturerName='' MasterCatalogID='' MaxOrderQuantity='0.00' MinOrderQuantity='0.00' MinimumCapacityQuantity='0.00' "
				+ "NumSecondarySerials='0' OrderingQuantityStrategy='ENT' PricingQuantityConvFactor='0.00' PricingQuantityStrategy='IQTY'"
				+ " PricingUOM='EA' PricingUOMStrategy='INV' PrimaryEnterpriseCode='' PrimarySupplier='' ProductLine='' "
				+ "RequiresProdAssociation='N' ReturnWindow='0' RunQuantity='0.00' SerializedFlag='N' ServiceTypeID='' "
				+ "ShortDescription='testitem1' Status='3000' TaxableFlag='N' UnitCost='0.00' UnitHeight='0.00' UnitHeightUOM='' "
				+ "UnitLength='0.00' UnitLengthUOM='' UnitWeight='0.00' UnitWeightUOM='' UnitWidth='0.00' UnitWidthUOM=''/>"
				+ "		<InventoryParameters ATPRule='' AdvanceNotificationTime='0' DefaultExpirationDays='0' InventoryMonitorRule='' "
				+ "IsFifoTracked='N' IsItemBasedAllocationAllowed='Y' IsSerialTracked='N' LeadTime='0' MaximumNotificationTime='0.00'"
				+ " MinNotificationTime='0' NodeLevelInventoryMonitorRule='' ProcessingTime='0' TagControlFlag='N' TimeSensitive='N' "
				+ "UseUnplannedInventory='N'/><ClassificationCodes CommodityCode='' ECCNNo='' HarmonizedCode='' HazmatClass='' "
				+ "NAICSCode='' NMFCClass='' NMFCCode='' OperationalConfigurationComplete='N' PickingType='' PostingClassification='' "
				+ "Schedule_B_Code='' StorageType='' TaxProductCode='' UNSPSC='' VelocityCode=''/></Item></ItemList>";

		YFCDocument docManageItemInput = YFCDocument.getDocumentFor(ItemXml);
		YFCElement eleItem = docManageItemInput.getDocumentElement().getElementsByTagName("Item").item(0);

		eleItem.setAttribute("ItemID", sItemId);
		eleItem.setAttribute("UnitOfMeasure", sUOM);
		eleItem.setAttribute("OrganizationCode", sOrganizationCode);

		invokeYantraApi("manageItem", docManageItemInput);

		System.out.println("<--------- Item Created = "+sItemId+" --------->");

	}

	protected YFCDocument getOrderXmlWIthOneLine() throws Exception {
		String orderXmlString = 
				"<Order PriorityCode='N' SearchCriteria2='' SearchCriteria1='' Division=''>"
						+ "  <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777'"
						+ "  AddressLine4='' City='Wien' ZipCode='1130  W AT' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854'/>"
						+ " <OrderLines>"
						+ " <OrderLine OrderedQty='10' PrimeLineNo='1' ChangeStatus=''  ReceivingNode=''>"
						+ " <Item ItemID='TestItem1' UnitOfMeasure='EA' />"
						+ " </OrderLine>"
						+ " </OrderLines>"
						+ " </Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}

	protected YFCDocument getOrderXmlWIthOneLineInvalidItem() throws Exception {
		String orderXmlString = 
				"<Order PriorityCode='N' SearchCriteria2='' SearchCriteria1='' Division=''>"
						+ "  <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777'"
						+ "  AddressLine4='' City='Wien' Country='NT' ZipCode='1130  W AT' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854'/>"
						+ " <OrderLines>"
						+ " <OrderLine OrderedQty='10' PrimeLineNo='1' ChangeStatus=''  ReceivingNode=''>"
						+ " <Item ItemID='TestItemx' UnitOfMeasure='EA' />"
						+ " </OrderLine>"
						+ " </OrderLines>"
						+ " </Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}

	protected YFCDocument orderListTemplate() throws Exception {
		String orderXmlString = "<OrderList><Order OrderNo='' EnterpriseCode='' BuyerOrganizationCode='' SellerOrganizationCode='' EntryType=''   "
				+ "DocumentType='' OrderName='' BillToID='' ShipToID=''    OrderType='' Division='' ShipNode='' Status='' DepartmentCode='' OrderHeaderKey=''>"
				+ "<PersonInfoBillTo FirstName='' AddressLine2='' AddressLine3='' AddressLine4='' City='' ZipCode='' State='' DayPhone='' DayFaxNo='' Country=''/>"
				+ "<OrderLines><OrderLine Status='' OrderedQty='' PrimeLineNo='' ReceivingNode='' ShipNode='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''>"
				+ "<Item ItemID='' UnitOfMeasure='' /><Schedules><Schedule/></Schedules></OrderLine></OrderLines><OrderStatuses><OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>"
				+ "</OrderStatuses></Order></OrderList>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}


	protected YFCDocument chainedOrderListTemplate() throws Exception {
		String orderXmlString = "<Order DocumentType='' EnterpriseCode='' OrderHeaderKey='' OrderName=''><ChainedOrderList>"
				+ "<Order DocumentType='' EnterpriseCode='' OrderHeaderKey='' OrderNo=''><OrderLines><OrderLine OrderedQty='' PrimeLineNo='' OrderLineKey='' "
				+ "OrderHeaderKey=''><Item ItemID='' UnitOfMeasure='' /></OrderLine></OrderLines></Order></ChainedOrderList></Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}



	protected YFCDocument getMultiLineOrder(int iOrderLineCount, String sEnterpriseCode) throws Exception {
		String orderXmlString = 		
				"<Order PriorityCode='N' SearchCriteria2='' SearchCriteria1='' Division=''>"
						+ "  <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777'"
						+ "  AddressLine4='' City='Wien' ZipCode='1130  W AT' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854'/>"
						+ " <OrderLines>"
						+ " </OrderLines>"
						+ " </Order>";
		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);

		for(int i=1;i<=iOrderLineCount; i++) {
			YFCDocument docOrderLine = YFCDocument.getDocumentFor("<OrderLine OrderedQty='10' PrimeLineNo='"+i+"' ChangeStatus=''  ReceivingNode=''>"
					+ " <Item ItemID='TestItem"+i+"' UnitOfMeasure='EA' />"
					+ " </OrderLine>");
			orderDoc.getElementsByTagName("OrderLines").item(0).importNode(docOrderLine.getDocumentElement());

			//Create Item which is used in Input XML
			createItem("TestItem"+i,"EA", sEnterpriseCode);

		}

		return orderDoc;
	}

	protected void manageCommonCodeForOrderValidation(String sCodeValue, String sValue) {
		YFCDocument manageCommonCodeInput = YFCDocument
				.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\"" + sValue + "\" CodeValue=\""
						+ sCodeValue + "\" CodeType='ORDER_VALIDATION' OrganizationCode='DEFAULT'/>");
		invokeYantraApi("manageCommonCode", manageCommonCodeInput);

		System.out.println("<--------- manageCommonCode called for = "+sCodeValue+" --------->");
	}

	protected void createCustomer(String sCustomerID, String sCustomerKey, String sExternalCustomerID) {

		String CustomerXml = "<Customer CustomerID='' CustomerKey='' CustomerType='02' ExternalCustomerID='' "
				+ "OrganizationCode='TELSTRA_SCLSA' Status='10'><Consumer><BillingPersonInfo PersonID='' AddressLine1='3ferntree Road' "
				+ "AddressLine2=' 20  '	AddressLine3='HUME ST 321777      ' AddressLine4='' AddressLine5=''	AddressLine6='' AlternateEmailID='' "
				+ "City='Wien'	Company='' Country='AU' DayFaxNo='     031 23 2854   ' DayPhone='     5461801' Department=''	EMailID='' "
				+ "FirstName='Divisi  n de Personal Model-New' 	LastName='' MiddleName=''	MobilePhone='' State='VI' Suffix='' Title='' ZipCode='     1130  W AT'/>"
				+ "</Consumer></Customer>";

		YFCDocument docCustomerInput = YFCDocument.getDocumentFor(CustomerXml);
		YFCElement eleCustomer = docCustomerInput.getDocumentElement();

		eleCustomer.setAttribute("CustomerID", sCustomerID);
		eleCustomer.setAttribute("CustomerKey", sCustomerKey);
		eleCustomer.setAttribute("ExternalCustomerID", sExternalCustomerID);

		docCustomerInput.getElementsByTagName("BillingPersonInfo").item(0).setAttribute("PersonID", sCustomerKey);

		invokeYantraApi("createCustomer", docCustomerInput);

		System.out.println("<--------- Customer Create = "+sCustomerID+" --------->");

	}

	protected YFCDocument createAndConfirmChainedOrderShipment(YFCDocument docChainedOrder) {

		String shipmentInput = "<Shipment DocumentType='' ShipmentKey='' EnterpriseCode=''><ShipmentLines></ShipmentLines></Shipment>";

		YFCDocument docShipmentXML = YFCDocument.getDocumentFor(shipmentInput);
		YFCElement eleShipment = docShipmentXML.getDocumentElement();

		YFCElement eleChainedOrderList = docChainedOrder.getElementsByTagName("ChainedOrderList").item(0);
		eleShipment.setAttribute("DocumentType", eleChainedOrderList.getChildElement("Order").getAttribute("DocumentType"));
		eleShipment.setAttribute("EnterpriseCode", eleChainedOrderList.getChildElement("Order").getAttribute("EnterpriseCode"));

		YFCElement eleShipmentLines = eleShipment.getChildElement("ShipmentLines");
		for(YFCElement eleOrderLine : docChainedOrder.getElementsByTagName("OrderLine")) {
			YFCElement eleShipmentLine = docShipmentXML.createElement("ShipmentLine");
			eleShipmentLine.setAttribute("OrderLineKey", eleOrderLine.getAttribute("OrderLineKey"));
			eleShipmentLine.setAttribute("OrderHeaderKey", eleOrderLine.getAttribute("OrderHeaderKey"));
			eleShipmentLine.setAttribute("ItemID", eleOrderLine.getChildElement("Item").getAttribute("ItemID"));
			eleShipmentLine.setAttribute("Quantity", eleOrderLine.getAttribute("OrderedQty"));
			eleShipmentLine.setAttribute("UnitOfMeasure", eleOrderLine.getChildElement("Item").getAttribute("UnitOfMeasure"));
			eleShipmentLines.appendChild(eleShipmentLine);
		}

		YFCDocument output = invokeYantraApi("confirmShipment", docShipmentXML, YFCDocument.getDocumentFor("<Shipment ShipmentKey='' Status='' />"));

		System.out.println("<--------- Shipment Create And Confirmed for Chained Order --------->");

		return output;
	}

	protected YFCDocument createAndConfirmSalesOrderShipment(YFCDocument docSalesOrder) {
		YFCElement eleOrder = docSalesOrder.getElementsByTagName("Order").item(0);
		String sOrderReleaseKey ="";
		for(YFCElement eleOrderStatus : docSalesOrder.getElementsByTagName("OrderStatus")) {
			if(eleOrderStatus.getAttribute("Status").equals("3200")) {
				sOrderReleaseKey = eleOrderStatus.getAttribute("OrderReleaseKey");
			}
		}
		YFCDocument inDoc = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+eleOrder.getAttribute("OrderHeaderKey")+"' "
				+ "OrderHeaderKey='"+eleOrder.getAttribute("OrderHeaderKey")+"'><OrderReleases><OrderRelease OrderReleaseKey="
				+ "'"+sOrderReleaseKey+"'><Order DocumentType='"+eleOrder.getAttribute("DocumentType")+"'"
				+ " EnterpriseCode='"+eleOrder.getAttribute("EnterpriseCode")+"' OrderNo='"+eleOrder.getAttribute("OrderNo")+"'/>"
				+ "</OrderRelease></OrderReleases></Shipment>"); 

		YFCDocument docOut = invokeYantraApi("createShipment",inDoc); 

		invokeYantraApi("confirmShipment",docOut); 


		System.out.println("<--------- Shipment Create And Confirmed for Sales Order --------->");

		return docOut;
	}

	protected void setNodeType(String sShipNode, String sNodeType) {

		String ManageOrgXml = "<Organization OrganizationCode='"+sShipNode+"'><Node ShipNode='"+sShipNode+"' NodeType='"+sNodeType+"'/></Organization>"; 

		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));

		System.out.println("<--------- NodeType set for "+sShipNode+" to "+sNodeType+" --------->");

	}
	protected void setInventoryTrackNoForShipNode(String sShipNode) {

		String ManageOrgXml = "<Organization OrganizationCode='"+sShipNode+"'><Node ShipNode='"+sShipNode+"' InventoryTracked='N'/></Organization>"; 

		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));

		System.out.println("<--------- InventoryTracked set to N for "+sShipNode+" --------->");

	}



	protected void setRequiresChainedOrderForSeller(String sSellerOrgCode) {

		String ManageOrgXml = "<Organization OrganizationCode='"+sSellerOrgCode+"' RequiresChainedOrder='Y'/>"; 

		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));

		System.out.println("<--------- RequiresChainedOrder set to Y for "+sSellerOrgCode+" --------->");

	}

	protected void updateNodeItemDefn(String sItemID, String sUOM, String sShipNode, String sEnterpriseCode, String sRunQuantity) {

		String ItemNodeDefnXml = "<ItemNodeDefn ItemID='"+sItemID+"' Node='"+sShipNode+"' OrganizationCode='"+sEnterpriseCode+"' UnitOfMeasure='"+sUOM+"'><Extn RunQuantity='"+sRunQuantity+"'/></ItemNodeDefn>"; 

		invokeYantraApi("createItemNodeDefn", YFCDocument.getDocumentFor(ItemNodeDefnXml));

		System.out.println("<--------- RunQuantity = "+sRunQuantity+" set for an Item = "+sItemID+" --------->");

	}

}
