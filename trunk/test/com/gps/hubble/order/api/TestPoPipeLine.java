package com.gps.hubble.order.api;
import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.TestBase;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;


public class TestPoPipeLine extends TestBase {

	static int i=1;

	@Test
	public void testPoPipeline1(){
		try {
			/*
			Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed--> Partially Shipped -->Partially Intransit -->Partially Delivered 

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "VAARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";
			String sDepartmentCode="NBN";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			//createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","Y");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			System.out.println("******************* Order Creation Test Case 1 *******************");
			
			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			String sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
			System.out.println("*******************  PO Not Sent To Vendor *******************");

			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
			System.out.println("*******************  PO Sent To Vendor *******************");

			
			invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
			System.out.println("******************* PO Committed *******************");

			
			invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='5' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
			System.out.println("******************* PO Shipped *******************");
           
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Shipped");

			String sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");
					
			invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Intransit' TransportStatusTxt='Intransit' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
			System.out.println("<******************* Order Intransit *******************>");


			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Intransit");

			sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400.10000");


			invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Delivered' TransportStatusTxt='Delivered' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
			System.out.println("<******************* Order Delivered *******************>");

			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Order "
					+ "Delivered");

			sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1500");
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

		
	}

	@Test
	public void testPoPipeline2(){
		try {
			/*
			Scenario:-

Expected Outcome:-
Created-->PO  Sent To Vendor --> Committed--> Partially Shipped -->Partially Intransit -->Partially Delivered 

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "VAARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";
			String sDepartmentCode="NBN";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			//createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","Y");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			System.out.println("******************* Order Creation Test Case 2 *******************");
			
		
			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			String sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
			System.out.println("*******************  PO Sent To Vendor *******************");

			
			invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
			System.out.println("*******************  PO Committed *******************");

			
			invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='5' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
			System.out.println("******************* PO Shipped *******************");
           
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Shipped");

			String sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");
					
			invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Intransit' TransportStatusTxt='Intransit' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
			System.out.println("<******************* Order Intransit *******************>");


			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Intransit");

			sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400.10000");


			invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Delivered' TransportStatusTxt='Delivered' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
			System.out.println("<******************* Order Delivered *******************>");

			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Order "
					+ "Delivered");

			sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1500");
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

	}
	
	@Test
	public void testPoPipeline3(){
		try {
			/*
			Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed--> Partially Shipped -->Partially Delivered 

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "VAARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";
			String sDepartmentCode="NBN";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			//createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","Y");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			System.out.println("******************* Order Creation Test Case 3 *******************");
			
			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			String sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
			System.out.println("*******************  PO Not Sent To Vendor *******************");

			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
			System.out.println("*******************  PO Sent To Vendor *******************");

			
			invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
			System.out.println("******************* PO Committed *******************");

			
			invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='5' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
			System.out.println("******************* PO Shipped *******************");
           
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Shipped");

			String sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");


			invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Delivered' TransportStatusTxt='Delivered' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
			System.out.println("<******************* Order Delivered *******************>");

			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Partially Order "
					+ "Delivered");

			sShipmentStatus=getShipmentStatus(sOrderName);
			Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1500");
			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}	
	}
	
	@Test
	public void testPoPipeline4(){
		try {
			/*
			Scenario:-

Expected Outcome:-
Created--> PO Sent To Vendor --> Rejected 

			 */
			//get Default Order XML
			YFCDocument input = getOrderXmlWIthOneLine();

			//setting Test Case Related Data
			String sEnterpriseCode = "TELSTRA_SCLSA";		
			String sSellerOrganizationCode = "TELSTRA";
			String sBuyerOrganizationCode = "TELSTRA";
			String sEntryType = "INTEGRAL_PLUS";
			String sOrderType = "TRANSPORT_ORDER";
			String sInterfaceNo = "INT_ODR_3";
			String sOrderName = "TestCaseOrder"+i++;
			String sBillToID = "VAARON";
			String sShipToID = "000021844";
			String sExternalCustomerID = "TestOrg5";
			String sShipNode="TestOrg10";
			String sNodeTypeRecv = "DC";
			String sNodeTypeShip = "DC";
			String sDepartmentCode="NBN";


			input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
			input.getDocumentElement().setAttribute("OrderName", sOrderName);
			input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
			input.getDocumentElement().setAttribute("EntryType", sEntryType);
			input.getDocumentElement().setAttribute("OrderType", sOrderType);
			input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
			input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
			input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
			input.getDocumentElement().setAttribute("BillToID", sBillToID);
			input.getDocumentElement().setAttribute("ShipToID", sBillToID);
			input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
			input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


			//create Customer which is used in Input XML
			createCustomer(sBillToID, sShipToID, sExternalCustomerID);

			//Create Item which is used in Input XML
			//createItem("TestItem1","EA", sEnterpriseCode);

			//Setting all the Order Validations OFF
			manageCommonCodeForOrderValidation("COST_CENTER","N");
			manageCommonCodeForOrderValidation("ITEM_ROUND","N");
			manageCommonCodeForOrderValidation("NEW_DAC","Y");
			manageCommonCodeForOrderValidation("ZIP_CODE","N");

			//setting NodeType as per test case
			setNodeType(sExternalCustomerID,sNodeTypeRecv);
			setNodeType(sShipNode,sNodeTypeShip);			

			//call ManageOrder Service to Test
			invokeYantraService("GpsManageOrder", input);
			System.out.println("<--------- Order Create = "+sOrderName+" --------->");

			//get details of newly created Order
			YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
			YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
			YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


			Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
			System.out.println("******************* Order Creation Test Case 4 *******************");
			
			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			String sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
			System.out.println("*******************  PO Not Sent To Vendor *******************");

			
			invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
			System.out.println("*******************  PO Sent To Vendor *******************");

			
			invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='9000.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
			sStatus=getOrderStatus(sOrderName);
			//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
			Assert.assertEquals("Order Line Status Check = ",sStatus,"Rejected");
			System.out.println("*******************  PO Rejected *******************");

			
		} catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}

		
	}



@Test
public void testPoPipeline5(){
	try {
		/*
		Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed--> Partially Shipped -->Partially Intransit --> Change Status=C 

		 */
		//get Default Order XML
		YFCDocument input = getOrderXmlWIthOneLine();

		//setting Test Case Related Data
		String sEnterpriseCode = "TELSTRA_SCLSA";		
		String sSellerOrganizationCode = "TELSTRA";
		String sBuyerOrganizationCode = "TELSTRA";
		String sEntryType = "INTEGRAL_PLUS";
		String sOrderType = "TRANSPORT_ORDER";
		String sInterfaceNo = "INT_ODR_3";
		String sOrderName = "TestCaseOrder"+i++;
		String sBillToID = "VAARON";
		String sShipToID = "000021844";
		String sExternalCustomerID = "TestOrg5";
		String sShipNode="TestOrg10";
		String sNodeTypeRecv = "DC";
		String sNodeTypeShip = "DC";
		String sDepartmentCode="NBN";


		input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
		input.getDocumentElement().setAttribute("OrderName", sOrderName);
		input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
		input.getDocumentElement().setAttribute("EntryType", sEntryType);
		input.getDocumentElement().setAttribute("OrderType", sOrderType);
		input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
		input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
		input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
		input.getDocumentElement().setAttribute("BillToID", sBillToID);
		input.getDocumentElement().setAttribute("ShipToID", sBillToID);
		input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
		input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


		//create Customer which is used in Input XML
		createCustomer(sBillToID, sShipToID, sExternalCustomerID);

		//Create Item which is used in Input XML
		//createItem("TestItem1","EA", sEnterpriseCode);

		//Setting all the Order Validations OFF
		manageCommonCodeForOrderValidation("COST_CENTER","N");
		manageCommonCodeForOrderValidation("ITEM_ROUND","N");
		manageCommonCodeForOrderValidation("NEW_DAC","Y");
		manageCommonCodeForOrderValidation("ZIP_CODE","N");

		//setting NodeType as per test case
		setNodeType(sExternalCustomerID,sNodeTypeRecv);
		setNodeType(sShipNode,sNodeTypeShip);			

		//call ManageOrder Service to Test
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Create = "+sOrderName+" --------->");

		//get details of newly created Order
		YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
		YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


		Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
		System.out.println("******************* Order Creation Test Case 5 *******************");
		
		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		String sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
		System.out.println("*******************  PO Not Sent To Vendor *******************");

		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
		System.out.println("*******************  PO Sent To Vendor *******************");

		
		invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
		System.out.println("******************* PO Committed *******************");

		
		invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='10' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
		System.out.println("******************* PO Shipped *******************");
       
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Shipped");

		String sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");
				
		invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Intransit' TransportStatusTxt='Intransit' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
		System.out.println("<******************* Order Intransit *******************>");


		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Intransit");

		sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400.10000");


		input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", "C");
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Modify = "+sOrderName+" --------->");


		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Order "
				+ "Delivered");

		sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1500");
		
			
	} catch (Exception e) {
		e.printStackTrace();
		Assert.fail(e.getMessage());
	}

	
}

@Test
public void testPoPipeline6(){
	try {
		/*
		Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed--> Partially Shipped --> Change Status=C (Delivered)

		 */
		//get Default Order XML
		YFCDocument input = getOrderXmlWIthOneLine();

		//setting Test Case Related Data
		String sEnterpriseCode = "TELSTRA_SCLSA";		
		String sSellerOrganizationCode = "TELSTRA";
		String sBuyerOrganizationCode = "TELSTRA";
		String sEntryType = "INTEGRAL_PLUS";
		String sOrderType = "TRANSPORT_ORDER";
		String sInterfaceNo = "INT_ODR_3";
		String sOrderName = "TestCaseOrder"+i++;
		String sBillToID = "VAARON";
		String sShipToID = "000021844";
		String sExternalCustomerID = "TestOrg5";
		String sShipNode="TestOrg10";
		String sNodeTypeRecv = "DC";
		String sNodeTypeShip = "DC";
		String sDepartmentCode="NBN";


		input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
		input.getDocumentElement().setAttribute("OrderName", sOrderName);
		input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
		input.getDocumentElement().setAttribute("EntryType", sEntryType);
		input.getDocumentElement().setAttribute("OrderType", sOrderType);
		input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
		input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
		input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
		input.getDocumentElement().setAttribute("BillToID", sBillToID);
		input.getDocumentElement().setAttribute("ShipToID", sBillToID);
		input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
		input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


		//create Customer which is used in Input XML
		createCustomer(sBillToID, sShipToID, sExternalCustomerID);

		//Create Item which is used in Input XML
		//createItem("TestItem1","EA", sEnterpriseCode);

		//Setting all the Order Validations OFF
		manageCommonCodeForOrderValidation("COST_CENTER","N");
		manageCommonCodeForOrderValidation("ITEM_ROUND","N");
		manageCommonCodeForOrderValidation("NEW_DAC","Y");
		manageCommonCodeForOrderValidation("ZIP_CODE","N");

		//setting NodeType as per test case
		setNodeType(sExternalCustomerID,sNodeTypeRecv);
		setNodeType(sShipNode,sNodeTypeShip);			

		//call ManageOrder Service to Test
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Create = "+sOrderName+" --------->");

		//get details of newly created Order
		YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
		YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


		Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
		System.out.println("******************* Order Creation Test Case 6 *******************");
		
		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		String sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
		System.out.println("*******************  PO Not Sent To Vendor *******************");

		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
		System.out.println("*******************  PO Sent To Vendor *******************");

		
		invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
		System.out.println("******************* PO Committed *******************");

		
		invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='10' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
		System.out.println("******************* PO Shipped *******************");
       
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Shipped");

		String sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");
				
		
		input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", "C");
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Modify = "+sOrderName+" --------->");


		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Order "
				+ "Delivered");

		sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1500");
		
			
	} catch (Exception e) {
		e.printStackTrace();
		Assert.fail(e.getMessage());
	}

	
}

@Test
public void testPoPipeline7(){
	try {
		/*
		Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed--> Partially Shipped -->Partially Intransit --> Futile

		 */
		//get Default Order XML
		YFCDocument input = getOrderXmlWIthOneLine();

		//setting Test Case Related Data
		String sEnterpriseCode = "TELSTRA_SCLSA";		
		String sSellerOrganizationCode = "TELSTRA";
		String sBuyerOrganizationCode = "TELSTRA";
		String sEntryType = "INTEGRAL_PLUS";
		String sOrderType = "TRANSPORT_ORDER";
		String sInterfaceNo = "INT_ODR_3";
		String sOrderName = "TestCaseOrder"+i++;
		String sBillToID = "VAARON";
		String sShipToID = "000021844";
		String sExternalCustomerID = "TestOrg5";
		String sShipNode="TestOrg10";
		String sNodeTypeRecv = "DC";
		String sNodeTypeShip = "DC";
		String sDepartmentCode="NBN";


		input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
		input.getDocumentElement().setAttribute("OrderName", sOrderName);
		input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
		input.getDocumentElement().setAttribute("EntryType", sEntryType);
		input.getDocumentElement().setAttribute("OrderType", sOrderType);
		input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
		input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
		input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
		input.getDocumentElement().setAttribute("BillToID", sBillToID);
		input.getDocumentElement().setAttribute("ShipToID", sBillToID);
		input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
		input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


		//create Customer which is used in Input XML
		createCustomer(sBillToID, sShipToID, sExternalCustomerID);

		//Create Item which is used in Input XML
		//createItem("TestItem1","EA", sEnterpriseCode);

		//Setting all the Order Validations OFF
		manageCommonCodeForOrderValidation("COST_CENTER","N");
		manageCommonCodeForOrderValidation("ITEM_ROUND","N");
		manageCommonCodeForOrderValidation("NEW_DAC","Y");
		manageCommonCodeForOrderValidation("ZIP_CODE","N");

		//setting NodeType as per test case
		setNodeType(sExternalCustomerID,sNodeTypeRecv);
		setNodeType(sShipNode,sNodeTypeShip);			

		//call ManageOrder Service to Test
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Create = "+sOrderName+" --------->");

		//get details of newly created Order
		YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
		YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


		Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
		System.out.println("******************* Order Creation Test Case 7 *******************");
		
		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		String sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
		System.out.println("*******************  PO Not Sent To Vendor *******************");

		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
		System.out.println("*******************  PO Sent To Vendor *******************");

		
		invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
		System.out.println("******************* PO Committed *******************");

		
		invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='10' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
		System.out.println("******************* PO Shipped *******************");
       
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Shipped");

		String sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");
				
		invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Intransit' TransportStatusTxt='Intransit' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
		System.out.println("<******************* Order Intransit *******************>");


		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Intransit");

		sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400.10000");


		invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Futile' TransportStatusTxt='Unsuccessful' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
		System.out.println("<******************* Order Futile *******************>");


		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Futile Delivery");

		sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"9000.30000");

		
			
	} catch (Exception e) {
		e.printStackTrace();
		Assert.fail(e.getMessage());
	}

	
}

@Test
public void testPoPipeline8(){
	try {
		/*
		Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed--> Partially Shipped --> Futile 

		 */
		//get Default Order XML
		YFCDocument input = getOrderXmlWIthOneLine();

		//setting Test Case Related Data
		String sEnterpriseCode = "TELSTRA_SCLSA";		
		String sSellerOrganizationCode = "TELSTRA";
		String sBuyerOrganizationCode = "TELSTRA";
		String sEntryType = "INTEGRAL_PLUS";
		String sOrderType = "TRANSPORT_ORDER";
		String sInterfaceNo = "INT_ODR_3";
		String sOrderName = "TestCaseOrder"+i++;
		String sBillToID = "VAARON";
		String sShipToID = "000021844";
		String sExternalCustomerID = "TestOrg5";
		String sShipNode="TestOrg10";
		String sNodeTypeRecv = "DC";
		String sNodeTypeShip = "DC";
		String sDepartmentCode="NBN";


		input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
		input.getDocumentElement().setAttribute("OrderName", sOrderName);
		input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
		input.getDocumentElement().setAttribute("EntryType", sEntryType);
		input.getDocumentElement().setAttribute("OrderType", sOrderType);
		input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
		input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
		input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
		input.getDocumentElement().setAttribute("BillToID", sBillToID);
		input.getDocumentElement().setAttribute("ShipToID", sBillToID);
		input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
		input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


		//create Customer which is used in Input XML
		createCustomer(sBillToID, sShipToID, sExternalCustomerID);

		//Create Item which is used in Input XML
		//createItem("TestItem1","EA", sEnterpriseCode);

		//Setting all the Order Validations OFF
		manageCommonCodeForOrderValidation("COST_CENTER","N");
		manageCommonCodeForOrderValidation("ITEM_ROUND","N");
		manageCommonCodeForOrderValidation("NEW_DAC","Y");
		manageCommonCodeForOrderValidation("ZIP_CODE","N");

		//setting NodeType as per test case
		setNodeType(sExternalCustomerID,sNodeTypeRecv);
		setNodeType(sShipNode,sNodeTypeShip);			

		//call ManageOrder Service to Test
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Create = "+sOrderName+" --------->");

		//get details of newly created Order
		YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
		YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


		Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
		System.out.println("******************* Order Creation Test Case 8 *******************");
		
		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		String sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
		System.out.println("*******************  PO Not Sent To Vendor *******************");

		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
		System.out.println("*******************  PO Sent To Vendor *******************");

		
		invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
		System.out.println("******************* PO Committed *******************");

		
		invokeYantraService("GpsProcessPOASNMsg", YFCDocument.getDocumentFor("<Shipment ActualShipmentDate='20160426' OrderName='"+sOrderName+"' ShipmentNo='"+sOrderName+"' ><ShipmentLines><ShipmentLine ItemID='TestItem1' PrimeLineNo='1' Quantity='10' UnitOfMeasure='EA' /></ShipmentLines></Shipment>"));
		System.out.println("******************* PO Shipped *******************");
       
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Shipped");

		String sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"1400");
				
		
		invokeYantraService("GpsCarrierUpdate", YFCDocument.getDocumentFor("<Shipment ShipmentNo='' OrderName='"+sOrderName+"' TrackingNo=''><Extn><CarrierUpdateList><CarrierUpdate TransportStatus='Futile' TransportStatusTxt='Unsuccessful' TransportStatusDate='20160908' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='PODSigObj' /></CarrierUpdateList></Extn><Containers><Container ContainerNo='44556677238'/></Containers></Shipment>"));
		System.out.println("<******************* Order Futile *******************>");


		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Futile Delivery");

		sShipmentStatus=getShipmentStatus(sOrderName);
		Assert.assertEquals("Shipment Line Status Check = ",sShipmentStatus,"9000.30000");
			
	} catch (Exception e) {
		e.printStackTrace();
		Assert.fail(e.getMessage());
	}

	
}

@Test
public void testPoPipeline9(){
	try {
		/*
		Scenario:-

Expected Outcome:-
Created-->PO Not Sent To Vendor -->  PO Sent To Vendor --> Committed-->ChangeStatus --> D

		 */
		//get Default Order XML
		YFCDocument input = getOrderXmlWIthOneLine();

		//setting Test Case Related Data
		String sEnterpriseCode = "TELSTRA_SCLSA";		
		String sSellerOrganizationCode = "TELSTRA";
		String sBuyerOrganizationCode = "TELSTRA";
		String sEntryType = "INTEGRAL_PLUS";
		String sOrderType = "TRANSPORT_ORDER";
		String sInterfaceNo = "INT_ODR_3";
		String sOrderName = "TestCaseOrder"+i++;
		String sBillToID = "VAARON";
		String sShipToID = "000021844";
		String sExternalCustomerID = "TestOrg5";
		String sShipNode="TestOrg10";
		String sNodeTypeRecv = "DC";
		String sNodeTypeShip = "DC";
		String sDepartmentCode="NBN";


		input.getDocumentElement().setAttribute("EnterpriseCode", sEnterpriseCode);
		input.getDocumentElement().setAttribute("OrderName", sOrderName);
		input.getDocumentElement().setAttribute("OrderHeaderKey", sOrderName);
		input.getDocumentElement().setAttribute("EntryType", sEntryType);
		input.getDocumentElement().setAttribute("OrderType", sOrderType);
		input.getDocumentElement().setAttribute("SellerOrganizationCode", sSellerOrganizationCode);
		input.getDocumentElement().setAttribute("BuyerOrganizationCode", sBuyerOrganizationCode);
		input.getDocumentElement().setAttribute("InterfaceNo", sInterfaceNo);
		input.getDocumentElement().setAttribute("BillToID", sBillToID);
		input.getDocumentElement().setAttribute("ShipToID", sBillToID);
		input.getDocumentElement().setAttribute("DepartmentCode", sDepartmentCode);
		input.getDocumentElement().getElementsByTagName("OrderLine").item(0).setAttribute("ShipNode", sShipNode);


		//create Customer which is used in Input XML
		createCustomer(sBillToID, sShipToID, sExternalCustomerID);

		//Create Item which is used in Input XML
		//createItem("TestItem1","EA", sEnterpriseCode);

		//Setting all the Order Validations OFF
		manageCommonCodeForOrderValidation("COST_CENTER","N");
		manageCommonCodeForOrderValidation("ITEM_ROUND","N");
		manageCommonCodeForOrderValidation("NEW_DAC","Y");
		manageCommonCodeForOrderValidation("ZIP_CODE","N");

		//setting NodeType as per test case
		setNodeType(sExternalCustomerID,sNodeTypeRecv);
		setNodeType(sShipNode,sNodeTypeShip);			

		//call ManageOrder Service to Test
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Create = "+sOrderName+" --------->");

		//get details of newly created Order
		YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput,orderListTemplate());
		YFCElement eleOutputOrder = output.getElementsByTagName("Order").item(0); 


		Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0005");
		System.out.println("******************* Order Creation Test Case 9 *******************");
		
		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.05000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		String sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Not Sent To Vendor");
		System.out.println("*******************  PO Not Sent To Vendor *******************");

		
		invokeYantraService("GpsPOAcknowledgement", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1100.10000' OrderName='"+sOrderName+"' TransactionId='GPS_PO_TECH_CONFIRM.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"PO Sent To Vendor");
		System.out.println("*******************  PO Sent To Vendor *******************");

		
		invokeYantraApi("changeOrderStatus", YFCDocument.getDocumentFor("<OrderStatusChange BaseDropStatus='1260.10000' OrderHeaderKey='"+sOrderName+"' TransactionId='GPS_VENDOR_UPDATE.0005.ex'/>"));
		sStatus=getOrderStatus(sOrderName);
		//Assert.assertEquals(eleOutputOrder.getAttribute("DocumentType"),"0001");
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Committed");
		System.out.println("******************* PO Committed *******************");

		
		input.getElementsByTagName("OrderLine").item(0).setAttribute("ChangeStatus", "D");
		invokeYantraService("GpsManageOrder", input);
		System.out.println("<--------- Order Modify = "+sOrderName+" --------->");

		sStatus=getOrderStatus(sOrderName);
		Assert.assertEquals("Order Line Status Check = ",sStatus,"Cancelled");
			
	} catch (Exception e) {
		e.printStackTrace();
		Assert.fail(e.getMessage());
	}

	
}



	protected void createItem(String sItemId, String sUOM, String sOrganizationCode) {

		String ItemXml = "<ItemList><Item CanUseAsServiceTool='N' GlobalItemID='' ItemGroupCode='PROD' ItemID='' OrganizationCode='' "
				+ " UnitOfMeasure=''><PrimaryInformation AllowGiftWrap='N' AssumeInfiniteInventory='N' BundleFulfillmentMode='01' "
				+ "BundlePricingStrategy='PARENT' CapacityQuantityStrategy='' CostCurrency='AUD' CountryOfOrigin='' CreditWOReceipt='N'"
				+ " DefaultProductClass='' Description='' ExtendedDescription='' ExtendedDisplayDescription='Test Item' "
				+ "FixedCapacityQtyPerLine='0.00' FixedPricingQtyPerLine='0.00' InvoiceBasedOnActuals='N' IsAirShippingAllowed='Y' "
				+ "IsDeliveryAllowed='N' IsEligibleForShippingDiscount='Y' IsFreezerRequired='N' IsHazmat='N' IsParcelShippingAllowed='Y'"
				+ " IsPickupAllowed='N' IsProcurementAllowed='Y' IsReturnService='N' IsReturnable='Y' IsShippingAllowed='Y' "
				+ "IsStandaloneService='' IsSubOnOrderAllowed='' ItemType='' KitCode='' ManufacturerItem='' ManufacturerItemDesc='' "
				+ "ManufacturerName='' MasterCatalogID='' MaxOrderQuantity='0.00' MinOrderQuantity='0.00' MinimumCapacityQuantity='0.00' "
				+ "NumSecondarySerials='0' OrderingQuantityStrategy='ENT' PricingQuantityConvFactor='0.00' PricingQuantityStrategy='IQTY'"
				+ " PricingUOM='EA' PricingUOMStrategy='INV' PrimaryEnterpriseCode='' PrimarySupplier='' ProductLine='' "
				+ "RequiresProdAssociation='N' ReturnWindow='0' RunQuantity='0.00' SerializedFlag='N' ServiceTypeID='' "
				+ "ShortDescription='testitem1' Status='3000' TaxableFlag='N' UnitCost='0.00' UnitHeight='0.00' UnitHeightUOM='' "
				+ "UnitLength='0.00' UnitLengthUOM='' UnitWeight='0.00' UnitWeightUOM='' UnitWidth='0.00' UnitWidthUOM=''/>"
				+ "		<InventoryParameters ATPRule='' AdvanceNotificationTime='0' DefaultExpirationDays='0' InventoryMonitorRule='' "
				+ "IsFifoTracked='N' IsItemBasedAllocationAllowed='Y' IsSerialTracked='N' LeadTime='0' MaximumNotificationTime='0.00'"
				+ " MinNotificationTime='0' NodeLevelInventoryMonitorRule='' ProcessingTime='0' TagControlFlag='N' TimeSensitive='N' "
				+ "UseUnplannedInventory='N'/><ClassificationCodes CommodityCode='' ECCNNo='' HarmonizedCode='' HazmatClass='' "
				+ "NAICSCode='' NMFCClass='' NMFCCode='' OperationalConfigurationComplete='N' PickingType='' PostingClassification='' "
				+ "Schedule_B_Code='' StorageType='' TaxProductCode='' UNSPSC='' VelocityCode=''/></Item></ItemList>";

		YFCDocument docManageItemInput = YFCDocument.getDocumentFor(ItemXml);
		YFCElement eleItem = docManageItemInput.getDocumentElement().getElementsByTagName("Item").item(0);

		eleItem.setAttribute("ItemID", sItemId);
		eleItem.setAttribute("UnitOfMeasure", sUOM);
		eleItem.setAttribute("OrganizationCode", sOrganizationCode);

		invokeYantraApi("manageItem", docManageItemInput);

		System.out.println("<--------- Item Created = "+sItemId+" --------->");

	}

	protected YFCDocument getOrderXmlWIthOneLine() throws Exception {
		String orderXmlString = 
				"<Order PriorityCode='N' SearchCriteria2='' SearchCriteria1='' Division=''>"
						+ "  <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777'"
						+ "  AddressLine4='' City='BELYUEN' ZipCode='0800' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854'/>"
						+ " <OrderLines>"
						+ " <OrderLine OrderedQty='10' PrimeLineNo='1' ChangeStatus=''  ReceivingNode=''>"
						+ " <Item ItemID='TestItem1' UnitOfMeasure='EA' />"
						+ " </OrderLine>"
						+ " </OrderLines>"
						+ " </Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}


	protected YFCDocument orderListTemplate() throws Exception {
		String orderXmlString = "<OrderList><Order OrderNo='' EnterpriseCode='' BuyerOrganizationCode='' SellerOrganizationCode='' EntryType=''   "
				+ "DocumentType='' OrderName='' BillToID='' ShipToID=''    OrderType='' Division='' ShipNode='' Status='' DepartmentCode='' OrderHeaderKey=''>"
				+ "<PersonInfoBillTo FirstName='' AddressLine2='' AddressLine3='' AddressLine4='' City='' ZipCode='' State='' DayPhone='' DayFaxNo='' Country=''/>"
				+ "<OrderLines><OrderLine Status='' OrderedQty='' PrimeLineNo='' ReceivingNode='' ShipNode='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''>"
				+ "<Item ItemID='' UnitOfMeasure='' /><Schedules><Schedule/></Schedules></OrderLine></OrderLines><OrderStatuses><OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>"
				+ "</OrderStatuses></Order></OrderList>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}


	protected void manageCommonCodeForOrderValidation(String sCodeValue, String sValue) {
		YFCDocument manageCommonCodeInput = YFCDocument
				.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\"" + sValue + "\" CodeValue=\""
						+ sCodeValue + "\" CodeType='ORDER_VALIDATION' OrganizationCode='DEFAULT'/>");
		invokeYantraApi("manageCommonCode", manageCommonCodeInput);

		System.out.println("<--------- manageCommonCode called for = "+sCodeValue+" --------->");
	}

	protected void createCustomer(String sCustomerID, String sCustomerKey, String sExternalCustomerID) {

		String CustomerXml = "<Customer CustomerID='' CustomerKey='' CustomerType='02' ExternalCustomerID='' "
				+ "OrganizationCode='TELSTRA_SCLSA' Status='10'><Consumer><BillingPersonInfo PersonID='' AddressLine1='3ferntree Road' "
				+ "AddressLine2=' 20  '	AddressLine3='HUME ST 321777      ' AddressLine4='' AddressLine5=''	AddressLine6='' AlternateEmailID='' "
				+ "City='Wien'	Company='' Country='AU' DayFaxNo='     031 23 2854   ' DayPhone='     5461801' Department=''	EMailID='' "
				+ "FirstName='Divisi  n de Personal Model-New' 	LastName='' MiddleName=''	MobilePhone='' State='VI' Suffix='' Title='' ZipCode='     1130  W AT'/>"
				+ "</Consumer></Customer>";

		YFCDocument docCustomerInput = YFCDocument.getDocumentFor(CustomerXml);
		YFCElement eleCustomer = docCustomerInput.getDocumentElement();

		eleCustomer.setAttribute("CustomerID", sCustomerID);
		eleCustomer.setAttribute("CustomerKey", sCustomerKey);
		eleCustomer.setAttribute("ExternalCustomerID", sExternalCustomerID);

		docCustomerInput.getElementsByTagName("BillingPersonInfo").item(0).setAttribute("PersonID", sCustomerKey);

		invokeYantraApi("createCustomer", docCustomerInput);

		System.out.println("<--------- Customer Create = "+sCustomerID+" --------->");

	}


	protected void setNodeType(String sShipNode, String sNodeType) {

		String ManageOrgXml = "<Organization OrganizationCode='"+sShipNode+"'><Node ShipNode='"+sShipNode+"' NodeType='"+sNodeType+"'/></Organization>"; 

		invokeYantraApi("manageOrganizationHierarchy", YFCDocument.getDocumentFor(ManageOrgXml));

		System.out.println("<--------- NodeType set for "+sShipNode+" to "+sNodeType+" --------->");

	}


	protected String getOrderStatus(String sOrderName) {
		YFCDocument orderListTemplate =  YFCDocument.getDocumentFor("<OrderList><Order OrderNo='' EnterpriseCode='' BuyerOrganizationCode='' SellerOrganizationCode='' EntryType=''   "
				+ "DocumentType='' OrderName='' BillToID='' ShipToID=''    OrderType='' Division='' ShipNode='' Status='' DepartmentCode='' OrderHeaderKey=''>"
				+ "<PersonInfoBillTo FirstName='' AddressLine2='' AddressLine3='' AddressLine4='' City='' ZipCode='' State='' DayPhone='' DayFaxNo='' Country=''/>"
				+ "<OrderLines><OrderLine Status='' OrderedQty='' PrimeLineNo='' ReceivingNode='' ShipNode='' SubLineNo='' OrderLineKey='' OrderHeaderKey=''>"
				+ "<Item ItemID='' UnitOfMeasure='' /><Schedules><Schedule/></Schedules></OrderLine></OrderLines><OrderStatuses><OrderStatus OrderReleaseKey='' Status='' StatusQty=''/>"
				+ "</OrderStatuses></Order></OrderList>");


		YFCDocument getOrderListInput = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderName+"'/>");
		YFCDocument output = invokeYantraApi("getOrderList", getOrderListInput, orderListTemplate);
		return output.getElementsByTagName("Order").item(0).getAttribute("Status"); 

	}

	protected String getShipmentStatus(String sOrderName) {

		YFCDocument inXml = YFCDocument.getDocumentFor("<Shipment DocumentType='0005' OrderHeaderKey='"+sOrderName+"'></Shipment>");
		YFCDocument shipmentListTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment Status=''></Shipment></Shipments>");
		YFCDocument outXml =invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, inXml, shipmentListTemplate);
		return outXml.getElementsByTagName("Shipment").item(0).getAttribute("Status");



	}
}
