package com.gps.hubble.order.api;

import org.junit.Assert;
import org.junit.Test;

import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class TestPickOrder extends TestBase{
	
    @Override
	protected void beforeTestCase() {
		super.beforeTestCase();
	    manageCommonCode("ORDER_VALIDATION", "COST_CENTER", "N");
	    manageCommonCode("ORDER_VALIDATION", "ITEM_ROUND", "N");
	    manageCommonCode("ORDER_VALIDATION", "NEW_DAC", "N");
	    manageCommonCode("ORDER_VALIDATION", "ZIP_CODE", "N");
	}
  
  
  
  private YFCDocument getOrdDoc() {
    return YFCDocument.getDocumentFor("<Order EnterpriseCode='TELSTRA_SCLSA' Division='CXS041'  OrderName='' InterfaceNo='INT_ODR_1' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS'  BillToID='TESTCUSTOMER' ShipToID='DAC'   OrderType='MATERIAL_RESERVATION' > " +
        "<PersonInfoBillTo FirstName='Keerthi' AddressLine2='' AddressLine3='' AddressLine4='' City='Bangalore' ZipCode='' State='' DayPhone='' DayFaxNo=''/><OrderLines/></Order>");
  }

  @Test
  public void mandatoryFieldsLineLevel() throws Exception{
    String errorString = "TEL_ERR_006_001";
    try{
      invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='TC001' >" +
          "<OrderLines>" +
          "<OrderLine Action='' PrimeLineNo='' SubLineNo='' ItemID='' StatusQuantity=''/>" +
          "</OrderLines>" +
          "</OrderRelease>"));
    }catch(Exception e){
      if(!e.toString().contains(errorString)){
        Assert.fail("This error string should come "+errorString);
      }
    } 
  }

  @Test
  public void orderAvailableValidation() throws Exception{
    String errorString = "TEL_ERR_006_007-1";
    try{
      invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='TC001000' >" +
          "<OrderLines>" +
          "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='1111' StatusQuantity='1'/>" +
          "</OrderLines>" +
          "</OrderRelease>"));
    }catch(Exception e){
      if(!e.toString().contains(errorString)){
        Assert.fail("This error string should come "+errorString);
      }
    } 
  }
 

  @Test
  public void mandatoryFieldsOrderName() throws Exception{
    String errorString = "TEL_ERR_006_001";
    try{
      invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='' >" +
          "<OrderLines>" +
          "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
          "</OrderLines>" +
          "</OrderRelease>"));
    }catch(Exception e){
      if(e.toString().contains(errorString)){
        Assert.assertEquals("Pass", "Pass");
      }else{
        Assert.assertEquals("Pass", "Fail");
      }
    } 
  }

  @Test
  public void orderHoldValidation() throws Exception{
    String errorStr = "TEL_ERR_006_002";

    YFCDocument ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T1");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    YFCElement itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());
    invokeYantraApi("createOrder", ordDoc);

    ordDoc = getOrdDoc();   
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T2");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());
    invokeYantraApi("createOrder", ordDoc);

    try{
      invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T2' >" +
          "<OrderLines>" +
          "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
          "</OrderLines>" +
          "</OrderRelease>"));
    }catch (Exception e){
      if(e.toString().contains(errorStr)){
        System.out.println("Exception : " + e.toString());
        Assert.assertEquals("Pass", "Pass");        
      }else{
        Assert.assertEquals("Pass", "Fail");
      }
    }

  }

  @Test
  public void orderLineVsCEVMsgLineValidation() throws Exception{
    String errorStr = "TEL_ERR_006_006";

    YFCDocument ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T3");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    YFCElement itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());
    invokeYantraApi("createOrder", ordDoc);

    try{
      invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T3' >" +
          "<OrderLines>" +
          "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
          "<OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='1'/>" +
          "</OrderLines>" +
          "</OrderRelease>"));
    }catch (Exception e){
      if(e.toString().contains(errorStr)){
        System.out.println("Exception : " + e.toString());
        Assert.assertEquals("Pass", "Pass");        
      }else{
        Assert.assertEquals("Pass", "Fail");
      }
    }
  }

  @Test
  public void backOrderForAllOrdLineQts() throws Exception{
    YFCDocument ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T4");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    YFCElement itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());
    YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

    invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T4' >" +
        "<OrderLines>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='0'/>" +
        "</OrderLines>" +
        "</OrderRelease>"));   
    YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
    String expectedTotalNumberOfRecords="0";
    String resultTotalNumberOfRecords=Integer.toString(shipmentDoc.getElementsByTagName("Shipment").getLength());
    System.out.println("shipmentDoc : " + shipmentDoc.toString());
    Assert.assertEquals(expectedTotalNumberOfRecords, resultTotalNumberOfRecords);
  }

  @Test
  public void createShipmentForPartialPick() throws Exception{
    YFCDocument ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T5");
    ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T5");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    YFCElement itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());
    YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

    invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T5' >" +
        "<OrderLines>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
        "</OrderLines>" +
        "</OrderRelease>"));   
    YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"));
    String expectedTotalNumberOfRecords="1";
    String resultTotalNumberOfRecords=Integer.toString(shipmentDoc.getElementsByTagName("Shipment").getLength());
    System.out.println("shipmentDoc : " + shipmentDoc.toString());
    Assert.assertEquals(expectedTotalNumberOfRecords, resultTotalNumberOfRecords);
  }

  @Test
  public void createShipmentForFullPickMultiline() throws Exception{
    YFCDocument ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T6");
    ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T6");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    YFCElement itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).createChild("OrderLine");
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "3");
    orderLine.setAttribute("PrimeLineNo", "2");
    orderLine.setAttribute("ChangeStatus", "");
    itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());

    YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);    

    invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T6' >" +
        "<OrderLines>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='17'/>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
        "</OrderLines>" +
        "</OrderRelease>"));   
    YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"),YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey=''><ShipmentLines ><ShipmentLine ShipmentLineKey='' />" +
        "</ShipmentLines></Shipment></Shipments>"));
    String expectedTotalNumberOfRecords="2";
    String resultTotalNumberOfRecords=Integer.toString(shipmentDoc.getElementsByTagName("ShipmentLine").getLength());
    System.out.println("shipmentDoc : " + shipmentDoc.toString());
    Assert.assertEquals(expectedTotalNumberOfRecords, resultTotalNumberOfRecords);
  }

  @Test
  public void createShipmentWithTwoOrderReleaseKeys() throws Exception{
    YFCDocument ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T8");
    ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T8");
    System.out.println("Order constant values doc : " + ordDoc.toString());
    YFCElement orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).getChildElement("OrderLine",true);
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "17");
    orderLine.setAttribute("PrimeLineNo", "1");
    orderLine.setAttribute("SubLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    YFCElement itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).createChild("OrderLine");
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "3");
    orderLine.setAttribute("PrimeLineNo", "2");
    orderLine.setAttribute("SubLineNo", "1");
    orderLine.setAttribute("ChangeStatus", "");
    itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    System.out.println("Order doc with Order Line : " + ordDoc.toString());    
    YFCDocument ordDetailDoc = invokeYantraApi("createOrder", ordDoc);   
    YFCDocument sInDoc= YFCDocument.getDocumentFor("<ScheduleOrder OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"'  ScheduleAndRelease='Y' CheckInventory='N' />");
    System.out.println("sInDoc : " + sInDoc.toString());
    try{
      invokeYantraApi("scheduleOrder", sInDoc);
    }catch (Exception e){
      System.out.println("e : " + e.toString());
    }

    ordDoc = getOrdDoc();       
    ordDoc.getDocumentElement().setAttribute("OrderName", "JUNIT-T8");
    ordDoc.getDocumentElement().setAttribute("OrderNo", "JUNIT-T8");
    System.out.println("Order constant values doc : " + ordDoc.toString());    
    orderLine = ordDoc.getDocumentElement().getChildElement("OrderLines",true).createChild("OrderLine");
    orderLine.setAttribute("ShipNode", "TestOrg5");
    orderLine.setAttribute("OrderedQty", "5");
    orderLine.setAttribute("SubLineNo", "1");
    orderLine.setAttribute("PrimeLineNo", "3");
    orderLine.setAttribute("ChangeStatus", "");
    itemEle = orderLine.getChildElement("Item", true);
    itemEle.setAttribute("ItemID", "PenStand");
    itemEle.setAttribute("UnitOfMeasure", "Each");
    invokeYantraService("GpsManageOrder", ordDoc);

    invokeYantraService("GpsPickOrder", YFCDocument.getDocumentFor("<OrderRelease OrderName='JUNIT-T8' >" +
        "<OrderLines>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' ItemID='PenStand' StatusQuantity='17'/>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' ItemID='PenStand' StatusQuantity='3'/>" +
        "<OrderLine Action='BACKORDER' PrimeLineNo='3' SubLineNo='1' ItemID='PenStand' StatusQuantity='5'/>" +
        "</OrderLines>" +
        "</OrderRelease>"));   
    YFCDocument shipmentDoc = invokeYantraApi("getShipmentList", YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='"+XPathUtil.getXpathAttribute(ordDetailDoc, "//@OrderHeaderKey")+"' />"),YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentKey=''><ShipmentLines ><ShipmentLine ShipmentLineKey='' OrderReleaseKey='' />" +
        "</ShipmentLines></Shipment></Shipments>"));
    String expectedTotalNumberOfRecords="3";
    String resultTotalNumberOfRecords=Integer.toString(shipmentDoc.getElementsByTagName("ShipmentLine").getLength());
    System.out.println("shipmentDoc : " + shipmentDoc.toString());
    Assert.assertEquals(expectedTotalNumberOfRecords, resultTotalNumberOfRecords);
  }
  
  
  private void manageCommonCode(String codeType,String codeValue, String shortDescription){
	  YFCDocument manageCommonCodeInput = YFCDocument.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\""+shortDescription+"\" CodeValue=\""+codeValue+"\" CodeType='"+codeType+"' />");
	  YFCDocument output = invokeYantraApi("manageCommonCode", manageCommonCodeInput);
	  System.out.println("manage common code "+output);
  }

}
