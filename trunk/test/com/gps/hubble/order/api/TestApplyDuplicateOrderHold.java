package com.gps.hubble.order.api;

import java.io.IOException;

import javax.xml.transform.TransformerException;

import org.apache.xpath.XPathAPI;
import org.junit.Assert;
import org.junit.Test;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.util.YFCCommon;


public class TestApplyDuplicateOrderHold extends TestBase {

	@Test
	public void testDuplicateOrderHoldAfter() throws SAXException, IOException, TransformerException {
		boolean expected = true;
		createItem();
		createOrder("2016-07-14T00:00:00-04:00", "CISCO_INT");
		YFCDocument docOrderLatestCreatedInOMS = createOrder("2016-07-15T00:00:00-04:00", "CISCO_INT");
		boolean isOrderOnHold = checkHoldOnOrder(docOrderLatestCreatedInOMS);
		System.out.println("Expected1: "+expected);
		System.out.println("Actual1: "+isOrderOnHold);
		Assert.assertEquals(expected, isOrderOnHold);
			}
	
	@Test
	public void testDuplicateOrderHoldBefore() throws SAXException, IOException, TransformerException {
		boolean expected = true;
		createItem();
		createOrder("2016-07-15T00:00:00-04:00", "CISCO_INT");
		YFCDocument docOrderLatestCreatedInOMS = createOrder("2016-07-14T00:00:00-04:00", "CISCO_INT");
		boolean isOrderOnHold = checkHoldOnOrder(docOrderLatestCreatedInOMS);
		System.out.println("Expected2: "+expected);
		System.out.println("Actual2: "+isOrderOnHold);
		Assert.assertEquals(expected, isOrderOnHold);
	}


	@Test
	public void testDuplicateOrderHoldForInvalidDeptCode()  throws SAXException, IOException, TransformerException {
		boolean expected = false;
		createItem();
		createOrder("2016-07-14T00:00:00-04:00", "CISCO_NOT_INT");
		YFCDocument docOrderLatestCreatedInOMS = createOrder("2016-07-13T00:00:00-04:00", "CISCO_NOT_INT");
		boolean isOrderOnHold = checkHoldOnOrder(docOrderLatestCreatedInOMS);
		System.out.println("Expected3: "+expected);
		System.out.println("Actual3: "+isOrderOnHold);
		Assert.assertEquals(expected, isOrderOnHold);
	}

	@Test
	public void testDuplicateOrderHoldForSignificantDiffInOrderDate()  throws SAXException, IOException, TransformerException {
		boolean expected = false;
		createItem();
		createOrder("2016-07-14T00:00:00-04:00", "CISCO_NOT_INT");
		YFCDocument docOrderLatestCreatedInOMS = createOrder("2016-07-16T00:00:00-04:00", "CISCO_INT");
		boolean isOrderOnHold = checkHoldOnOrder(docOrderLatestCreatedInOMS);
		System.out.println("Expected4: "+expected);
		System.out.println("Actual4: "+isOrderOnHold);
		Assert.assertEquals(expected, isOrderOnHold);

	}



	// Data creation methods


	private void createItem() throws SAXException, IOException {
		// TODO Auto-generated method stub
		String manageItem = "<ItemList><Item Action='' ItemID='Item01' OrganizationCode='TELSTRA_SCLSA' UnitOfMeasure='BAG'> "
				+ "<PrimaryInformation Description='BLOCK,S5000 128PR PROT RH' IsHazmat='' ItemType='NTRD' ManufacturerItem='SIEMENS S30264' ManufacturerName=''"
				+ " ProductLine='29'  ShortDescription='BLOCK,S5000 128PR PROT RH' Status='3000' UnitCost='0.65' />  </Item> </ItemList>";
		invokeYantraService("GpsManageCatalog", YFCDocument.parse(manageItem));
	}

	private YFCDocument createOrder(String sOrderDate, String sDepartmentCode) throws SAXException, IOException {

		String createOrder = "<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS' OrderName='VA197' OrderType='PURCHASE_ORDER' Division='CXS053' DepartmentCode='"+sDepartmentCode+"' OrderDate='"+sOrderDate+"' > "
				+ " <PersonInfoBillTo FirstName='Divisi  n de Personal Model-New' Country='AU' AddressLine1='3ferntree Gully RD' AddressLine2='20' AddressLine3='HUME ST 321777' AddressLine4='' City='Gleeson Hill' ZipCode='5520' State='SA' DayPhone='5461801' DayFaxNo='031 23 2854'/> "
				+ " <OrderLines> <OrderLine ReqDeliveryDate='20160724' OrderedQty='50' ShipNode='TestOrg3'> "
				+ "   <Item ItemID='Item01' UnitOfMeasure='BAG' UnitCost='21.45'/>   </OrderLine> </OrderLines> </Order>";
		YFCDocument docCreateOrder = YFCDocument.parse(createOrder);		
		return (invokeYantraApi("createOrder", docCreateOrder));
	}

	private boolean checkHoldOnOrder(YFCDocument docOrderLatestCreatedInOMS) throws SAXException, IOException, TransformerException {

		boolean isOrderOnHold = false;
		YFCDocument docOrderDetailTemplate = YFCDocument.parse("<OrderList><Order OrderNo=''> <OrderHoldTypes> <OrderHoldType HoldType='' Status='' /> "
				+ "</OrderHoldTypes> </Order> </OrderList>");
		YFCDocument docOrderDetailOp = invokeYantraApi("getOrderList", docOrderLatestCreatedInOMS, docOrderDetailTemplate);
		Document  w3DocOrderDetails = docOrderDetailOp.getDocument();
		Element eleHoldType = (Element) XPathAPI.selectSingleNode(w3DocOrderDetails, "//OrderHoldType[@HoldType='GPS_DUP_ORD_HOLD' and @Status='1100']");
		if(!YFCCommon.isVoid(eleHoldType)){
			isOrderOnHold = true;
		}
		return isOrderOnHold;
	}

}
