package com.gps.hubble.order.api;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class TestGPSCopyAttributesToChainedOrderAPI  extends TestBase {

	
	@Test
	public void testCopyAttributes() throws SAXException, IOException{
		
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		YFCDocument getOrderListOp  = callGetOrderListApi(sOrderHeaderKey);		
		System.out.println("TestGPSCopyAttributesToChainedOrderAPI :: getOrderListOp\n"+getOrderListOp);
		String sSoOrderName = getOrderListOp.getElementsByTagName("Order").item(0).getAttribute("OrderName") ;
		String sSoPriorityCode = getOrderListOp.getElementsByTagName("Order").item(0).getAttribute("PriorityCode") ;
		String sSoDivision = getOrderListOp.getElementsByTagName("Order").item(0).getAttribute("Division") ;
		String sSoBillToID = getOrderListOp.getElementsByTagName("Order").item(0).getAttribute("BillToID") ;
		String sSoShipToID = getOrderListOp.getElementsByTagName("Order").item(0).getAttribute("ShipToID") ;		
		String sSoBillAddressLine1 = XPathUtil.getXpathAttribute(getOrderListOp, "//PersonInfoBillTo/@AddressLine1");
		String sSoShipAddressLine1 = XPathUtil.getXpathAttribute(getOrderListOp, "//PersonInfoShipTo/@AddressLine1");
		String sSoAdditionalAddressLine1 = XPathUtil.getXpathAttribute(getOrderListOp, "//PersonInfo/@AddressLine1");
		//
		scheduleAndReleaseOrder(sOrderHeaderKey);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));

		//
		YFCDocument docChainedOrderOp =  createChainedOrder(sOrderHeaderKey);
		System.out.println("TestGPSCopyAttributesToChainedOrderAPI :: docChainedOrderOp\n"+docChainedOrderOp);
		YFCElement eleChainedOrderList = docChainedOrderOp.getElementsByTagName("ChainedOrderList").item(0);
		String sPOOrderName = eleChainedOrderList.getChildElement("Order").getAttribute("OrderName") ;
		String sPoPriorityCode = eleChainedOrderList.getChildElement("Order").getAttribute("PriorityCode") ;
		String sPoDivision = eleChainedOrderList.getChildElement("Order").getAttribute("Division") ;
		String sPoBillToID = eleChainedOrderList.getChildElement("Order").getAttribute("BillToID") ;
		String sPoShipToID = eleChainedOrderList.getChildElement("Order").getAttribute("ShipToID") ;
		String sPoBillAddressLine1 = XPathUtil.getXpathAttribute(docChainedOrderOp, "//PersonInfoBillTo/@AddressLine1");
		String sPoShipAddressLine1 = XPathUtil.getXpathAttribute(docChainedOrderOp, "//PersonInfoShipTo/@AddressLine1");
		String sPoAdditionalAddressLine1 = XPathUtil.getXpathAttribute(docChainedOrderOp, "//PersonInfo/@AddressLine1");

		System.out.println("Expected Order Name: "+sSoOrderName);
		System.out.println("Actual Order Name: "+sPOOrderName);
		Assert.assertEquals(sSoOrderName, sPOOrderName);
		
		System.out.println("Expected PriorityCode: "+sSoPriorityCode);
		System.out.println("Actual PriorityCode: "+sPoPriorityCode);
		Assert.assertEquals(sSoPriorityCode, sPoPriorityCode);
		
		System.out.println("Expected Division: "+sSoDivision);
		System.out.println("Actual Division: "+sPoDivision);
		Assert.assertEquals(sSoDivision, sPoDivision);
		
		System.out.println("Expected BillToID: "+sSoBillToID);
		System.out.println("Actual BillToID: "+sPoBillToID);
		Assert.assertEquals(sSoBillToID, sPoBillToID);
		
		System.out.println("Expected ShipToID: "+sSoShipToID);
		System.out.println("Actual ShipToID: "+sPoShipToID);
		Assert.assertEquals(sSoShipToID, sPoShipToID);
		
		System.out.println("Expected BillFirstName: "+sSoBillAddressLine1);
		System.out.println("Actual BillFirstName: "+sPoBillAddressLine1);
		Assert.assertEquals(sSoBillAddressLine1, sPoBillAddressLine1);
		
		System.out.println("Expected ShipFirstName: "+sSoShipAddressLine1);
		System.out.println("Actual ShipFirstName: "+sPoShipAddressLine1);
		Assert.assertEquals(sSoShipAddressLine1, sPoShipAddressLine1);
				
		System.out.println("Expected AdditionalFirstName: "+sSoAdditionalAddressLine1);
		System.out.println("Actual AdditionalFirstName: "+sPoAdditionalAddressLine1);
		Assert.assertEquals(sSoAdditionalAddressLine1, sPoAdditionalAddressLine1);

		
//		YFCElement eleChainedOrderOrder = 
	}
	
	
	private YFCDocument callGetOrderListApi(String sOrderHeaderKey) {
		YFCDocument docGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList>    <Order BillToID='' OrderHeaderKey='' OrderNo='' ShipToID=''  "
				+ "OrderName='' PriorityCode='' Division=''> <OrderLines TotalNumberOfRecords=''> <OrderLine OrderHeaderKey='' OrderLineKey='' ShipNode='' PrimeLineNo='' >"
				+ " </OrderLine>  </OrderLines> <PersonInfoBillTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' "
				+ "AlternateEmailID='' Beeper='' City='' Company='' Country='' DayFaxNo='' DayPhone='' Department='' EMailID='' ErrorTxt='' EveningFaxNo='' EveningPhone='' "
				+ "FirstName='' HttpUrl='' JobTitle='' LastName='' MiddleName='' MobilePhone=''  OtherPhone='' PersonID=''  PreferredShipAddress='' State='' Suffix='' "
				+ "Title='' VerificationStatus='' ZipCode='' /> <PersonInfoShipTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' "
				+ "AddressLine6='' AlternateEmailID='' Beeper='' City='' Company='' Country='' DayFaxNo='' DayPhone='' Department='' EMailID='' ErrorTxt='' EveningFaxNo=''"
				+ " EveningPhone='' FirstName='' HttpUrl='' JobTitle='' LastName='' MiddleName='' MobilePhone=''  OtherPhone='' PersonID=''  PreferredShipAddress='' State=''"
				+ " Suffix='' Title='' VerificationStatus='' ZipCode=''/> <AdditionalAddresses> <AdditionalAddress AddressType=''> <PersonInfo FirstName='' AddressLine1=''"
				+ " City='' ZipCode='' State='' DayPhone='' /> </AdditionalAddress> </AdditionalAddresses> </Order> </OrderList>");
		
		YFCDocument docGetOrderListrIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
		
		return invokeYantraApi("getOrderList", docGetOrderListrIp, docGetOrderListTemp);
	}


	private YFCDocument createChainedOrder(String sOrderHeaderKey) {

		YFCDocument docCreateChainedOrderTemp = YFCDocument.getDocumentFor(
				"<Order OrderNo='' OrderName='' OrderHeaderKey=''><ChainedOrderList>    <Order BillToID='' OrderHeaderKey='' OrderNo='' ShipToID=''  "
				+ "OrderName='' PriorityCode='' Division=''> <OrderLines TotalNumberOfRecords=''> <OrderLine OrderHeaderKey='' OrderLineKey='' ShipNode='' PrimeLineNo='' >"
				+ " </OrderLine>  </OrderLines> <PersonInfoBillTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' AddressLine6='' "
				+ "AlternateEmailID='' Beeper='' City='' Company='' Country='' DayFaxNo='' DayPhone='' Department='' EMailID='' ErrorTxt='' EveningFaxNo='' EveningPhone='' "
				+ "FirstName='' HttpUrl='' JobTitle='' LastName='' MiddleName='' MobilePhone=''  OtherPhone='' PersonID=''  PreferredShipAddress='' State='' Suffix='' "
				+ "Title='' VerificationStatus='' ZipCode='' /> <PersonInfoShipTo AddressLine1='' AddressLine2='' AddressLine3='' AddressLine4='' AddressLine5='' "
				+ "AddressLine6='' AlternateEmailID='' Beeper='' City='' Company='' Country='' DayFaxNo='' DayPhone='' Department='' EMailID='' ErrorTxt='' EveningFaxNo=''"
				+ " EveningPhone='' FirstName='' HttpUrl='' JobTitle='' LastName='' MiddleName='' MobilePhone=''  OtherPhone='' PersonID=''  PreferredShipAddress='' State=''"
				+ " Suffix='' Title='' VerificationStatus='' ZipCode=''/> <AdditionalAddresses> <AdditionalAddress AddressType=''> <PersonInfo FirstName='' AddressLine1=''"
				+ " City='' ZipCode='' State='' DayPhone='' /> </AdditionalAddress> </AdditionalAddresses> </Order> </ChainedOrderList> </Order>");
		
		YFCDocument docCreateChainedOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>");
		
		return invokeYantraApi("createChainedOrder", docCreateChainedOrderIp, docCreateChainedOrderTemp);
		
	}


	private YFCDocument createOrder() throws SAXException, IOException {

		String sCreateOrderIpXML = "<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS' "
				+ "DocumentType='' PriorityCode='1' InterfaceNo='INT_ODR_2' OrderName='KEINV89' BillToID='INV_MAT2' ShipToID='INV_MAT2' SearchCriteria2='+61 413 014 829' OrderType='MATERIAL_RESERVATION' SearchCriteria1='9090909090' Division='CXS053'> "
				+ "  <PersonInfoBillTo FirstName='' AddressLine1='Hosur road' AddressLine2='' AddressLine3='' AddressLine4='' City='Alpurrurulam' ZipCode='4830' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854' Country='AU' /> "
				+ "  <References> <Reference Name='DELIVERY_INSTRUCTIONS' Value='Handle with care' /> <Reference Name='PM_ORDER' Value='' /> <Reference Name='ACCOUNT_NUMBER' Value='00007777' /> "
				+ " <Reference Name='ACTIVITY_NUMBER' Value='00007771' /> </References> <OrderLines> <OrderLine ShipNode='Vendor20_N1' OrderedQty='10' PrimeLineNo='1' ChangeStatus='' ReceivingNode=''> "
				+ " <Item ItemID='000000000002900508' UnitOfMeasure='EACH' ItemShortDesc='WASHER,FLAT MS GALV 12MM' /> </OrderLine>  <OrderLine ShipNode='Vendor20_N1' OrderedQty='10' PrimeLineNo='2' ChangeStatus='' ReceivingNode=''> "
				+ " <Item ItemID='000000000006700205' UnitOfMeasure='EACH' ItemShortDesc='WASHER,FLAT MS GALV 12MM' />  </OrderLine>   </OrderLines>  <OrderDates> "
				+ "     <OrderDate DateTypeId='DATE_TO_WHAM' RequestedDate='20160718' />   </OrderDates> <AdditionalAddresses> <AdditionalAddress AddressType='SHIP_FROM'> <PersonInfo FirstName='Vendor Name' AddressLine1='Street Address' "
				+ " City='vendor city' ZipCode='vendor zip code' State='vendor state' DayPhone='vendor phone' /> </AdditionalAddress> </AdditionalAddresses>  </Order>";

		

		YFCDocument docCreateOrderIp = YFCDocument.parse(sCreateOrderIpXML);
		return invokeYantraService("GpsManageOrder", docCreateOrderIp);

	}
	
	private void scheduleAndReleaseOrder(String sOrderHeaderKey) throws SAXException, IOException {

		String sScheduleOrderIp = "<ScheduleOrder OrderHeaderKey='" + sOrderHeaderKey
				+ "' CheckInventory='N' ScheduleAndRelease='Y'/>";
		YFCDocument docScheduleOrderIp = YFCDocument.parse(sScheduleOrderIp);
		invokeYantraApi("scheduleOrder", docScheduleOrderIp);

	}
	
	private String maxOrderStatus(String sOrderHeaderKey) {
		// TODO Auto-generated method stub
		return XPathUtil.getXpathAttribute(invokeYantraApi("getOrderList", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>"),  YFCDocument.getDocumentFor("<OrderList><Order MaxOrderStatusDesc='' /></OrderList>")),"//@MaxOrderStatusDesc");
	}
}
