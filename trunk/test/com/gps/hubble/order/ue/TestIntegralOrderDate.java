package com.gps.hubble.order.ue;

import java.util.ArrayList;
import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCDateUtils;

public class TestIntegralOrderDate extends DateCalculationTestBase{
	
	@Test
	public void testUrgentBeforeCutOff() throws Exception{
		System.out.println("testUrgentBeforeCutOff test starting");
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(true, shipNode);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Urgent")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate expShipDate = new YDate(true);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testUrgentBeforeCutOffAndHazmat() throws Exception{
		    System.out.println("testUrgentBeforeCutOffAndHazmat begins");
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY001";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.URGENT, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			manageCommonCodeForCutOff(true, shipNode);
			int monTransitTime = 2;
			int tueTransitTime = 3;
			int wedTransitTime = 4;
			int thuTransitTime = 5;
			int friTransitTime = 6;
			YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Urgent")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(monTransitTime)
			                   .tuesdayTransitTime(tueTransitTime)
			                   .wednesdayTransitTime(wedTransitTime)
			                   .thursdayTransitTime(thuTransitTime)
			                   .fridayTransitTime(friTransitTime)
			                   .build();
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setShipNode(input,shipNode);
			YDate expShipDate = new YDate(true);
			System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			Assert.assertEquals(reqShipDate, expShipDate);
			YFCDocument calendarDoc = getCalendarDoc(fromState);
			YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, true);
			Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
			checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testUrgentAfterCutOff() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(false, shipNode);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Urgent")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		YDate expShipDate = new YDate(true);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YFCElement allDayShiftElement = getAllDayShiftElement(calendarDoc, expShipDate);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		expShipDate = getNextBusinessDay(expShipDate, allDayShiftElement, exceptionDates);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	//Holiday on deliveryState
	@Test
	public void testUrgentOrderDeliveryStateHoliday() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String deliveryState = "SA";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb,deliveryState);
		manageCommonCodeForCutOff(false, shipNode);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Urgent")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		YDate expShipDate = new YDate(true);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YFCElement allDayShiftElement = getAllDayShiftElement(calendarDoc, expShipDate);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		expShipDate = getNextBusinessDay(expShipDate, allDayShiftElement, exceptionDates);
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		List<YDate> deliveryStateHolidays = new ArrayList<>();
		deliveryStateHolidays.add(expDeliveryDate);
		changeCalendarAddExceptionDates(deliveryState, deliveryStateHolidays);
		expDeliveryDate = getNextBusinessDay(expDeliveryDate, allDayShiftElement, exceptionDates);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testUrgentOrderDeliveryStateConsecutiveHoliday() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String deliveryState = "SA";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb,deliveryState);
		manageCommonCodeForCutOff(false, shipNode);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Urgent")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		YDate expShipDate = new YDate(true);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		YFCElement allDayShiftElement = getAllDayShiftElement(calendarDoc, expShipDate);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		expShipDate = getNextBusinessDay(expShipDate, allDayShiftElement, exceptionDates);
		YDate firstDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, false);
		List<YDate> deliveryStateHolidays = new ArrayList<>();
		deliveryStateHolidays.add(firstDeliveryDate);
		YDate expDeliveryDate2 = new YDate(firstDeliveryDate.getTime(),true);
		expDeliveryDate2 = getNextBusinessDay(expDeliveryDate2, allDayShiftElement, exceptionDates);
		deliveryStateHolidays.add(expDeliveryDate2);
		changeCalendarAddExceptionDates(deliveryState, deliveryStateHolidays);
		YDate expDeliveryDate = getNextBusinessDay(expDeliveryDate2, allDayShiftElement, exceptionDates);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	
	
	
	
	@Test
	public void testUrgentAfterCutOffAndHazmat() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY001";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.URGENT, true,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		manageCommonCodeForCutOff(false, shipNode);
		int monTransitTime = 2;
		int tueTransitTime = 3;
		int wedTransitTime = 4;
		int thuTransitTime = 5;
		int friTransitTime = 6;
		YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Urgent")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(monTransitTime)
		                   .tuesdayTransitTime(tueTransitTime)
		                   .wednesdayTransitTime(wedTransitTime)
		                   .thursdayTransitTime(thuTransitTime)
		                   .fridayTransitTime(friTransitTime)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		YDate expShipDate = new YDate(true);
		YFCDocument calendarDoc = getCalendarDoc(fromState);
		List<String> exceptionDates = getExceptionDates(calendarDoc);
		YFCElement allDayShift = getAllDayShiftElement(calendarDoc, expShipDate);
		expShipDate = getNextBusinessDay(expShipDate, allDayShift, exceptionDates);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, expShipDate);
		
		YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, true);
		Assert.assertEquals(reqDeliveryDate,expDeliveryDate);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testDacAndMilkRunOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		setOrderLineDates(input, new YDate("22/07/2016", "dd/MM/yyyy", true), new YDate("26/07/2016", "dd/MM/yyyy", true));
		YFCDocument template = getOrderTemplate();
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.THIS_WEEK,"ABCDE");
		System.out.println("Creating DAC record");
		createDACRecord("DC13", "DUMMY101", schedule, "DUMMYROUTE101", "DUMMYDEST101");
		YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
		System.out.println("creating milk run record");
		createMilkRunRecord("DUMMYROUTE101", "DUMMY_MILK_RUN", "C","WEEKLY" , effectiveFrom, null);
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY101");
		setShipNode(input, "DC13");
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("27/07/2016","dd/MM/yyyy",true));
		checkDateAttributeOnLine(output,new YDate("22/07/2016", "dd/MM/yyyy", true), new YDate("26/07/2016", "dd/MM/yyyy", true));
	}
	
	
	@Test
	public void testDacAndMilkRunAndDeliveryStateHoliday() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String deliveryState = "SA";
		List<YDate> deliveryStateHolidays = new ArrayList<>();
		deliveryStateHolidays.add(new YDate("27/07/2016","dd/MM/yyyy",true));
		changeCalendarAddExceptionDates(deliveryState, deliveryStateHolidays);
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.THIS_WEEK,"ABCDE");
		System.out.println("Creating DAC record");
		createDACRecord("DC13", "DUMMY101", schedule, "DUMMYROUTE101", "DUMMYDEST101");
		YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
		System.out.println("creating milk run record");
		createMilkRunRecord("DUMMYROUTE101", "DUMMY_MILK_RUN", "C","WEEKLY" , effectiveFrom, null);
		setPersonInfoShipTo(input.getDocumentElement(), "", "", deliveryState);
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY101");
		setShipNode(input, "DC13");
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testDacAndMilkRunOrderCase2() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.THIS_WEEK,"ABCDE");
		System.out.println("Creating DAC record");
		createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
		YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
		System.out.println("creating milk run record");
		createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Fortnightly" , effectiveFrom, null);
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
		setShipNode(input, "DC13");
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testDacAndMilkRunOrderCase3() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
			YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
			System.out.println("creating milk run record");
			createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Fortnightly" , effectiveFrom, null);
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
			setShipNode(input, "DC13");
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("11/08/2016","dd/MM/yyyy",true));
			checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testDacAndMilkRunOrderCase4() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
		System.out.println("Creating DAC record and schedule is "+schedule);
		createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
		YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
		System.out.println("creating milk run record");
		createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Weekly" , effectiveFrom, null);
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
		setShipNode(input, "DC13");
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("04/08/2016","dd/MM/yyyy",true));
		String routeId = getRouteId(output);
		Assert.assertEquals("DUMMYROUTE102", routeId);
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testDacAndTransportMatrixCase1() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), "9999", "DummyTown");
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY103");
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
		System.out.println("Creating DAC record and schedule is "+schedule);
		createDACRecord("DC13", "DUMMY103", schedule, "DUMMYROUTE103", "DUMMYDEST103");
		//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Standard")
		                   .fromState("VIC")
		                   .toPostCode("9999")
		                   .suburb("DummyTown")
		                   .mondayTransitTime(3)
		                   .tuesdayTransitTime(3)
		                   .wednesdayTransitTime(3)
		                   .thursdayTransitTime(3)
		                   .fridayTransitTime(3)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY103");
		setShipNode(input, "DC13");
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("02/08/2016","dd/MM/yyyy",true));
		checkDateAttributeOnLine(output);
	}
	
	
	@Test
	public void testDacAndTransportMatrixCase2() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY104";
		String routeId = "DUMMYROUTE104";
		String destId = "DUMMYDEST104";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, true,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
		System.out.println("Creating DAC record and schedule is "+schedule);
		createDACRecord(shipNode, shipToId, schedule, routeId, destId);
		//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Standard")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(3)
		                   .tuesdayTransitTime(3)
		                   .wednesdayTransitTime(3)
		                   .thursdayTransitTime(3)
		                   .fridayTransitTime(3)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, new YDate("27/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("02/08/2016","dd/MM/yyyy",true));
		checkDateAttributeOnLine(output);
	}
	
	@Test
	public void testDacAndTransportMatrixCase3() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY104";
		String routeId = "DUMMYROUTE104";
		String destId = "DUMMYDEST104";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, true,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
		System.out.println("Creating DAC record and schedule is "+schedule);
		createDACRecord(shipNode, shipToId, schedule, routeId, destId);
		//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Standard")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(3)
		                   .tuesdayTransitTime(3)
		                   .wednesdayTransitTime(4)
		                   .thursdayTransitTime(4)
		                   .fridayTransitTime(3)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, new YDate("27/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("03/08/2016","dd/MM/yyyy",true));
		checkDateAttributeOnLine(output);
	}
	
	//StandardHomeDelivery
	@Test
	public void testStandardNonDacOrderAndHazmat() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY104";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, true,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Standard")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(3)
		                   .tuesdayTransitTime(3)
		                   .wednesdayTransitTime(4)
		                   .thursdayTransitTime(5)
		                   .fridayTransitTime(2)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(new YDate("29/07/2016","dd/MM/yyyy",true),reqDeliveryDate);
	}
	
	
	@Test
	public void testStandardNonDacOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY104";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Standard")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(3)
		                   .tuesdayTransitTime(3)
		                   .wednesdayTransitTime(4)
		                   .thursdayTransitTime(5)
		                   .fridayTransitTime(2)
		                   .build();
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(new YDate("28/07/2016","dd/MM/yyyy",true),reqDeliveryDate);
	}
	
	//Home Delivery test cases
	
	@Test
	public void testStandardHomeDelivery() throws Exception{
		YFCDocument input = getOrderDocument();
		input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
		YFCDocument template = getOrderTemplate();
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
		YFCDocument weekNumbers = getWeekNumbers();
		int weekNumber = getWeekNumber(weekNumbers, dateToWham);
		String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
		System.out.println("Creating DAC record and schedule is "+schedule);
		createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
		YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
		System.out.println("creating milk run record");
		createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Weekly" , effectiveFrom, null);
		setShipNode(input, "DC13");
		System.out.println("Input is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(reqDeliveryDate, new YDate("04/08/2016","dd/MM/yyyy",true));
	}
	
	@Test
	public void testStandardHomeDeliveryNonDacOrder() throws Exception{
		YFCDocument input = getOrderDocument();
		YFCDocument template = getOrderTemplate();
		String shipNode = "DC13";
		String shipToId = "DUMMY104";
		String fromState = "VIC";
		String suburb = "DummyTown2";
		String toPostCode = "9998";
		input.getDocumentElement().setAttribute("ShipToID", shipToId);
		setShipNode(input,shipNode);
		YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
		setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
		setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
		MatrixRecordBuilder.getInstance(this)
		                   .materialClass("Materials")
		                   .priority("Home Delivery")
		                   .fromState(fromState)
		                   .toPostCode(toPostCode)
		                   .suburb(suburb)
		                   .mondayTransitTime(3)
		                   .tuesdayTransitTime(3)
		                   .wednesdayTransitTime(4)
		                   .thursdayTransitTime(5)
		                   .fridayTransitTime(2)
		                   .build();
		System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
		YFCDocument output =  invokeYantraApi("createOrder", input,template);
		System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
		YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
		System.out.println("required shipdate is "+reqShipDate.getString());
		YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
		System.out.println("required delivery date is "+reqDeliveryDate.getString());
		Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
		Assert.assertEquals(new YDate("28/07/2016","dd/MM/yyyy",true),reqDeliveryDate);
	}
	
	@Test
	public void testStandardHomeDeliveryDacAndMilkRunOrder() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY101");
			setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.THIS_WEEK,"ABCDE");
			System.out.println("Creating DAC record");
			createDACRecord("DC13", "DUMMY101", schedule, "DUMMYROUTE101", "DUMMYDEST101");
			YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
			System.out.println("creating milk run record");
			createMilkRunRecord("DUMMYROUTE101", "DUMMY_MILK_RUN", "C","WEEKLY" , effectiveFrom, null);
			setShipNode(input, "DC13");
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("27/07/2016","dd/MM/yyyy",true));
		}
		
		
		@Test
		public void testStandardHomeDeliveryDacAndMilkRunOrderCase2() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
			setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.THIS_WEEK,"ABCDE");
			System.out.println("Creating DAC record");
			createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
			YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
			System.out.println("creating milk run record");
			createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Fortnightly" , effectiveFrom, null);
			setShipNode(input, "DC13");
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		}
		
		
		@Test
		public void testStandardHomeDeliveryDacAndMilkRunOrderCase3() throws Exception{
				YFCDocument input = getOrderDocument();
				YFCDocument template = getOrderTemplate();
				YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
				input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
				setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
				YFCDocument weekNumbers = getWeekNumbers();
				int weekNumber = getWeekNumber(weekNumbers, dateToWham);
				String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
				System.out.println("Creating DAC record and schedule is "+schedule);
				createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
				YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
				System.out.println("creating milk run record");
				createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Fortnightly" , effectiveFrom, null);
				setShipNode(input, "DC13");
				System.out.println("Input is ==========\n"+input);
				YFCDocument output =  invokeYantraApi("createOrder", input,template);
				System.out.println("Output is =========\n"+output);
				YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
				YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
				Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
				Assert.assertEquals(reqDeliveryDate, new YDate("11/08/2016","dd/MM/yyyy",true));
		}
		
		@Test
		public void testStandardHomeDeliveryDacAndMilkRunOrderCase4() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY102");
			setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord("DC13", "DUMMY102", schedule, "DUMMYROUTE102", "DUMMYDEST101");
			YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
			System.out.println("creating milk run record");
			createMilkRunRecord("DUMMYROUTE102", "DUMMY_MILK_RUN 2", "D","Weekly" , effectiveFrom, null);
			setShipNode(input, "DC13");
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("04/08/2016","dd/MM/yyyy",true));
		}
		
		@Test
		public void testStandardHomeDeliveryDacAndTransportMatrixCase1() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY103");
			setOrderAttributes(input, Priority.STANDARN_HOME, false,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), "9999", "DummyTown");
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY103");
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"D");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord("DC13", "DUMMY103", schedule, "DUMMYROUTE103", "DUMMYDEST103");
			//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Home Delivery")
			                   .fromState("VIC")
			                   .toPostCode("9999")
			                   .suburb("DummyTown")
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(3)
			                   .thursdayTransitTime(3)
			                   .fridayTransitTime(3)
			                   .build();
			setShipNode(input, "DC13");
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			Assert.assertEquals(reqShipDate, new YDate("28/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("02/08/2016","dd/MM/yyyy",true));
		}
		
		
		@Test
		public void testStandardHomeDeliveryDacAndTransportMatrixCase2() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY104";
			String routeId = "DUMMYROUTE104";
			String destId = "DUMMYDEST104";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setOrderAttributes(input, Priority.STANDARN_HOME, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord(shipNode, shipToId, schedule, routeId, destId);
			//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Home Delivery")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(3)
			                   .thursdayTransitTime(3)
			                   .fridayTransitTime(3)
			                   .build();
			setShipNode(input,shipNode);
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			Assert.assertEquals(reqShipDate, new YDate("27/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("02/08/2016","dd/MM/yyyy",true));
		}
		
		@Test
		public void testStandardHomeDeliveryDacAndTransportMatrixCase3() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY104";
			String routeId = "DUMMYROUTE104";
			String destId = "DUMMYDEST104";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.STANDARN_HOME, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord(shipNode, shipToId, schedule, routeId, destId);
			//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Home Delivery")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(4)
			                   .thursdayTransitTime(4)
			                   .fridayTransitTime(3)
			                   .build();
			setShipNode(input,shipNode);
			System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			Assert.assertEquals(reqShipDate, new YDate("27/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("03/08/2016","dd/MM/yyyy",true));
		}
		
		//StandardHomeDelivery
		@Test
		public void testStandardHomeDeliveryNonDacOrderAndHazmat() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY104";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setOrderAttributes(input, Priority.STANDARN_HOME, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Home Delivery")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(4)
			                   .thursdayTransitTime(5)
			                   .fridayTransitTime(2)
			                   .build();
			setShipNode(input,shipNode);
			System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(new YDate("29/07/2016","dd/MM/yyyy",true),reqDeliveryDate);
		}
		
		
		@Test
		public void testStandardHomeDeliveryNonDacOrderAndHazmatDefaultDateToWham() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY104";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			YDate dateToWham = null;
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setOrderAttributes(input, Priority.STANDARN_HOME, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			YFCDocument transportMatrix = MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Home Delivery")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(4)
			                   .thursdayTransitTime(5)
			                   .fridayTransitTime(2)
			                   .build();
			setShipNode(input,shipNode);
			System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			YDate expShipDate = new YDate(true);
			
			YFCDocument calendarDoc = getCalendarDoc(fromState);
			YFCElement allDayShiftElem = getAllDayShiftElement(calendarDoc, expShipDate);
			List<String> exceptionDates = getExceptionDates(calendarDoc);
			expShipDate = getNextBusinessDay(expShipDate, allDayShiftElem, exceptionDates);
			YDate expDeliveryDate = calculateDeliveryDate(calendarDoc, transportMatrix, expShipDate, true);
			Assert.assertEquals(reqShipDate, expShipDate);
			Assert.assertEquals(expDeliveryDate,reqDeliveryDate);
		}
		
		//Remove calendar for 
		@Test
		public void testDacAndTransportMatrixAndDefaultCalendar() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC14";
			String shipToId = "DUMMY104";
			String routeId = "DUMMYROUTE104";
			String destId = "DUMMYDEST104";
			String fromState = "QLD";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			removeCalendarFor(fromState, TelstraConstants.ORG_TELSTRA_SCLSA);
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.STANDARD, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord(shipNode, shipToId, schedule, routeId, destId);
			//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Standard")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(4)
			                   .thursdayTransitTime(4)
			                   .fridayTransitTime(3)
			                   .build();
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setShipNode(input,shipNode);
			System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output testDatesForDacAndTransportMatrixCase3 is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			System.out.println("required shipdate is "+reqShipDate.getString());
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			System.out.println("required delivery date is "+reqDeliveryDate.getString());
			Assert.assertEquals(reqShipDate, new YDate("27/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("03/08/2016","dd/MM/yyyy",true));
		}
		
		
		//Negative test case scenario, if no calendar found then do not throw exception
		@Test
		public void testDacAndTransportMatrixAndNoCalendar() throws Exception{
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY104";
			String routeId = "DUMMYROUTE104";
			String destId = "DUMMYDEST104";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			removeCalendarFor(fromState, TelstraConstants.ORG_TELSTRA_SCLSA);
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.STANDARD, true,dateToWham);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord(shipNode, shipToId, schedule, routeId, destId);
			//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Standard")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(4)
			                   .thursdayTransitTime(4)
			                   .fridayTransitTime(3)
			                   .build();
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setShipNode(input,shipNode);
			System.out.println("Input testDatesForDacAndTransportMatrixCase3 is ==========\n"+input);
			invokeYantraApi("createOrder", input,template);
			//Date will not be calculate..if no exception means test passed
		}
		
		@Test
		public void testTranseferOrder() throws Exception{
			System.out.println("============testTranseferOrder execution starts=============");
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			String shipNode = "DC13";
			String shipToId = "DUMMY104";
			String routeId = "DUMMYROUTE104";
			String destId = "DUMMYDEST104";
			String fromState = "VIC";
			String suburb = "DummyTown2";
			String toPostCode = "9998";
			String receivingNode = "DC16";
			String deliveryState = "SA";
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.STANDARD, true,dateToWham);
			input.getDocumentElement().setAttribute("DocumentType", "0003");
			setReceivingNode(input, receivingNode);
			setPersonInfoShipTo(input.getDocumentElement(), toPostCode, suburb,"");
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.NEXT_WEEK,"C");
			System.out.println("Creating DAC record and schedule is "+schedule);
			createDACRecord(shipNode, shipToId, schedule, routeId, destId);
			//createTransportMatrixRecord("Materials", "Standard", "CEVA NETWORK", "VIC", 10000, "DummyArea", 4, 4, 4, 4, 4, null, null);
			MatrixRecordBuilder.getInstance(this)
			                   .materialClass("Materials")
			                   .priority("Standard")
			                   .fromState(fromState)
			                   .toPostCode(toPostCode)
			                   .suburb(suburb)
			                   .mondayTransitTime(3)
			                   .tuesdayTransitTime(3)
			                   .wednesdayTransitTime(4)
			                   .thursdayTransitTime(4)
			                   .fridayTransitTime(3)
			                   .build();
			input.getDocumentElement().setAttribute("ShipToID", shipToId);
			setShipNode(input,shipNode);
			List<YDate> holidays = new ArrayList<>();
			holidays.add(new YDate("3/08/2016","dd/MM/yyyy",true));
			changeCalendarAddExceptionDates(deliveryState, holidays);
			YFCDocument output = invokeYantraApi("createOrder", input,template);
			System.out.println("output is "+output);
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			Assert.assertEquals(new YDate("4/08/2016","dd/MM/yyyy",true), reqDeliveryDate);
		}
		
		@Test
		public void testDacAndMilkRunOrderAndTranseferOrder() throws Exception{
			String receivingNode = "DC16";
			String deliveryState = "SA";
			YFCDocument input = getOrderDocument();
			YFCDocument template = getOrderTemplate();
			input.getDocumentElement().setAttribute("DocumentType", "0003");
			setReceivingNode(input, receivingNode);
			YDate dateToWham = new YDate("20/07/2016", "dd/MM/yyyy", true);
			setOrderAttributes(input, Priority.STANDARD, false,dateToWham);
			YFCDocument weekNumbers = getWeekNumbers();
			int weekNumber = getWeekNumber(weekNumbers, dateToWham);
			String schedule = createScheduleString(weekNumber, ShipOn.THIS_WEEK,"ABCDE");
			System.out.println("Creating DAC record");
			createDACRecord("DC13", "DUMMY101", schedule, "DUMMYROUTE101", "DUMMYDEST101");
			YDate effectiveFrom = new YDate("11/07/2016","dd/MM/yyyy",true);
			System.out.println("creating milk run record");
			createMilkRunRecord("DUMMYROUTE101", "DUMMY_MILK_RUN", "C","WEEKLY" , effectiveFrom, null);
			input.getDocumentElement().setAttribute("ShipToID", "DUMMY101");
			setPersonInfoShipTo(input.getDocumentElement(), "5001", "ADELAIDE","");
			setShipNode(input, "DC13");
			List<YDate> holidays = new ArrayList<>();
			holidays.add(new YDate("27/07/2016", "dd/MM/yyyy", true));
			changeCalendarAddExceptionDates(deliveryState, holidays);
			System.out.println("Input is ==========\n"+input);
			YFCDocument output =  invokeYantraApi("createOrder", input,template);
			System.out.println("Output is =========\n"+output);
			YDate reqShipDate = output.getDocumentElement().getYDateAttribute("ReqShipDate");
			YDate reqDeliveryDate = output.getDocumentElement().getYDateAttribute("ReqDeliveryDate");
			Assert.assertEquals(reqShipDate, new YDate("21/07/2016","dd/MM/yyyy",true));
			Assert.assertEquals(reqDeliveryDate, new YDate("28/07/2016","dd/MM/yyyy",true));
		}
		
		
		
		
}
