package com.gps.hubble.logistics.api;


import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.TestBase;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class TestCreateAdhocLogisticsOrder extends TestBase {

	@Test
	public void TestCreateAdhocLogisticsOrder1(){

		try {
			/*

	Scenario 1 Single ContainerElement with conatinerNo=4
	1.   create item ADHOC_LOGISTICS
	2.   create Order(remove container)(Node is stamped)(Priority Urgent)
	3.   Schedule Release
	4.   CreateConfirmShipment(add container)
	5.   ACCEPT response with shipment Number
	6.   Sending incorrect OrderNo


	Outcome
	1. 	LogisticsStatus=Pending
	2.	Shipment Contains Container=2
	3.  Node as passed
	4.  Service Urgent
	5.  LogisticsStatus=ACCEPT
	6.  Shipment Status=1400
	7.  checking ErrorCode=TEL_ERR_0931_001

			 */

			//createItem();
			//get Default Order XML

			YFCDocument input = getOrderXmlWithOneContainerLine();
			YFCDocument serviceoutput=invokeYantraService("GpsCreateAdhocLogisticsOrder", input);
			String sShipmentNo = serviceoutput.getElementsByTagName("Shipment").item(0).getAttribute(TelstraConstants.SHIPMENT_NO); 

			//get details of newly created Order
			YFCDocument getShipmentListInput = YFCDocument.getDocumentFor("<Shipment ShipmentNo='"+sShipmentNo+"'/>");
			YFCDocument getShipmentListTemp = YFCDocument.getDocumentFor("<Shipments><Shipment Status='' CarrierServiceCode='' ShipNode=''><Extn LogisticsStatus=''/><Containers TotalNumberOfRecords=''/></Shipment></Shipments>");
			YFCDocument output = invokeYantraApi("getShipmentList", getShipmentListInput,getShipmentListTemp);
			YFCElement eleOutputShipment = output.getElementsByTagName("Shipment").item(0); 
			YFCElement eleOutputShipmentExtn = eleOutputShipment.getElementsByTagName("Extn").item(0); 


			Assert.assertEquals(eleOutputShipment.getAttribute("ShipNode"),"TestOrg9");
			Assert.assertEquals(eleOutputShipment.getAttribute("Status"),"1400");
			Assert.assertEquals(eleOutputShipmentExtn.getAttribute("LogisticsStatus"),"PENDING");
			Assert.assertEquals(eleOutputShipment.getAttribute("CarrierServiceCode"),"URGENT_Carrier");
			Assert.assertEquals(eleOutputShipment.getElementsByTagName("Containers").item(0).getAttribute("TotalNumberOfRecords"),"2");

			YFCDocument inputresponse = YFCDocument.getDocumentFor("<Shipment LogisticsStatus='ACC' ShipmentNo='"+sShipmentNo+"'/>");
			invokeYantraService("GpsManageLogisticsResponse", inputresponse);
			YFCDocument getShipmentListresponseInput = YFCDocument.getDocumentFor("<Shipment ShipmentNo='"+sShipmentNo+"'/>");
			YFCDocument getShipmentListresponseTemp = YFCDocument.getDocumentFor("<Shipments><Shipment Status='' CarrierServiceCode='' ShipNode=''><Extn LogisticsStatus=''/><Containers TotalNumberOfRecords=''/></Shipment></Shipments>");
			YFCDocument outputresponse = invokeYantraApi("getShipmentList", getShipmentListresponseInput,getShipmentListresponseTemp);
			YFCElement eleOutputShipmentresponse = outputresponse.getElementsByTagName("Shipment").item(0); 
			YFCElement eleOutputShipmentExtnresponse = eleOutputShipmentresponse.getElementsByTagName("Extn").item(0); 

			Assert.assertEquals(eleOutputShipmentExtnresponse.getAttribute("LogisticsStatus"),"ACC");

		}

		catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
	}

	@Test
	public void TestCreateAdhocLogisticsOrder2(){

		try {
			/*

Scenario 4 Multiple ContainerElement with conatinerNo=2
1.   create item ADHOC_LOGISTICS
2.   create Order(remove container)(Node is not stamped)(Priority Standard)
3.   Schedule Release
4.   CreateConfirmShipment(add container)
5.   ACCEPT response with shipment Number

Outcome
1. 	LogisticsStatus=Pending
2.	Shipment Contains Container
3.  Node is AL_N1
4.  Carrier STANDARD_Carrier
5.  LogisticsStatus=ACCEPT
6.  Shipment Status=1400

			 */
			//createItem();
			//get Default Order XML
			YFCDocument input = getOrderXmlWithMultipleContainerLine();
			//setting Test Case Related Data
			YFCDocument serviceoutput=invokeYantraService("GpsCreateAdhocLogisticsOrder", input);
			String sShipmentNo = serviceoutput.getElementsByTagName("Shipment").item(0).getAttribute(TelstraConstants.SHIPMENT_NO); 

			//get details of newly created Order
			YFCDocument getShipmentListInput = YFCDocument.getDocumentFor("<Shipment ShipmentNo='"+sShipmentNo+"'/>");
			YFCDocument getShipmentListTemp = YFCDocument.getDocumentFor("<Shipments><Shipment Status='' CarrierServiceCode='' ShipNode=''><Extn LogisticsStatus=''/><ShipmentLines><ShipmentLine OrderNo=''/></ShipmentLines><Containers TotalNumberOfRecords=''/></Shipment></Shipments>");
			YFCDocument output = invokeYantraApi("getShipmentList", getShipmentListInput,getShipmentListTemp);
			YFCElement eleOutputShipment = output.getElementsByTagName("Shipment").item(0); 
			String sOrderNo = output.getElementsByTagName("ShipmentLines").item(0).getElementsByTagName("ShipmentLine").item(0).getAttribute("OrderNo"); 
			YFCElement eleOutputShipmentExtn = eleOutputShipment.getElementsByTagName("Extn").item(0); 

			Assert.assertEquals(eleOutputShipment.getAttribute("ShipNode"),"AL_N1");
			Assert.assertEquals(eleOutputShipment.getAttribute("Status"),"1400");
			Assert.assertEquals(eleOutputShipmentExtn.getAttribute("LogisticsStatus"),"PENDING");
			Assert.assertEquals(eleOutputShipment.getAttribute("CarrierServiceCode"),"STANDARD_Carrier");
			Assert.assertEquals(eleOutputShipment.getElementsByTagName("Containers").item(0).getAttribute("TotalNumberOfRecords"),"8");


			YFCDocument inputresponse = YFCDocument.getDocumentFor("<Shipment LogisticsStatus='REJ' OrderNo='"+sOrderNo+"'/>");
			invokeYantraService("GpsManageLogisticsResponse", inputresponse);
			YFCDocument getShipmentListresponseInput = YFCDocument.getDocumentFor("<Shipment ShipmentNo='"+sShipmentNo+"'/>");
			YFCDocument getShipmentListresponseTemp = YFCDocument.getDocumentFor("<Shipments><Shipment Status='' CarrierServiceCode='' ShipNode=''><Extn LogisticsStatus=''/><Containers TotalNumberOfRecords=''/></Shipment></Shipments>");
			YFCDocument outputresponse = invokeYantraApi("getShipmentList", getShipmentListresponseInput,getShipmentListresponseTemp);
			YFCElement eleOutputShipmentresponse = outputresponse.getElementsByTagName("Shipment").item(0); 
			YFCElement eleOutputShipmentExtnresponse = eleOutputShipmentresponse.getElementsByTagName("Extn").item(0); 
			
			Assert.assertEquals(eleOutputShipmentExtnresponse.getAttribute("LogisticsStatus"),"REJ");

		}

		catch (Exception e) {
			e.printStackTrace();
			Assert.fail(e.getMessage());
		}
		try{
			YFCDocument inputresponseexception = YFCDocument.getDocumentFor("<Shipment LogisticsStatus='REJ' OrderNo='123'/>");
			invokeYantraService("GpsManageLogisticsResponse", inputresponseexception);
		}
		catch(Exception e){
			if(e.toString().contains("TEL_ERR_0931_001")){
				System.out.println("inside error");
				Assert.assertEquals("Pass", "Pass");
			}else{
				Assert.assertEquals("Pass", "Fail");
			}
		} 
	}

	protected YFCDocument createItem() throws Exception {
		String orderXmlString =
				"<ItemList><Item Action='Manage' ItemID='ADHOC_LOGISTICS' OrganizationCode='TELSTRA_SCLSA' UnitOfMeasure='EA' >"
						+ "<PrimaryInformation Description='Item For Adhoc Logistics Order' ShortDescription='Item For Adhoc Logistics Order' Status='3000' AssumeInfiniteInventory='Y'/>"
						+ "</Item></ItemList>";
		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}

	protected YFCDocument getOrderXmlWithOneContainerLine() throws Exception {
		String orderXmlString = 
				"<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' PriorityCode='URGENT' OrderType='ADHOC_LOGISTICS' OrderNo='' EntryType='STERLING' DocumentType='0001' >"
						+ "<References><Reference Name='32455' Value=''/></References>"
						+ "<PersonInfoBillTo FirstName='Keerthi' DayPhone='32321' EMailID='k@' AddressLine1='1' AddressLine2='1' AddressLine3='1' AddressLine4='1' AddressLine5='1' City='' Country='AU' State='' ZipCode=''/>"
						+ "<AdditionalAddresses>"
						+ "<AdditionalAddress AddressType='SHIP_FROM'>"
						+ "<PersonInfo FirstName='Yadav' DayPhone='2786' EMailID='z' AddressLine1='2' AddressLine2='2' AddressLine3='2' AddressLine4='2' AddressLine5='' City='' Country='AU' State='' ZipCode=''/>"
						+ "	</AdditionalAddress>"
						+ "</AdditionalAddresses>"
						+ "<OrderLines>"
						+ "	<OrderLine ShipNode='TestOrg9' >"
						+ "	</OrderLine>"
						+ "</OrderLines>"
						+ "<Containers>"
						+ "	<Container ContainerType='X' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>"
						+ "</Containers>"
						+ "</Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}

	protected YFCDocument getOrderXmlWithMultipleContainerLine() throws Exception {
		String orderXmlString = 
				"<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' PriorityCode='STANDARD' OrderType='ADHOC_LOGISTICS' OrderNo='' EntryType='STERLING' DocumentType='0001' >"
						+ "<References><Reference Name='324255' Value=''/></References>"
						+ "<PersonInfoBillTo FirstName='Keerthi' DayPhone='323221' EMailID='k@' AddressLine1='1' AddressLine2='1' AddressLine3='1' AddressLine4='1' AddressLine5='1' City='' Country='AU' State='' ZipCode=''/>"
						+ "<AdditionalAddresses>"
						+ "<AdditionalAddress AddressType='SHIP_FROM'>"
						+ "<PersonInfo FirstName='Yadav' DayPhone='2786' EMailID='z' AddressLine1='2' AddressLine2='2' AddressLine3='2' AddressLine4='2' AddressLine5='' City='' Country='AU' State='' ZipCode=''/>"
						+ "	</AdditionalAddress>"
						+ "</AdditionalAddresses>"
						+ "<OrderLines>"
						+ "	<OrderLine ShipNode='' >"
						+ "	</OrderLine>"
						+ "</OrderLines>"
						+ "<Containers>"
						+ "	<Container ContainerType='A' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>"
						+ "	<Container ContainerType='B' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>"
						+ "	<Container ContainerType='C' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>"
						+ "	<Container ContainerType='D' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>"
						+ "</Containers>"
						+ "</Order>";

		YFCDocument orderDoc = YFCDocument.parse(orderXmlString);
		return orderDoc;
	}
}