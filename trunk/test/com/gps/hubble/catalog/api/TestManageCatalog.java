package com.gps.hubble.catalog.api;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class TestManageCatalog extends TestBase {

	@Test
	public void testManageItemAPI() throws SAXException, IOException {

		YFCDocument input = getManageItemInputDocument();
		invokeYantraService("GpsManageCatalog", input);

		YFCDocument getItemListIp = getGetItemListInputDocument(input);
		YFCDocument getItemListOp = invokeYantraApi("getItemList", getItemListIp);

		YFCElement eleItem = getItemListOp.getElementsByTagName("Item").item(0);
		String sItemID = eleItem.getAttribute("ItemID");
		System.out.println("Expected ItemID" + sItemID);
		System.out.println("Actual ItemID" + input.getElementsByTagName("Item").item(0).getAttribute("ItemID"));
		Assert.assertEquals(sItemID, input.getElementsByTagName("Item").item(0).getAttribute("ItemID"));

	}

	private YFCDocument getGetItemListInputDocument(YFCDocument input) throws SAXException, IOException {
		String sGetItemList = "<Item Action='' ItemID='"
				+ input.getElementsByTagName("Item").item(0).getAttribute("ItemID") + "' />";

		return YFCDocument.parse(sGetItemList);
	}

	public YFCDocument getManageItemInputDocument() throws SAXException, IOException {
		String manageItem = "<ItemList><Item Action='' ItemID='TestManageCatalog_junit' OrganizationCode='TELSTRA_SCLSA' UnitOfMeasure='BAG'> "
				+ "<PrimaryInformation Description='BLOCK,S5000 128PR PROT RH' IsHazmat='' ItemType='NTRD' ManufacturerItem='SIEMENS S30264' ManufacturerName=''"
				+ " ProductLine='29'  ShortDescription='BLOCK,S5000 128PR PROT RH' Status='3000' UnitCost='0.65' />  </Item> </ItemList>";
		return YFCDocument.parse(manageItem);
	}

	
	@Test
	public void testItemNodeDefn() throws SAXException, IOException {

		YFCDocument manageItemIp = getManageItemInputDocument();
		invokeYantraService("GpsManageCatalog", manageItemIp);

		
		YFCDocument createItemNodeDefnIp = getCreateItemNodeDefnDoc();
		invokeYantraService("GpsManageCatalog", createItemNodeDefnIp);

		YFCDocument getItemNodeDefnList = getItemNodeDefnListIp(createItemNodeDefnIp);
		Assert.assertEquals(createItemNodeDefnIp.getElementsByTagName("ItemNodeDefn").item(0).getAttribute("ItemID") ,  getItemNodeDefnList.getElementsByTagName("ItemNodeDefn").item(0).getAttribute("ItemID") );
		Assert.assertEquals(createItemNodeDefnIp.getElementsByTagName("ItemNodeDefn").item(0).getAttribute("Node") ,  getItemNodeDefnList.getElementsByTagName("ItemNodeDefn").item(0).getAttribute("Node") );
	}

	private YFCDocument getItemNodeDefnListIp(YFCDocument input) throws SAXException, IOException {
		String getItemNodeDefn = "<ItemNodeDefn ItemID='"
				+ input.getElementsByTagName("ItemNodeDefn").item(0).getAttribute("ItemID") + "' Node='"
				+ input.getElementsByTagName("ItemNodeDefn").item(0).getAttribute("Node") + "'/>";
		return YFCDocument.parse(getItemNodeDefn);
	}

	private YFCDocument getCreateItemNodeDefnDoc() throws SAXException, IOException {

		String itemNodeDefn = "<ItemNodeDefn ItemID='TestManageCatalog_junit' UnitOfMeasure='BAG' Node='TestOrg3' OrganizationCode='TELSTRA_SCLSA' > "
				+ "<Extn MaterialStatus='A3' MrpType='VB' MrpProcuedure='' GrProcTime='0' DelPeriod='21' /> "
				+ "</ItemNodeDefn>";
		return YFCDocument.parse(itemNodeDefn);
	}

	
}
