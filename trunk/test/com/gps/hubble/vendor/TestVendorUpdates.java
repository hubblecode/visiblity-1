package com.gps.hubble.vendor;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.junit.Assert;
import org.junit.Test;

import com.gps.hubble.TestBase;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCDateUtils;

public class TestVendorUpdates extends TestBase{
	
	private static final String COMMITTED = "Committed";
    private static final String PARTIALLY_COMMITTED = "Partially Committed";
    private static final String REJECTED = "Rejected";
	
	static enum ActionType{
		ACCEPT,REJECT,COMMIT,COMMIT_ALL;
	}
	
	@Override
	protected void beforeTestCase() {
		super.beforeTestCase();
	    manageCommonCode("ORDER_VALIDATION", "COST_CENTER", "N");
	    manageCommonCode("ORDER_VALIDATION", "ITEM_ROUND", "N");
	    manageCommonCode("ORDER_VALIDATION", "NEW_DAC", "N");
	    manageCommonCode("ORDER_VALIDATION", "ZIP_CODE", "N");
	}
	
	@Test
	public void testVendorAccept(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.ACCEPT, docCreatePO);
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(true)
	    		                                               .build();
	    System.out.println("docOrderDetails is "+docGetOrderDetails);
	    YFCElement noteElement = docGetOrderDetails.getDocumentElement().getChildElement("Notes").getChildElement("Note");
	    String noteText = noteElement.getAttribute("NoteText");
	    System.out.println("notetext is "+noteText);
	    Assert.assertEquals("ORDER ACCEPTED",noteText);
	}
	
	@Test
	public void testVendorReject(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.REJECT, docCreatePO);
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(REJECTED, status);
        
        YFCDocument docExceptionList = getOpenAlerts(docGetOrderDetails, "GPS_PO_REJECTED_ALERT");
        Assert.assertTrue("There should be open alert for reject", hasOpenAlert(docExceptionList));
	}
	
	@Test
	public void testVendorCommit(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 4, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(7.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);
	}
	
	@Test
	public void testVendorCommitCase1(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 5, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 5, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(10.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(COMMITTED, status);
	    System.out.println("==========================testVendorCommitCase1 ends============================");
	}
	
	
	@Test
	public void testVendorCommitCase2(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(5.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);
	    //Add one more commitment line
        docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 5, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    inputDoc = getOrderInputDocument(docCreatePO);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
                .hasNote(false)
                .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(10.0, totalCommittedQuantity,0);
	    status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
	    Assert.assertEquals(COMMITTED, status);
	    System.out.println("==========================testVendorCommitCase2 ends============================");
	    
	}
	
	/*
	 * Ideally sterling should handle the scenario and throw an exception based on modification rules.
	 * Need to dig deeper, whether we have to throw an exception if trying to reject and partially committed
	 * or committed order.
	 */
	@Test
	public void testRejectCommittedOrder(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(5.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);
        YFCDocument docVendorUpdateForReject = createVendorUpdateInput(ActionType.REJECT, docCreatePO);
        System.out.println("docVendorUpdateForReject is "+docVendorUpdateForReject);
        docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdateForReject);
        System.out.println("output of reject is "+docVendorUpdateOutput);
        inputDoc = getOrderInputDocument(docCreatePO);
        docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
                .hasNote(false)
                .build();
        System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	}
	
	
	@Test
	public void testVendorCommitAll(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT_ALL, docCreatePO);
	    
	    YFCElement orderLines = docVendorUpdate.getDocumentElement().getChildElement(TelstraConstants.ORDER_LINES);
	    docVendorUpdate.getDocumentElement().removeChild(orderLines);
	    System.out.println("Input is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(10.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(COMMITTED, status);
	}
	
	/*
	 * From UI the input does not have Order as root element but service name as root element
	 */
	@Test
	public void testMimicInputFromUI(){
		YFCDocument docCreatePO = createPurchaceOrder();
		System.out.println("docCreatePO is "+docCreatePO);
	    YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    YFCDocument docVendorUpdateNew = getCopy(docVendorUpdate, "GpsVendorUpdates");
	    System.out.println("docVendorUpdate is "+docVendorUpdateNew);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdateNew);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(5.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);
	    //Add one more commitment line
        docVendorUpdateNew = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
        docVendorUpdateNew = getCopy(docVendorUpdateNew, "GpsHandleVendorUpdates");
	    addCommitmentLine(docVendorUpdateNew, 1, null, -1, 5, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("Input to handleVendorUpdate is "+docVendorUpdateNew);
	    invokeYantraService("GpsHandleVendorUpdates", docVendorUpdateNew);
	    inputDoc = getOrderInputDocument(docCreatePO);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
                .hasNote(false)
                .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(10.0, totalCommittedQuantity,0);
	    status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
	    Assert.assertEquals(COMMITTED, status);
	    System.out.println("==========================testVendorCommitCase2 ends============================");
	}
	
		
	/*
	 * This case we will pass only new lines and will not pass old lines
	 */
	@Test
	public void testWith2OrderLineCase2(){
		YFCDocument docPOInput = getPurchaceOrderDocumentWithoutLines();
		YDate reqDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		addOrderLine(docPOInput, new LineObject(10, "Item1", "EACH", reqDeliveryDate, new YDate(true),1));
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		addOrderLine(docPOInput, new LineObject(15, "Item1", "EACH", reqDeliveryDate, new YDate(true),2));
		YFCDocument docCreatePOTemplate = getCreatePurchaceOrderTemplate();
		YFCDocument docCreatePO = invokeYantraApi("createOrder",docPOInput,docCreatePOTemplate);
		
		YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
		
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput for first line is "+docVendorUpdateOutput);
	    
	    Assert.assertNotNull(docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(5.0, totalCommittedQuantity,0);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);
        
        //Now commit 2nd line
        docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
        addCommitmentLine(docVendorUpdate, 2, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate for commiting 2nd line is "+docVendorUpdate);
	   
	   
	    
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    inputDoc = getOrderInputDocument(docCreatePO);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    totalCommittedQuantity = totalCommittedQuantity + getOrderLineFor(docGetOrderDetails, 2).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(10.0, totalCommittedQuantity,0);
	    status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);
        
        //Now fully commit first line
        docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
        addCommitmentLine(docVendorUpdate, 1, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate for commiting 2nd line is "+docVendorUpdate);
	   
	    
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    inputDoc = getOrderInputDocument(docCreatePO);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    double line1CommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(10.0, line1CommittedQuantity,0);
	    totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity") + getOrderLineFor(docGetOrderDetails, 2).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(15.0, totalCommittedQuantity,0);
	    status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(PARTIALLY_COMMITTED, status);

        //Now fully commit first line
        docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
        addCommitmentLine(docVendorUpdate, 2, null, -1, 6, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 4, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate for commiting 2nd line is "+docVendorUpdate);
	   
	    
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    Assert.assertNotNull(docVendorUpdateOutput);
	    inputDoc = getOrderInputDocument(docCreatePO);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails is "+docGetOrderDetails);
	    totalCommittedQuantity = getOrderLineFor(docGetOrderDetails, 1).getDoubleAttribute("CommittedQuantity") + getOrderLineFor(docGetOrderDetails, 2).getDoubleAttribute("CommittedQuantity");
	    Assert.assertEquals(25.0, totalCommittedQuantity,0);
	    status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(COMMITTED, status);  
	}
	
	
	@Test
	public void testUpdateSameLine() throws Exception{
		YFCDocument docPOInput = getPurchaceOrderDocumentWithoutLines();
		YDate reqDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		addOrderLine(docPOInput, new LineObject(10, "Item1", "EACH", reqDeliveryDate, new YDate(true),1));
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		addOrderLine(docPOInput, new LineObject(15, "Item1", "EACH", reqDeliveryDate, new YDate(true),2));
		YFCDocument docCreatePOTemplate = getCreatePurchaceOrderTemplate();
		System.out.println("docCreatePOTemplate is "+docCreatePOTemplate);
		System.out.println("docCreatePOInput is "+docPOInput);
		YFCDocument docCreatePO = invokeYantraApi("createOrder",docPOInput,docCreatePOTemplate);
		System.out.println("Output of docCreatePO is "+docCreatePO);
		YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 2, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
//		
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput for first line is "+docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    
	    //Now commit the same line with some more quantity
	    Map<Integer, List<YFCElement>> lineCommitmentMap = new HashMap<>();
        lineCommitmentMap.put(1, getCommitmentLines(docGetOrderDetails,1));
        docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
        addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{3.0,5.0} ,1);
        System.out.println("docVendorUpdate is "+docVendorUpdate);
        docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput is "+docVendorUpdateOutput);
	    
	    //Now commit the second line
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 5, new YDate("29/7/2016","dd/MM/yyyy",true), new YDate("29/7/2016","dd/MM/yyyy",true));
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 5, new YDate("30/7/2016","dd/MM/yyyy",true), new YDate("30/7/2016","dd/MM/yyyy",true));
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
		
	    
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput after partially committing second line is "+docVendorUpdateOutput);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
                .hasNote(false)
                .build();
	    lineCommitmentMap.put(1, getCommitmentLines(docGetOrderDetails,1));
	    lineCommitmentMap.put(2, getCommitmentLines(docGetOrderDetails,2));
	    
	    //Now fully commit first line
	    removeCommitmentListFor(docVendorUpdate, 1);
	    removeCommitmentListFor(docVendorUpdate, 2);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{5.0,5.0} ,1);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{5.0,5.0} ,2);
	    System.out.println("docVendorUpdate input for committing first line is "+docVendorUpdate);
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput after partially committing second line is "+docVendorUpdateOutput);
	    
	    //Now fully commit second line
	    removeCommitmentListFor(docVendorUpdate, 2);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{10.0,5.0} ,2);
	    System.out.println("docVendorUpdate input for fully committing second line is "+docVendorUpdate);
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput after fully committing second line is "+docVendorUpdateOutput);
	}
	
	
	
	@Test
	public void testAlertRaisedForQunatityMismatch(){
		YFCDocument docPOInput = getPurchaceOrderDocumentWithoutLines();
		YDate reqDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		YDate reqDeliveryDateLine1 = new YDate(reqDeliveryDate.getTime(),true);
		addOrderLine(docPOInput, new LineObject(10, "Item1", "EACH", reqDeliveryDateLine1, new YDate(true),1));
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		YDate reqDeliveryDateLine2 = new YDate(reqDeliveryDate.getTime(),true);
		addOrderLine(docPOInput, new LineObject(15, "Item1", "EACH", reqDeliveryDateLine2, new YDate(true),2));
		YFCDocument docCreatePOTemplate = getCreatePurchaceOrderTemplate();
		System.out.println("docCreatePOTemplate is "+docCreatePOTemplate);
		System.out.println("docCreatePOInput is "+docPOInput);
		YFCDocument docCreatePO = invokeYantraApi("createOrder",docPOInput,docCreatePOTemplate);
		System.out.println("Output of docCreatePO is "+docCreatePO);
		YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
		//Creating input for partially commit first line and partially commit second line
		YDate commitShipDate = new YDate(true);
		YDate today = new YDate(true);
		YFCDateUtils.addHours(today, 24*3);
		YDate lin1com1Date = new YDate(today.getTime(),true);
		YFCDateUtils.addHours(today, 24*4);
		YDate lin1com2Date = new YDate(today.getTime(),true);
		
		YDate today2 = new YDate(true);
		YFCDateUtils.addHours(today2, 24*7);
		YFCDateUtils.addHours(today2, 24*3);
		YDate lin2com1Date = new YDate(today2.getTime(),true);
		YFCDateUtils.addHours(today2, 24*4);
		YDate lin2com2Date = new YDate(today2.getTime(),true);
		
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 4, commitShipDate, lin1com1Date);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 3, commitShipDate, lin1com2Date);
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 4, commitShipDate, lin2com1Date);
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 6, commitShipDate, lin2com2Date);
	    System.out.println("docVendorUpdate is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorOutput is "+docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    
	    //Now commit the same line with some more quantity
	    Map<Integer, List<YFCElement>> lineCommitmentMap = new HashMap<>();
        lineCommitmentMap.put(1, getCommitmentLines(docGetOrderDetails,1));
        lineCommitmentMap.put(2, getCommitmentLines(docGetOrderDetails,2));
	    
	    
	    //Check if alerts are present
	    YFCDocument docExceptionList = getOpenAlerts(docVendorUpdate);
	    Assert.assertTrue("there should be open alert", hasOpenAlert(docExceptionList));
	    
	    //Now fully commit first line
	    removeCommitmentListFor(docVendorUpdate, 1);
	    removeCommitmentListFor(docVendorUpdate, 2);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{5.0,5.0} ,1);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{5.0,5.0} ,2);
	    System.out.println("docVendorUpdate input for committing first line is "+docVendorUpdate);
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput after partially committing second line is "+docVendorUpdateOutput);
	    
	    docExceptionList = getOpenAlerts(docVendorUpdate);
	    Assert.assertTrue("there should be open alert", hasOpenAlert(docExceptionList));
	    //Raise count should be 2
	    YFCElement docException = (YFCElement)docExceptionList.getDocumentElement().getFirstChild();
	    System.out.println("docException is "+docException);
	    int consolidationCount = docException.getIntAttribute("ConsolidationCount");
	    Assert.assertEquals(2, consolidationCount);
	    
	    //Now fully commit second line
	    removeCommitmentListFor(docVendorUpdate, 1);
	    removeCommitmentListFor(docVendorUpdate, 2);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{5.0,5.0} ,1);
	    addExistingCommitmentLines(docVendorUpdate, lineCommitmentMap, new Double[]{10.0,5.0} ,2);
	    System.out.println("docVendorUpdate input for committing second line so committing entire order is "+docVendorUpdate);
	    docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorUpdateOutput after fully committing second line is "+docVendorUpdateOutput);
	    docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
                .hasNote(false)
                .build();
	    //Now there are 
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(COMMITTED, status);  
	    docExceptionList = getOpenAlerts(docVendorUpdate);
	    System.out.println("after fully commit line exception list is "+docExceptionList);
	    Assert.assertFalse("Alert should have been resolved", hasOpenAlert(docExceptionList));
	}
	
	
	@Test
	public void testAlertRaisedForDeliveryDateMismatch(){
		YFCDocument docPOInput = getPurchaceOrderDocumentWithoutLines();
		YDate reqDeliveryDate = new YDate(true);
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		YDate reqDeliveryDateLine1 = new YDate(reqDeliveryDate.getTime(),true);
		addOrderLine(docPOInput, new LineObject(10, "Item1", "EACH", reqDeliveryDateLine1, new YDate(true),1));
		YFCDateUtils.addHours(reqDeliveryDate, 24*7);
		YDate reqDeliveryDateLine2 = new YDate(reqDeliveryDate.getTime(),true);
		addOrderLine(docPOInput, new LineObject(15, "Item1", "EACH", reqDeliveryDateLine2, new YDate(true),2));
		YFCDocument docCreatePOTemplate = getCreatePurchaceOrderTemplate();
		System.out.println("docCreatePOTemplate is "+docCreatePOTemplate);
		System.out.println("docCreatePOInput is "+docPOInput);
		YFCDocument docCreatePO = invokeYantraApi("createOrder",docPOInput,docCreatePOTemplate);
		System.out.println("Output of docCreatePO is "+docCreatePO);
		YFCDocument docVendorUpdate = createVendorUpdateInput(ActionType.COMMIT, docCreatePO);
		//Creating input for partially commit first line and partially commit second line
		YDate commitShipDate = new YDate(true);
		YDate today = new YDate(true);
		YFCDateUtils.addHours(today, 24*3);
		YDate lin1com1Date = new YDate(today.getTime(),true);
		YFCDateUtils.addHours(today, 24*4);
		YDate lin1com2Date = new YDate(today.getTime(),true);
		
		YDate today2 = new YDate(true);
		YFCDateUtils.addHours(today2, 24*7);
		YFCDateUtils.addHours(today2, 24*3);
		YDate lin2com1Date = new YDate(today2.getTime(),true);
		YFCDateUtils.addHours(today2, 24*6);
		YDate lin2com2Date = new YDate(today2.getTime(),true);
		
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 4, commitShipDate, lin1com1Date);
	    addCommitmentLine(docVendorUpdate, 1, null, -1, 6, commitShipDate, lin1com2Date);
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 6, commitShipDate, lin2com1Date);
	    addCommitmentLine(docVendorUpdate, 2, null, -1, 9, commitShipDate, lin2com2Date);
	    System.out.println("docVendorUpdateInput  is "+docVendorUpdate);
	    YFCDocument docVendorUpdateOutput = invokeYantraService("GpsHandleVendorUpdates", docVendorUpdate);
	    System.out.println("docVendorOutput is "+docVendorUpdateOutput);
	    YFCDocument inputDoc = getOrderInputDocument(docCreatePO);
	    YFCDocument docGetOrderDetails = GetOrderDetailsBuilder.getInstance(inputDoc, this)
	    		                                               .hasNote(false)
	    		                                               .build();
	    System.out.println("docGetOrderDetails for testcase testAlertRaisedForDeliveryDateMismatch "+docGetOrderDetails);
	    String status = docGetOrderDetails.getDocumentElement().getAttribute("Status");
        Assert.assertEquals(COMMITTED, status);  
	    //Check if alerts are present
	    YFCDocument docExceptionList = getOpenAlerts(docVendorUpdate);
	    System.out.println("docExceptionList is "+docExceptionList);
	    Assert.assertTrue("there should be open alert", hasOpenAlert(docExceptionList));
	    
	}
	
	
	private YFCDocument getOpenAlerts(YFCDocument inXml){
		return  getOpenAlerts(inXml, "GPS_PO_COMMIT_MISMATCH_ALERT");
	}
	
	private YFCDocument getOpenAlerts(YFCDocument inXml,String exceptionType){
		YFCDocument docGetOpenAlerts = YFCDocument.getDocumentFor("<Inbox ActiveFlag='Y' OrderHeaderKey='"+inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY)+"' ExceptionType='"+exceptionType+"' />");
		return invokeYantraApi("getExceptionList", docGetOpenAlerts); 
	}
	
	
	private boolean hasOpenAlert(YFCDocument docExceptionList){
		if(docExceptionList == null || docExceptionList.getDocumentElement() == null){
			return false;
		}
		YFCElement elemExceptionList = docExceptionList.getDocumentElement();
		if(elemExceptionList.getChildren() == null || elemExceptionList.getFirstChild() == null){
			return false;
		}
		
		return true;
	}
	
	
	
	
	private void removeCommitmentListFor(YFCDocument orderDetails, int primeLineNo){
		YFCElement elemOrderLine = getOrderLineFor(orderDetails, primeLineNo);
		YFCElement elemOrderLineExtn = elemOrderLine.getChildElement(TelstraConstants.EXTN);
		if(elemOrderLineExtn == null){
			return;
		}
		YFCElement elemOrderLineCommitmentList = elemOrderLineExtn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
		if(elemOrderLineCommitmentList != null){
			elemOrderLineExtn.removeChild(elemOrderLineCommitmentList);
		}
	}
	
	
	
	private void addExistingCommitmentLines(YFCDocument orderDetails, Map<Integer, List<YFCElement>> lineCommitmentMap,Double[] commitmentLineQuantities, int primeLineNo){
		YFCElement elemOrderLine = getOrderLineFor(orderDetails, primeLineNo);
		YFCElement elemExtn = elemOrderLine.getChildElement("Extn", true);
		YFCElement elemOrderLineCommitList = elemExtn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST,true);
		int i = 0;
		List<YFCElement> commitments = lineCommitmentMap.get(primeLineNo);
		for(YFCElement commitment : commitments){
			Double quantity = commitmentLineQuantities[i];
			i = i + 1;
			commitment.setAttribute(TelstraConstants.COMMITTED_QUANTITY, quantity);
			elemOrderLineCommitList.importNode(commitment);
		}
	}
	
	
	private List<YFCElement> getCommitmentLines(YFCDocument orderDetails,int primeLineNo){
		YFCElement elemOrderLine = getOrderLineFor(orderDetails, primeLineNo);
		YFCElement extn = elemOrderLine.getChildElement("Extn");
		if(extn == null){
			return null;
		}
		YFCElement lineCommitments = extn.getChildElement(TelstraConstants.ORDER_LINE_COMMITMENT_LIST);
		if(lineCommitments == null || lineCommitments.getChildren() == null){
			return null;
		}
		List<YFCElement> lineCommitmentList = new ArrayList<>();
		for(Iterator<YFCElement> itr = lineCommitments.getChildren().iterator();itr.hasNext();){
			lineCommitmentList.add(itr.next());
		}
		return lineCommitmentList;
	}
	
	
	
	private YFCDocument createVendorUpdateInput(ActionType action, YFCDocument poDoc){
		YFCDocument poCopyDoc = poDoc.getCopy();
		if(action == ActionType.ACCEPT){
			poCopyDoc.getDocumentElement().setAttribute("Action", "ACCEPT");
		}else if(action == ActionType.REJECT){
			poCopyDoc.getDocumentElement().setAttribute("Action", "REJECT");
		}else if(action == ActionType.COMMIT){
			poCopyDoc.getDocumentElement().setAttribute("Action", "COMMIT");
		}else if(action == ActionType.COMMIT_ALL){
			poCopyDoc.getDocumentElement().setAttribute("Action", "COMMIT_ALL");
		}else{
			return null;
		}
		return poCopyDoc;
	}
	
	private static YFCDocument getCopy(YFCDocument inXml, String rootNode){
		YFCDocument copyDoc = YFCDocument.createDocument(rootNode);
		copyDoc.getDocumentElement().setAttributes(inXml.getDocumentElement().getAttributes());
		if(inXml.getDocumentElement().getChildren() != null){
			for(Iterator<YFCElement> itr = inXml.getDocumentElement().getChildren().iterator();itr.hasNext();){
				copyDoc.getDocumentElement().importNode(itr.next());
			}
		}
		return copyDoc;
	}
	
	private void addCommitmentLine(YFCDocument docVendorUpdate, int primeLineNo, String lineCommitmentKey, int scheduleNo, double commitQunatity, YDate committedShipDate, YDate committedDeliveryDate){
		YFCElement elemOrderLine = getOrderLineFor(docVendorUpdate, primeLineNo);
		Assert.assertNotNull(elemOrderLine);
		YFCElement elemOrderLineExtn = elemOrderLine.getChildElement("Extn", true);
		YFCElement orderLineCommitments = elemOrderLineExtn.getChildElement("OrderLineCommitmentList", true);
		YFCElement orderLineCommitment = orderLineCommitments.createChild("OrderLineCommitment");
		orderLineCommitment.setAttribute("OrderLineKey", elemOrderLine.getAttribute("OrderLineKey"));
		if(!YFCObject.isVoid(lineCommitmentKey)){
			orderLineCommitment.setAttribute("OrderLineCommitKey", lineCommitmentKey);
		}
		if(scheduleNo > 0){
			orderLineCommitment.setIntAttribute("ScheduleNo", scheduleNo);
		}
		orderLineCommitment.setDoubleAttribute("CommittedQuantity", commitQunatity);
		orderLineCommitment.setDateAttribute("CommitShipDate", committedShipDate);
		orderLineCommitment.setDateAttribute("CommitDeliveryDate", committedDeliveryDate);
		orderLineCommitment.setAttribute("OrderLineKey", elemOrderLine.getAttribute("OrderLineKey"));
	}
	
	private YFCElement getOrderLineFor(YFCDocument docVendorUpdate, int primeLineNo){
		YFCElement elemVendorUpdate = docVendorUpdate.getDocumentElement();
		YFCElement elemOrderLines = elemVendorUpdate.getChildElement("OrderLines");
		for(Iterator<YFCElement> itr = elemOrderLines.getChildren().iterator(); itr.hasNext(); ){
			YFCElement elemOrderLine = itr.next();
			int orderPrimeLineNo = elemOrderLine.getIntAttribute("PrimeLineNo");
			if(orderPrimeLineNo == primeLineNo){
				return elemOrderLine;
			}
		}
		return null;
	}
	
	private YFCDocument createPurchaceOrder(){
		YFCDocument docCreatePO = getCreatePurchaceOrderDocument();
		System.out.println(docCreatePO);
		YFCDocument docCreatePOTemplate = getCreatePurchaceOrderTemplate();
		return invokeYantraApi("createOrder",docCreatePO,docCreatePOTemplate);
	}
	
	private YFCDocument getCreatePurchaceOrderDocument(){
		String poOrderString = "<Order  DocumentType='0005' DepartmentCode='PMC' DraftOrderFlag='N' EnteredBy='admin' EnterpriseCode='TELSTRA_SCLSA' OrderName='testPOOrder' OrderNo='' OrderPurpose='' OriginalTotalAmount='2300' ReqDeliveryDate='2016-01-10' ReqShipDate='2016-01-08' SellerOrganizationCode='Vendor1' SkipBOMValidations='N' ValidatePromotionAward='Y' >"
				+ " <OrderLines>"
				+ " <OrderLine AllocationDate='2016-06-24' MinShipByDate='2016-07-30' OrderedQty='10' ReqDeliveryDate='2016-07-30' ReqShipDate='2016-07-30'"
				+ " ReservationID='' ReservationMandatory='Y' Status='Created'>"
				+ " <Item ItemID='Item1' ItemShortDesc='Item1' ProductClass='' UnitOfMeasure= 'EACH'/>"
				+ " <OrderDates>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MAX_DELIVERY' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MAX_SHIPMENT' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MIN_DELIVERY' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MIN_SHIPMENT' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " </OrderDates>"
				+ " </OrderLine>"
				+ " </OrderLines>"
				+ " <PersonInfoBillTo Country='AU' />"
				+ " </Order>";
		System.out.println("poOrder string is "+poOrderString);
		return YFCDocument.getDocumentFor(poOrderString);
	}
	
	
	private YFCDocument getPurchaceOrderDocumentWithoutLines(){
		String poOrderString = "<Order  DocumentType='0005' DepartmentCode='PMC' DraftOrderFlag='N' EnteredBy='admin' EnterpriseCode='TELSTRA_SCLSA' OrderName='testPOOrder' OrderNo='' OrderPurpose='' OriginalTotalAmount='2300' ReqDeliveryDate='2016-01-10' ReqShipDate='2016-01-08' SellerOrganizationCode='Vendor1' SkipBOMValidations='N' ValidatePromotionAward='Y' >"
				+ " <OrderLines>"
				+ " </OrderLines>"
				+ " <PersonInfoBillTo Country='AU' />"
				+ " </Order>";
		System.out.println("poOrder string is "+poOrderString);
		return YFCDocument.getDocumentFor(poOrderString);
	}
	
	
	private YFCDocument getOrderLineDocument(){
		return YFCDocument.getDocumentFor("<OrderLine AllocationDate='2016-06-24' DepartmentCode='TELDEPT2' MinShipByDate='2016-07-30' OrderedQty='10' ReqDeliveryDate='2016-07-30' ReqShipDate='2016-07-30'"
				+ " ReservationID='' ReservationMandatory='Y' Status='Created'>"
				+ " <Item ItemID='Item1' ItemShortDesc='Item1' ProductClass='' UnitOfMeasure= 'EACH'/>"
				+ " <OrderDates>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MAX_DELIVERY' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MAX_SHIPMENT' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MIN_DELIVERY' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " <OrderDate ActualDate='2016-07-30' CommittedDate='2016-07-30' DateTypeId='MIN_SHIPMENT' ExpectedDate='2016-07-30' RequestedDate='2016-07-30'/>"
				+ " </OrderDates>"
				+ " </OrderLine>");
	}
	
	private YFCDocument getCreatePurchaceOrderTemplate(){
		return YFCDocument.getDocumentFor("<Order  OrderHeaderKey='' DocumentType='' DraftOrderFlag='' EnteredBy='' EnterpriseCode='' OrderName='' OrderNo='' OrderPurpose='' OriginalTotalAmount=''"
				+ " ReqDeliveryDate='' ReqShipDate='' SellerOrganizationCode='' SkipBOMValidations='' ValidatePromotionAward='' >"
				+ " <OrderLines>"
				+ " <OrderLine AllocationDate='' DepartmentCode='' MinShipByDate='' OrderedQty='' ReqDeliveryDate='' ReqShipDate=''"
				+ " ReservationID='' ReservationMandatory='' Status='' PrimeLineNo='' OrderLineKey='' >"
				+ " <Item ItemID='' ItemShortDesc='' ProductClass='' UnitOfMeasure= ''/>"
				+ " <OrderDates>"
				+ " <OrderDate ActualDate='' CommittedDate='' DateTypeId='' ExpectedDate='' RequestedDate=''/>"
				+ " </OrderDates>"
				+ " </OrderLine>"
				+ " </OrderLines>"
				+ " <PersonInfoBillTo Country='AU' />"
				+ " </Order>");
	}
	
	private YFCDocument getOrderInputDocument(YFCDocument poDoc){
		YFCDocument docInput = YFCDocument.createDocument("Order");
		docInput.getDocumentElement().setAttribute("OrderHeaderKey", poDoc.getDocumentElement().getAttribute("OrderHeaderKey"));
		docInput.getDocumentElement().setAttribute("OrderNo", poDoc.getDocumentElement().getAttribute("OrderNo"));
		docInput.getDocumentElement().setAttribute("DocumentType", poDoc.getDocumentElement().getAttribute("DocumentType"));
		return  docInput;
	}
	
	
	protected static class GetOrderDetailsBuilder{
		TestBase classInstance;
		YFCDocument inputDoc;
		boolean hasNote;
		boolean hasLines;
		boolean hasCommitments;
		private GetOrderDetailsBuilder(){
		}
		
		public static GetOrderDetailsBuilder getInstance(YFCDocument inputDoc, TestBase classInstance){
			GetOrderDetailsBuilder builder = new GetOrderDetailsBuilder();
			builder.inputDoc = inputDoc;
			builder.classInstance = classInstance;
			return builder;
		}
		
		protected GetOrderDetailsBuilder hasNote(boolean hasNote){
			this.hasNote = hasNote;
			return this;
		}
		
		protected GetOrderDetailsBuilder hasLines(boolean hasLines){
			this.hasLines = hasLines;
			return this;
		}
		
		protected GetOrderDetailsBuilder hasCommitments(boolean hasCommitments){
			this.hasCommitments = hasCommitments;
			return this;
		}
		
		protected YFCDocument build(){
			YFCDocument template = prepareTemplate();
			System.out.println("template is "+template);
			return classInstance.invokeYantraApi("getOrderDetails", inputDoc, template);
		}
		
		private YFCDocument prepareTemplate(){
			YFCDocument template = getBasicTemplate();
			if(hasNote){
				addNotes(template);
			}
			return template;
		}
		
		private void addNotes(YFCDocument template){
			YFCDocument notesDoc = YFCDocument.getDocumentFor("<Notes NumberOfNotes=''>"
					+ " <Note AuditTransactionId='' ContactReference='' ContactTime=''"
					+ " ContactType='' ContactUser='' CustomerSatIndicator=''"
					+ " NoteText='' Priority='' ReasonCode='' SequenceNo=''"
					+ " Tranid='' Tranname='' VisibleToAll=''>"
					+ " <User Username='' Usertype=''/>"
					+ " </Note>"
					+ " </Notes>");
			template.getDocumentElement().importNode(notesDoc.getDocumentElement());
		}
		
		private YFCDocument getBasicTemplate(){
			String templateString = "<Order OrderHeaderKey=\"\" OrderNo=\"\" EnterpriseCode=\"\" SellerOrganizationCode=\"\" DepartmentCode=\"\" Status=\"\" DocumentType=\"\" > <Extn HasVendorAccepted='' /><OrderLines>  <OrderLine OrderLineKey=\"\" PrimeLineNo=\"\" SubLineNo=\"\" ReqDeliveryDate=\"\" CommittedQuantity=\"\" OrderedQty=\"\" Status=\"\" ><Extn><OrderLineCommitmentList><OrderLineCommitment OrderLineCommitKey=\"\" ScheduleNo=\"\" OrderLineKey=\"\" CommitShipDate=\"\" CommitDeliveryDate=\"\" CommittedQuantity=\"\" /> </OrderLineCommitmentList></Extn></OrderLine> </OrderLines> </Order>";
			return YFCDocument.getDocumentFor(templateString);
		}
	
	}
	
	
	private class LineObject{
		private int quantity;
		private String itemId;
		private String uom;
		private YDate reqDeliveryDate;
		private YDate reqShipDate;
		private int primeLineNo;
		
		LineObject(int quantity, String itemId, String uom, YDate reqDeliveryDate, YDate reqShipDate,int primeLineNo){
			this.quantity = quantity;
			this.itemId = itemId;
			this.uom = uom;
			this.reqDeliveryDate = reqDeliveryDate;
			this.reqShipDate = reqShipDate;
			this.primeLineNo = primeLineNo;
		}

		public int getQuantity() {
			return quantity;
		}

		public String getItemId() {
			return itemId;
		}

		public String getUom() {
			return uom;
		}

		public YDate getReqDeliveryDate() {
			return reqDeliveryDate;
		}

		public YDate getReqShipDate() {
			return reqShipDate;
		}
		
		public int getPrimeLineNo(){
			return primeLineNo;
		}
			
	}
	
	private void addOrderLine(YFCDocument docPOInput, LineObject lineObject){
		YFCElement orderLines = docPOInput.getDocumentElement().getChildElement("OrderLines");
		YFCDocument docOrderLine = getOrderLineDocument();
		YFCElement elemOrderLine = docOrderLine.getDocumentElement();
		elemOrderLine.setAttribute("OrderedQty", lineObject.getQuantity());
		elemOrderLine.setAttribute("ReqDeliveryDate", lineObject.getReqDeliveryDate());
		elemOrderLine.setAttribute("ReqShipDate", lineObject.getReqShipDate());
		YFCElement elemItem = elemOrderLine.getChildElement("Item");
		elemItem.setAttribute(TelstraConstants.ITEM_ID, lineObject.getItemId());
		elemItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, lineObject.getUom());
		elemItem.setAttribute("PrimeLineNo", lineObject.getPrimeLineNo() + "");
		orderLines.importNode(elemOrderLine);
	}
	
	private void manageCommonCode(String codeType,String codeValue, String shortDescription){
		  YFCDocument manageCommonCodeInput = YFCDocument.getDocumentFor("<CommonCode Action=\"Manage\" CodeShortDescription=\""+shortDescription+"\" CodeValue=\""+codeValue+"\" CodeType='"+codeType+"' />");
		    invokeYantraApi("manageCommonCode", manageCommonCodeInput);
	  }
	
}
