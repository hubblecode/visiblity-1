package com.gps.hubble.carrier.api;

import java.io.IOException;

import org.junit.Assert;
import org.junit.Test;
import org.xml.sax.SAXException;

import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.TestBase;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.util.YFCCommon;



public class TestCarrierUpdate extends TestBase {

	// only one shipment created
	@SuppressWarnings("deprecation")
	@Test
	public void testCarrierUpdateConPickedUp() throws SAXException, IOException {
		//createItem("VAI1", "VAI2");
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		String sOrderName = createOrderOp.getDocumentElement().getAttribute("OrderName");
		//
		scheduleAndReleaseOrder(sOrderHeaderKey);
		
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		//
		PickOrder(createOrderOp, sOrderName);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		String shipmentNo = getShipmentNo(sOrderHeaderKey);
		
		YFCDocument docCarrierPickedUpOp = carrierUpdatePickedUp(sOrderName,shipmentNo, "Consignment Picked Up");
		System.out.println("docCarrierPickedUpOp>>>>\n"+docCarrierPickedUpOp);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		YFCElement eleCarrierUpdate = docCarrierPickedUpOp.getElementsByTagName("CarrierUpdate").item(0);
		
		String sTransportStatus = "";
		if(!YFCCommon.isVoid(eleCarrierUpdate)){
			sTransportStatus = eleCarrierUpdate.getAttribute("TransportStatus");
		}
		
		Assert.assertEquals(sTransportStatus, "Consignment Picked Up");


	}

	@Test
	public void testCarrierUpdateConDel() throws SAXException, IOException{

		//createItem("VAI1", "VAI2");
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		String sOrderName = createOrderOp.getDocumentElement().getAttribute("OrderName");
		//
		scheduleAndReleaseOrder(sOrderHeaderKey);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		//
		PickOrder(createOrderOp, sOrderName);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		String shipmentNo = getShipmentNo(sOrderHeaderKey);
		//
		carrierUpdatePickedUp(sOrderName,shipmentNo, "Consignment Picked Up");
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		//
		YFCDocument docCarrierConDelOp = carrierUpdatePickedUp(sOrderName,shipmentNo, "Consignment Delivered");
		YFCElement eleCarrierUpdate = docCarrierConDelOp.getElementsByTagName("CarrierUpdate").item(0);
		
		System.out.println("docCarrierConDelOp>>>\n "+docCarrierConDelOp);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));

		String sTransportStatus = "";
		if(!YFCCommon.isVoid(eleCarrierUpdate)){
			sTransportStatus = eleCarrierUpdate.getAttribute("TransportStatus");
		}
		Assert.assertEquals(sTransportStatus, "Consignment Delivered");
//		YFCDocument docCarrierConDelOp = carrierUpdatePickedUp(sOrderName,shipmentNo, "Consignment Delivered");

	
	}
	
	@Test
	public void testCarrierUpdateUnsuccessful() throws SAXException, IOException{

		//createItem("VAI1", "VAI2");
		YFCDocument createOrderOp = createOrder();
		String sOrderHeaderKey = createOrderOp.getElementsByTagName("OrderLine").item(0).getAttribute("OrderHeaderKey");
		String sOrderName = createOrderOp.getDocumentElement().getAttribute("OrderName");
		//
		scheduleAndReleaseOrder(sOrderHeaderKey);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));

		//
		PickOrder(createOrderOp, sOrderName);
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));

		String shipmentNo = getShipmentNo(sOrderHeaderKey);
		
		carrierUpdatePickedUp(sOrderName,shipmentNo, "Consignment Picked Up");
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
//
		YFCDocument docCarrierConFutileOp = carrierUpdatePickedUp(sOrderName,shipmentNo, "Unsuccessful");
		System.out.println("maxOrderStatus>>> "+maxOrderStatus(sOrderHeaderKey));
		System.out.println("docCarrierConFutileOp>>>\n "+docCarrierConFutileOp);

		YFCElement eleCarrierUpdate = docCarrierConFutileOp.getElementsByTagName("CarrierUpdate").item(0);
		
		String sTransportStatus = "";
		if(!YFCCommon.isVoid(eleCarrierUpdate)){
			sTransportStatus = eleCarrierUpdate.getAttribute("TransportStatus");
		}
		Assert.assertEquals(sTransportStatus, "Unsuccessful");

	
	}
	
	
	
	
	
	
	
	
	
	
	
	

	private String maxOrderStatus(String sOrderHeaderKey) {
		// TODO Auto-generated method stub
		return XPathUtil.getXpathAttribute(invokeYantraApi("getOrderList", YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'/>"),  YFCDocument.getDocumentFor("<OrderList><Order MaxOrderStatusDesc='' /></OrderList>")),"//@MaxOrderStatusDesc");
	}

	private YFCDocument carrierUpdatePickedUp(String sOrderName, String shipmentNo, String sTransportStatus) {

		YFCDocument docCarrierUpdateIp = YFCDocument.getDocumentFor(
				"<Shipment ShipmentNo='"+shipmentNo+"' OrderName='"+sOrderName+"' TrackingNo=''> <Extn> <CarrierUpdateList> <CarrierUpdate TransportStatus='"+sTransportStatus+"' TransportStatusTxt='from shipment picked to Transport Status Intransit' TransportStatusDate='20160208' ReceiverName='Keerthi' ReceiverPhone='225522255' Pod='abc' /> </CarrierUpdateList> </Extn> <Containers> <Container ContainerNo='414556677288' /> </Containers> </Shipment>");
		return invokeYantraService("GpsCarrierUpdate", docCarrierUpdateIp);
	}


	private String getShipmentNo(String sOrderHeaderKey) {
		
		YFCDocument getShipmentListOp = invokeYantraApi("getShipmentList",
				YFCDocument.getDocumentFor("<Shipment OrderHeaderKey='" + sOrderHeaderKey + "'/>"),
				YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentNo=''/></Shipments>"));
		return getShipmentListOp.getElementsByTagName("Shipment").item(0).getAttribute("ShipmentNo");
	}

	/*private void createItem(String Item1, String Item2) throws SAXException, IOException {
		String manageItem = "<ItemList><Item Action='' ItemID='' OrganizationCode='TELSTRA_SCLSA' UnitOfMeasure='BAG'> "
				+ "<PrimaryInformation Description='BLOCK,S5000 128PR PROT RH' IsHazmat='' ItemType='NTRD' ManufacturerItem='SIEMENS S30264' ManufacturerName=''"
				+ " ProductLine='29'  ShortDescription='BLOCK,S5000 128PR PROT RH' Status='3000' UnitCost='0.65' />  </Item> </ItemList>";

		YFCDocument docManageItemIp = YFCDocument.parse(manageItem);
		YFCElement eleItem = docManageItemIp.getElementsByTagName("Item").item(0);
		eleItem.setAttribute("ItemID", Item1);
		invokeYantraService("GpsManageCatalog", docManageItemIp);

		// create 2nd item
		eleItem.setAttribute("ItemID", Item2);
		invokeYantraService("GpsManageCatalog", docManageItemIp);

	}*/

	private YFCDocument createOrder() throws SAXException, IOException {

		String sCreateOrderIpXML = "<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' EntryType='INTEGRAL_PLUS' "
				+ "DocumentType='' PriorityCode='1' InterfaceNo='INT_ODR_2' OrderName='KEINVJHJHTOC7689' BillToID='INV_MAT2' ShipToID='INV_MAT2' SearchCriteria2='+61 413 014 829' OrderType='MATERIAL_RESERVATION' SearchCriteria1='9090909090' Division='CXS053'> "
				+ "  <PersonInfoBillTo FirstName='' AddressLine1='Hosur road' AddressLine2='' AddressLine3='' AddressLine4='' City='Alpurrurulam' ZipCode='4830' State='NT' DayPhone='5461801' DayFaxNo='031 23 2854' Country='AU' /> "
				+ "  <References> <Reference Name='DELIVERY_INSTRUCTIONS' Value='Handle with care' /> <Reference Name='PM_ORDER' Value='' /> <Reference Name='ACCOUNT_NUMBER' Value='00007777' /> "
				+ " <Reference Name='ACTIVITY_NUMBER' Value='00007771' /> </References> <OrderLines> <OrderLine ShipNode='TestOrg9' OrderedQty='10' PrimeLineNo='1' ChangeStatus='' ReceivingNode=''> "
				+ " <Item ItemID='000000000002900508' UnitOfMeasure='EACH' ItemShortDesc='WASHER,FLAT MS GALV 12MM' /> </OrderLine>  <OrderLine ShipNode='TestOrg9' OrderedQty='10' PrimeLineNo='2' ChangeStatus='' ReceivingNode=''> "
				+ " <Item ItemID='000000000006700205' UnitOfMeasure='EACH' ItemShortDesc='WASHER,FLAT MS GALV 12MM' />  </OrderLine>   </OrderLines>  <OrderDates> "
				+ "     <OrderDate DateTypeId='DATE_TO_WHAM' RequestedDate='20160718' />   </OrderDates> <AdditionalAddresses> <AdditionalAddress AddressType='SHIP_FROM'> <PersonInfo FirstName='Vendor Name' AddressLine1='Street Address' "
				+ " City='vendor city' ZipCode='vendor zip code' State='vendor state' DayPhone='vendor phone' /> </AdditionalAddress> </AdditionalAddresses>  </Order>";

		

		YFCDocument docCreateOrderIp = YFCDocument.parse(sCreateOrderIpXML);
		return invokeYantraService("GpsManageOrder", docCreateOrderIp);

	}

	private void scheduleAndReleaseOrder(String sOrderHeaderKey) throws SAXException, IOException {

		String sScheduleOrderIp = "<ScheduleOrder OrderHeaderKey='" + sOrderHeaderKey
				+ "' CheckInventory='N' ScheduleAndRelease='Y'/>";
		YFCDocument docScheduleOrderIp = YFCDocument.parse(sScheduleOrderIp);
		invokeYantraApi("scheduleOrder", docScheduleOrderIp);

	}

	private void PickOrder(YFCDocument createOrderOp, String sOrderName) throws SAXException, IOException {
		
		String PickOrderMsg = "<OrderRelease OrderName='"+sOrderName+"' ShipNode='TestOrg9'> <OrderLines> <OrderLine Action='BACKORDER' PrimeLineNo='1' SubLineNo='1' StatusQuantity='5' ItemID='000000000002900508' UnitOfMeasure='EACH' /> <OrderLine Action='BACKORDER' PrimeLineNo='2' SubLineNo='1' StatusQuantity='7' ItemID='000000000002900508' UnitOfMeasure='EACH' /> </OrderLines> </OrderRelease>";
		invokeYantraService("GpsPickOrder", YFCDocument.parse(PickOrderMsg));
	}

	

}
