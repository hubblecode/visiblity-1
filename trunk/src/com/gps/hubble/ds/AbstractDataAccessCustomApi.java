package com.gps.hubble.ds;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

public abstract class AbstractDataAccessCustomApi extends AbstractCustomApi{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(AbstractDataAccessCustomApi.class);
	
	@SuppressWarnings("unchecked")
	protected boolean isDataAccessPolicyBypassRequired(YFCDocument inXml){
		Object isDataAccessPolicyOverrideReqObj = getTxnObject("IsCustomDataAccessPolicyRequired");
		if(isDataAccessPolicyOverrideReqObj == null){
			return false;
		}
		Boolean isDataAccessPolicyOverrideReq = (Boolean)isDataAccessPolicyOverrideReqObj;
		boolean bypassRequired =  isDataAccessPolicyOverrideReq.booleanValue();
		if(bypassRequired){
			Object byPassMapObj = getTxnObject("DSBypassMap");
			if(byPassMapObj == null){
				return false;
			}
			Map<String, List<String>> bypassMap = null;
			try{
				bypassMap = ((Map<String, List<String>>)byPassMapObj);
			}catch(ClassCastException cse){
				logger.error("Invalid txn attribute found ", cse);
				return false;
			}
			return isBypassRequired(inXml, bypassMap);
		}
		return false;
	}
	
	
	protected YFCDocument getUser(YFCDocument inXml){
		YFCElement elemAccessPolicy = inXml.getDocumentElement();
		String userId = elemAccessPolicy.getAttribute("ClaimUserID");
		if(YFCObject.isVoid(userId)){
			userId = elemAccessPolicy.getAttribute("UserID");
		}
		if(YFCObject.isVoid(userId)){
			return null;
		}
		YFCDocument docGetUserDetails = YFCDocument.getDocumentFor("<User Loginid='"+userId+"' />");
		YFCDocument docGetUserDetailsTemplate = YFCDocument.getDocumentFor("<UserList><User Loginid='' EnterpriseCode='' UsergroupKey='' ><UserGroupLists><UserGroupList UsergroupKey='' /></UserGroupLists></User></UserList>");
		YFCDocument docUserList =  invokeYantraApi("getUserList", docGetUserDetails, docGetUserDetailsTemplate);
		if(docUserList == null || docUserList.getDocumentElement() == null || docUserList.getDocumentElement().getChildElement("User") == null){
			return null;
		}
		return YFCDocument.getDocumentFor(docUserList.getDocumentElement().getChildElement("User").getString());
		
	}
	

	
	protected boolean isValidVendorUser(YFCDocument docUser){
		String vendorOrgCode = getVendorOrgCode();
		String organizationCode = docUser.getDocumentElement().getAttribute("EnterpriseCode");
		if(YFCObject.isVoid(organizationCode) || YFCObject.isVoid(vendorOrgCode)){
			return false;
		}
		if(vendorOrgCode.equals(organizationCode)){
			return isVendorOrg(organizationCode);
		}
		return false;
	}
	
	protected boolean isVendorOrg(String organizationCode){
		if(YFCObject.isVoid(organizationCode)){
			return false;
		}
		YFCDocument docGetVendorList = YFCDocument.getDocumentFor("<Vendor SellerOrgainzationCode='"+organizationCode+"' />");
		YFCDocument docGetVendorListTemplate = YFCDocument.getDocumentFor("<VendorList><Vendor VendorKey='' VendorID='' OrganizationCode='' SellerOrganizationCode='' /></VendorList>");
		YFCDocument docVendorList = invokeYantraApi("getVendorList",docGetVendorList,docGetVendorListTemplate);
		if(docVendorList == null || docVendorList.getDocumentElement() == null || docVendorList.getDocumentElement().getChildElement("Vendor") == null){
			logger.debug("Organization "+organizationCode+" is not mapped as vendor");
			return false;
		}
		return true;
	}
	
	
	protected YFCDocument getDataAccessPolicyDocument(boolean applyAccessPolicy){
		if(applyAccessPolicy){
			return YFCDocument.getDocumentFor("<AccessPolicy ApplyAccessPolicy='Y' />");
		}else{
			return YFCDocument.getDocumentFor("<AccessPolicy ApplyAccessPolicy='N' />");
		}
	}
	
	protected String getVendorOrgCode(){
		Object obj = getTxnObject("DSVendorOrgCode");
		if(obj == null){
			return null;
		}
		return (String)obj;
	}
	
	protected boolean isBypassRequired(YFCDocument inXml, Map<String, List<String>> bypassMap){
		if(bypassMap == null || bypassMap.isEmpty()){
			return false;
		}
		Set<String> apiNames = bypassMap.keySet();
		YFCElement elemAccessPolicy = inXml.getDocumentElement();
		String apiName = elemAccessPolicy.getAttribute("APIName");
		if(YFCObject.isVoid(apiName)){   //TODO to check if we get valid api name in service execution also, and if it is void should return false or true
			Object bypassEntitiesObj = getTxnObject("DSBypassEntities");
			if(bypassEntitiesObj == null){
				return false;
			}
			try{
				List<String> bypassEntities = (List<String>)bypassEntitiesObj;
				List<String> inputEntities = getEntityNames(elemAccessPolicy);
				return bypassAllowedForEntities(bypassEntities, inputEntities);
			}catch(Exception e){
				return false;
			}
		}
		if(apiNames.contains(apiName)){
			List<String> bypassEntities = bypassMap.get(apiName);
			List<String> inputEntities = getEntityNames(elemAccessPolicy);
			return bypassAllowedForEntities(bypassEntities, inputEntities);
		}
		return false;
	}
	
	private List<String> getEntityNames(YFCElement elemAccessPolicy){
		List<String> entityNames = new ArrayList<>();
		YFCElement elemEntities = elemAccessPolicy.getChildElement("Entities");
		if(elemEntities != null && elemEntities.getChildren("Entity") != null){
			for(Iterator<YFCElement> itr = elemEntities.getChildren("Entity").iterator();itr.hasNext();){
				YFCElement elemEntity = itr.next();
				String tableName = elemEntity.getAttribute("TableName");
				if(!YFCObject.isVoid(tableName)){
					entityNames.add(tableName);
				}
			}
		}
		return entityNames;
	}
	
	private boolean bypassAllowedForEntities(List<String> bypassEntities, List<String> inputEntities){
		if(inputEntities == null || inputEntities.isEmpty()){
			return true;
		}
		if(bypassEntities == null || bypassEntities.isEmpty()){
			return false;
		}
		boolean bypass = true;
		for(String inputEntity : inputEntities){
			if(!bypassEntities.contains(inputEntity)){
				bypass = false;
				break;
			}
		}
		return bypass;
	}
	
}
