/***********************************************************************************************
 * File Name        : GpsLdapLoad.java
 *
 * Description      : Custom API for loading data into custom table GPS_LDAP_EMPLOYEE of the LDAP Users
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      16/02/2017      Keerthi Yadav                     Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.ldap.api;




import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**Sample Input xml
* <Employee FirstName=" EmployeeID='' AccountName='' EMailID='' ManagerAccountName=''/>
 */
public class GpsLdapLoad extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsLdapLoad.class);

	/** 
	 *Base Method:  The LDAP data would be stored in the GPS_LDAP_EMPLOYEE custom table which contains 
	 *EmployeeKey, EmployeeID, FirstName, AccountName, ManagerAccountName, PreviousManagerAccountName, EMailID and CTFlag as Y (Defaulted Value). 
	 * 
	 * @param yfcInDoc
	 * @return yfcInDoc
	 * @throws YFSException
	 */

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);

		/*Insert record into custom table if the Root element is Employee and invoke GpsLdapProcessor service.*/

		YFCElement eleEmployeetag = yfcInDoc.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);

		if(!YFCElement.isNull(eleEmployeetag)){
			/* Method to insert record into the custom GPS_LDAP table */
			uploadLdapData(yfcInDoc);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * 
	 * @param yfcInDoc
	 */
	private void uploadLdapData(YFCDocument yfcInDoc) {

		/*
		 * call get  api to validate if the employee exists.
		 * if the employee does not exist, create the employee record with no PreviousManagerID attribute.
		 */

		YFCElement eleinEmployeetag = yfcInDoc.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);
		String sEmployeeID = eleinEmployeetag.getAttribute(TelstraConstants.EMPLOYEE_ID,"");


		if(!YFCObject.isNull(sEmployeeID)){
			YFCDocument inXmlEmployeeList = YFCDocument.getDocumentFor("<Employee EmployeeID='"+ sEmployeeID+"' />");
			YFCDocument outXmlEmployeeList = invokeYantraService("GpsGetEmployeeList", inXmlEmployeeList);

			YFCElement eleoutEmployeeList = outXmlEmployeeList.getDocumentElement();
			YFCElement eleoutEmployee = eleoutEmployeeList.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);


			if(YFCObject.isNull(eleoutEmployee)){
				//Inserting a new record as the employee does not exist
				logger.verbose("Creating New Employee : Input Document : : "+yfcInDoc.toString());
				invokeYantraService("GpsCreateEmployee", yfcInDoc);
				
				//invoke the GpsLdapProcessor Logic
				invokeYantraService("GpsLdapProcessor", yfcInDoc);

			}else{
				/* if the employee exists, validate if the existing ManagerAccountName is the
				 *  same as the one passed in the input.*/

				boolean bIsCT = changeEmployee(yfcInDoc,outXmlEmployeeList);
				if(bIsCT) {
					//invoke the GpsLdapProcessor Logic
					outXmlEmployeeList = invokeYantraService("GpsGetEmployeeList", inXmlEmployeeList);
					eleoutEmployee = outXmlEmployeeList.getDocumentElement().getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);
					String outXmlEmployee = eleoutEmployee.toString();
					invokeYantraService("GpsLdapProcessor", YFCDocument.getDocumentFor(outXmlEmployee));
				}
			}

		}else{
			//throw exception when EmployeeId is null
			logger.verbose("Blank Employee ID Attribute has been passed");

			throw  ExceptionUtil.getYFSException(TelstraErrorCodeConstants.NULL_EMPLOYEEID_ERROR_CODE,new YFSException());	
		}

	}

	/**
	 * 
	 * @param yfcInDoc
	 * @param outXmlEmployeeList 
	 * @param sExistingManagerEmployeeID 
	 * @param sNewManagerEmployeeID 
	 */
	private boolean changeEmployee(YFCDocument yfcInDoc, YFCDocument outXmlEmployeeList) {
		/*  Compare the ManagerAccountName in the input with the existing value
		 *  If its the same, and no other attribute has changed, don't do anything.
		 *  If its the same, and other attributes have changed, call the update API with no PreviousManagerAccountName attribute.
		 *  If its not the same, call the update API with PreviousManagerAccountName attribute as the existing ManagerAccountName. 
		 */

		YFCElement eleinEmployeetag = yfcInDoc.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);
		YFCElement eleoutEmployeeList = outXmlEmployeeList.getDocumentElement();
		YFCElement eleoutEmployee = eleoutEmployeeList.getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);		

		String sNewManagerAccountName = eleinEmployeetag.getAttribute(TelstraConstants.MANAGER_ACCOUNT_NAME,"");
		String sExistingManagerAccountName = eleoutEmployee.getAttribute(TelstraConstants.MANAGER_ACCOUNT_NAME,"");

		boolean bNoDiff = compareAttibutes(eleinEmployeetag,eleoutEmployee,sNewManagerAccountName,sExistingManagerAccountName);

		if(bNoDiff && (YFCObject.isNull(sExistingManagerAccountName) || YFCObject.equals(sNewManagerAccountName, sExistingManagerAccountName))){
			//If there are no changes and existing manager is blank do no call Api and return
			logger.verbose("No changes in attributes");
			return false;
		}


		if(!YFCObject.equals(sNewManagerAccountName, sExistingManagerAccountName)){
			//In case of manger change and also other attribute change
			logger.verbose("Change Employee with New Manger Input Document : : "+yfcInDoc.toString());

			eleinEmployeetag.setAttribute(TelstraConstants.PREVIOUS_MANAGER_ACCOUNT_NAME, sExistingManagerAccountName);
			invokeYantraService("GpsChangeEmployee", yfcInDoc);
			return true;
		}else{
			//In case of other attribute change except new Manger
			logger.verbose("Change Employee with New attributes Input Document : : "+yfcInDoc.toString());

			invokeYantraService("GpsChangeEmployee", yfcInDoc);
			return true;
		}
	}

	/**
	 * This method checks if EmployeeName and EMailID has changed and returns true if same.
	 * @param eleinEmployeetag
	 * @param eleoutEmployee
	 * @param sExistingManagerAccountName 
	 * @param sNewManagerAccountName 
	 * @return boolean
	 */
	private boolean compareAttibutes(YFCElement eleinEmployeetag, YFCElement eleoutEmployee, String sNewManagerAccountName, String sExistingManagerAccountName) {

		String sNewEmployeeFirstName = eleinEmployeetag.getAttribute(TelstraConstants.FIRST_NAME,"");
		String sExistingEmployeeFirstName = eleoutEmployee.getAttribute(TelstraConstants.FIRST_NAME,"");
		
		String sNewEMailID = eleinEmployeetag.getAttribute(TelstraConstants.EMAIL_ID,"");
		String sExistingEmailID = eleoutEmployee.getAttribute(TelstraConstants.EMAIL_ID,"");
		


		if(YFCObject.equals(sNewEmployeeFirstName, sExistingEmployeeFirstName) && YFCObject.equals(sNewEMailID, sExistingEmailID) 
				&& YFCObject.equals(sNewManagerAccountName, sExistingManagerAccountName)){
			return true;
		}
		return false;
	}

}