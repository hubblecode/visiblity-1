package com.gps.hubble.file.service;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;

import javax.mail.MessagingException;

import org.apache.commons.codec.binary.Base64;
import org.apache.poi.openxml4j.exceptions.InvalidFormatException;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.FileProcessorFactory;
import com.gps.hubble.file.RowInsertService;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCConfigurator;
import com.yantra.yfs.japi.YFSException;

public class RowDataConsolidationService extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(RowInsertService.class);
	private static final int MAX_RECORD_TO_FETCH = 1000;

	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		String transactionType = inXml.getDocumentElement().getAttribute("TRANSACTION_TYPE");
		String fileUploadKey = inXml.getDocumentElement().getAttribute("FILE_UPLOAD_KEY");
		if(YFCObject.isVoid(transactionType) || YFCObject.isVoid(fileUploadKey)){
			//TODO throw exception
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.NULL_FILE_KEY_AND_TRANSACTION_TYPE, new YFSException());
		}
		
		YFCElement elemFileUpload = getFileUploadElement(fileUploadKey, transactionType);
		boolean isProcessed = elemFileUpload.getBooleanAttribute("ProcessedFlag");
		if(isProcessed){
			//This is already processed
			return inXml;
		}
		if(!isAllRowProcessed(fileUploadKey, transactionType)){
			boolean leaveService = true;
			long maxSleepCount = 10;
			long sleepInterval = 1000;
			for(int i=0;i<maxSleepCount;i++){
				try{
					Thread.sleep(sleepInterval);
					if(isAllRowProcessed(fileUploadKey, transactionType)){
						leaveService = false;
						break;
					}
				}catch(Exception e){
					break;
				}
			}
			if(leaveService){
				logger.warn("As all records for "+transactionType+" and file upload key "+fileUploadKey+ " are not processed..exiting the RowDataConsolidationService.");
				return inXml;
			}
		}
		processFileUpload(elemFileUpload, fileUploadKey, transactionType);
		invokeUploadedFileProcessorAgent(transactionType);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return inXml;
	}
	
	
	private void processFileUpload(YFCElement elemFileUpload, String fileUploadKey, String transactionType){
		logger.debug("RowDataConsolidation for fileUploadKey"+fileUploadKey+" and transactionType "+transactionType);
		String strEncodedExcel = elemFileUpload.getAttribute("FileObject");
		byte[] decodedBytes = Base64.decodeBase64(strEncodedExcel.getBytes());
		try{
			logger.debug("Creating workbook");
			Workbook workbook = createWorkbook(decodedBytes);
			logger.debug("workbook created");
			consolidateRows(workbook, transactionType, fileUploadKey, -1);
			byte[] encodedBytes = getEncodedBytes(workbook);
			updateFileUpload(elemFileUpload, encodedBytes);
			sendEmail(elemFileUpload, transactionType,fileUploadKey, workbook, encodedBytes);
		}catch(IOException ioe){
			throw new  YFSException(ioe.getMessage());
		}catch(InvalidFormatException ife){
			throw new  YFSException(ife.getMessage());
		}catch(MessagingException me){
			throw new  YFSException(me.getMessage());
		}
	}
	
  private YFCElement getFileUploadElemFromList(YFCDocument docFileUploadList){
		if(docFileUploadList == null){
			return null;
		}
		return docFileUploadList.getDocumentElement().getChildElement("FileUpload");
	}
	
	
	private void consolidateRows(Workbook workbook, String transactionType, String fileUploadKey, int rowNo){
		List<YFCElement> fileUploadRows = getRowsToConsolidate(transactionType, fileUploadKey, rowNo);
		rowNo = rowNo + MAX_RECORD_TO_FETCH;
		consolidateRows(workbook, transactionType, fileUploadKey, rowNo, fileUploadRows);
	}
	
	private void consolidateRows(Workbook workbook, String transactionType, String fileUploadKey, int rowNo, List<YFCElement> fileUploadRows){
		logger.debug("consolidateRows is getting called");
		Sheet sheet = workbook.getSheetAt(0);
		Row row = sheet.getRow(0);
		FileProcessor processor = FileProcessorFactory.create(transactionType, getServiceInvoker());
		int errorCellPosition = processor.getNumberOfCells(transactionType);
		if(errorCellPosition == 0){
			errorCellPosition = row.getPhysicalNumberOfCells();
		}
		for(YFCElement fileUploadRow : fileUploadRows){
			processRow(workbook, sheet, fileUploadRow,errorCellPosition);
		}
		List<YFCElement> nextFileUploadRows = getRowsToConsolidate(transactionType, fileUploadKey, rowNo);
		if(nextFileUploadRows.isEmpty()){
			return;
		}
		rowNo = rowNo + MAX_RECORD_TO_FETCH;
		consolidateRows(workbook, transactionType, fileUploadKey, rowNo,nextFileUploadRows);
	}
	
	public void processRow(Workbook workbook,Sheet sheet, YFCElement fileUploadRow,int errorCellPosition){
		int rowNo = fileUploadRow.getIntAttribute("RowNumber");
		Row row = sheet.getRow(rowNo);
		if(rowNo == 0){
			ExcelUtil.moveRow(workbook, row, true,"Error Message",errorCellPosition);
			ExcelUtil.moveRow(workbook, row, false,"");
			return;
		}
		boolean hasError = fileUploadRow.getBooleanAttribute("HasError");
		if(hasError){
			String errorMessageDoc = fileUploadRow.getAttribute("ErrorMessage");
			String errorMessage = getReadableDescription(errorMessageDoc);
			ExcelUtil.moveRow(workbook, row, true, errorMessage,errorCellPosition);
		}else{
			ExcelUtil.moveRow(workbook, row, false, "");
		}
		
	}
	
	private List<YFCElement> getRowsToConsolidate(String transactionType, String fileUploadKey, int rowNo){
	    YFCDocument docGetFileUploadRows = getFileUploadRowDocument(transactionType, fileUploadKey, rowNo);
	    YFCDocument docFileUploadRowOutput = invokeYantraService("GpsGetFileUploadRowList", docGetFileUploadRows);
	    List<YFCElement> fileUploadRows = new ArrayList<>();
	    if(docFileUploadRowOutput == null || docFileUploadRowOutput.getDocumentElement() == null){
	    	return fileUploadRows;
	    }
	    YFCElement elemFileUploadRows = docFileUploadRowOutput.getDocumentElement();
	    if(elemFileUploadRows.getChildren() == null){
	    	return fileUploadRows;
	    }
	    for(Iterator<YFCElement> itr = elemFileUploadRows.getChildren().iterator();itr.hasNext();){
	    	fileUploadRows.add(itr.next());
	    }
	    sort(fileUploadRows);
		return fileUploadRows;
	}
	
	private void sort(List<YFCElement> list){
		Collections.sort(list, new Comparator<YFCElement>(){
			@Override
			public int compare(YFCElement o1, YFCElement o2) {
				if(o1 == null){
					return -1;
				}
				if(o2 == null){
					return 1;
				}
				int o1RowNo = o1.getIntAttribute("RowNumber");
				int o2RowNo = o2.getIntAttribute("RowNumber");
				if(o1RowNo > o2RowNo){
					return 1;
				}else if(o1RowNo == o2RowNo){
					return 0;
				}else{
					return -1;
				}
			}
			
		});
	}
	
	private YFCDocument getFileUploadRowDocument(String transactionType, String fileUploadKey, int rowNo){
		return YFCDocument.getDocumentFor("<FileUploadRow FileUploadKey='"+fileUploadKey+"' TransactionType='"+transactionType+"' RowNumberQryType='GT' RowNumber='"+rowNo+"' MaximumRecords='"+MAX_RECORD_TO_FETCH+"' />");
	}
	
	
	
	private byte[] getEncodedBytes(Workbook workbook) throws IOException{
		ByteArrayOutputStream out = new ByteArrayOutputStream();
		workbook.write(out);
		byte[] processByteArray = out.toByteArray();
		out.close();
		byte[] encodedBytes = Base64.encodeBase64(processByteArray);
		return encodedBytes;
	}
	
	
	/**
	 * @param yfcRootEle
	 * @param processExcel
	 * @return
	 * @throws IOException
	 */
	private void updateFileUpload(YFCElement yfcRootEle, byte[] encodedBytes) throws IOException {
		logger.debug("Updating file upload");
		YFCDocument yfcChangeInDoc = YFCDocument.getDocumentFor("<UploadedFile/>");
		YFCElement yfcUploadedFileEle = yfcChangeInDoc.getDocumentElement();
		String strUploadedFileKey = yfcRootEle.getAttribute("FileUploadKey");
		yfcUploadedFileEle.setAttribute("FileUploadKey", strUploadedFileKey);
		yfcUploadedFileEle.setAttribute("FileObject", new String(encodedBytes));
		yfcUploadedFileEle.setAttribute("ProcessedFlag", "Y");
		invokeYantraService("GpsChangeFileUpload", yfcChangeInDoc);
		logger.debug("Updating file upload finished");
	}
	
	
	/**
	 * @param yfcRootEle
	 * @param strTransactionType
	 * @param processExcel
	 * @param encodedBytes
	 * @throws MessagingException
	 * @throws IOException
	 */
	private void sendEmail(YFCElement yfcRootEle, String strTransactionType,String fileUploadKey, Workbook processExcel,
			byte[] encodedBytes) throws MessagingException, IOException {
		String strToEmailId = yfcRootEle.getAttribute("NotifyEmail");
		if(!YFCCommon.isVoid(strToEmailId)){
			
			String strFromEmailId = YFCConfigurator.getInstance().getProperty("gps.email.from.id", TelstraConstants.EMAIL_FROM_ID);
			String strPort =		YFCConfigurator.getInstance().getProperty("gps.email.port", TelstraConstants.EMAIL_PORT);
			String strHost =		YFCConfigurator.getInstance().getProperty("gps.email.hostname", TelstraConstants.EMAIL_HOST);
			String sFileExtension = ".xls";
			if(processExcel instanceof org.apache.poi.xssf.usermodel.XSSFWorkbook){
				sFileExtension = ".xlsx";
			}
			String fileName = strTransactionType + "_" + fileUploadKey;
			ExcelUtil.sendExcelAttachmentEmail(encodedBytes, strToEmailId, strFromEmailId, fileName, sFileExtension, strPort, strHost);
		}
	}
	
	private Workbook createWorkbook(byte[] decodedBytes) throws IOException,InvalidFormatException{
		InputStream file = null;
		try {
			file = new ByteArrayInputStream(decodedBytes);
			Workbook workbook = WorkbookFactory.create(file);
			return workbook;
		} finally{
			if(null!=file){
				file.close();
			}
		}
	}
	
	private boolean isAllRowProcessed(String fileUploadKey, String transactionType){
		YFCDocument docGetUnprocessedRows = YFCDocument.getDocumentFor("<FileUploadRow FileUploadKey='"+fileUploadKey+"' TransactionType='"+transactionType+"' ProcessedFlag='N' />");
		YFCDocument docUnprocessedRowList = invokeYantraService("GpsGetFileUploadRowList", docGetUnprocessedRows);
		if(docUnprocessedRowList != null && docUnprocessedRowList.getDocumentElement() != null && docUnprocessedRowList.getDocumentElement().getChildren() != null && docUnprocessedRowList.getDocumentElement().getChildren().hasNext()){
			int count = 0;
			for(Iterator<YFCElement> itr = docUnprocessedRowList.getDocumentElement().getChildren();itr.hasNext();){
				count = count + 1;
			}
			return false;
		}
		return true;
	}
	
	private YFCElement getFileUploadElement(String fileUploadKey, String transactionType){
		YFCDocument docGetFileUpload = YFCDocument.getDocumentFor("<FileUpload FileUploadKey='"+fileUploadKey+"' />");
		YFCDocument docFileUploadList = invokeYantraService("GpsGetFileUploadList", docGetFileUpload);
		return getFileUploadElemFromList(docFileUploadList);
	}
	
	private void invokeUploadedFileProcessorAgent(String transactionType){
		String criteriaId = transactionType;
		YFCDocument docTriggerAgent = YFCDocument.createDocument("TriggerAgent");
		YFCElement elemTriggerAgent = docTriggerAgent.getDocumentElement();
		elemTriggerAgent.setAttribute("CriteriaId", criteriaId); //This will be based on transactionType
		YFCElement elemCriteriaAttributes = elemTriggerAgent.createChild("CriteriaAttributes");
		YFCElement elemCriteriaAttributeTransaction = elemCriteriaAttributes.createChild("Attribute");
		elemCriteriaAttributeTransaction.setAttribute("Name","TRANSACTION_TYPE");
		elemCriteriaAttributeTransaction.setAttribute("Value",transactionType);
		invokeYantraApi("triggerAgent", docTriggerAgent);
	}
	
	private String getReadableDescription(String errorDesc){
		if(YFCObject.isVoid(errorDesc)){
			return errorDesc;
		}
		if(!isDocument(errorDesc)){
			return errorDesc;
		}
		YFCDocument errorDoc = getExceptionDocument(errorDesc);
		if(errorDoc == null || errorDoc.getDocumentElement() == null){
			return errorDesc;
		}
		YFCElement elemError = errorDoc.getDocumentElement();
		String internalDesc = elemError.getAttribute("ErrorDescription");
		if(YFCObject.isVoid(internalDesc)){
			String elemErrorTagName = elemError.getTagName();
			if(elemErrorTagName.equals("Errors")){
				YFCElement elemInternalError = getErrorElement(elemError);
				if(elemInternalError != null){
					internalDesc = elemInternalError.getAttribute("ErrorDescription");
					if(!YFCCommon.isVoid(internalDesc)){
						return internalDesc;
					}
				}
			}
			return errorDesc;
		}
		return internalDesc;
	}
	
	private YFCElement getErrorElement(YFCElement elemErrors){
		if(elemErrors == null || elemErrors.getChildren("Error") == null){
			return null;
		}
		for(Iterator<YFCElement> itr = elemErrors.getChildren("Error");itr.hasNext();){
			return itr.next();
		}
		return null;
	}
	
	private YFCDocument getExceptionDocument(String errorDesc){
		try{
			return YFCDocument.getDocumentFor(errorDesc);
		}catch(Exception e){
			//The error message is normal string
		}
		return null;
	}
	
	private boolean isDocument(String errorDesc){
		return errorDesc.startsWith("<");
	}

}
