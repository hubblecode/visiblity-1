package com.gps.hubble.file.service;

import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;

import com.gps.hubble.file.Row;
import com.gps.hubble.utils.ExcelUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

public class RowDataConsolidationServiceExtender extends RowDataConsolidationService {
  
  /**
   * This method will override the processRow method in RowDataConsolidationService since the behaviour of preparing the SPS_INVENTORY_REPORT is different from
   * all other uploads.
   * 
   * @param workbook
   * @param fileUploadRow
   * @param sheet
   * @param errorCellPosition
   * 
   */
  public void processRow(Workbook workbook, Sheet sheet, YFCElement fileUploadRow, int errorCellPosition) {
    String rowObject = fileUploadRow.getAttribute("RowObject");
    YFCDocument rowObjectDoc = YFCDocument.getDocumentFor(rowObject);
    Row row = new Row(rowObjectDoc);
    moveAnalysisRow(workbook, row);
  }

  /**
   * This method will move the row to the analysis sheet.
   * @param workbook
   * @param row
   */
  public static void moveAnalysisRow(Workbook workbook, com.gps.hubble.file.Row row) {
    Sheet sheet = workbook.getSheet("Analysis Sheet");
    if (sheet == null) {
      sheet = workbook.createSheet("Analysis Sheet");
      createHeaderForAnalysis(sheet);
      return;
    }

    ExcelUtil.createApacheRow(sheet, row);
  }
  
  /**
   * This method will create the header for the analysis sheet.
   * @param sheet
   */
  private static void createHeaderForAnalysis(Sheet sheet) {
    YFCDocument spsInvReportHeaderDoc =
        YFCDocument.getDocumentFor("<Row RowNumber='0'> <Cells> "
            + "<Cell CellIndex='0' DataType='String' Value='RT Mat ID'/> "
            + "<Cell CellIndex='1' DataType='String' Value='MATNR'/> "
            + "<Cell CellIndex='2' DataType='String' Value='RT Serial'/> "
            + "<Cell CellIndex='3' DataType='String' Value='RT Barcode'/> "
            + "<Cell CellIndex='4' DataType='String' Value='RT Plant'/> "
            + "<Cell CellIndex='5' DataType='String' Value='RT Current Storage Location'/> "
            + "<Cell CellIndex='6' DataType='String' Value='Status'/> "
            + "<Cell CellIndex='7' DataType='String' Value='Plant'/> "
            + "<Cell CellIndex='8' DataType='String' Value='Location'/> "
            + "<Cell CellIndex='9' DataType='String' Value='Equipment'/> "
            + "<Cell CellIndex='10' DataType='String' Value='Inbound Delivery'/> "
            + "<Cell CellIndex='11' DataType='String' Value='ID Loc'/> "
            + "<Cell CellIndex='12' DataType='String' Value='RT HW Config'/> " + "</Cells> </Row>");
    Row row = new Row(spsInvReportHeaderDoc);
    ExcelUtil.createApacheRow(sheet, row);
  }

}
