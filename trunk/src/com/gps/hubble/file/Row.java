/**
 * 
 */
package com.gps.hubble.file;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;

/**
 * @author Shravan
 *
 */
public class Row {
	private int rowNumber;
	
	// HUB-7088 - SPS and ITEC Inventoy - Begin
	private YFCDocument rowDoc;
	public YFCDocument getRowDoc() {
      return rowDoc;
    }
  
    public void setRowDoc(YFCDocument rowDoc) {
      this.rowDoc = rowDoc;
    }
    
    public YFCElement getCell(int cellIndex){
      return cellsEle.get(cellIndex);
     }
    
    public void setCellValue(int cellIndex,String value){
      YFCElement yfcElement = cellsEle.get(cellIndex);
      if(yfcElement==null){
        YFCElement createChild = cellsEle.get(0).getParentElement().createChild("Cell");
        createChild.setAttribute("CellIndex", cellIndex);
        createChild.setAttribute("DataType", "String");
        createChild.setAttribute("Value", value);
        //create
      } else {
        yfcElement.setAttribute("Value", value);
      }
     }
    
    public int getNumberOfCells(){
      return cells.size();
    }
    //HUB-7088 - SPS and ITEC Inventoy - End
	private Map<Integer, Cell> cells;
  private Map<Integer, YFCElement> cellsEle;
	
	
	public Row(YFCDocument docRow){
		parseRowDocument(docRow);
	}
	
	private void parseRowDocument(YFCDocument docRow){
		 this.rowDoc=docRow;
	        YFCElement elemRow = docRow.getDocumentElement();
	        rowNumber = elemRow.getIntAttribute("RowNumber");
	        YFCElement elemCells = elemRow.getChildElement("Cells");
	        cells = new HashMap<>();
	        cellsEle = new HashMap<Integer, YFCElement>();
	        if(elemCells == null){
	            return;
	        }
	        YFCIterable<YFCElement> eleCell = elemCells.getChildren("Cell");
	        for(YFCElement elemCell : eleCell){
	            Cell cell = parseCell(elemCell);
	            cells.put(cell.getCellIndex(), cell);
	            cellsEle.put(cell.getCellIndex(), elemCell);
	        }
	}
	
	
	private Cell parseCell(YFCElement elemCell){
		Cell cell = new Cell();
		cell.setCellIndex(elemCell.getIntAttribute("CellIndex"));
		cell.setDataType(elemCell.getAttribute("DataType"));
		cell.setValue(elemCell.getAttribute("Value"));
		return cell;
	}
	
	public String getCellValue(int cellIndex){
		Cell cell = cells.get(cellIndex);
		if(cell == null){
			return null;
		}
		return (String)cell.getValue();
	}
	
	/*
	 * Date is serialized as string in server format..so we need to parse  
	 */
	public String getCellValue(int cellIndex, SimpleDateFormat sdf){
		Cell cell = cells.get(cellIndex);
		if(cell == null){
			return null;
		}
		String value = cell.getValue();
		if(value == null){
			return null;
		}
		if(!cell.getDataType().equalsIgnoreCase("Date")){
			return null;
		}
		YDate dateObj = new YDate(value, TelstraConstants.TELSTRA_DATE_TIME_FORMAT, false);
		//YDate dateObj = (YDate)cell.getValue();
		return dateObj.getString(sdf.toPattern());
	}
	
	public int getRowNumber(){
		return rowNumber;
	}
	
	public String getHeaderKey(){
		return getCellValue(0);
	}
	
}
