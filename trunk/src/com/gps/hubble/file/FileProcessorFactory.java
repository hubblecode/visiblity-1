package com.gps.hubble.file;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.file.agent.asn.AsnFileProcessor;
import com.gps.hubble.file.agent.integralorder.DacScheduleFileProcessor;
import com.gps.hubble.file.agent.integralorder.MaterialTransportMapFileProcessor;
import com.gps.hubble.file.agent.integralorder.MilkRunFileProcessor;
import com.gps.hubble.file.agent.integralorder.TransportMatrixFileProcessor;
import com.gps.hubble.file.agent.integralorder.WeekNumberFileProcessor;
import com.gps.hubble.file.agent.inventory.InventoryUploadFileProcessor;
import com.gps.hubble.file.agent.inventory.SPSInventoryReportFileProcessor;
import com.gps.hubble.file.agent.inventory.StockMovementFileProcessor;
//import com.gps.hubble.file.agent.inventory.InventoryUploadFileProcessor;
import com.gps.hubble.file.agent.orgload.OrgLoadFileProcessor;
import com.yantra.yfs.japi.YFSException;
/**
 * Class description goes here.
 *
 * @version 1.0 30 June 2016
 * @author VGupta
 *
 */
public final class FileProcessorFactory {
	private FileProcessorFactory(){

	}

	/**
	 * 
	 * @param sTransactionType
	 * @param serviceInvoker
	 * @return
	 */
	public static FileProcessor create(String sTransactionType, ServiceInvoker serviceInvoker) {
		FileProcessor fileProcessor = null ;

		if("ORG_LOAD".equalsIgnoreCase(sTransactionType)){
			fileProcessor = new OrgLoadFileProcessor();
		} else  if(TelstraConstants.EXCEL_INVENTORY_UPLOAD_TRANSACTION_TYPE.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new InventoryUploadFileProcessor();
		} else  if(TelstraConstants.EXCEL_STOCK_MOVEMENT_REPORT_UPLOAD_TRANSACTION_TYPE.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new StockMovementFileProcessor();
		} else  if(TelstraConstants.TRANSACTION_TYPE_WEEK_NUMBER.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new WeekNumberFileProcessor();
		} else if(TelstraConstants.TRANSACTION_TYPE_MATERIAL_TRANSPORT_MAP.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new MaterialTransportMapFileProcessor();
		} else if(TelstraConstants.TRANSACTION_TYPE_TRANSPORT_MATRIX.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new TransportMatrixFileProcessor();
		} else if(TelstraConstants.TRANSACTION_TYPE_DAC_SCHEDULE.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new DacScheduleFileProcessor();
		} else if(TelstraConstants.TRANSACTION_TYPE_MILK_RUN.equalsIgnoreCase(sTransactionType)){
			fileProcessor = new MilkRunFileProcessor();
		} else if(TelstraConstants.TRANSACTION_TYPE_ASN_UPLOAD.equalsIgnoreCase(sTransactionType)){
          fileProcessor = new AsnFileProcessor();
        } // HUB-7088 - SPS and ITEC Inventoy - Begin
		else  if(TelstraConstants.TRANSACTION_TYPE_SPS_INVENTORY_REPORT.equalsIgnoreCase(sTransactionType)){
          fileProcessor = new SPSInventoryReportFileProcessor();
        } // HUB-7088 - SPS and ITEC Inventoy - End
		else  if("".equalsIgnoreCase(sTransactionType)){
			fileProcessor = null;
		}

		if(null!= fileProcessor){
			fileProcessor.setServiceInvoker(serviceInvoker);
			return fileProcessor;
		}

		throw new YFSException("FileProcessor Parameter Missing", "FileProcessor Parameter Missing",
				"TRANSACTION_TYPE or TransactionType configuration is required");
	} 
}
