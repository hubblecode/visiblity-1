package com.gps.hubble.file.agent;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.FileProcessorFactory;
import com.gps.hubble.file.Row;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

public class RowProcessorAgent extends AbstractCustomBaseAgent{
	private static YFCLogCategory logger = YFCLogCategory.instance(RowProcessorAgent.class);
	
	@Override
	public List<YFCDocument> getJobs(YFCDocument msgXml, YFCDocument lastMsgXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		int rowNumber = -1;
		if(lastMsgXml != null){
			rowNumber = lastMsgXml.getDocumentElement().getIntAttribute("RowNumber");
		}
		String fileUploadKey = msgXml.getDocumentElement().getAttribute("FILE_UPLOAD_KEY");
		if(YFCObject.isVoid(fileUploadKey)){
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.NULL_FILE_UPLOAD_KEY, new YFSException());
		}
		String transactionType = msgXml.getDocumentElement().getAttribute("TRANSACTION_TYPE");
		if(YFCObject.isVoid(transactionType)){
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.NULL_TRANSACTION_TYPE, new YFSException());
		}
		YFCDocument docGetJobsInput  = prepareGetJobsInput(fileUploadKey, transactionType, rowNumber);
		logger.debug("docGetJobsInput is "+docGetJobsInput);
		YFCDocument docGetJobs = invokeYantraService("GpsGetFileUploadRowList", docGetJobsInput);
		List<YFCDocument> listGetJobs = new ArrayList<>();
		if(docGetJobs == null || docGetJobs.getDocumentElement() == null || docGetJobs.getDocumentElement().getChildren() == null){
			return listGetJobs;
		}
		for(Iterator<YFCElement> itr = docGetJobs.getDocumentElement().getChildren();itr.hasNext();){
			YFCElement elemGetJob = itr.next();
			listGetJobs.add(YFCDocument.getDocumentFor(elemGetJob.getString()));
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		return listGetJobs;
	}

	@Override
	public void executeJob(YFCDocument executeJobXml) {
		logger.debug("Row processing agent execute starts");
		YFCElement elemExecuteJob = executeJobXml.getDocumentElement();
		String transactionType = elemExecuteJob.getAttribute("TransactionType");
		String fileUploadKey = elemExecuteJob.getAttribute("FileUploadKey");
		String fileUploadRowKey = elemExecuteJob.getAttribute("FileUploadRowKey");
		int rowNumber = elemExecuteJob.getIntAttribute("RowNumber");
		String key = null;
		Row row = null;
		String errorMsg = "Error While Processing: ";
		FileProcessor fileProcessor = FileProcessorFactory.create(transactionType, getServiceInvoker());
		boolean hasError = false;
		String strErrorDesc = null;
		if(0 != rowNumber){
			try{
				String rowData = elemExecuteJob.getAttribute("RowObject");
				YFCDocument docRow = YFCDocument.getDocumentFor(rowData);
				row = new Row(docRow);
				key = row.getHeaderKey();
				if(fileProcessor.isRowEmpty(row)){
					hasError = true;
					strErrorDesc = TelstraConstants.EXCEL_PROCESS_EMTPY_ROW_MESSAGE;
				}else{
					fileProcessor.processRow(row);
				}
			}catch (YFSException yfsEx){
				hasError = true;
				strErrorDesc = yfsEx.getErrorDescription();
				logger.debug("YFSException.getErrorDescription got as "+strErrorDesc);
				if(YFCCommon.isVoid(strErrorDesc)){
					strErrorDesc = yfsEx.getMessage();
				}
				//Remove stack trace element if any
				strErrorDesc = removeStackTraceElement(strErrorDesc);
				logger.error("YFSException : " + errorMsg + key + ":" + strErrorDesc);
				logger.error(yfsEx);
			}catch (Exception ex){
				hasError = true;
				strErrorDesc = ex.getMessage();
				if(YFCObject.isVoid(strErrorDesc)){
					strErrorDesc = "Error While Processing: Unidentified Error"; 
				}
				logger.error(errorMsg + key + ":" + strErrorDesc);
				logger.error(ex);
			}
		}
		//Update the row record
		YFCDocument docFileUploadRow = getFileUploadRowDocument(transactionType, fileUploadKey, rowNumber,row);
		docFileUploadRow.getDocumentElement().setAttribute("FileUploadRowKey", fileUploadRowKey);
		docFileUploadRow.getDocumentElement().setAttribute("ProcessedFlag", true);
		if(hasError){
			docFileUploadRow.getDocumentElement().setAttribute("HasError", true);
			docFileUploadRow.getDocumentElement().setAttribute("ErrorMessage", getStrippedErrorMessage(strErrorDesc));
		}
		invokeYantraService("GpsChangeFileUploadRow", docFileUploadRow);
	}
	
	
	
	@Override
	protected String getLockParameters(YFSEnvironment env, Document inMessage) {
		YFCDocument yfcMessageDoc = YFCDocument.getDocumentFor(inMessage);
		String fileUploadKey = yfcMessageDoc.getDocumentElement().getAttribute("FILE_UPLOAD_KEY");
		String transactionType = yfcMessageDoc.getDocumentElement().getAttribute("TRANSACTION_TYPE");
		return "FILE_UPLOAD_KEY='"+fileUploadKey+"' AND TRANSACTION_TYPE='"+transactionType+"' ";
	}
	
	
	private YFCDocument prepareGetJobsInput(String fileUploadKey, String transactionType,int rowNumber){
		YFCDocument docGetJobsInput = YFCDocument.getDocumentFor("<FileUploadRow FileUploadKey='"+fileUploadKey+"' TransactionType='"+transactionType+"' ProcessedFlag='N' RowNumberQryType='GT' RowNumber='"+rowNumber+"' MaximumRecords='500' />");
		return docGetJobsInput;
	}
	
	private YFCDocument getFileUploadRowDocument(String transactionType, String fileUploadKey,int rowNumber, Row row){
		YFCDocument docFileUploadRow = YFCDocument.getDocumentFor("<FileUploadRow FileUploadKey='"+fileUploadKey+"' TransactionType='"+transactionType+"' RowNumber='"+rowNumber+"' />");
	    // HUB-7088 - SPS and ITEC Inventoy - Begin
		if (row != null && null != row.getRowDoc()) {
		  docFileUploadRow.getDocumentElement().setAttribute("RowObject", row.getRowDoc().getString());
		}
		// HUB-7088 - SPS and ITEC Inventoy - End
		return docFileUploadRow;
	}
	
	private String getStrippedErrorMessage(String errorMsg){
		if(errorMsg.length() > 1500){
			return errorMsg.substring(0, 1499);
		}
		return errorMsg;
	}
	
	private String removeStackTraceElement(String errorDesc){
		if(YFCObject.isVoid(errorDesc) || !isDocument(errorDesc)){
			return errorDesc;
		}
		try{
			YFCDocument errorDoc = YFCDocument.getDocumentFor(errorDesc);
			YFCElement elemError = errorDoc.getDocumentElement();
			if(elemError.getTagName() != null && elemError.getTagName().equals("Errors") && elemError.getChildren("Error") != null){
				for(Iterator<YFCElement> itr = elemError.getChildren("Error").iterator();itr.hasNext();){
					YFCElement elemChildError = itr.next();
					removeStackTraceElement(elemChildError);
				}
			}else{
				removeStackTraceElement(elemError);
			}
			return elemError.getString();
		}catch(Exception e){
			return errorDesc;
		}
	}
	
	private void removeStackTraceElement(YFCElement errorElem){
		YFCElement stackElem = errorElem.getChildElement("Stack");
		if(stackElem != null){
			errorElem.removeChild(stackElem);
		}
	}
	
	private boolean isDocument(String errorDesc){
		return errorDesc.startsWith("<");
	}
}
