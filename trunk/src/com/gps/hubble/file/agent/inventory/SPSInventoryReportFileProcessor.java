package com.gps.hubble.file.agent.inventory;

import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
public class SPSInventoryReportFileProcessor extends FileProcessor {

  
  private static YFCLogCategory logger = YFCLogCategory.instance(SPSInventoryReportFileProcessor.class);
  
  /**
   * @param row
   * This method will validate the row data and prepare the analysis data for the analysis sheet
   */
  public void processRow(Row row) {
    LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", row);
    ValidateAndPrepareAnalysisSheet(row);
    LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", row);
  }
  
  /**
   * 
   * @param row
   * This Method will compare the row data with the GPS_SPS_INV table for the MaterialID and serialNo combination,
   * If an entry exists in the GPS_SPS_INV for the MaterialId and SerialNo combination, then the corresponding SPS columns for the row object will be updated with
   * the SPS Information. Viz, Status, Plant, Location Equipment, Inbound Delivery, ID Loc columns in the analysis sheet.
   * If an entry does not exist in the GPS_SPS_INV table then no modification will be done for the Row object
   */
  private void ValidateAndPrepareAnalysisSheet(Row row) {
    String materialId = row.getCellValue(0);
    String serialNo = row.getCellValue(2);
    String equipment = row.getCellValue(4);
    
    YFCDocument getItecInventoryInDoc = YFCDocument.getDocumentFor("<ItecSerial EquipmentNo='"+equipment+"' "
        + "MaterialId='"+materialId+"' SerialNo='"+serialNo+"' />");
    YFCDocument getItecInventoryOutDoc = getServiceInvoker().invokeYantraService("GpsGetItecSerialList", getItecInventoryInDoc);
    if(getItecInventoryOutDoc != null && getItecInventoryOutDoc.getDocumentElement().hasChildNodes()) {
      setValuesInAnalysisSheet(row, getItecInventoryOutDoc);
    } else {
      setValuesInAnalysisSheet(row, null);
    }
  }
  
  private void setValuesInAnalysisSheet(Row row, YFCDocument getItecInventoryOutDoc) {
    if(getItecInventoryOutDoc != null) {
      YFCElement itecInventory = getItecInventoryOutDoc.getElementsByTagName("ItecSerial").item(0);
      row.setCellValue(0, itecInventory.getAttribute("MaterialId"));
      row.setCellValue(1, row.getCellValue(1));
      row.setCellValue(2, itecInventory.getAttribute("SerialNo"));
      row.setCellValue(3, itecInventory.getAttribute("Barcode"));
      row.setCellValue(4, itecInventory.getAttribute("Location"));
      row.setCellValue(5, itecInventory.getAttribute("Status"));
      row.setCellValue(12, itecInventory.getAttribute("RevisionNo"));
    } else {
      row.setCellValue(0, row.getCellValue(0));
      row.setCellValue(1, row.getCellValue(1));
      row.setCellValue(2, "");
      row.setCellValue(3, "");
      row.setCellValue(4, "");
      row.setCellValue(5, "");
      row.setCellValue(12, "");
    }
    row.setCellValue(6, row.getCellValue(5));
    row.setCellValue(7, row.getCellValue(3));
    row.setCellValue(8, row.getCellValue(4));
    row.setCellValue(9, row.getCellValue(1));
    row.setCellValue(10, "");
    row.setCellValue(11, "");
  }
}
