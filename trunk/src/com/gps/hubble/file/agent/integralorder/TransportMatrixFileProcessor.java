package com.gps.hubble.file.agent.integralorder;


import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.file.FileProcessor;
import com.gps.hubble.file.Row;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class TransportMatrixFileProcessor extends FileProcessor{
	private static YFCLogCategory logger = YFCLogCategory.instance(TransportMatrixFileProcessor.class);


	public void processRow(Row row) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "processRow", row);
		String matrixName = row.getCellValue( 0);
		String fromState = row.getCellValue( 1);
		String toPostCode = row.getCellValue( 2);
		String toSuburb = row.getCellValue( 3);
		if(YFCObject.isVoid(matrixName) || YFCObject.isVoid(fromState) || YFCObject.isVoid(toPostCode) || YFCObject.isVoid(toSuburb)){
			LoggerUtil.verboseLog("Mandatory parameter for transport matrix is missing", logger, " .This row will be moved to error");
//			YFCDocument erroDoc = ExceptionUtil
//					.getYFSExceptionDocument(TelstraErrorCodeConstants.TRANSPORT_MATRIX_NAME_OR_STATE_MISSING_ERROR_CODE);
//			throw new YFSException(erroDoc.toString());
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.TRANSPORT_MATRIX_NAME_OR_STATE_MISSING_ERROR_CODE, new YFSException());
		}
		matrixName = matrixName.toUpperCase();
		YFCDocument transportMatrix = YFCDocument.createDocument(TelstraConstants.TRANSPORT_MATRIX);
		YFCElement transportMatrixElem = transportMatrix.getDocumentElement();
		transportMatrixElem.setAttribute(TelstraConstants.MATRIX_NAME, matrixName.toUpperCase());
		transportMatrixElem.setAttribute(TelstraConstants.FROM_STATE, fromState.toUpperCase());
		transportMatrixElem.setAttribute(TelstraConstants.TO_POSTCODE, toPostCode.toUpperCase());
		transportMatrixElem.setAttribute(TelstraConstants.TO_SUBURB, toSuburb.toUpperCase());
		YFCDocument output = getServiceInvoker().invokeYantraService(TelstraConstants.API_GET_TRANSPORT_MATRIX, transportMatrix);
		
		String mondayTransitTime = row.getCellValue( 4);
		String tuesdayTransitTime = row.getCellValue( 5);
		String wednesdayTransitTime = row.getCellValue( 6);
		String thursdayTransitTime = row.getCellValue( 7);
		String fridayTransitTime = row.getCellValue( 8);
		transportMatrixElem.setAttribute(TelstraConstants.MONDAY_TRANSIT_TIME, mondayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.TUESDAY_TRANSIT_TIME, tuesdayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.WEDNESDAY_TRANSIT_TIME, wednesdayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.THURSDAY_TRANSIT_TIME, thursdayTransitTime);
		transportMatrixElem.setAttribute(TelstraConstants.FRIDAY_TRANSIT_TIME, fridayTransitTime);
		setDateAttributes(row, transportMatrixElem);
		if(isRecordExist(output)){
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CHANGE_TRANSPORT_MATRIX, transportMatrix);
		}else{
			getServiceInvoker().invokeYantraService(TelstraConstants.API_CREATE_TRANSPORT_MATRIX, transportMatrix);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "processRow", row);
	}
	
	private void setDateAttributes(Row row,YFCElement transportMatrixElem){
		java.text.SimpleDateFormat sdf = new java.text.SimpleDateFormat(TelstraConstants.TELSTRA_DATE_TIME_FORMAT);
		String fromDateStr = row.getCellValue( 9,sdf);
		if(!YFCObject.isVoid(fromDateStr)){
			transportMatrixElem.setAttribute("FromDate", convertToYDate(fromDateStr));
		}
		String toDateStr = row.getCellValue( 10,sdf);
		if(!YFCObject.isVoid(toDateStr)){
			transportMatrixElem.setAttribute("ToDate", convertToYDate(toDateStr));
		}
	}
	
	private static YDate convertToYDate(String dateTimeString){
		return new YDate(dateTimeString, TelstraConstants.TELSTRA_DATE_TIME_FORMAT, true);
	}
	
	private boolean isRecordExist(YFCDocument output){
		if(output == null || output.getDocumentElement() == null || output.getDocumentElement().getAttributes() == null || output.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}

}
