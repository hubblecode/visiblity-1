package com.gps.hubble.file;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

public class RowInsertService extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(RowInsertService.class);
	
	public YFCDocument invoke(YFCDocument docRowList){
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", docRowList);
		if(docRowList == null || docRowList.getDocumentElement() == null || docRowList.getDocumentElement().getChildren() == null){
			return docRowList;
		}
		YFCElement elemRowList = docRowList.getDocumentElement();
		for(YFCElement elemRow : elemRowList.getChildren()){
			YFCDocument docGetFileUploadRow = getExistingFileUploadRow(elemRow);
			if(!isRecordExist(docGetFileUploadRow)){
				YFCDocument docCreateFileUploadRow = YFCDocument.getDocumentFor(elemRow.getString());
			    invokeYantraService("GpsCreateFileUploadRow", docCreateFileUploadRow);
			}
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docRowList);
		return docRowList;
	}
	
	private YFCDocument getExistingFileUploadRow(YFCElement elemRow){
		YFCDocument docFileUploadRows = YFCDocument.createDocument("FileUploadRow");
		YFCElement elemFileUploadRows = docFileUploadRows.getDocumentElement();
		elemFileUploadRows.setAttribute("TransactionType", elemRow.getAttribute("TransactionType"));
		elemFileUploadRows.setAttribute("FileUploadKey", elemRow.getAttribute("FileUploadKey"));
		elemFileUploadRows.setAttribute("RowNumber", elemRow.getIntAttribute("RowNumber"));
		return invokeYantraService("GpsGetFileUploadRow", docFileUploadRows);
	}
	
	private boolean isRecordExist(YFCDocument output){
		if(output == null || output.getDocumentElement() == null || output.getDocumentElement().getAttributes() == null || output.getDocumentElement().getAttributes().isEmpty()){
			return false;
		}
		return true;
	}
}
