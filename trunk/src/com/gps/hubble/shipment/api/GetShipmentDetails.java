/***********************************************************************************************
 * File	Name		: GetShipmentDetails.java
 *
 * Description		: This class is called from the UI as a service to paint the shipment details page. 
 * 						It fetches the shipment details, and consolidates the duplicate container records, 
 * 						so that it can be easily used to paint the UI.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Aug 23,2016	  	Manish Kumar 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.shipment.api;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to get the shipment details with unique container information.
 * This is required by the POrtal Shipment Details page.
 * 
 * @author Manish Kumar
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */

public class GetShipmentDetails extends AbstractCustomApi
{
	private static YFCLogCategory logger = YFCLogCategory.instance(GetShipmentDetails.class);

	/*
	 * yfInDoc : Input to the service 
	 * Sample Input: 	<Shipment ShipmentKey="2016072913091874863"/>
	 * Sample Output:	Output of getShipmentDetails with Container element consolidated for multiple containers with the same 
	 * 						ContainerType, ContainerWidth, ContainerWidthUOM, ContainerHeight, ContainerHeightUOM, ContainerLength, ContainerLengthUOM, 
	 * 						ContainerGrossWeight and ContainerGrossWeightUOM attributes.
	 */
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException 
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		
		//getShipmentDetails for the passed input xml
		YFCDocument yfcDocShipmentDetails = getShipmentDetails(yfcInDoc);
		if(YFCObject.isNull(yfcDocShipmentDetails)){
			return null;
		}
		YFCElement yfcEleShipment = yfcDocShipmentDetails.getDocumentElement();
		YFCElement yfcEleContainers = yfcEleShipment.getChildElement("Containers");
		
		//If there is no Containers element, dont do anything
		if(!YFCObject.isNull(yfcEleContainers)){
			//If Containers element is not null
			
			//HashMap to store the container unique key and the container element with the NoOfContainers attribute.
			HashMap<String, YFCElement> mapContainerElement = new HashMap<String, YFCElement>();
			
			YFCNodeList<YFCElement> yfcNLContainer = yfcEleContainers.getElementsByTagName("Container");
			for(int i=0; i<yfcNLContainer.getLength(); i++)
			{
				//Iterate through all the Container elements (from the output of getShipmentDetails
				//And prepare the key to find duplicate container records.
				YFCElement yfcEleContainer = (YFCElement)yfcNLContainer.item(i);
				String strContainerType = yfcEleContainer.getAttribute("ContainerType", "");
				String strContainerWidth = yfcEleContainer.getAttribute("ContainerWidth", "");
				String strContainerWidthUOM = yfcEleContainer.getAttribute("ContainerWidthUOM", "");
				String strContainerLength = yfcEleContainer.getAttribute("ContainerLength", "");
				String strContainerLengthUOM = yfcEleContainer.getAttribute("ContainerLengthUOM", "");
				String strContainerHeight = yfcEleContainer.getAttribute("ContainerHeight", "");
				String strContainerHeightUOM = yfcEleContainer.getAttribute("ContainerHeightUOM", "");
				String strContainerGrossWeight = yfcEleContainer.getAttribute("ContainerGrossWeight", "");
				String strContainerGrossWeightUOM = yfcEleContainer.getAttribute("ContainerGrossWeightUOM", "");
				//Preparing the key attribute the identify duplicate containers.
				//The key is the concatenated value of ContainerType, ContainerWidth, ContainerWidthUOM,
				//ContainerHeight, ContainerHeightUOM, ContainerLength, ContainerLengthUOM,
				//ContainerGrossWeight and ContainerGrossWeightUOM attributes.
				String strUniqueContainer = strContainerType + strContainerWidth + strContainerWidthUOM
												+ strContainerLength + strContainerLengthUOM + strContainerHeight
												+ strContainerHeightUOM + strContainerGrossWeight + strContainerGrossWeightUOM;
				
				if(mapContainerElement.containsKey(strUniqueContainer))
				{
					//The key is already present in the map.
					//Increase the NoOfContainers by 1.
					YFCElement yfcEleSavedContainer = mapContainerElement.get(strUniqueContainer);
					int iNoOfContainer = yfcEleSavedContainer.getIntAttribute("NoOfContainers");
					iNoOfContainer = iNoOfContainer + 1;
					yfcEleSavedContainer.setAttribute("NoOfContainers", iNoOfContainer);
					mapContainerElement.put(strUniqueContainer, yfcEleSavedContainer);	
				}
				else
				{
					//The key is not present in the map.
					//Set the NoOfContainers to 1.
					yfcEleContainer.setAttribute("NoOfContainers", 1);
					mapContainerElement.put(strUniqueContainer, yfcEleContainer);
				}
			}
			
			Set setContainer = mapContainerElement.entrySet();
			Iterator itrContainer = setContainer.iterator();
			//Remove the Containers element from the output of getShipmentDetails. This will remove all the child Container elements (if any).
			yfcEleShipment.removeChild(yfcEleContainers);
			//Add a new Containers element to the output of getShipmentDetails.
			YFCElement yfcEleNewContainers = yfcEleShipment.createChild("Containers");
			while(itrContainer.hasNext())
			{
				//Iterate through the map.
				Map.Entry mapEntryContainer = (Map.Entry)itrContainer.next();
				YFCElement yfcEleUniqueContainerMap = (YFCElement) mapEntryContainer.getValue();
				//Append the unique Container elements to the newly added Containers element.
				yfcEleNewContainers.appendChild(yfcEleUniqueContainerMap);
			}
		}
		
		//Return the modified output of getShipmentDetails
		return yfcDocShipmentDetails;
	}

	private YFCDocument getShipmentDetails(YFCDocument yfcInDoc) {
		
		YFCDocument yfcTemplateDoc = YFCDocument.getDocumentFor("<Shipment>"
					+ "<ShipmentLines>"
						+ "<ShipmentLine/>"
						+ "</ShipmentLines>"
					+ "<FromAddress />"
					+ "<ToAddress />"
					+ "<Extn/>"
					+ "<Containers>"
						+ "<Container ContainerType='' ContainerNo='' ContainerHeightUOM='' ContainerHeight='' "
							+ "ContainerLengthUOM='' ContainerLength='' ContainerWidthUOM='' ContainerWidth='' "
							+ "ContainerVolumeUOM='' ContainerVolume='' ContainerGrossWeightUOM='' ContainerGrossWeight=''/>"
					+ "</Containers>"
				+ "</Shipment>");
		YFCDocument yfcDocShipmentDetailsOut = invokeYantraApi("getShipmentDetails", yfcInDoc, yfcTemplateDoc);
		//YFCDocument yfcDocShipmentDetailsOut = YFCDocument.getDocumentForXMLFile("C:\\Users\\Manish\\Desktop\\Test\\TestShipmentDetails.xml");
		return yfcDocShipmentDetailsOut;
	}

}
