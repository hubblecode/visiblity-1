/***********************************************************************************************
 * File Name        : CarrierUpdate.java
 *
 * Description      : Custom API for Carrier updates to update the shipment to Intransit 
 * 						(alongwith the intransit updates), Delivered (alongwith Proof 
 * 						Of Delivery) or Futile.
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0                      Murali								Initial Version
 * 1.1      Jan 13,2017     Manish Kumar						Fixed defects HUB-8120, HUB-8121
 * 1.2		Jan 18, 2017	Prateek Kumar						Fixed HUB-8301 : Updating the carrier update table if the shipment is already delivered/futile and carrier update comes for it.
 * 1.3		May 23, 2017	Prateek Kumar						Fixed defect HUB-9093
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.carrier.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

public class CarrierUpdate extends AbstractCustomApi {

    private static YFCLogCategory logger = YFCLogCategory.instance(CarrierUpdate.class);
    private String sOrderNo="";
    /**
     * Base Method execution starts here
     * 
     */
    @Override
    public YFCDocument invoke(YFCDocument inXml) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
        try{
            boolean bShipmentLinesPassed = validateInput(inXml);
            getOrderLineList(inXml, bShipmentLinesPassed);
            YFCElement inRootEle = inXml.getDocumentElement();
            String strTrackingNo = inRootEle.getAttribute(TelstraConstants.TRACKING_NO,"");
            String strShipmentNo = inRootEle.getAttribute(TelstraConstants.SHIPMENT_NO,"");
            String strOrderName = inRootEle.getAttribute(TelstraConstants.ORDER_NAME,"");
            
            String strDocumentType = getDocumentType(strOrderName);

            
            if(YFCObject.isNull(strDocumentType)) {
            	/*
                 * If the Document Type is null, the order does not exist in the system.
                 * It is possible that carrier update is adhoc logistics for ASN (with Book Freight). 
                 * In this case, the OrderName attribute will be the ShipmentNo. 
                 */

                if(!YFCObject.isNull(strOrderName)) {
                	/*
                	 * This logic takes care of the following scenarios:
                	 * If no order exists with the passed OrderName, it is 
                	 * possible that the carrier update is for adhoc logistics shipment or an ASN for which the 
                	 * freight was booked using the Vendor Portal. In this case, the Carrier Update logic  
                	 * will be called with the ShipmentNo attribute instead of the OrderName attribute.
                	 */

                    inRootEle.setAttribute(TelstraConstants.SHIPMENT_NO,strOrderName);
                    strShipmentNo = strOrderName;
                    inRootEle.removeAttribute(TelstraConstants.ORDER_NAME);
                    strOrderName ="";
                }
            }
            
            //Call getShipment with the tracking number
            YFCDocument getShipListOutXml = getShipmentList(inRootEle);
            /*
             * If OrderName and (ShipmentNo and/or TrackingNo) is passed (Either ShipmentNo or TrackingNo should be passed),
             * 		- 0 shipments are returned, it is possible that the shipment has already been delivered/futile, or
             * 			the shipment has not yet been created (for a SO/TO), or 
             * 			the ASN does not have the tracking number (for a PO).
             * 		- 1 or more shipments are returned, update the shipments.
             * 
             * If only OrderName is passed, 
             * 		- 0 shipment is returned, it is possible that the carrier update message has reached before the pick 
             * 			and ship confirmation messages
             * 		- 1 shipment is returned, update the shipment
             * 		- 2 or more shipments are returned, throw an exception. This is because the system cannot determine
             * 			shipment to update
             * 
             * 
             */

            // HUB-8057: Carrier Update Fails [START]
            String shipmentCount = getShipListOutXml.getDocumentElement().getAttribute("TotalNumberOfRecords","");
            if(shipmentCount.equals("0")) {
                /*
                 * If the number of shipments with the passed tracking or shipment number is zero, 
                 * there are few possibilities:
                 * 1. The tracking number (if passed in the input) is not stamped on the shipment
                 *      -This could be because the ASN does not have the tracking number (valid only for POs)
                 *      -Or, The ship confirm for SO/TO was not processed.
                 * 2. The shipment has already been delivered/received or marked futile.
                 *  
                 */
                
                if(!YFCObject.isNull(strTrackingNo) || !YFCObject.isNull(strShipmentNo))
                {
                    /*
                     * The check of not null Tracking No or Shipment No is done to ensure that the update 
                     * does not happen for other shipments.
                     * If both the tracking number and the shipment number are not passed,
                     * don't get in the method to update the carrier message   
                     */
                    if(updateCarrierMessage(inXml)) {
                        /*
                         * This takes care of the scenario: 
                         * The shipment has already been delivered/received or marked futile.
                         * 
                         * If shipments were found with the passed tracking number and/or the shipment no,
                         * then update the shipments and exit.
                         */
                        return inXml;
                    }
                }
                
                if(!YFCObject.isNull(strTrackingNo))
                {
                    /*
                     * This takes care of the scenario:
                     * The tracking number (if passed in the input) is not stamped on the shipment
                     */
                    
                    if(bShipmentLinesPassed && (strDocumentType.equals(TelstraConstants.DOCUMENT_TYPE_0001) || 
                            strDocumentType.equals(TelstraConstants.DOCUMENT_TYPE_0006)) ) {
                        /*
                         * If the carrier update is for the a sales order or a transfer order
                         * and shipment lines are passed in the carrier update.
                         * call Ship Confirm logic.
                         */
                        
                        /*
                         * If there are zero or more shipments (with no tracking number) for SO/TO, 
                         * the pick confirm message was not received/processed (if zero shipments)
                         * or the ship confirm message was not received/processed (if one or more shipments).
                         * Prepare an input to ship confirm 
                         * (as, ship confirm will internally call pick confirm, if the order has not been picked),
                         * and call GpsShipOrder service.
                         * Do this only if shipment lines are passed in the carrier update messages.
                         */
                        
                        YFCDocument shipConfirmDocument = prepareInputForShipConfirm(inRootEle, strTrackingNo, strOrderName);
                        if(YFCObject.equals(shipConfirmDocument.getDocumentElement().getAttribute("CallShipOrder"), "Y")) {
                          invokeYantraService("GpsShipOrRejectOrder", shipConfirmDocument);
                        } else {
                          YFCDocument getShipListInDoc = YFCDocument.getDocumentFor(inRootEle.toString());
                          getShipListInDoc.getDocumentElement().removeAttribute("TrackingNo");
                          getShipListOutXml = getShipmentList(getShipListInDoc.getDocumentElement());
                          YFCNodeList<YFCElement> shipments = getShipListOutXml.getElementsByTagName("Shipment");
                          List<String> shipmentList = new ArrayList<String>();
                          for(YFCElement shipment : shipments) {
                              boolean allLinesPresentInShipment = true;
                              YFCNodeList<YFCElement> inShipmentLines = inRootEle.getElementsByTagName("ShipmentLine");
                              for(YFCElement inShipmentLine : inShipmentLines) {
                                String plNo = inShipmentLine.getAttribute("PrimeLineNo");
                                YFCElement matchingEle = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(shipment.getString()), "//Shipment/ShipmentLines/ShipmentLine[@PrimeLineNo='"+plNo+"']");
                                if(YFCObject.isVoid(matchingEle)) {
                                  allLinesPresentInShipment = false;
                                }
                              }
                              if(allLinesPresentInShipment) {
                                shipmentList.add(shipment.getAttribute("ShipmentKey"));
                              }
                          }
                          if(shipmentList.size() > 1 || shipmentList.size() == 0) {
                            // TODO throw an exception
                            LoggerUtil.verboseLog("Unable to determine the shipments to Deliver", logger, " throwing exception");
                            throw ExceptionUtil.getYFSException("Unable to Determine the Shipments To Deliver", "Unable to Determine the Shipments to Deliver "+shipmentList, new YFSException());
                          } else {
                            YFCDocument confirmShipmentInDoc = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+shipmentList.get(0)+"' TrackingNo='"+strTrackingNo+"' />");
                            invokeYantraApi("confirmShipment", confirmShipmentInDoc);
                          }
                        }
                        
                        /*
                         * As a new shipment is created, calling getShipment list again.
                         */
                        getShipListOutXml = getShipmentList(inRootEle);
                        YFCNodeList<YFCElement> yfcNlshipment = getShipListOutXml.getElementsByTagName(TelstraConstants.SHIPMENT);
                        updateCarrierStatus(inXml, yfcNlshipment, strTrackingNo, true);
                        // returning from the code as carrier update has already been processed
                        return inXml;
                    }
                    else {
                        /*
                         * If the shipment lines are not passed in the carrier update.
                         */
                        //call getShipmentlist without the tracking number
                        getShipListOutXml = getShipmentList(inRootEle, false);
                        //the output contains all the shipments with blank/no tracking number. 
                        int iShipmentCount = ((getShipListOutXml.getDocumentElement())
                                                                .getElementsByTagName(TelstraConstants.SHIPMENT))
                                                                .getLength();
                        if(iShipmentCount==0) {
                            /*
                             * If there are no shipments for a PO, the ASN has not yet been created, 
                             * so this is an invalid carrier update.
                             * Throw an exception
                             */
                            
                            /*
                             * If there are no shipments for SO/TO, the pick confirm message was not received/processed.
                             * Prepare an input to ship confirm 
                             * (as, ship confirm will internally call pick confirm, if the order has not been picked),
                             * and call GpsShipOrRejectOrder service.
                             * Do this only if shipment lines are passed in the carrier update messages.
                             * Else, throw an exception 
                             * The logic to call ship confirm service has been handled aleardy   
                             */
                            LoggerUtil.verboseLog("No Shipment Found", logger, " throwing exception");
                            throw ExceptionUtil.getYFSException("TEL_ERR_1171_003", new YFSException());
                            
                        }
                        if(iShipmentCount==1) {
                            /*
                             * If there is only one shipment (without a tracking number) for a PO, 
                             * it can be assumed that the carrier update is for the ASN.
                             * Update the tracking number on the shipment.
                             * Do the carrier update.
                             */

                            /*
                             * If there is only one shipment (without a tracking number) for a SO/TO.
                             * Call GpsShipOrder service, if shipment lines are passed in the carrier update messages.
                             * This will ensure that the shipment is confirmed (if the lines match),
                             * or a new shipment is picked and shipped if the lines dont match; 
                             * and the tracking number updated.
                             * Do the carrier update.
                             * 
                             * If shipment lines are not passed in the carrier update message, 
                             * it can be assumed that the carrier update is for the shipment.
                             * Update the tracking number on the shipment.
                             * Do the carrier update. 
                             */
                             //Call changeShipment to update the tracking number
                             //Carrier Update
                            
                             YFCNodeList<YFCElement> yfcNlshipment = getShipListOutXml.getElementsByTagName(TelstraConstants.SHIPMENT);
                             updateCarrierStatus(inXml, yfcNlshipment, strTrackingNo, true);
                             // returning from the code as carrier update has already been processed
                             return inXml;
                        }
                        else {
                            /*
                             * If there are more than one shipments (without a tracking number) for a PO,
                             * throw an exception.
                             */
                            
                            /*
                             * If there are more than one shipments (without a tracking number) for a SO/TO.
                             * Call GpsShipOrder service, if shipment lines are passed in the carrier update messages.
                             * This will ensure that the shipment is confirmed (if the lines match),
                             * or a new shipment is picked and shipped if the lines dont match; 
                             * and the tracking number updated.
                             * Do the carrier update.
                             * 
                             * If shipment lines are not passed in the carrier update message, 
                             * throw an exception. 
                             */
                            LoggerUtil.verboseLog("Too Many Shipments Found", logger, " throwing exception");
                            throw ExceptionUtil.getYFSException("TEL_ERR_1171_004", new YFSException());
                        }
                    }
                }
                else {
                	/*
                	 * If the number of shipments is zero and no condition above could be met, throw an exception.
                	 * It may happen if the the GpscarrierUpdate is called for an AdhocLogistics, or ASN for 
                	 * which the book freight was done from the Vendor Portal. 
                	 */
                	LoggerUtil.verboseLog("Couldnt find a Shipment to update", logger, " throwing exception");
                    throw ExceptionUtil.getYFSException("TEL_ERR_1171_006", new YFSException());
                }
            }
            else {
            	/*
                 * If the number of shipments with the passed tracking or shipment number 1 or more, update the shipments.
                 * 
                 * If the number of shipments with just the order name passed in the input is 1, update the shipment.
                 * If the number of shipments with just the order name passed in the input is more than 1, 
                 * throw an exception. This is because the system cannot determine shipment to update.
                 */
            	
            	if(!YFCObject.isNull(strTrackingNo) || !YFCObject.isNull(strShipmentNo)) {
            		/*
            		 * HUB-8080 [Begin]
            		 * if there are multiple shipment with the same tracking number, 
            		 * pass the complete shipment list to the updateCarrierStatus method.
            		 * In that method loop through each shipment and update the status for each shipment
            		 */
            		YFCNodeList<YFCElement> yfcNlshipment = getShipListOutXml.getElementsByTagName(TelstraConstants.SHIPMENT);
                    updateCarrierStatus(inXml,yfcNlshipment);
            	}
            	else if(!YFCObject.isNull(strOrderName)) {
            		/*
            		 * HUB-8120 [Begin] 
            		 * Multiple Shipments get delivered when Order Name only is passed in the carrier update message.
            		 * When tracking and or the shipment number is not passed, the expectation is that an exception 
            		 * should be thrown, if multiple shipments are returned. This is because it can not be determined 
            		 * by Sterling that the carrier update is for one shipment or multiple shipments.
            		 */
            		if(shipmentCount.equals("1")) {
            			YFCNodeList<YFCElement> yfcNlshipment = getShipListOutXml.getElementsByTagName(TelstraConstants.SHIPMENT);
                        updateCarrierStatus(inXml,yfcNlshipment);
            		}
            		else {
            			LoggerUtil.verboseLog("Too Many Shipments Found", logger, " throwing exception");
                        throw ExceptionUtil.getYFSException("TEL_ERR_1171_004", new YFSException());
            		}
            	}
            	else {
            		/*
                	 * If none of the 3 attributes: OrderName, ShipmentNo and TrackingNo is passed
                	 * throw an exception. 
                	 */
                	LoggerUtil.verboseLog("Couldn't identify any logic to update the shipment", logger, " throwing exception");
                    throw ExceptionUtil.getYFSException("TEL_ERR_1171_007", new YFSException());
            	}
            	
            }

        }
        catch (YFSException exp) {
            LoggerUtil.verboseLog("Carrier Update Failure ", logger, exp);
            throw exp;
        }
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
        return inXml;
    }

    
    private void getOrderLineList(YFCDocument inXml, boolean isShipmentLinePassed) {

    	String sOrderName = inXml.getDocumentElement().getAttribute("OrderName");
    	if(!YFCCommon.isVoid(sOrderName)){
    		YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor("<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='"+sOrderName+"' /></OrderLine>");
    		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
    				+ "<OrderLine PrimeLineNo=''><Order OrderName='' /></OrderLine>"
    				+ "</OrderLineList>");
    		YFCDocument outputGetOrderLineList = invokeYantraApi("getOrderLineList", inDocgetOrdLineList, templateForGetOrdLineList);
    		if(!YFCObject.isVoid(outputGetOrderLineList) 
    				&& outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 0) {
    			if(outputGetOrderLineList.getDocumentElement().getDoubleAttribute("TotalLineList") > 1) {
    				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID, new YFSException());
    			}
    			String primeLineNo = outputGetOrderLineList.getElementsByTagName("OrderLine").item(0).getAttribute("PrimeLineNo");
    			String orderName = outputGetOrderLineList.getElementsByTagName("Order").item(0).getAttribute("OrderName");
    			inXml.getDocumentElement().setAttribute("OrderName", orderName);
    			if(isShipmentLinePassed) {
    				inXml.getElementsByTagName("ShipmentLine").item(0).setAttribute("PrimeLineNo", primeLineNo);
    			}
    		}
    	}
    }

    /**
     * This method prepares the input to ship confirm logic by removing the Extn element and
     * adding Action="BACKORDER" for all the lines
     * @param inXml
     * @return
     */
    private YFCDocument prepareInputForShipConfirm(YFCElement inEle, String strTrackingNo, String strOrderName) {
        YFCDocument shipConfirmInDoc = YFCDocument.getDocumentFor("<Shipment OrderName='"+strOrderName+"' "
                                                                            + "TrackingNo='"+strTrackingNo+"' CallShipOrder='Y'/>");
        YFCElement shipConfirmEle = shipConfirmInDoc.getDocumentElement();
        YFCElement shipmentLinesEle = shipConfirmEle.createChild("ShipmentLines");
        
        YFCNodeList<YFCElement> shipmentLineNL = inEle.getElementsByTagName("ShipmentLine");
        for(YFCElement shipmentLineEle : shipmentLineNL){
        	//HUB-9093 - Begin
        	if(shipmentLineEle.hasAttribute(TelstraConstants.QUANTITY)){
        		shipmentLineEle.setAttribute(TelstraConstants.STATUS_QUANTITY, shipmentLineEle.getAttribute(TelstraConstants.QUANTITY));
        		shipmentLineEle.removeAttribute(TelstraConstants.QUANTITY);
        	}
        	//HUB-9093 - End
            if(YFCObject.isVoid(shipmentLineEle.getAttribute("StatusQuantity"))) {
              return YFCDocument.getDocumentFor("<Shipment CallShipOrder='N' />");
            }
            if(YFCObject.isVoid(shipmentLineEle.getAttribute("PrimeLineNo"))) {
              LoggerUtil.verboseLog("Invalid Input XML", logger, " throwing exception");
              throw  ExceptionUtil.getYFSException("Invalid Input XML", "Invalid Input XML",new YFSException());
            }
            //add the shipment element to the noTrackingNoShipmentsEle
            YFCElement shipLineConfirmEle = shipConfirmInDoc.importNode(shipmentLineEle, true);            
            shipLineConfirmEle.setAttribute("Action", "BACKORDER");
            shipmentLinesEle.appendChild(shipLineConfirmEle);
        }
        return shipConfirmInDoc;
    }


    /**
     * This method returns the document type of the passed OrderName
     * @param strOrderName
     * @return
     */
    private String getDocumentType(String strOrderName) 
    {
        YFCDocument getOrderListInDoc = YFCDocument.getDocumentFor("<Order OrderName='"+strOrderName+"'/>");
        YFCDocument getOrderListTemplate = YFCDocument.getDocumentFor("<OrderList><Order DocumentType=''/></OrderList>");
        YFCDocument getOrderListOutDoc = invokeYantraApi(TelstraConstants.GET_ORDER_LIST, getOrderListInDoc, getOrderListTemplate);
        YFCElement orderListEle = getOrderListOutDoc.getDocumentElement();
        if(orderListEle.hasChildNodes())
        {
            YFCElement orderEle = orderListEle.getChildElement("Order");
            sOrderNo = orderEle.getAttribute(TelstraConstants.ORDER_NO);
            return orderEle.getAttribute("DocumentType","");
        }
        return "";
    }


    /**
     * This method will validate of the carrier status received is correct or not.
     * If it is not correct, an exception will be thrown before any other api is called.
     * If it is valid, it will return true if the carrier update has shipment lines.
     * Else, it will return false.
     * Based on this flag (if true), and if the document type is sales order or transfer order, 
     * the ship confirm logic will be called.
     * @param inXml
     */
    private boolean validateInput(YFCDocument inXml) {
        String carrierInTransit =  getProperty("INTRANSIT_STATUS",true);
        String carrierDelivered = getProperty("DELIVERED_STATUS",true);
        String carrierFailed = getProperty("FAILED_STATUS",true);
        
        String carrierStatus = XPathUtil.getXpathAttribute(inXml, "//CarrierUpdate/@TransportStatus");

        //throw an exception if invalid transport status is passed
        if(!(carrierInTransit.equals(carrierStatus) || carrierFailed.equals(carrierStatus) || 
                carrierDelivered.equals(carrierStatus))){
            LoggerUtil.verboseLog("Shipment Status was not Found", logger, " throwing exception");
            String strErrorCode = getProperty("InvalidStatusFailureCode", true);
            throw  ExceptionUtil.getYFSException(strErrorCode,new YFSException());  
        }
        
        YFCElement shipmentEle = inXml.getDocumentElement();
        YFCElement firstShipmentLineEle = shipmentEle.getElementsByTagName("ShipmentLine").item(0);
        if(YFCObject.isNull(firstShipmentLineEle)){
            //If ShipmentLine element is not in the carrier update input, return false
            return false;
        }
        else{
            /*if(YFCObject.isNull(firstShipmentLineEle.getAttribute("ItemID")) ||
                    YFCObject.isNull(firstShipmentLineEle.getAttribute("PrimeLineNo")) ||
                    YFCObject.isNull(firstShipmentLineEle.getAttribute("StatusQuantity")) ){
                //If ShipmentLine element is passed, but any one of the attributes is missing, return false
                return false;
            }
            else{*/
                //If ShipmentLine element is passed, and all the attributes are there, return true
                return true;
            //}
        }
    }

    /**
     * Call getShipmentlist by passing the tracking number
     * @param inEle
     * @return
     */
    private YFCDocument getShipmentList(YFCElement inEle) {
        return getShipmentList(inEle, true);
    }

    /**
     * Method to invoke getShipmentList to get input 
     * shipment information
     * 
     * @param inEle
     * @return
     */
    private YFCDocument getShipmentList(YFCElement inEle, boolean passTrackingNo) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getShipmentStatus", inEle);
        YFCDocument getShipListXml = YFCDocument.getDocumentFor("<Shipment />");
        YFCElement shipListEle = getShipListXml.getDocumentElement();
        shipListEle.setAttribute(TelstraConstants.ORDER_NO, inEle.getAttribute(TelstraConstants.ORDER_NO));
        //FROM_STATUS is 1100 (Shipment Created)
        shipListEle.setAttribute(TelstraConstants.FROM_STATUS, getProperty(TelstraConstants.FROM_STATUS,true));
        //TO_STATUS is 1400.10000 (Shipment Intransit)
        //GetShipmentList will return the shipments in ToStatus as well.
        shipListEle.setAttribute(TelstraConstants.TO_STATUS, getProperty(TelstraConstants.TO_STATUS,true));
        shipListEle.setAttribute(TelstraConstants.SHIPMENT_NO, 
                inEle.getAttribute(TelstraConstants.SHIPMENT_NO));
        if(passTrackingNo){
            shipListEle.setAttribute(TelstraConstants.TRACKING_NO, inEle.getAttribute(TelstraConstants.TRACKING_NO));
        }
        shipListEle.createChild(TelstraConstants.SHIPMENT_LINES).createChild(TelstraConstants.SHIPMENT_LINE)
        .createChild(TelstraConstants.ORDER).setAttribute(TelstraConstants.ORDER_NAME,
                inEle.getAttribute(TelstraConstants.ORDER_NAME));
        shipListEle.setAttribute("StatusQryType", "BETWEEN");
        YFCDocument tempXml = YFCDocument.getDocumentFor("<Shipments TotalNumberOfRecords=''>"
                + "<Shipment DocumentType='' ShipmentNo='' ShipmentKey='' Status='' TrackingNo=''><ShipmentLines><ShipmentLine PrimeLineNo='' /></ShipmentLines></Shipment></Shipments>");
        YFCDocument getShipmentlistOutDoc = invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, getShipListXml, tempXml);
        if(!passTrackingNo)
        {
            YFCDocument noTrackingNoShipmentsDoc = YFCDocument.getDocumentFor("<Shipments/>");
            YFCElement noTrackingNoShipmentsEle = noTrackingNoShipmentsDoc.getDocumentElement();
            //if no tracking number is passed, only return shipments without any tracking number
            YFCElement outShipListEle = getShipmentlistOutDoc.getDocumentElement();
            YFCNodeList<YFCElement> outShipmentNL = outShipListEle.getElementsByTagName(TelstraConstants.SHIPMENT);
            for(YFCElement outShipmentEle : outShipmentNL){
                String strTrackingNo = outShipmentEle.getAttribute(TelstraConstants.TRACKING_NO,"");
                if(YFCObject.isNull(strTrackingNo))
                {
                    //add the shipment element to the noTrackingNoShipmentsEle
                    YFCElement nullShipmentEle = noTrackingNoShipmentsDoc.importNode(outShipmentEle, true);
                    noTrackingNoShipmentsEle.appendChild(nullShipmentEle);
                }
            }
            return noTrackingNoShipmentsDoc;
        }
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getShipmentStatus", "Get Shipment List Success");
        return getShipmentlistOutDoc;
            
    }


    /**
     * Method to change Shipment status based on the Carrier Information
     * 
     * If Carrier Status is Consignment Picked Up then Shipment status is 
     * In transit.
     * If Carrier Status is Unsuccessful then Shipment status is Futile Delivery
     * @param shipmentKey
     * @param transactionId
     * @param baseDropStatus
     */
    private void changeShipmentStatus(String shipmentKey,String transactionId, String baseDropStatus) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeShipmentStatus", "ShipmentKey :"+shipmentKey+
                "BaseDropStatus :"+baseDropStatus);
        YFCDocument changeShipStatXml = YFCDocument.getDocumentFor("<Shipment />");
        changeShipStatXml.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_KEY, shipmentKey);
        changeShipStatXml.getDocumentElement().setAttribute("TransactionId", transactionId);
        changeShipStatXml.getDocumentElement().setAttribute("BaseDropStatus", baseDropStatus);
        invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT_STATUS_API, changeShipStatXml);
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeShipmentStatus", "Change Shipment Status Success");
    }
    
    /**
     * Method to change Order status based on the Carrier Information
     * 
     * If Carrier Status is Consignment Picked Up then Order status is 
     * In transit.
     * If Carrier Status is Unsuccessful then Order status is Futile Delivery
     * If Carrier Status is Successful then Order status is Delivered
     * @param shipmentKey
     * @param transactionId
     * @param baseDropStatus
     */
    private void changeOrderStatus(String shipmentKey,String transactionId, String baseDropStatus) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeOrderStatus", "ShipmentKey :"+shipmentKey+
                "BaseDropStatus :"+baseDropStatus);
        
        YFCDocument changeOrderStatusInDoc = YFCDocument.getDocumentFor("<OrderStatusChange "
                                        + "BaseDropStatus='"+baseDropStatus+"' TransactionId='"+transactionId+"'>"
                                        + "<OrderLines/></OrderStatusChange>");
        YFCElement orderStatusChangeELe = changeOrderStatusInDoc.getDocumentElement();
        YFCElement orderLinesEle = orderStatusChangeELe.getChildElement("OrderLines");
        
        YFCDocument getShipmentLineListInDoc = YFCDocument.getDocumentFor("<ShipmentLine ShipmentKey='"+shipmentKey+"'/>");
        YFCDocument getShipmentLineListTemp = YFCDocument.getDocumentFor("<ShipmentLines>"
                                + "<ShipmentLine OrderHeaderKey='' OrderLineKey='' OrderReleaseKey='' Quantity=''/></ShipmentLines>");
        YFCDocument getShipmentLineListOutDoc = invokeYantraApi("getShipmentLineList", getShipmentLineListInDoc, getShipmentLineListTemp);
        YFCElement shipmentLinesEle = getShipmentLineListOutDoc.getDocumentElement();
        YFCNodeList<YFCElement> shipmentLineNL = shipmentLinesEle.getElementsByTagName("ShipmentLine");
        for(int i=0; i<shipmentLineNL.getLength(); i++)
        {
            YFCElement shipmentLineEle = (YFCElement)shipmentLineNL.item(i);
            String strOrderLineKey = shipmentLineEle.getAttribute("OrderLineKey");
            String strOrderReleaseKey = shipmentLineEle.getAttribute("OrderReleaseKey","");
            String strQuantity = shipmentLineEle.getAttribute("Quantity");
            
            if(i==0)
            {
                String strOrderHeaderKey = shipmentLineEle.getAttribute("OrderHeaderKey");
                orderStatusChangeELe.setAttribute("OrderHeaderKey", strOrderHeaderKey);
            }
            
            YFCElement orderLineEle = orderLinesEle.createChild("OrderLine");
            orderLineEle.setAttribute("OrderLineKey", strOrderLineKey);
            if(!strOrderReleaseKey.equals(""))
            {
                orderLineEle.setAttribute("OrderReleaseKey", strOrderReleaseKey);
            }
            orderLineEle.setAttribute("Quantity", strQuantity);
        }
        
        invokeYantraApi("changeOrderStatus", changeOrderStatusInDoc);
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeOrderStatus", "Change Order Status Success");
    }

    /**
     * 
     * Method to confirm / un confirm / deliver Status based
     * on carrier status and shipment status
     * @param shipmentStatus
     * @param shipmentKey
     * @param api
     */
    private void changeShipmentTranStatus(String shipmentKey,String api) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "validateConfirmShipment", "ShipmentKey :"+shipmentKey+
                "API :"+api);
        YFCDocument shipConfirmXml = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+shipmentKey+"' />");
        invokeYantraApi(api, shipConfirmXml);
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "validateConfirmShipment", "API Success");
    }


    /**
     * Method to add the carrier update information 
     * in HangOff Table for Shipment.
     * 
     * @param inXml
     * @param shipmentKey
     */
    private void modifyShipment(YFCDocument inXml,String shipmentKey, String strTrackingNo, boolean bUpdateTrackingNo) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "modifyShipment", inXml);
        YFCElement extnEle = inXml.getDocumentElement().getChildElement(TelstraConstants.EXTN);
        YFCDocument modifyShipXml = YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+shipmentKey+"' />");
        YFCElement modifyShipEle = modifyShipXml.getDocumentElement();
        if(bUpdateTrackingNo) {
            modifyShipEle.setAttribute(TelstraConstants.TRACKING_NO, strTrackingNo);
        }
        modifyShipEle.importNode(extnEle);
		//HUB-8301 : [Begin]
        /*
		 * If the carrier update message is coming for the shipment which is already delivered or in futile status, 
		 * update the carrier update table instead of creating the new entry.
		 */
        String sTransportStatus = XPathUtil.getXpathAttribute(inXml, "//CarrierUpdate/@TransportStatus");
        String sCarrierUpdateKeyForDelivered = "";
        if(TelstraConstants.DELIVERED.equalsIgnoreCase(sTransportStatus)||TelstraConstants.FUTILE.equalsIgnoreCase(sTransportStatus)){
        	sCarrierUpdateKeyForDelivered = getCarrierUpdateKeyForDelivered(shipmentKey);
        }
        if(!YFCCommon.isStringVoid(sCarrierUpdateKeyForDelivered)){
        	updateCarrierUpdateStatus(inXml, sCarrierUpdateKeyForDelivered);
        }
        else{
        	invokeYantraApi(TelstraConstants.CHANGE_SHIPMENT, modifyShipXml);
        }
		//HUB-8301 : [End]
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "modifyShipment", "Shipment Modification Success");
    }

    /**
     * 
     * @param inXml
     * @param sCarrierUpdateKeyForDelivered
     */
    private void updateCarrierUpdateStatus(YFCDocument inXml, String sCarrierUpdateKeyForDelivered) {

    	YFCElement yfcEleCarrierUpdate = inXml.getElementsByTagName(TelstraConstants.CARRIER_UPDATE).item(0);
    	Map<String, String> mapCarrierUpdateAttri = yfcEleCarrierUpdate.getAttributes();
    	
    	YFCDocument yfcDocChangeCarrierUpdateIp = YFCDocument.getDocumentFor("<CarrierUpdate/>");
    	YFCElement yfcEleChangeCarrierUpdateRoot = yfcDocChangeCarrierUpdateIp.getDocumentElement();
    	yfcEleChangeCarrierUpdateRoot.setAttributes(mapCarrierUpdateAttri);
    	yfcEleChangeCarrierUpdateRoot.setAttribute(TelstraConstants.CARRIER_UPDATE_KEY, sCarrierUpdateKeyForDelivered);
    	LoggerUtil.verboseLog("CarrierUpdate :: checkCarrierUpdateTable :: yfcDocChangeCarrierUpdateIp ::", logger, yfcDocChangeCarrierUpdateIp);
    	invokeYantraService(TelstraConstants.GPS_CHANGE_CARRIER_UPDATE, yfcDocChangeCarrierUpdateIp);
		
	}

    /**
     * 
     * @param shipmentKey
     * @return
     */
    private String getCarrierUpdateKeyForDelivered(String shipmentKey) {

    	String sCarrierUpdateKeyForDelivered = "";

    	YFCDocument yfcDocGetCarrierUpdateListIp = YFCDocument.getDocumentFor("<CarrierUpdate ShipmentKey='"+shipmentKey+"' TransportStatus='Delivered' />");

    	LoggerUtil.verboseLog("CarrierUpdate :: checkCarrierUpdateTable :: yfcDocGetCarrierUpdateListIp ::", logger, yfcDocGetCarrierUpdateListIp);
    	YFCDocument yfcDocGetCarrierUpdateListOp = invokeYantraService(TelstraConstants.GET_CARRIER_UPDATE_LIST, yfcDocGetCarrierUpdateListIp);

    	YFCNodeList<YFCNode> yfcNodeCarrierUpdate = yfcDocGetCarrierUpdateListOp.getDocumentElement().getChildNodes();
    	if(yfcNodeCarrierUpdate.getLength()>0){
    		sCarrierUpdateKeyForDelivered = ((YFCElement)yfcNodeCarrierUpdate.item(0)).getAttribute(TelstraConstants.CARRIER_UPDATE_KEY);
    	}
    	return sCarrierUpdateKeyForDelivered;
    }


	private void updateCarrierStatus(YFCDocument inXml, YFCNodeList<YFCElement> yfcNlShipment){
        updateCarrierStatus(inXml, yfcNlShipment, "", false);
    }


    /**
     * This method validates the different status and
     * changes the shipment status accordingly. Also
     * updates the HangOff table.
     *
     * @param inXml
     */
    private void updateCarrierStatus(YFCDocument inXml, YFCNodeList<YFCElement> yfcNlShipment, 
                                        String strTrackingNo, boolean bUpdateTrackingNo) {
        LoggerUtil.startComponentLog(logger, this.getClass().getName(), "updateCarrierStatus", yfcNlShipment);

        for(YFCElement shipmentEle : yfcNlShipment){
        	if(!XmlUtils.isVoid(shipmentEle)) {
        		String inTransit = getProperty("GPS_IN_TRANSIT",true);
        		String futileDle = getProperty("GPS_FUTILE_DEL",true);
        		String carrierInTransit =  getProperty("INTRANSIT_STATUS",true);
        		String carrierDelivered = getProperty("DELIVERED_STATUS",true);
        		String carrierFailed = getProperty("FAILED_STATUS",true);
        		Double shipmentStatus = shipmentEle.getDoubleAttribute(TelstraConstants.STATUS);
        		String documentType = shipmentEle.getAttribute(TelstraConstants.DOCUMENT_TYPE);
        		String carrierStatus = XPathUtil.getXpathAttribute(inXml, "//CarrierUpdate/@TransportStatus");
        		String shipmentKey = shipmentEle.getAttribute(TelstraConstants.SHIPMENT_KEY);

        		if(shipmentStatus < 1400) {
        			changeShipmentTranStatus(shipmentKey,
        					TelstraConstants.CONFIRM_SHIPMENT_API);
        			shipmentStatus = (double) 1400;
        		}
        		if(carrierInTransit.equals(carrierStatus)&&shipmentStatus!=1400.10000) {
        			changeShipmentStatus(shipmentKey,"GPS_IN_TRANSIT."+documentType+".ex",inTransit);
        			changeOrderStatus(shipmentKey,"GPS_INTRANSIT."+documentType+".ex","3700.10000");

        		} else if(carrierFailed.equals(carrierStatus)) {
        			//HUB-5242 [Begin]
        			if(shipmentStatus < 1400.10000) {
        				changeShipmentStatus(shipmentKey,"GPS_IN_TRANSIT."+documentType+".ex",inTransit);
        				changeOrderStatus(shipmentKey,"GPS_INTRANSIT."+documentType+".ex","3700.10000");
        			}
        			//HUB-5242 [End]
        			changeShipmentTranStatus(shipmentKey,
        					TelstraConstants.UN_CONFIRM_SHIPMENT);
        			changeShipmentStatus(shipmentKey, "GPS_FUTILE_DEL."+documentType+".ex", futileDle);
        			changeOrderStatus(shipmentKey, "GPS_FUTILE_DELIVERY."+documentType+".ex", "9000.30000");

        		} else if(carrierDelivered.equals(carrierStatus)&&shipmentStatus!=1500) {
        			if(shipmentStatus < 1400.10000) {
        				changeShipmentStatus(shipmentKey,"GPS_IN_TRANSIT."+documentType+".ex",inTransit);
        				changeOrderStatus(shipmentKey,"GPS_INTRANSIT."+documentType+".ex","3700.10000");
        			}
        			changeShipmentTranStatus(shipmentKey,
        					TelstraConstants.DELIVER_SHIPMENT_API);
        			changeOrderStatus(shipmentKey,"GPS_DELIVER."+documentType+".ex","3700.7777");
        		}    
        		modifyShipment(inXml,shipmentKey, strTrackingNo, bUpdateTrackingNo);        
        	}
        }
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "updateCarrierStatus", "Carrier Update Success");
    }

    /**
     * If the Shipment is already delivered and the carrier update is received later.
     * Instead of ignoring the message, the carrier update is captured.
     * To do that, the shipment should have already been created with the tracking number.
     * So, before any processing is done, it is validate that the tracking number is passed in the input.
     * @param shipListXml
     * @return boolean
     */
    private boolean updateCarrierMessage(YFCDocument inXml) {
        // HUB-8057: Carrier Update Fails [START]

        /* Modifying and then returning if the shipment is in Delivered/Included In Receipt/Receipt Closed Status to avoid throwing of exception */

        YFCElement inEle = inXml.getDocumentElement();
        String sTrackingNo = inEle.getAttribute(TelstraConstants.TRACKING_NO,"");
        
        String sShipmentNo = inEle.getAttribute(TelstraConstants.SHIPMENT_NO,"");
        YFCDocument getShipListXml = YFCDocument.getDocumentFor("<Shipment ShipmentNo= '"+sShipmentNo+"' TrackingNo='"+sTrackingNo+"' OrderNo='"+sOrderNo+"'></Shipment>");
        YFCDocument tempXml = YFCDocument.getDocumentFor("<Shipments TotalNumberOfRecords = ''><Shipment ShipmentNo='' ShipmentKey='' /></Shipments>");
        YFCDocument shipListOutXml = invokeYantraApi(TelstraConstants.GET_SHIPMENT_LIST_API, getShipListXml, tempXml);
        LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getShipmentList", "Get Shipment List for tracking No Success");
        String shipmentCount = shipListOutXml.getDocumentElement().getAttribute("TotalNumberOfRecords","");
        if(!("0".equals(shipmentCount))) {
            YFCElement shipListEle = shipListOutXml.getDocumentElement();
            YFCNodeList<YFCElement> nlShipment = shipListEle.getElementsByTagName(TelstraConstants.SHIPMENT);
            for (YFCElement shipmentEle : nlShipment){
                //Modify the Shipment Attributes
                String strShipmentKey = shipmentEle.getAttribute(TelstraConstants.SHIPMENT_KEY, "");
                if(!YFCObject.isNull(strShipmentKey)){
                    modifyShipment(inXml, shipListOutXml.getDocumentElement().getChildElement(TelstraConstants.SHIPMENT)
                                                        .getAttribute(TelstraConstants.SHIPMENT_KEY,""), "", false);
                }
            }
            return true;
        }
        return false;
    }
    // HUB-8057: Carrier Update Fails [END]
}