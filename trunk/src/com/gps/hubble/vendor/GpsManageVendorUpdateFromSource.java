/***********************************************************************************************
 * File	Name		: GpsHandleVendorUpdateFromSource.java
 *
 * Description		: 
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Apr 03,2017		Prateek Kumar		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.vendor;

import java.util.Iterator;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Input template
 * <Order Action='' OrderName=''> <OrderLines> <OrderLine Action='' PrimeLineNo=
 * ''> <Extn> <OrderLineCommitmentList> <OrderLineCommitment CommitDeliveryDate=''
 * CommittedQuantity=''/> </OrderLineCommitmentList> </Extn> </OrderLine> </OrderLines>
 * </Order>
 * 
 * @author Prateek
 *
 */
public class GpsManageVendorUpdateFromSource extends AbstractCustomApi {

	enum ActionType{
		COMMIT,REJECT,COMMIT_ALL, REJECT_ALL;
	}

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsManageVendorUpdateFromSource.class);


	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		YFCElement yfcEleInputRoot = yfcInDoc.getDocumentElement();

		String sOrderName = yfcEleInputRoot.getAttribute(TelstraConstants.ORDER_NAME);
		/*
		 * Throwing exception if order name is missing
		 */
		if(YFCCommon.isStringVoid(sOrderName)){
			LoggerUtil.errorLog("Order Name Missing ", logger, TelstraConstants.THROW_EXCEPTION);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_MISSING_ORDER_NAME_ERROR_CODE, new YFSException());
		}
		YFCDocument yfcDocGetOrderListOp = callGetOrderList(sOrderName);

		/*
		 * Preparing the input for GpsHandleVendorUpdates service
		 */

		prepareInputForGpsHandleVendorUpdates(yfcInDoc, yfcDocGetOrderListOp);

		LoggerUtil.verboseLog(this.getClass().getName() + " :: invoke ::  yfcDocGpsHandleVendorUpdatesIp", logger, yfcInDoc);

		splitOrderLineBasedOnActionAndUpdateOrder(yfcInDoc);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);		
		return yfcInDoc;
	}

	/**
	 * 
	 * @param yfcInDoc
	 * @return
	 */
	private void splitOrderLineBasedOnActionAndUpdateOrder(YFCDocument yfcInDoc) {

		String sOrderAction = yfcInDoc.getDocumentElement().getAttribute(TelstraConstants.ACTION);
		/*
		 * If action is at the header level, call the service directly as action validation has already been done
		 */
		if(!YFCCommon.isStringVoid(sOrderAction)){
			invokeYantraService(TelstraConstants.SERVICE_GPS_HANDLE_VENDOR_UPDATES, yfcInDoc);
			return;
		}
		/*
		 * If the action is at the line level, split the input into two. One for commit and other for reject
		 * Reject part
		 */
//		YFCDocument yfcInDocReject = getCopy(yfcInDoc, TelstraConstants.ORDER);

		/*for(Iterator<YFCElement> itr = yfcInDocReject.getElementsByTagName(TelstraConstants.ORDER_LINE).iterator();itr.hasNext();){
			YFCElement yfcEleOrderLine =  itr.next();
			String sAction = yfcEleOrderLine.getAttribute(TelstraConstants.ACTION);
			ActionType actionType = validateAndGetActionType(sAction);
			if(ActionType.COMMIT==actionType){
				yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
			}
		}*/
		
		YFCNodeList<YFCElement> yfcNlOrderLineIp = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		
		YFCDocument yfcInDocReject = YFCDocument.createDocument(TelstraConstants.ORDER);
		yfcInDocReject.getDocumentElement().setAttributes(yfcInDoc.getDocumentElement().getAttributes());
		YFCElement yfcEleOrderLinesReject = yfcInDocReject.getDocumentElement().createChild(TelstraConstants.ORDER_LINES);
		
		YFCDocument yfcInDocCommit = YFCDocument.createDocument(TelstraConstants.ORDER);
		yfcInDocCommit.getDocumentElement().setAttributes(yfcInDoc.getDocumentElement().getAttributes());
		YFCElement yfcEleOrderLinesCommit = yfcInDocCommit.getDocumentElement().createChild(TelstraConstants.ORDER_LINES);
		
		for(YFCElement yfcEleOrderLineIp : yfcNlOrderLineIp){
			String sAction = yfcEleOrderLineIp.getAttribute(TelstraConstants.ACTION);
			if(ActionType.REJECT.toString().equals(sAction)){
				yfcEleOrderLinesReject.importNode(yfcEleOrderLineIp);
			}
			else if(ActionType.COMMIT.toString().equals(sAction)){
				yfcEleOrderLinesCommit.importNode(yfcEleOrderLineIp);
			}
		}
				
		yfcInDocReject.getDocumentElement().setAttribute(TelstraConstants.ACTION, ActionType.REJECT.toString());
		LoggerUtil.verboseLog(this.getClass().getName() + " :: splitOrderLineBasedOnActionAndUpdateOrder ::  yfcInDocReject", logger, yfcInDocReject);
		if(yfcInDocReject.getElementsByTagName(TelstraConstants.ORDER_LINE).getLength()>0){
			invokeYantraService(TelstraConstants.SERVICE_GPS_HANDLE_VENDOR_UPDATES, yfcInDocReject);
		}
		/*
		 * Commit part
		 */
		/*YFCDocument yfcInDocCommit = getCopy(yfcInDoc, TelstraConstants.ORDER);
		for(Iterator<YFCElement> itr = yfcInDocCommit.getElementsByTagName(TelstraConstants.ORDER_LINE).iterator();itr.hasNext();){
			YFCElement yfcEleOrderLine =  itr.next();
			String sAction = yfcEleOrderLine.getAttribute(TelstraConstants.ACTION);
			ActionType actionType = validateAndGetActionType(sAction);
			if(ActionType.REJECT==actionType){
				yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
			}
		}*/
		yfcInDocCommit.getDocumentElement().setAttribute(TelstraConstants.ACTION, ActionType.COMMIT.toString());
		LoggerUtil.verboseLog(this.getClass().getName() + " :: splitOrderLineBasedOnActionAndUpdateOrder ::  yfcInDocCommit", logger, yfcInDocCommit);
		if(yfcInDocCommit.getElementsByTagName(TelstraConstants.ORDER_LINE).getLength()>0){			
			invokeYantraService(TelstraConstants.SERVICE_GPS_HANDLE_VENDOR_UPDATES, yfcInDocCommit);
		}
	}

	/**
	 * 
	 * @param yfcInDoc
	 * @param yfcDocGetOrderListOp
	 */
	private void prepareInputForGpsHandleVendorUpdates(YFCDocument yfcInDoc, YFCDocument yfcDocGetOrderListOp) {

		String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order/@OrderHeaderKey");

		YFCElement yfcInputEle = yfcInDoc.getDocumentElement();
		yfcInputEle.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);

		/*
		 * If action at the header level is other than COMMIT_ALL or REJECT_ALL, throw an exception.
		 * If action at the header level is COMMIT_ALL or REJECT_ALL, return from the method after setting order header key at the header
		 * If action at the header level is blank, then proceed to check for the order line action
		 */
		String sAction = yfcInputEle.getAttribute(TelstraConstants.ACTION);

		if(!YFCCommon.isStringVoid(sAction)){
			ActionType actionType = validateAndGetActionType(sAction);

			if(!(ActionType.COMMIT_ALL == actionType || ActionType.REJECT_ALL == actionType)){
				LoggerUtil.errorLog("Invalid action at the header level", logger, yfcInDoc);
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_ACTION_ERROR_CODE, new YFSException());
			}			
			return;
		}

		/*
		 * proceeding with the assumption that action is passed at the line level
		 */

		YFCNodeList<YFCElement> yfcNlInputOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for(YFCElement yfcEleIpOrderLine : yfcNlInputOrderLine){

			if(YFCCommon.isStringVoid(yfcEleIpOrderLine.getAttribute(TelstraConstants.ACTION))){
				LoggerUtil.errorLog("No action found in the header or line level", logger, yfcInDoc);
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_ACTION_ERROR_CODE, new YFSException());
			}
			/*
			 * validating the action type
			 */
			validateAndGetActionType(sAction);

			String sPrimeLineNo = yfcEleIpOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);

			YFCElement yfcEleOrderListOrderLine = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			String sOrderLineKey = yfcEleOrderListOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
			yfcEleIpOrderLine.setAttribute(TelstraConstants.ORDER_LINE_KEY, sOrderLineKey);
			yfcEleIpOrderLine.setAttribute("SubLineNo", "1");
			YFCNodeList<YFCElement> yfcNlOrderListOrderLineCommitment = yfcEleIpOrderLine.getElementsByTagName(TelstraConstants.ORDER_LINE_COMMITMENT);
			long lScheduleNo = 0;
			for(YFCElement orderLineCommitment : yfcNlOrderListOrderLineCommitment) {
	            lScheduleNo++;
	            //YFCElement yfcEleIpOrderLineCommitment = yfcEleIpOrderLine.getElementsByTagName(TelstraConstants.ORDER_LINE_COMMITMENT).item(0);
	            orderLineCommitment.setAttribute(TelstraConstants.SCHEDULE_NO, lScheduleNo);
			}
			
			
		}			
	}


	/**
	 * 
	 * @param sOrderName
	 * @return
	 */
	private YFCDocument callGetOrderList(String sOrderName) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='"+sOrderName+"'/>");
		LoggerUtil.verboseLog(this.getClass().getName() + " :: callGetOrderList ::  yfcDocGetOrderListIp", logger, yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList> <Order OrderHeaderKey='' OrderNo='' DocumentType=''> <OrderLines> <OrderLine OrderLineKey='' PrimeLineNo=''> <Extn> <OrderLineCommitmentList> <OrderLineCommitment ScheduleNo='' CommittedQuantity=''/> </OrderLineCommitmentList> </Extn> </OrderLine> </OrderLines> </Order> </OrderList>"); 

		YFCDocument yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, yfcDocGetOrderListIp, yfcDocGetOrderListTemp); 
		LoggerUtil.verboseLog(this.getClass().getName() + " :: callGetOrderList ::  yfcDocGetOrderListOp", logger, yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;
	}

	/**
	 * 
	 * @param sAction
	 * @return
	 */
	private ActionType validateAndGetActionType(String sAction){

		if(!YFCObject.isVoid(sAction)){
			try{				
				return ActionType.valueOf(sAction);
			}catch(Exception e){
				LoggerUtil.errorLog("Invalid action found in document", logger, sAction);
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VENDOR_COMMITMENT_INVALID_ACTION_ERROR_CODE, new YFSException());
			}
		}
		return null;
	}

	/**
	 * 
	 * @param inXml
	 * @param rootNode
	 * @return
	 */
	private static YFCDocument getCopy(YFCDocument inXml, String rootNode){
		YFCDocument copyDoc = YFCDocument.createDocument(rootNode);
		copyDoc.getDocumentElement().setAttributes(inXml.getDocumentElement().getAttributes());
		if(inXml.getDocumentElement().getChildren() != null){
			for(Iterator<YFCElement> itr = inXml.getDocumentElement().getChildren().iterator();itr.hasNext();){
				copyDoc.getDocumentElement().importNode(itr.next());
			}
		}
		return copyDoc;
	}

}
