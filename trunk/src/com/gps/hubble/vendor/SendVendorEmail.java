/**
 * 
 */
package com.gps.hubble.vendor;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to send email to vendor and add a note to the order
 * @author Bridge
 *
 */
public class SendVendorEmail extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(SendVendorEmail.class);
	/* (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inpDoc) throws YFSException {
		try {
			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
			YFCElement orderElem = inpDoc.getDocumentElement();
			String messgedtl=orderElem.getAttribute(TelstraConstants.MESSAGE_DETAIL);
			LoggerUtil.verboseLog("Vendor Email Body", logger, messgedtl);
			
			YFCElement ordersendMailElem = orderElem.getChildElement("SendMail",true);
			YFCDocument sendMailDoc = YFCDocument.createDocument();
			YFCElement sendMailElem = sendMailDoc.importNode(ordersendMailElem, true);
			sendMailDoc.appendChild(sendMailElem);
			YFCElement messageHeaderElem = sendMailElem.getChildElement(TelstraConstants.MESSAGE_HEADER,true);
			//Adding Failure Email Address
			messageHeaderElem.setAttribute("FailureEmailAddress", messageHeaderElem.getAttribute("FromAddress"));
			YFCElement messageElem= sendMailElem.createChild(TelstraConstants.MESSAGE_DETAIL);
			messageElem.setNodeValue(messgedtl);
			sendMailElem.appendChild(messageElem);
			
			String initialText= getProperty("VENDOR_EMAIL_TEXT");
			if(initialText == null || "".equals(initialText) ) {
				initialText = TelstraConstants.EMAIL_VENDOR_TEXT;
			}
			
			String subject=inpDoc.getElementsByTagName(TelstraConstants.MESSAGE_HEADER).item(0).getAttribute(TelstraConstants.SUBJECT_TEXT);
			String toMailID=inpDoc.getElementsByTagName(TelstraConstants.TO_ADDRESS).item(0).getAttribute("EmailID");
			String note = initialText +" '"+ subject+"' to mail id '"+ toMailID+"'" ;
			
			StringBuilder noteText = new StringBuilder(note);
			
			if(orderElem.hasAttribute("VendorContactId") && ! "".equals(orderElem.getAttribute("VendorContactId"))) {
				noteText.append(" ").append(" Contact Person : ").append(orderElem.getAttribute("VendorContactId"));
			}
			YFCDocument changeOrderDoc = YFCDocument.createDocument(TelstraConstants.ORDER);
			YFCElement changeOrderElem = changeOrderDoc.getDocumentElement();
			changeOrderElem.setAttribute(TelstraConstants.ORDER_HEADER_KEY, orderElem.getAttribute(TelstraConstants.ORDER_HEADER_KEY));
			YFCElement notesElem = changeOrderElem.createChild(TelstraConstants.NOTES);
			YFCElement noteElem = notesElem.createChild(TelstraConstants.NOTE);
			noteElem.setAttribute(TelstraConstants.NOTE_TEXT, noteText.toString());
			noteElem.setAttribute(TelstraConstants.CONTACT_USER, orderElem.getAttribute(TelstraConstants.CONTACT_USER));
			LoggerUtil.verboseLog("Send Mail Input", logger, sendMailDoc);
			
			//@Todo: Once the Backend code is fixed for Email failure, send mail needs to point to custom API

			invokeYantraService("GpsSendEmail", sendMailDoc);
		     //invokeYantraApi("sendMail",sendMailDoc);
			
			LoggerUtil.verboseLog("Change Order Input", logger, changeOrderDoc);
			return invokeYantraApi("changeOrder", changeOrderDoc);
		}finally {
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}
	}

}
