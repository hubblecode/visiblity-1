/**
 * 
 */
package com.gps.hubble.inventory.api;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to invoke getInventoryItemList
 * @author Bridge
 *
 */
public class GpsInventoryItemList extends AbstractCustomApi {
	private YFCDocument retDoc; 

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsInventoryItemList.class);
	/* (non-Javadoc)
	 * @see com.bridge.sterling.framework.api.AbstractCustomApi#invoke(com.yantra.yfc.dom.YFCDocument)
	 */
	@Override
	public YFCDocument invoke(YFCDocument inpDoc) throws YFSException {

		try {

			LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
			String itemID= inpDoc.getDocumentElement().getAttribute("ItemID");
			String itemDesc=inpDoc.getDocumentElement().getAttribute("ShortDescription");
			String productLine = inpDoc.getDocumentElement().getAttribute("ProductLine");
			String maxrecords = inpDoc.getDocumentElement().getAttribute("MaximumRecords");

			Connection connection = getServiceInvoker().getDBConnection();


			try (PreparedStatement ps = createQuery(connection, itemID, itemDesc, productLine);
					ResultSet rs = ps.executeQuery()) {
				retDoc = constructDocument(rs, maxrecords);

			}catch(SQLException sqlEx){
				logger.error("SQLException while executing query ",sqlEx);
				throw new YFCException(sqlEx);
			} 
			return retDoc;

		}finally {

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inpDoc);
		}

	}
	private PreparedStatement createQuery(Connection connection,String itemID,String itemDesc, String productLine) throws SQLException{

		int psIndex = 1;
		String query= "select YII.ITEM_ID ITEM_ID,YI.SHORT_DESCRIPTION SHORT_DESCRIPTION,YII.UOM UOM,YII.ORGANIZATION_CODE ORGANIZATION_CODE,YI.PRODUCT_LINE PRODUCT_LINE"
				+" FROM YFS_INVENTORY_ITEM YII LEFT OUTER JOIN YFS_ITEM YI"
				+" ON YII.ITEM_ID = YI.ITEM_ID AND YII.UOM = YI.UOM"
				+" WHERE YII.ORGANIZATION_CODE='"+TelstraConstants.ORG_TELSTRA+"'";		

		StringBuilder builder= new StringBuilder();
		builder.append(query);

		if(!XmlUtils.isVoid(itemID)){
			builder.append(" AND YII.ITEM_ID LIKE ? ");			
		}

		if(!XmlUtils.isVoid(productLine)){
			builder.append(" AND YI.PRODUCT_LINE = ? ");			
		}

		if(!XmlUtils.isVoid(itemDesc)){
			builder.append(" AND YI.SHORT_DESCRIPTION LIKE ?");
		}

		PreparedStatement ps= connection.prepareStatement(builder.toString());
		if(!XmlUtils.isVoid(itemID)) {
			ps.setString(psIndex, "%" + itemID + "%");
			psIndex++;
		}
		if(!XmlUtils.isVoid(productLine)) {
			ps.setString(psIndex, productLine );
			psIndex++;
		}
		if(!XmlUtils.isVoid(itemDesc)) {
			ps.setString(psIndex, "%" +itemDesc + "%");
			psIndex++;
		}

		logger.verbose("Query to be executed "+builder.toString());
		return ps;
	}

	private YFCDocument constructDocument(ResultSet rs, String maxrecords) throws SQLException{
		YFCDocument doc = YFCDocument.getDocumentFor("<InventoryList />");
		YFCElement elem = doc.getDocumentElement();
		int noOfRecords=Integer.parseInt(maxrecords); 
		int i=0;

		while(rs.next())
		{
			if(i == noOfRecords){
				break;
			}

			YFCElement eleInventoryItem = elem.createChild("InventoryItem");

			eleInventoryItem.setAttribute("ItemID", rs.getString("ITEM_ID").trim());
			eleInventoryItem.setAttribute("UnitOfMeasure", rs.getString("UOM").trim());
			eleInventoryItem.setAttribute("InventoryOrganizationCode", rs.getString("ORGANIZATION_CODE").trim());

			YFCElement elemItem = eleInventoryItem.createChild("Item");
			elemItem.setAttribute("OrganizationCode", rs.getString("ORGANIZATION_CODE").trim());

			YFCElement elemPrimaryInfo = elemItem.createChild("PrimaryInformation".trim());
			String shortDescription = rs.getString("SHORT_DESCRIPTION");
			if(shortDescription != null) {
				shortDescription = shortDescription.trim();
			}else {
				shortDescription = "";
			}
			elemPrimaryInfo.setAttribute("ShortDescription", shortDescription);
			String productLine = rs.getString("PRODUCT_LINE");
			if(productLine != null) {
				productLine = productLine.trim();
			} else {
				productLine = "";
			}
			
			elemPrimaryInfo.setAttribute("ProductLine", productLine);

			i++;		

		}

		logger.verbose("Sql return document"+ doc);
		return doc;
	}
}
