/***********************************************************************************************
 * File Name        : ManageItecInv.java
 *
 * Description      : This class is called from UI to retrieve the ITEC inventory data based on 
 *                    the search criteria
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                  Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      Jan 06,2017     Santosh Nagaraj            Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.inventory.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class GetItecInventory extends AbstractCustomApi {
  private static YFCLogCategory logger = YFCLogCategory.instance(GetItecInventory.class);
  
  /**
   * @param inXml
   * This method will get the supply and serial information for the ITEC inventory based on the search criteira passed from UI
   */
  public YFCDocument invoke(YFCDocument inXml) throws YFSException {
    logger.verbose("getting the ITEC inventory data for the search criteria passed");
    String revisionNo = inXml.getDocumentElement().getAttribute("RevisionNo");
    String serialNo = inXml.getDocumentElement().getAttribute("SerialNo");
    String barcode = inXml.getDocumentElement().getAttribute("Barcode");
    String materialName = inXml.getDocumentElement().getAttribute("MaterialName");
    
    YFCDocument itecInventoryListDoc = YFCDocument.getDocumentFor("<ItecInventoryList />");
    
    /*
     * Checking if one among revisionNo, serialNo or barcode is passed in the UI Search screen.
     * Since all these three attributes are specific to the records in GPS_ITEC_SERIAL table, 
     * the search will be performed based on the GPS_ITEC_SERIAL table 
     * 
     * */
    if(!YFCObject.isNull(revisionNo) || !YFCObject.isNull(serialNo) || !YFCObject.isNull(barcode)) {
      // Calling itecSerialList for the passed revisionNo, serialNo or barcode
      YFCDocument itecSerialListOutDoc = getItecSerialListOutputDocument(inXml);
      
        YFCDocument getItemListInDoc = YFCDocument.getDocumentFor("<Item OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' >"
            + "<ComplexQuery Operator='AND'> "
            + "</ComplexQuery> "
                + "<AdditionalAttributeList> "
                + "<AdditionalAttribute AttributeDomainID='ItemAttribute' AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='ITEC'/>"
                + "</AdditionalAttributeList> </Item>");
        YFCElement complexQuery = getItemListInDoc.getElementsByTagName("ComplexQuery").item(0);
        if(!YFCObject.isNull(itecSerialListOutDoc) || itecSerialListOutDoc.getDocumentElement().hasChildNodes()) {
          YFCElement andEleForMaterialID = complexQuery.createChild("And");
          YFCElement orForMaterialID = andEleForMaterialID.createChild("Or");
          
          YFCNodeList<YFCElement> serialsEle = itecSerialListOutDoc.getElementsByTagName("ItecSerial");

          for(YFCElement serial : serialsEle) {
            String materialID = serial.getAttribute("MaterialId");
            YFCElement expressionEle = orForMaterialID.createChild("Exp");
            expressionEle.setAttribute("Name", "ItemID");
            expressionEle.setAttribute("QryType", "EQ");
            expressionEle.setAttribute("Value", materialID);
          }

          if(!YFCObject.isVoid(materialName)) {
            YFCElement andEleForMaterialName = complexQuery.createChild("And");
            YFCElement expressionEle = andEleForMaterialName.createChild("Exp");
            expressionEle.setAttribute("Name", "Model");
            expressionEle.setAttribute("QryType", "LIKE");
            expressionEle.setAttribute("Value", materialName);
          }
          
          
          YFCDocument getItemListOutputDoc = getServiceInvoker().invokeYantraApi("getItemList", getItemListInDoc, getItemListTemplate());
          if(!YFCObject.isVoid(getItemListOutputDoc) && getItemListOutputDoc.getDocumentElement().hasChildNodes()) {
            YFCNodeList<YFCElement> items = getItemListOutputDoc.getElementsByTagName("Item");
            for(YFCElement item : items) {
              YFCElement itemEle = itecInventoryListDoc.createElement("Item");
              itemEle.setAttribute("MaterialId", item.getAttribute("ItemID"));
              itemEle.setAttribute("MaterialDescription", item.getElementsByTagName("PrimaryInformation").item(0).getAttribute("Description", ""));
              YFCElement classificationCode = item.getElementsByTagName("ClassificationCodes").item(0);
              if(!YFCObject.isVoid(classificationCode)) {
                itemEle.setAttribute("Model", classificationCode.getAttribute("Model",""));
              }
              itecInventoryListDoc.getDocumentElement().appendChild(itemEle);
            }
          }
        }
        return itecInventoryListDoc;
    } else {
      /*
       *  If the search criteria is passed without RevisionNo, Barcode or serial, then the search is performed for the 
       *  GPS_ITEC_SUPPLY table.  This will result in giving the supply for the search criteria.
       */
      
      String isDetail = inXml.getDocumentElement().getAttribute("IsDetail");
      double availableQty = 0;
      YFCElement itemEle = null;
      YFCElement inventoryListEle = null;
        if("Y".equals(isDetail)) {
          itemEle = itecInventoryListDoc.getDocumentElement().createChild("Item");
          YFCDocument itecSupplyListOutDoc = getSupplyDetails(inXml);
          if(!YFCObject.isNull(itecSupplyListOutDoc) && itecSupplyListOutDoc.getDocumentElement().hasChildNodes()) {
            
              //If IsDetail flag is passed as Y, then it is assumed that the search being performed is for Material details.
              inventoryListEle = itemEle.createChild("InventoryList");
              setItemLevelAttributesForDetail(itemEle, inXml.getDocumentElement().getAttribute("MaterialId"));
              /*
               * The statuses to be considered for quantitiy computations are passed as service arguments with comma seperated
               * values.  
               */
              String statusesToBeConsidered = getProperty("StatusesToBeConsidered","");
              String[] statuses = statusesToBeConsidered.split(",");
              for(String status : statuses) {
                itemEle.setAttribute(status+"Qty", "0");
              }
            }
            YFCNodeList<YFCElement> supplyEles = itecSupplyListOutDoc.getElementsByTagName("ItecSupply");
            for(YFCElement supply : supplyEles) {
              if(supply.getDoubleAttribute("Quantity") <= 0) {
                continue;
              }
                // Setting the location and inventory level information for the material for Detail API
                YFCElement locationEle = itecInventoryListDoc.getDocumentElement().createChild("Location");
                locationEle.setAttribute("Location", getSPSLocation(supply.getAttribute("Location")));
                locationEle.setAttribute("ITECLocation", supply.getAttribute("ITECLocation"));
                YFCElement inventoryEle = itecInventoryListDoc.getDocumentElement().createChild("Inventory");
                inventoryEle.setAttribute("Qty", supply.getAttribute("Quantity"));
                inventoryEle.setAttribute("Status", supply.getAttribute("Status"));

                String statusesToBeConsidered = getProperty("StatusesToBeConsidered","");
                double supplyQty = supply.getDoubleAttribute("Quantity",0);
                /*
                 * If the supply status is one among the statuses to be considered then the available quantity will be 
                 * computed.  Else the quantity will not be computed for the AvailableQuantity attribute 
                 */
                if(statusesToBeConsidered.contains(supply.getAttribute("Status"))) {
                  availableQty = supplyQty + availableQty;
                }
                double statusQuantity = itemEle.getDoubleAttribute(supply.getAttribute("Status")+"Qty",0);
                // The status quantity break up will be computed for every status.
                itemEle.setAttribute(supply.getAttribute("Status")+"Qty", supplyQty+statusQuantity);
                locationEle.appendChild(inventoryEle);
                inventoryListEle.appendChild(locationEle);
              }
            
              itemEle.setAttribute("AvailableQuantity", availableQty);
              YFCElement alternateItems = addItemAssociations(itecInventoryListDoc , inXml.getDocumentElement().getAttribute("MaterialId"));
              itemEle.appendChild(alternateItems);
              return itecInventoryListDoc;
          }else {
            /*
             * If IsDetail="N", only the supply level information to be displayed 
             */
            
            /*
             * Calling getItemList API for every material id to set the PrimaryInformation and the classification
             * code information at the Item element
             */
            YFCDocument itemListDoc = null;
            if(!YFCObject.isVoid(materialName)) {
              itemListDoc = getItemListDoc(inXml.getDocumentElement().getAttribute("MaterialId"), inXml.getDocumentElement().getAttribute("MaterialName"), "MaterialName", true);
            } else {
              itemListDoc = getItemListDoc(inXml.getDocumentElement().getAttribute("MaterialId"), "", "ItemID", true);
            }
            if(!YFCObject.isVoid(itemListDoc) && itemListDoc.getDocumentElement().hasChildNodes()) {
              YFCNodeList<YFCElement> items = itemListDoc.getElementsByTagName("Item");
              for(YFCElement item : items) {
                itemEle = itecInventoryListDoc.getDocumentElement().createChild("Item");
                itemEle.setAttribute("MaterialID", item.getAttribute("ItemID"));
                itemEle.setAttribute("MaterialDescription", item.getElementsByTagName("PrimaryInformation").item(0).getAttribute("Description", ""));
                YFCElement classificationCode = item.getElementsByTagName("ClassificationCodes").item(0);
                if(!YFCObject.isVoid(classificationCode)) {
                  itemEle.setAttribute("Model", classificationCode.getAttribute("Model",""));
                }
                itecInventoryListDoc.getDocumentElement().appendChild(itemEle);
              }
            }
            return itecInventoryListDoc;
          }
        }
    }
  
  /**
   * This Method will return the SPS Location Description for the corresponding location code passed.
   * EX:
   * If the Location is IS01, the value returned will be MELBOURNE
   * If the Location is IS02, the value returned will be SYDNEY
   * If the Location is TELSTRA, the value returned will be TELSTRA
   * @param attribute
   * @return
   */
  private String getSPSLocation(String location) {
    if(YFCObject.isVoid(location)) {
      return "";
    }
    YFCDocument docGetCommonCodeListOutput =
        invokeYantraApi(
            TelstraConstants.API_GET_COMMON_CODE_LIST,
            YFCDocument.getDocumentFor("<CommonCode CodeType=\"SPS_LOCATIONS\" CodeValue='"+location+"'/>"));
    YFCElement commonCodeEle = docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0);
    if (!YFCObject.isVoid(commonCodeEle)) {
      return commonCodeEle.getAttribute(TelstraConstants.CODE_SHORT_DESCRIPTION);
    }
    return "";
  }

  /**
   * 
   * @param supply
   * @param itemEle
   * This method will set the supply level attributes
   */
  private void setSupplyAttributesForList(YFCElement supply, YFCElement itemEle) {
    itemEle.setAttribute("MaterialId", supply.getAttribute("MaterialId"));
    itemEle.setAttribute("ITECLocation", supply.getAttribute("ITECLocation"));
    itemEle.setAttribute("Location", getSPSLocation(supply.getAttribute("Location")));
    itemEle.setAttribute("Quantity", supply.getAttribute("Quantity"));
    itemEle.setAttribute("Status", supply.getAttribute("Status"));
  }
  
  /**
   * 
   * @param itemEle
   * @param materialId
   * This method will set the item level attributes for detail view at the <Item /> tag.
   * The attributes includes the classification codes as well
   */
  private void setItemLevelAttributesForDetail(YFCElement itemEle, String materialId) {
    YFCDocument itemListDoc = getItemListDoc(materialId, "", "ItemID", false);
    itemEle.setAttribute("MaterialId", materialId);
    if(!YFCObject.isNull(itemListDoc) && itemListDoc.getDocumentElement().hasChildNodes()) {
      itemEle.setAttribute("MaterialDescription", itemListDoc.getElementsByTagName("PrimaryInformation").item(0).getAttribute("Description"));
      itemEle.setAttribute("ManufacturerName", itemListDoc.getElementsByTagName("PrimaryInformation").item(0).getAttribute("ManufacturerName"));
      itemEle.setAttribute("ItemType", itemListDoc.getElementsByTagName("PrimaryInformation").item(0).getAttribute("ItemType"));
      itemEle.setAttribute("ProductLine", itemListDoc.getElementsByTagName("PrimaryInformation").item(0).getAttribute("ProductLine"));
      itemEle.setAttribute("UnitOfMeasure", itemListDoc.getElementsByTagName("Item").item(0).getAttribute("UnitOfMeasure"));
      YFCElement classificationCode = itemListDoc.getElementsByTagName("ClassificationCodes").item(0);
      if(!YFCObject.isVoid(classificationCode)) {
        itemEle.setAttribute("Model", classificationCode.getAttribute("Model",""));
        itemEle.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
      }
    }
  }
  
  /**
   * 
   * @param itecSupplyListInDoc
   * @return
   * This method will get the supply for the Material Passed from the GPS_ITEC_SUPPLY Table
   */
  private YFCDocument getSupplyDetails(YFCDocument itecSupplyListInDoc) {
    YFCDocument getItecSupplyListOutDoc = getServiceInvoker().invokeYantraService("GpsGetItecSupplyList", itecSupplyListInDoc);
    return getItecSupplyListOutDoc;
  }
  
  /**
   * 
   * @param itecInventoryListDoc
   * @param itemId
   * @return
   * 
   * This Method will get the item associations for the materials passed along with the inventory information 
   * available for the associated items in the supply table.
   */
  private YFCElement addItemAssociations(YFCDocument itecInventoryListDoc, String itemId) {
    YFCElement alternateItems = itecInventoryListDoc.createElement("AlternateItemList");
    YFCDocument getItemAssociationsInDoc = YFCDocument.getDocumentFor("<AssociationList AssociationType='Alternative.Y' ItemID='"+itemId+"' "
        + "OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' UnitOfMeasure='"+TelstraConstants.ITEC_INVENTORY_UOM+"'/>");
    /*
     * Getting the Item associations for the material passed 
     */
    YFCDocument getItemAssociationsOutDoc = getServiceInvoker().invokeYantraApi("getItemAssociations", getItemAssociationsInDoc, getItemAssociationsTemplate());
    if(getItemAssociationsOutDoc != null && getItemAssociationsOutDoc.getDocumentElement().hasChildNodes()) {
      YFCNodeList<YFCElement> itemList =  getItemAssociationsOutDoc.getElementsByTagName("Item");
      for(YFCElement itemEle : itemList) {
        /*
         * For every Associated item calling the getItemList API to get the primary information and classifications
         */
        YFCDocument getItemListOutput = getItemListDoc(itemEle.getAttribute("ItemID"), "", "ItemID", false);
        String itemDesc = "";
        if(!YFCObject.isVoid(getItemListOutput)) {
          itemDesc = getItemListOutput.getElementsByTagName("PrimaryInformation").item(0).getAttribute("Description");
        }
        YFCDocument itecSupplyListInDoc = YFCDocument.getDocumentFor("<ItecInventory MaterialId='"+itemEle.getAttribute("ItemID")+"' />");
        YFCDocument itecSupplyListOutDoc = getSupplyDetails(itecSupplyListInDoc);
        /*
         * Getting the Supply information for the Associated item passed
         */
        if(!YFCObject.isVoid(itecSupplyListOutDoc) && itecSupplyListOutDoc.getDocumentElement().hasChildNodes()) {
          /*
           * If inventory exists in the GPS_ITEC_SUPPLY and GPS_ITEC_SERIAL table, then setting the supply
           * information and item details for the associated material id
           */
          YFCNodeList<YFCElement> supplyEles = itecSupplyListOutDoc.getElementsByTagName("ItecSupply");
          for(YFCElement supply : supplyEles) {
            YFCElement alternateItem = itecInventoryListDoc.createElement("AlternateItem");
            alternateItem.setAttribute("MaterialDescription", itemDesc);
            YFCElement classificationCode = getItemListOutput.getElementsByTagName("ClassificationCodes").item(0);
            if(!YFCObject.isVoid(classificationCode)) {
              alternateItem.setAttribute("Model", classificationCode.getAttribute("Model",""));
              alternateItem.setAttribute("CommodityCode", classificationCode.getAttribute("CommodityCode",""));
            }
            // Setting the supply level attributes for the associated item
            setSupplyAttributesForList(supply, alternateItem);
            alternateItems.appendChild(alternateItem);
          }
        } else {
          /*
           * If No inventory found for the associated item, then setting the Quantity to 0 along witht he 
           * Item Primary information and classifications
           */
          YFCElement alternateItem = itecInventoryListDoc.createElement("AlternateItem");
          alternateItem.setAttribute("MaterialDescription", itemDesc);
          alternateItem.setAttribute("Model", "");
          alternateItem.setAttribute("CommodityCode", "");
          alternateItem.setAttribute("MaterialId", itemEle.getAttribute("ItemID"));
          alternateItem.setAttribute("Quantity", "0");
          alternateItems.appendChild(alternateItem);
        }
      }
    }
    return alternateItems;
  }


  private YFCDocument getItemAssociationsTemplate() {
    YFCDocument itemAssociationsTemplate = YFCDocument.getDocumentFor("<AssociationList> <Association> "
        + "<Item ItemID='' OrganizationCode='' UnitOfMeasure=''/> "
            + "</Association> </AssociationList>");
    return itemAssociationsTemplate;
  }
  
  /**
   * 
   * @param itemId
   * @return
   * This Method will return the Item Details for the MaterialId passed.
   */
  private YFCDocument getItemListDoc(String materialId, String materialName, String identifier, boolean expressionSearch) {
    YFCDocument getItemListInput = getInputDocumentforItemList(materialId, materialName, identifier, expressionSearch);
    YFCDocument getItemListOutputDoc = getServiceInvoker().invokeYantraApi("getItemList", getItemListInput, getItemListTemplate());
    if(getItemListOutputDoc != null && getItemListOutputDoc.getDocumentElement().hasChildNodes()) {
      /*
       * If getItemList returns the valid output then return the same
       */
      return getItemListOutputDoc;
    }
    /*
     * If getItemList is not valid for the MaterialId passed, then return null
     */
    return null;
  }
  
  /**
   * 
   * @param itecSerialListInDoc
   * @return
   * This method is called to get the ITEC Serials for the input passed
   */
  private YFCDocument getItecSerialListOutputDocument(YFCDocument itecSerialListInDoc) {
    YFCDocument getItecSerialListOutDoc = getServiceInvoker().invokeYantraService("GpsGetItecSerialList", itecSerialListInDoc);
    return getItecSerialListOutDoc;
  }
  
  /**
   * 
   * @param itemId
   * @return
   * This Method will return the input document for getItemList API
   */
  private YFCDocument getInputDocumentforItemList(String materialId, String materialName, String identifier, boolean expressionSearch) {
    YFCDocument itemListInputDoc = null;
    if("ItemID".equals(identifier)) {
      itemListInputDoc = YFCDocument.getDocumentFor("<Item ItemID='"+materialId+"' OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' />");
      if(expressionSearch) {
        itemListInputDoc.getDocumentElement().setAttribute("ItemIDQryType", "LIKE");
      }
    }
    /*
     * <Item OrganizationCode='TELSTRA_SCLSA' >
        <ComplexQuery Operator="AND">
            <And> 
                <Exp Name="Model" Value="9400048082" QryType="LIKE"/>   
            </And> 
        </ComplexQuery>
        <AdditionalAttributeList>
          <AdditionalAttribute AttributeDomainID='ItemAttribute' AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='ITEC'/></AdditionalAttributeList>
        </Item>
     */
    else if("MaterialName".equals(identifier)) {
      itemListInputDoc = YFCDocument.getDocumentFor("<Item OrganizationCode='"+TelstraConstants.ORG_TELSTRA_SCLSA+"' ItemID='"+materialId+"' ItemIDQryType='LIKE' >"
          + "<ComplexQuery Operator='AND'> "
              + "<And> <Exp Name='Model' Value='"+materialName+"' QryType='LIKE'/></And>  </ComplexQuery> "
                  + "<AdditionalAttributeList> "
                  + "<AdditionalAttribute AttributeDomainID='ItemAttribute' AttributeGroupID='ItemReferenceGroup' Name='SOURCE_SYSTEM' Value='ITEC'/>"
                  + "</AdditionalAttributeList> </Item>");
    }
    return itemListInputDoc;
  }
  
  /**
   * 
   * @return
   * This method will return he getItemListTemplate in the below format
   * <ItemList>
   *    <Item ItemID='' UnitOfMeasure=''>
   *        <PrimaryInformation ManufacturerName='' Description='' ItemType='' ProductLine='' />
   *        <ClassificationCodes Model='' CommodityCode=''/>
   *    </Item>
   * </ItemList>
   */
  private YFCDocument getItemListTemplate() {
    YFCDocument itemListTemplateDoc = YFCDocument.getDocumentFor("<ItemList><Item ItemID='' UnitOfMeasure=''><PrimaryInformation ManufacturerName='' Description='' ItemType='' ProductLine='' /><ClassificationCodes Model='' CommodityCode=''/></Item></ItemList>");
    return itemListTemplateDoc;
  }
}