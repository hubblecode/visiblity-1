package com.gps.hubble.inventory.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

/**
 * Custom API to Manage the Serials of Items
 * 
 * @author Keerthi Yadav
 * @version 1.0
 *
 *          Extends AbstractCustomApi Class
 */

public class ManageSerialNum extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory
			.instance(ManageSerialNum.class);

	public YFCDocument invoke(YFCDocument inXml) {
		/* Create, Update and Delete the Serial Number */

		modifySerial(inXml);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return inXml;

	}

	private void modifySerial(YFCDocument inXml) {
		String sSerialNo = inXml.getDocumentElement().getAttribute(TelstraConstants.SERIAL_NO,"");
		String sInventoryItemKey = inXml.getDocumentElement().getAttribute(TelstraConstants.INVENTORY_ITEM_KEY,"");
		String sShipNode = inXml.getDocumentElement().getAttribute(TelstraConstants.SHIP_NODE,"");
		
		LoggerUtil.verboseLog(TelstraConstants.SERIAL_NO + sSerialNo, logger, "");
		
		if (!YFCObject.isNull(sSerialNo)) {
			YFCDocument docInventoryItemListinXml = YFCDocument.getDocumentFor("<SerialNum InventoryItemKey='"+sInventoryItemKey+"' SerialNo='"+sSerialNo+"' ShipNode='"+sShipNode+"'/>");
			YFCDocument docGpsSerialNumberList = invokeYantraService("GpsGetSerialNumList", docInventoryItemListinXml);
			if (!YFCElement.isNull(docGpsSerialNumberList.getElementsByTagName(TelstraConstants.SERIAL_NUMBER).item(0))) {
				/* Update the Attributes of the Inventory Item */
				invokeYantraService("GpsChangeSerialNum", inXml);
			} else {
				/* Create the Serial of the Inventory Item */
				invokeYantraService("GpsCreateSerialNum", inXml);
			}

		}
	}

	private YFCDocument DeleteSerial(YFCDocument inXml) {
		/* Delete the Serial of the Inventory Item once the Inventory Shipped */
		return inXml;
	}				
}