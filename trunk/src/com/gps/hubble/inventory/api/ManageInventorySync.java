package com.gps.hubble.inventory.api;


import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XMLUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API to perform Inventory Sync 
 * 
 * @author Keerthi Yadav
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 */

public class ManageInventorySync extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageInventorySync.class);

	public YFCDocument invoke(YFCDocument inXml) {
		/* Method to decide whether it is a syncLoadedInventory or loadInventoryMismatch or to trigger agent */


		YFCElement eleItemstag = inXml.getElementsByTagName("Items").item(0);
		YFCElement eleInventory = inXml.getElementsByTagName(TelstraConstants.INVENTORY).item(0);
		YFCElement eleEndOfFile = inXml.getElementsByTagName("TriggerAgent").item(0);

		if(!YFCElement.isNull(eleItemstag)){
			/* Method to invoke loadInventoryMismatch and to insert record into the custom GPS_SERIAL_NUM table */
			loadInventoryMismatch(inXml);
		}
		else if(!YFCElement.isNull(eleInventory)){
			/* Method to invoke syncLoadedInventory  */		
			syncLoadedInventory(inXml);	
		}

		else if(!YFCElement.isNull(eleEndOfFile)){
			/* Method to invoke triggerAgent  */		
			//YFCDocument triggerAgentXml=endOfFile();
			invokeYantraApi("triggerAgent", inXml);
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return inXml;
	}	


	private void loadInventoryMismatch(YFCDocument inXml ) {
		YFCDocument docloadInventoryMistmatchinXml= XMLUtil.getDocumentFor(inXml.getDocumentElement().cloneNode(true));
		/* Method to invoke loadInventoryMismatch and to insert record into the custom GPS_SERIAL_NUM table */
		/*Remove SerialTag*/
		for (YFCElement eleItemtag : docloadInventoryMistmatchinXml.getElementsByTagName(TelstraConstants.ITEM)){	
			String sUOM=eleItemtag.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
			String sItemID=eleItemtag.getAttribute(TelstraConstants.ITEM_ID);
			sItemID=itemCheck(sUOM,sItemID);
			if(!YFCObject.isNull(sItemID)){
			eleItemtag.setAttribute(TelstraConstants.ITEM_ID, sItemID);
			}
			for (YFCElement eleSupply : eleItemtag.getElementsByTagName(TelstraConstants.SUPPLY)){
				for (YFCElement eleSerials : eleSupply.getElementsByTagName(TelstraConstants.SERIALS)){
					eleSupply.removeChild(eleSerials);
				}
			}
		}
		invokeYantraApi("loadInventoryMismatch", docloadInventoryMistmatchinXml);	

		/* Method to insert record into the custom GPS_SERIAL_NUM table */
		String sShipNode=inXml.getDocumentElement().getAttribute(TelstraConstants.SHIP_NODE,"");

		for (YFCElement eleItemtag : inXml.getElementsByTagName(TelstraConstants.ITEM)){		
			String sInventoryOrganizationCode=eleItemtag.getAttribute(TelstraConstants.INVENTORY_ORGANIZATION_CODE,"");
			String sUOM=eleItemtag.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
			String sItemID=eleItemtag.getAttribute(TelstraConstants.ITEM_ID);
			sItemID=itemCheck(sUOM,sItemID);																											
			for (YFCElement eleSupply : eleItemtag.getElementsByTagName(TelstraConstants.SUPPLY)){
				/* Method to invoke loadInventoryMistmatch  */
				if (!YFCElement.isNull(eleItemtag) && !YFCElement.isNull(eleSupply)) {
					loadInventoryMismatch(eleSupply, sInventoryOrganizationCode, sUOM, sItemID, sShipNode);
				}
			}
		}
	}	

	private YFCDocument createInputToCustomtable(YFCElement eleSerial, String sShipNode, String sInventoryItemKey,String sInventoryStatus) {
		/* Method to invoke syncLoadedInventory  */
		YFCDocument docSerialNum = YFCDocument.getDocumentFor(eleSerial.toString());
		YFCElement eleconvertSerial = docSerialNum.getElementsByTagName(TelstraConstants.SERIAL).item(0);
		docSerialNum.getDocument().renameNode(eleconvertSerial.getDOMNode(),null, "SerialNum");	
		docSerialNum.getDocumentElement().setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);	
		docSerialNum.getDocumentElement().setAttribute(TelstraConstants.AT_NODE, "Y");
		docSerialNum.getDocumentElement().setAttribute(TelstraConstants.INVENTORY_STATUS, sInventoryStatus);	
		docSerialNum.getDocumentElement().setAttribute(TelstraConstants.SHIP_NODE, sShipNode);		
		return docSerialNum;
	}	



	private void loadInventoryMismatch(YFCElement eleSupply, String sInventoryOrganizationCode,String sUOM, String sItemID, String sShipNode ) {
		/* Method to invoke loadInventoryMismatch  */
		String sInventoryStatus=eleSupply.getAttribute("SupplyType");

		YFCDocument docInventoryItemListinXml = YFCDocument.getDocumentFor("<InventoryItem InventoryOrganizationCode='" + sInventoryOrganizationCode + "'  UnitOfMeasure='" + sUOM + "' ItemID='" + sItemID + "'/>");
		YFCDocument docInventoryItemListtempXml = YFCDocument.getDocumentFor("<InventoryList LastInventoryItemKey=''><InventoryItem InventoryItemKey='' UnitOfMeasure='' ItemID=''/></InventoryList>");
		YFCDocument docInventoryItemListoutXml=invokeYantraApi("getInventoryItemList", docInventoryItemListinXml,docInventoryItemListtempXml);

		if(!YFCElement.isNull(docInventoryItemListoutXml.getElementsByTagName("InventoryItem").item(0))){
			String sInventoryItemKey=docInventoryItemListoutXml.getDocumentElement().getAttribute("LastInventoryItemKey","");

			for (YFCElement eleSerial : eleSupply.getElementsByTagName(TelstraConstants.SERIAL)){
				if(!YFCElement.isNull(eleSerial.getAttribute(TelstraConstants.SERIAL_NO))){
					/* Creating Input for Asyncrequest */

					YFCDocument docCreateInputToCustomtable=createInputToCustomtable(eleSerial,sShipNode,sInventoryItemKey,sInventoryStatus);		
					YFCDocument asynRequestXml = YFCDocument.getDocumentFor("<CreateAsyncRequest />");
					String message = docCreateInputToCustomtable.getDocumentElement().getString().replace("\\<\\?xml(.+?)\\?\\>", "").trim();
					String sGpsManageSerialNum = getProperty("SERVICE_NAME_GpsManageSerialNum", true);
					String sInterfaceNumber = getProperty("ASYNC_REQUEST_INTERFACE_NO", true);
					asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.SERVICE_NAME, sGpsManageSerialNum);
					asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.MESSAGE, message);
					asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.INTERFACE_NO, sInterfaceNumber);
					asynRequestXml.getDocumentElement().setAttribute(TelstraConstants.IS_FLOW, "Y");
					invokeYantraService("GpsCreateAsyncRequest", asynRequestXml);
				}
			}
		}
	}

	private String itemCheck(String sUOM,String sItemID) {
		/* Method to check the Item  */
		String sItemsID;
		YFCDocument docGetItemList = invokeYantraApi(TelstraConstants.GET_ITEM_LIST,YFCDocument.getDocumentFor("<Item OrganizationCode='TELSTRA_SCLSA' UnitOfMeasure='" + sUOM + "' ><ItemAliasList><ItemAlias AliasName='VECTOR_ITEM' AliasValue='"+ sItemID + "'/></ItemAliasList></Item>"));
		if (YFCElement.isNull(docGetItemList.getElementsByTagName(TelstraConstants.ITEM).item(0))) {
			docGetItemList = invokeYantraApi(TelstraConstants.GET_ITEM_LIST,YFCDocument.getDocumentFor("<Item OrganizationCode='TELSTRA_SCLSA' ItemID= '" + sItemID + "' UnitOfMeasure='" + sUOM + "' />"));
			if (!YFCElement.isNull(docGetItemList.getElementsByTagName(TelstraConstants.ITEM).item(0))) {
				sItemsID = docGetItemList.getElementsByTagName(TelstraConstants.ITEM).item(0).getAttribute(TelstraConstants.ITEM_ID);
			}else{
				//exception
				// throw exception
				LoggerUtil.verboseLog("Item was not Found", logger, " throwing exception");
				String strErrorCode = getProperty("ItemSearchFailureCode", true);
				throw  ExceptionUtil.getYFSException(strErrorCode,new YFSException());	

				}
		}else{
			sItemsID = docGetItemList.getElementsByTagName(TelstraConstants.ITEM).item(0).getAttribute(TelstraConstants.ITEM_ID);
		}
		return sItemsID;
	}	


	private void syncLoadedInventory(YFCDocument inXml) {
		/* Method to invoke syncLoadedInventory  */
		invokeYantraApi("syncLoadedInventory", inXml);		
	}	


	/*private YFCDocument endOfFile() {
		 Method to invoke TriggerAgent  

		String sInterfaceNumber = getProperty("TRIGGERAGENT_CRITERIA_INTERFACE_NO", true);
		YFCDocument triggerAgentXml = YFCDocument.getDocumentFor("<TriggerAgent><CriteriaAttributes>"+ "<Attribute/></CriteriaAttributes></TriggerAgent>");
		YFCElement  triggerAgentEle = triggerAgentXml.getDocumentElement();
		triggerAgentEle.setAttribute("CriteriaId", "ASYNC_REQ_1");
		YFCElement triggerAgentAttrEle = triggerAgentEle.getChildElement("CriteriaAttributes").getChildElement("Attribute");
		triggerAgentAttrEle.setAttribute("Name",TelstraConstants.INTERFACE_NO);
		triggerAgentAttrEle.setAttribute("Value", sInterfaceNumber);
		return triggerAgentXml;
	}	*/
}
