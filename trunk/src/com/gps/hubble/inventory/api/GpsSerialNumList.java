package com.gps.hubble.inventory.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;

/**
 * Custom API to get the Serials of Items
 * 
 * @author Sambeet Mohanty
 * @version 1.0
 *
 *          Extends AbstractCustomApi Class
 */

public class GpsSerialNumList extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory
			.instance(GpsSerialNumList.class);
	private YFCDocument retDoc;

	public YFCDocument invoke(YFCDocument inXml) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		try{

			String itemID= inXml.getDocumentElement().getAttribute("ItemID");
			String orgCode= inXml.getDocumentElement().getAttribute("OrganizationCode");
			String uom= inXml.getDocumentElement().getAttribute("UnitOfMeasure");

			Connection connection= getServiceInvoker().getDBConnection();

			try (PreparedStatement ps = createQuery(connection, itemID, orgCode, uom);
					ResultSet rs = ps.executeQuery()) {
				retDoc = constructDocument(rs);

			}catch(SQLException sqlEx){
				logger.error("SQLException while executing query ",sqlEx);
				throw new YFCException(sqlEx);
			} 
			return retDoc;
		}
		finally{
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		}



	}
	private PreparedStatement createQuery(Connection connection,String itemID,String orgCode, String uom) throws SQLException{
		String query="SELECT GSN.INVENTORY_ITEM_KEY INVENTORY_ITEM_KEY,COUNT(1) SERIAL_COUNT "
				+ " FROM YFS_INVENTORY_ITEM YII JOIN GPS_SERIAL_NUM GSN"
				+ " ON YII.INVENTORY_ITEM_KEY=GSN.INVENTORY_ITEM_KEY"
				+ " WHERE YII.ITEM_ID= ?"
				+ " AND YII.ORGANIZATION_CODE= ? "
				+ " AND YII.UOM= ? "
				+ " AND GSN.AT_NODE = ? "
				+ " GROUP BY GSN.INVENTORY_ITEM_KEY";

		PreparedStatement statement=connection.prepareStatement(query);
		statement.setString(1, itemID);
		statement.setString(2, orgCode);
		statement.setString(3, uom);
		statement.setString(4, "Y");

		return statement;
	}

	private YFCDocument constructDocument(ResultSet rs) throws SQLException{
		YFCDocument doc= YFCDocument.createDocument("SerialNumberList");
		while(rs.next()){
			doc.getDocumentElement().setAttribute("InventoryItemKey", rs.getString("INVENTORY_ITEM_KEY").trim());
			doc.getDocumentElement().setAttribute("SerialCount", rs.getString("SERIAL_COUNT").trim());
		}
        logger.verbose("Return Doc from Serial List"+ doc);
		return doc;
	}

}