package com.gps.hubble.utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

import com.bridge.sterling.utils.ExceptionUtil;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.order.ue.BeforeCreateOrderUE;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class TemplateProcessor {
	private static YFCLogCategory logger = YFCLogCategory.instance(BeforeCreateOrderUE.class);
	
	private static Map<String, String> templateCache = new HashMap<>();
	private static final String IF_BLOCK_START_SIGNAL = "<#if";
	private static final String IF_BLOCK_END_SIGNAL = "</#if>";
	private static final String ELSE_BLOCK_START_SIGNAL = "<#else>";
	private static final String ELSE_BLOCK_END_SIGNAL = "</#else>";
	private static final String VARIABLE_START_SIGNAL = "${";
	private static final String VARIABLE_END_SIGNAL ="}";
	
	
	public String process(String fileName, Map<String, Object> model){
		if(!templateCache.containsKey(fileName)){
			logger.debug("Template file "+fileName+" not found in cache..will try to read it.");
			loadFile(fileName);
		}
		String template = templateCache.get(fileName);
		if(YFCObject.isVoid(template)){
			return null;
		}
		LinkedList<Block> blocks = generateBlocks(template);
		return processBlocks(blocks, template, model);
	}
	
	public String processTemplate(String template, Map<String, Object> model){
		LinkedList<Block> blocks = generateBlocks(template);
		return processBlocks(blocks, template, model);
	}
	
	
	private String processBlocks(LinkedList<Block> blocks, String template, Map<String, Object> model){
		if(blocks == null || blocks.isEmpty()){
			return template;
		}
		StringBuffer buffer = new StringBuffer();
		for(Block block : blocks){
			if(block.getBlockType() == BlockType.NORMAL){
				String blockString = block.getBlockString();
				String resolvedString = resolveVariables(blockString, model, 0);
				buffer.append(resolvedString);
			}else{
				String condition = block.getConditionKey();
				String conditionVariable = getVariable(condition);
				if(model.containsKey(conditionVariable)){
					Boolean bool = (Boolean)model.get(conditionVariable);
					if(bool && block.getBlockType() == BlockType.IF_BLOCK){
						String resolvedString = resolveVariables(block.getBlockString(), model, 0);
						buffer.append(resolvedString);
					}else if(!bool && block.getBlockType() == BlockType.ELSE_BLOCK){
						String resolvedString = resolveVariables(block.getBlockString(), model, 0);
						buffer.append(resolvedString);
					}
				}else{
					//This should not happen
					String resolvedString = resolveVariables(block.getBlockString(), model, 0);
					buffer.append(resolvedString);
				}
			}
		}
		return buffer.toString();
	}
	
	
	private String resolveVariables(String blockString, Map<String, Object> model, int startPos){
		int varStartIndex = blockString.indexOf(VARIABLE_START_SIGNAL, startPos);
		if(varStartIndex < 0){
			return blockString;
		}
		int varEndIndex = blockString.indexOf(VARIABLE_END_SIGNAL, varStartIndex);
		String beforeVariable = blockString.substring(0, varStartIndex);
		String afterVariable = blockString.substring(varEndIndex + 1);
		String variableString = blockString.substring(varStartIndex, varEndIndex + 1);
		String variable = getVariable(variableString);
		if(model.containsKey(variable)){
			Object obj = model.get(variable);
			String newBlockString = beforeVariable + (String)obj + afterVariable;
			return resolveVariables(newBlockString, model, varEndIndex);
		}else{
			return resolveVariables(blockString, model, varEndIndex);
		}
	}
	
	
	
	
	private static synchronized void loadFile(String fileName){
		if(templateCache.containsKey(fileName)){
			logger.debug("File found in cache..will use the same "+fileName);
			return;
		}
		InputStream is = null;
		try{
			is = TemplateProcessor.class.getResourceAsStream(fileName);
			if(is == null){
				logger.error("Template File not found"+fileName);
				return;
			}
			BufferedReader reader = new BufferedReader(new InputStreamReader(is));
			String line = null;
			StringBuffer buffer = new StringBuffer();
			while(true){
				line = reader.readLine();
				if(YFCObject.isVoid(line)){
					break;
				}
				buffer.append(line);
				buffer.append("\n");
			}
			String fileString = buffer.toString();
			logger.debug("file successfully read "+fileName+" and fileString is "+fileString);
			templateCache.put(fileName, fileString);
		}catch(Exception e){
			logger.error("Exception while reading template file "+fileName, e);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.TEMPLATE_PROCESSOR_FILE_READ_EXCEPTION, new YFSException());
		}finally{
			try{
				if(is != null){
					is.close();
				}
			}catch(IOException e){
				logger.error("Exception while closing the output stream", e);
			}
		}
	}
	
	
	public LinkedList<Block> generateBlocks(String template){
		LinkedList<Block> blocks = new LinkedList<>();
		generateBlocks(blocks, template);
		return blocks;
	}
	
	private void generateBlocks(LinkedList<Block> blocks, String template){
		template = template.trim();
		if(template.length() == 0){
			return;
		}
		Block lastBlock = blocks.peekLast();
		if(lastBlock == null || lastBlock.getBlockType() == BlockType.ELSE_BLOCK){
			if(template.startsWith(ELSE_BLOCK_START_SIGNAL) || template.startsWith(ELSE_BLOCK_END_SIGNAL)){
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.TEMPLATE_PROCESSOR_INVALID_ELSE_BLOCK, new YFSException());
			}
			validateTemplate(template);
		}
		if(template.startsWith(IF_BLOCK_START_SIGNAL)){
			int endIndex = template.indexOf(IF_BLOCK_END_SIGNAL);
			String blockString = template.substring(0, endIndex + 6);
			Block block = getIfBlock(blockString);
			blocks.add(block);
			template = template.substring(endIndex + 6);
		}else if(template.startsWith(ELSE_BLOCK_START_SIGNAL)){
			if(lastBlock == null || lastBlock.blockType != BlockType.IF_BLOCK){
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.TEMPLATE_PROCESSOR_INVALID_ELSE_BLOCK, new YFSException());
			}
			int endIndex = template.indexOf(ELSE_BLOCK_END_SIGNAL);
			if(endIndex < -1){
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.TEMPLATE_PROCESSOR_INVALID_ELSE_BLOCK, new YFSException());
			}
			String blockString = template.substring(7, endIndex);
			Block block = new Block(blockString, BlockType.ELSE_BLOCK, lastBlock.getConditionKey());
			blocks.add(block);
			template = template.substring(endIndex + 8);
		}else{
			validateTemplate(template);
			int endIndex = template.length();
			int ifBlockStartPos = template.indexOf(IF_BLOCK_START_SIGNAL);
			if(ifBlockStartPos > 0){
				endIndex = ifBlockStartPos;
			}
			String blockString = template.substring(0, endIndex);
			Block block = new Block(blockString, BlockType.NORMAL, null);
			blocks.add(block);
			template = template.substring(endIndex);
		}
		generateBlocks(blocks, template);
	}
	
	private void validateTemplate(String template){
		int ifBlockStartPos = template.indexOf(IF_BLOCK_START_SIGNAL);
		int elseBlockStartPos = template.indexOf(ELSE_BLOCK_START_SIGNAL);
		if(elseBlockStartPos > 0 && elseBlockStartPos < ifBlockStartPos){
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.TEMPLATE_PROCESSOR_INVALID_ELSE_BLOCK, new YFSException());
		}
	}
	
	private Block getIfBlock(String ifBlockCompleteString){
		int ifConditionEnd = ifBlockCompleteString.indexOf(">");
		int ifBlockStringEndPos = ifBlockCompleteString.indexOf(IF_BLOCK_END_SIGNAL);
		String ifBlockString = ifBlockCompleteString.substring(ifConditionEnd + 1, ifBlockStringEndPos);
		String conditionString = ifBlockCompleteString.substring(4, ifConditionEnd);
		conditionString = conditionString.trim();
		Block block = new Block(ifBlockString, BlockType.IF_BLOCK, conditionString);
		return block;
	}
	
	private String getVariable(String str){
		if(!str.startsWith(VARIABLE_START_SIGNAL)){
			return null;
		}
		str = str.trim();
		return str.substring(2,str.length() - 1);
	}
	
	
	
	private class Block{
		String blockString;
		BlockType blockType;
		String conditionKey;
		Block(String blockString, BlockType blockType, String conditionKey){
			this.blockString = blockString;
			this.blockType = blockType;
			this.conditionKey = conditionKey;
		}	
		String getBlockString(){
			return blockString;
		}
		
		BlockType getBlockType(){
			return blockType;
		}
		
		String getConditionKey(){
			return conditionKey;
		}
	}
	
	private static enum BlockType{
		NORMAL,IF_BLOCK,ELSE_BLOCK
	}
	

}
