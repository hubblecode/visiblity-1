/***********************************************************************************************
 * File	Name		: GpsManageVectorOrder.java
 *
 * Description		: This class is contains different util required for the Manage Order code
 * 					 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * 
 * 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		Apr 11,2017	  	Prateek Kumar 		   	Initial	Version 
 * 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.utils;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

public class GpsManageOrderUtil {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsManageOrderUtil.class);
	
	private YFCDocument yfcDocGetOrderListOp;

	public YFCDocument getYfcDocGetOrderListOp() {
		return yfcDocGetOrderListOp;
	}

	public void setYfcDocGetOrderListOp(YFCDocument yfcDocGetOrderListOp) {
		this.yfcDocGetOrderListOp = yfcDocGetOrderListOp;
	}

	/**
	 * This method takes list of vector id in the input and return the map of the vector id and prime line no
	 * @param listVectorId
	 * @param serviceInvoker
	 * @param yfcDocGetOrderListOp 
	 * @return Map<String, String> mapVectorIdPrimeLineNo
	 */
	public Map<String, String> getVectorIdLineNoMap(List<String> listVectorId, ServiceInvoker serviceInvoker) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getVectorIdLineNoMap", listVectorId);

		YFCDocument yfcDocGetOrderListOp = callGetOrderListWithVectorId(listVectorId.get(0) ,serviceInvoker);
		setYfcDocGetOrderListOp(yfcDocGetOrderListOp); 
		Map<String, String> mapVectorIdPrimeLineNo = new HashMap<>();
		for (String sVectorId : listVectorId) {

			YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcDocGetOrderListOp,
					"//OrderLine[Extn/@VectorID='" + sVectorId + "']");
			if(!YFCCommon.isVoid(yfcEleOrderLine)){
				String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);			
				mapVectorIdPrimeLineNo.put(sVectorId, sPrimeLineNo);
			}
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getVectorIdLineNoMap", mapVectorIdPrimeLineNo);

		return mapVectorIdPrimeLineNo;

	}

	/**
	 * call get order list with vector id to get order information
	 * @param sVectorId 
	 * @return
	 */
	private YFCDocument callGetOrderListWithVectorId(String sVectorId, ServiceInvoker serviceInvoker) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order> <OrderLine> <ComplexQuery> <And> <Exp Name='Extn_VectorID' Value='"+sVectorId+"'/> </And> </ComplexQuery> </OrderLine> </Order>");

		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList> <Order OrderNo=''> <OrderLines> <OrderLine PrimeLineNo=''> <Extn VectorID=''/> <OrderStatuses> <OrderStatus Status=''/> </OrderStatuses> </OrderLine> </OrderLines> </Order> </OrderList>");

		YFCDocument yfcDocGetOrderListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);

		LoggerUtil.verboseLog("GpsManageOrderUtil :: callGetOrderListWithVectorId :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;
	}
	
}
