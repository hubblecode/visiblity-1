/***********************************************************************************************
 * File	Name		: GpsUpdateExistingCustomerNodeType.java
 *
 * Description		: This class will be use to update the node type of the existing customer.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		20/04/17		Prateek Kumar			Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.dac.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsUpdateExistingCustomerNodeType extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsUpdateExistingCustomerNodeType.class);


	@Override
	/**
	 * 
	 */
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);
		updateExisitingCustomerNodeType(yfcInDoc);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);

		return yfcInDoc;
	}

	/**
	 * 
	 * @param yfcInDoc
	 */
	private void updateExisitingCustomerNodeType(YFCDocument yfcInDoc) {

		YFCDocument yfcDocGetOrgListOp = callGetOrgList();

		for(YFCElement yfcEleOrg : yfcDocGetOrgListOp.getElementsByTagName(TelstraConstants.ORGANIZATION)){

			String sOrganizationCode = 	yfcEleOrg.getAttribute(TelstraConstants.ORGANIZATION_CODE);

			YFCElement yfcEleNode = yfcEleOrg.getChildElement(TelstraConstants.NODE);
			String sNodeType = yfcEleNode.getAttribute(TelstraConstants.NODE_TYPE);

			YFCDocument yfcDocGetCustomerListOp = callGetCustomerList(sOrganizationCode);
			YFCElement yfcEleCustomer = yfcDocGetCustomerListOp.getDocumentElement().getChildElement(TelstraConstants.CUSTOMER);
			if(!YFCCommon.isVoid(yfcEleCustomer)){
				/*
				 * Append node type in the customer
				 */
				YFCElement yfcEleExtn = yfcEleCustomer.createChild(TelstraConstants.EXTN);
				yfcEleExtn.setAttribute(TelstraConstants.CUSTOMER_NODE_TYPE, sNodeType);

				LoggerUtil.verboseLog("GpsManageVectorOrder :: updateExisitingCustomerNodeType :: Manage customer input \n", logger, yfcDocGetCustomerListOp);

				invokeYantraService("GpsDropToDacQ", YFCDocument.getDocumentFor(yfcEleCustomer.toString()));
			}
		}		
	}


	/**
	 * 
	 * @param sOrganizationCode
	 * @return
	 */
	private YFCDocument callGetCustomerList(String sOrganizationCode) {

		YFCDocument yfcDocGetCustomerListIp = YFCDocument.getDocumentFor("<Customer ExternalCustomerID='"+sOrganizationCode+"'/>");
		YFCDocument yfcDocGetCustomerListTemp = YFCDocument.getDocumentFor(
				"<CustomerList> <Customer ExternalCustomerID='' CustomerID='' CustomerKey=''/> </CustomerList>");

		YFCDocument yfcDocGetCustomerListOp = invokeYantraApi(TelstraConstants.API_GET_CUSTOMER_LIST, yfcDocGetCustomerListIp, yfcDocGetCustomerListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetCustomerList :: yfcDocGetCustomerListOp \n", logger, yfcDocGetCustomerListOp);

		return yfcDocGetCustomerListOp;
	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument callGetOrgList() {

		YFCDocument yfcDocGetOrgListIp = YFCDocument.getDocumentFor("<Organization IsNode='Y'/>");
		YFCDocument yfcDocGetOrgListTemp = YFCDocument.getDocumentFor("<OrganizationList> <Organization OrganizationCode=''> <Node NodeType=''/> </Organization> </OrganizationList>");
		YFCDocument yfcDocGetOrgListOp = invokeYantraApi(TelstraConstants.API_GET_ORGANIZATION_LIST, yfcDocGetOrgListIp, yfcDocGetOrgListTemp);

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrgList :: yfcDocGetOrgListOp \n", logger, yfcDocGetOrgListOp);

		return yfcDocGetOrgListOp;
	}
}
