/***********************************************************************************************
 * File Name        : GpsLdapLoad.java
 *
 * Description      : Custom API for loading data into custom table GPS_LDAP_EMPLOYEE of the LDAP Users
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date            Author                              Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0      28/06/2016      Sambeet Mohanty                     Initial Version
 * 1.1      15/02/2017		Keerthi Yadav						HUB-8437: Ldap backend development for loading and processing Employee data
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.dac.api;

//Misc imports 
import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
//Sterling imports
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;


public class CreateCTNode extends AbstractCustomApi {


	private static YFCLogCategory logger =YFCLogCategory.instance(CreateCTNode.class);
	private YFCDocument docManageOrgInput =null;

	/**
	 * Method name: invoke
	 * arguments: YFCDocument
	 * Base method Initial execution starts here
	 * Invokes manageOrganizationHierarchy API to create CT Nodes 
	 */
	@Override
	public YFCDocument invoke(YFCDocument docInXML) 
	{		
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", docInXML);
		String sCustID=docInXML.getDocumentElement().getAttribute("CustomerID");
		if(SCUtil.isVoid(sCustID))
		{
			//throw new YFSException("Customer ID is null");
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.NULL_CUSTOMER_ERROR_CODE, new YFSException());
		}

		if(!SCUtil.isVoid(sCustID) && sCustID.startsWith("V"))
		{
			docManageOrgInput=populateManageOrgRequest(docInXML, sCustID);
			//HUB-8437 [START]
			YFCDocument docManageOrgOutput = invokeYantraApi("manageOrganizationHierarchy", docManageOrgInput);

			/*This method will be invoked to create customer that are already present in the GPS_LDAP_EMPLOYEE custom table
			 * with the CTFlag as N. This would happen if CT Users are been loaded in the table before the CT DAC is created.
			 * Hence those Users would be created now ie after the DAC is created.
			 */

			createCTUser(sCustID);

			return docManageOrgOutput; 	
			//HUB-8437 [END]
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docInXML);
		return docInXML;

	}

	/**
	 * This method will be invoked to create customer that are already present in the GPS_LDAP_EMPLOYEE custom table
	 * with the CTFlag as N
	 * @param sCustID
	 */
	private void createCTUser(String sCustID) {
		//The assumption is that the EmployeeID when prefixed with 'V' is the CustomerID or the CT Node
		String strEmployeeID = sCustID.replaceFirst("V", "");

		YFCDocument inXmlEmployeeList = YFCDocument.getDocumentFor("<Employee EmployeeID='"+ strEmployeeID+"' CTFlag='N' />");
		YFCDocument outXmlEmployeeList = invokeYantraService("GpsGetEmployeeList", inXmlEmployeeList);
		YFCElement eleoutEmployee = outXmlEmployeeList.getDocumentElement().getElementsByTagName(TelstraConstants.EMPLOYEE).item(0);

		//If any record is present in custom table Create CT user
		if(!YFCObject.isNull(eleoutEmployee)){
			logger.verbose("Creating CT User after the CT DAC has been created for the Employee : : " +strEmployeeID);
			
			//Identifier to Set CTFlag to Y
			eleoutEmployee.setAttribute("DacCreated", TelstraConstants.YES);
			String outXmlEmployee = eleoutEmployee.toString();
			invokeYantraService("GpsLdapProcessor", YFCDocument.getDocumentFor(outXmlEmployee));
		}
	}

	/**
	 * Method name: getLocaleCode
	 * @param inXML
	 * @return String
	 * This methods determines the locale code of the CT node to be created based on the state code 
	 */
	public String getLocaleCode(YFCDocument inXML)
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getLocaleCode", inXML);
		String sState=inXML.getElementsByTagName("BillingPersonInfo").item(0).getAttribute("State");
		LoggerUtil.verboseLog("State code present in the input Document: "+sState, logger, inXML);
		if(SCUtil.isVoid(sState))
		{
			sState="VIC" ;
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getLocaleCode", inXML);
		return getProperty(sState, true);

	}

	/**
	 * Method name: populateManageOrgRequest
	 * @param inXML
	 * @param sCustID
	 * @return docManageOrg
	 * This methods populates the input request xml for manageOrganizationHierarchy API 
	 */
	public YFCDocument populateManageOrgRequest(YFCDocument inXML, String sCustID)
	{
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "populateManageOrgRequest", inXML);
		String sLoceleCode=getLocaleCode(inXML);

		YFCDocument docManageOrg = YFCDocument.getDocumentFor("<Organization />");
		YFCElement eleInput=docManageOrg.getDocumentElement();

		eleInput.setAttribute("OrganizationKey", sCustID);
		eleInput.setAttribute("OrganizationCode", sCustID);  
		eleInput.setAttribute("OrganizationName", sCustID); 
		eleInput.setAttribute("PrimaryEnterpriseKey", TelstraConstants.ORG_TELSTRA);
		eleInput.setAttribute("ParentOrganizationCode", TelstraConstants.ORG_TELSTRA);

		eleInput.setAttribute("InventoryPublished", "N");
		eleInput.setAttribute("LocaleCode", sLoceleCode);

		YFCElement  eleNode=docManageOrg.createElement("Node");
		eleNode.setAttribute("InventoryTracked", "N");
		eleNode.setAttribute("NodeType", "CT");
		eleInput.appendChild(eleNode);

		YFCElement eleBillingAddress=docManageOrg.importNode(inXML.getElementsByTagName("BillingPersonInfo").item(0),true);
		eleInput.appendChild(eleBillingAddress);

		YFCElement eleCorpAddress=docManageOrg.createElement("CorporatePersonInfo");
		eleCorpAddress.setAttributes(eleBillingAddress.getAttributes());
		eleInput.appendChild(eleCorpAddress);

		YFCElement eleOrgRoleList=docManageOrg.createElement("OrgRoleList");
		eleInput.appendChild(eleOrgRoleList);

		YFCElement eleOrgRole=docManageOrg.createElement("OrgRole");
		eleOrgRole.setAttribute("RoleKey", "NODE");
		eleOrgRoleList.appendChild(eleOrgRole);	

		LoggerUtil.verboseLog("Input Document docManageOrg:", logger, docManageOrg);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "populateManageOrgRequest", inXML);

		return docManageOrg;

	}

}
