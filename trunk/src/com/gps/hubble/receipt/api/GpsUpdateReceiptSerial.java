package com.gps.hubble.receipt.api;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

public class GpsUpdateReceiptSerial extends AbstractCustomApi{

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsUpdateReceiptSerial.class);

	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);


		YFCDocument yfcDocReceiveOrder = YFCDocument.getDocumentFor((Document) getTxnObject("ReceiveOrderYfcDoc"));
		updateReceiptSerial(yfcDocReceiveOrder);
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		return null;
	}

	private void updateReceiptSerial(YFCDocument docReceiveOrderIp) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "updateReceiptSerial", docReceiveOrderIp);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "updateReceiptSerial", docReceiveOrderIp);

		
	}
	
	
	/**
	 * 
	 * @param sInventoryItemKey
	 * @param sSerialNo
	 * @param sShipNode
	 */
	private void manageSerialNumber(String sInventoryItemKey, String sSerialNo, String sShipNode) {

		YFCDocument yfcDocManageSerial = YFCDocument.getDocumentFor("<SerialNumber SerialNo='' InventoryItemKey='' AtNode='' ShipNode=''/>");
		YFCElement yfcEleManageSerialRoot = yfcDocManageSerial.getDocumentElement();
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);
		yfcEleManageSerialRoot.setAttribute(TelstraConstants.AT_NODE, TelstraConstants.YES);
		LoggerUtil.verboseLog("GpsUpdateShippedOutSerial::manageSerialNumber::yfcDocManageSerial", logger, yfcDocManageSerial);

		
		YFCDocument yfcDocGetSerialNumList = YFCDocument.getDocumentFor("<SerialNum InventoryItemKey=''  SerialNo='' ShipNode=''/>");
		YFCElement yfcEleGetSerialNumListRoot = yfcDocGetSerialNumList.getDocumentElement();
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.SERIAL_NO, sSerialNo);
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.INVENTORY_ITEM_KEY, sInventoryItemKey);
		yfcEleGetSerialNumListRoot.setAttribute(TelstraConstants.SHIP_NODE, sShipNode);		

		YFCDocument yfcDocGetSerialNumberListOp = invokeYantraService("GpsGetSerialNumList", yfcDocGetSerialNumList);

		LoggerUtil.verboseLog("GpsUpdateShippedOutSerial::manageSerialNumber::yfcDocGetSerialNumberListOp", logger, yfcDocGetSerialNumberListOp);

		if (!YFCElement.isNull(yfcDocGetSerialNumberListOp.getElementsByTagName(TelstraConstants.SERIAL_NUMBER).item(0))) {
			/* Update the Attributes of the Inventory Item */
			invokeYantraService("GpsChangeSerialNum", yfcDocManageSerial);
		} else {
			/* Create the Serial of the Inventory Item */
			invokeYantraService("GpsCreateSerialNum", yfcDocManageSerial);
		}

	}

}
