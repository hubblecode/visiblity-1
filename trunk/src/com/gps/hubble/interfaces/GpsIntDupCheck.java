/***********************************************************************************************
 * File	Name		: GpsIntDupCheck.java
 *
 * Description		: 
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification


 * ---------------------------------------------------------------------------------------------
 * 1.0		Mar 31,2017		Gladston Xavier		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.interfaces;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.baseutil.SCUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;


public class GpsIntDupCheck extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsIntDupCheck.class);
	/**
	 * get batch number and interface no list if empty - make an entry if not empty - compare time
	 * stamp if T1 = T2 => return input else reject the message
	 */

	public YFCDocument invoke(YFCDocument inXml) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		if (isIpFrmInterface(inXml)) {
			try {
				return processInput(inXml);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}else{
			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
			return inXml;
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return null;
	}

	/**
	 * 
	 * @param inXml
	 * @return
	 * @throws Exception
	 */
	private YFCDocument processInput(YFCDocument inXml) throws Exception {

		YFCElement rootElem = inXml.getDocumentElement();
		YFCDocument getDupCheckListIpDoc = YFCDocument.createDocument("InterfaceDupCheck");
		YFCElement interfaceDupCheckElem = getDupCheckListIpDoc.getDocumentElement();
		interfaceDupCheckElem.setAttribute("BatchNo", rootElem.getAttribute("BatchNo"));
		interfaceDupCheckElem.setAttribute("InterfaceNo", rootElem.getAttribute("InterfaceNo"));
		LoggerUtil.verboseLog("GpsIntDupCheck::processInput::getDupCheckListIpDoc", logger, getDupCheckListIpDoc);
		YFCDocument getDupCheckListOpDoc =
				invokeYantraService("GpsGetInterfaceDupCheckList", getDupCheckListIpDoc);
		LoggerUtil.verboseLog("GpsIntDupCheck::processInput::getDupCheckListOpDoc", logger, getDupCheckListOpDoc);

		YFCElement getDupCheckListElem = getDupCheckListOpDoc.getDocumentElement();
		if (!getDupCheckListElem.hasChildNodes()) {
			insertRecord(inXml);
			removeInterfaceAttributes(rootElem);
			return inXml;
		} else {
			YFCElement intDupChkElem = getDupCheckListElem.getChildElement("InterfaceDupCheck");
			String strBatchDate1 = intDupChkElem.getAttribute("BatchDate");
			SimpleDateFormat sterlingFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
			Date dtBatchdate1 = sterlingFormat.parse(strBatchDate1);
			String strBatchDate2 = rootElem.getAttribute("BatchDate");
			Date dtBatchdate2 = sterlingFormat.parse(strBatchDate2);
			if (dtBatchdate1.equals(dtBatchdate2)) {
				removeInterfaceAttributes(rootElem);
				return inXml;
			} else {
				interfaceDupCheckElem.setAttribute("IsDuplicate", "Y");
				return getDupCheckListIpDoc;
			}
		}
	}

	/**
	 * 
	 * @param rootElem
	 */
	private void removeInterfaceAttributes(YFCElement rootElem) {
		rootElem.removeAttribute("BatchNo");
		rootElem.removeAttribute("InterfaceNo");
		rootElem.removeAttribute("BatchDate");
	}

	/**
	 * Sample Input <InterfaceDupCheck BatchDate="2016-05-23T18:40:00" BatchNo="7916042049748"
	 * InterfaceNo="CEV_ODR_2"/> 
	 * @param inXml
	 */
	private void insertRecord(YFCDocument inXml) {

		YFCElement rootElem = inXml.getDocumentElement();
		YFCDocument createDupCheckIpDoc = YFCDocument.createDocument("InterfaceDupCheck");
		YFCElement interfaceDupCheckElem = createDupCheckIpDoc.getDocumentElement();
		interfaceDupCheckElem.setAttribute("BatchNo", rootElem.getAttribute("BatchNo"));
		interfaceDupCheckElem.setAttribute("InterfaceNo", rootElem.getAttribute("InterfaceNo"));
		interfaceDupCheckElem.setAttribute("BatchDate", rootElem.getAttribute("BatchDate"));
		invokeYantraService("GpsCreateInterfaceDupCheck", createDupCheckIpDoc);

	}

	/**
	 * 
	 * @param inXml
	 * @return
	 */
	private boolean isIpFrmInterface(YFCDocument inXml) {
		YFCElement rootElem = inXml.getDocumentElement();
		if (!SCUtil.isVoid(rootElem.getAttribute("BatchNo"))
				&& !SCUtil.isVoid(rootElem.getAttribute("InterfaceNo"))
				&& !SCUtil.isVoid(rootElem.getAttribute("BatchDate"))) {
			return true;
		}
		return false;
	}

}
