package com.gps.hubble.alerts.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsConsolidateAlertCount extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsConsolidateAlertCount.class);

	/**
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument yfcDoc) throws YFSException {

		YFCDocument yfcDocGetExceptionListOp = (YFCDocument) getTxnObject("GpsGetSpecificExceptionOp");
		YFCDocument yfDocChangeExpectionOp = conslidateAlertCount(yfcDocGetExceptionListOp);
		return yfDocChangeExpectionOp;
	}

	/**
	 * 
	 * @param yfcDocGetExceptionListOp
	 * @return yfDocChangeExpectionOp
	 */
	public YFCDocument conslidateAlertCount(YFCDocument yfcDocGetExceptionListOp) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "conslidateAlertCount",
				yfcDocGetExceptionListOp);
		YFCElement yfcEleInbox = yfcDocGetExceptionListOp.getElementsByTagName("Inbox").item(0);
		int iConsolidateCount = yfcEleInbox.getIntAttribute("ConsolidationCount");
		String sInboxKey = yfcEleInbox.getAttribute("InboxKey");

		YFCDocument yfcDocChangeExceptionIp = YFCDocument.getDocumentFor("<Inbox ConsolidationCount='' InboxKey='' />");
		yfcDocChangeExceptionIp.getDocumentElement().setIntAttribute("ConsolidationCount", iConsolidateCount + 1);
		yfcDocChangeExceptionIp.getDocumentElement().setAttribute("InboxKey", sInboxKey);
		LoggerUtil.verboseLog("GpsConsolidateAlertCount::conslidateAlertCount::yfcDocChangeExceptionIp", logger,
				yfcDocChangeExceptionIp);

		YFCDocument yfDocChangeExpectionOp = invokeYantraApi("changeException", yfcDocChangeExceptionIp);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "conslidateAlertCount", yfDocChangeExpectionOp);

		return yfDocChangeExpectionOp;

	}
}
