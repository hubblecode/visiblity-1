package com.gps.hubble.alerts.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCIterable;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;


public class AssignAlerts extends AbstractCustomApi {
  
  private static YFCLogCategory logger = YFCLogCategory.instance(AssignAlerts.class);
  
    @Override
    public YFCDocument invoke(YFCDocument input) throws YFSException {
      logger.verbose("Input to AssignAlerts" + input);
      YFCElement docEle = input.getDocumentElement();
      YFCElement exceptionListEle = docEle.getChildElement("ExceptionList");
      YFCIterable<YFCElement> exceptions = exceptionListEle.getChildren("Exception");
      
      YFCDocument assignInput = YFCDocument.createDocument("AssignmentDetails");
      YFCElement assignInputEle = assignInput.getDocumentElement();
      YFCElement inboxEle = assignInputEle.createChild("Inbox");
      
      YFCDocument unassignExceptionInputDoc = YFCDocument.createDocument("Inbox");
      
      for (YFCElement exception : exceptions) {
        String assignToUserId = exception.getAttribute("AssignToUserId");
        if(XmlUtils.isVoid(assignToUserId)){
          logger.verbose("Invoking UnassignException for exception :: " + exception);
          unassignExceptionInputDoc.getDocumentElement().setAttribute("InboxKey", exception.getAttribute("InboxKey"));
          invokeYantraApi("unassignException", unassignExceptionInputDoc, "");
        }else{
        logger.verbose("Invoking AssignException for exception :: " + exception);
        assignInputEle.setAttribute("AssignToUserId", exception.getAttribute("AssignToUserId"));
        inboxEle.setAttribute("InboxKey", exception.getAttribute("InboxKey"));
        invokeYantraApi("assignException", assignInput, "");
        }        
      }
      
      return input;
    }

}
