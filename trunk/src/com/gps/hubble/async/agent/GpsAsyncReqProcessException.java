package com.gps.hubble.async.agent;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsAsyncReqProcessException extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(GpsAsyncReqProcessException.class);

	/**
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);

		YFCElement eleAsyncRequestList = XPathUtil.getXPathElement(inDoc, "//AsyncRequestList");
		if(!YFCCommon.isVoid(eleAsyncRequestList)){
			invokeYantraService(TelstraConstants.GPS_ASYNC_REQUEST_ALERT_QUEUE, inDoc);			
		}
		else{
			invokeYantraService(TelstraConstants.GPS_CHANGE_ASYNC_REQUEST, inDoc);						
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inDoc);

		return inDoc;
	}

}
