package com.gps.hubble.async.agent;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.agent.server.YCPAbstractAgent;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsTransportMatrixPurgeAgent extends YCPAbstractAgent {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsTransportMatrixPurgeAgent.class);

	@Override
	public void executeJob(YFSEnvironment env, Document inDoc) throws Exception {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "executeJob", inDoc);
		LoggerUtil.verboseLog("GpsTransportMatrixPurgeAgent::executeJob::inDoc\n", logger, YFCDocument.getDocumentFor(inDoc));

		// invoke delete api
		YIFClientFactory.getInstance().getApi().executeFlow(env, TelstraConstants.GPS_DELETE_TRANSPORT_MATRIX, inDoc);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "executeJob", inDoc);
		
	}

	@Override
	public List<Document> getJobs(YFSEnvironment env, Document inDoc, Document docLastMsg) throws Exception {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", inDoc);
		LoggerUtil.verboseLog("GpsTransportMatrixPurgeAgent::getJobs::InputDocument\n", logger,
				YFCDocument.getDocumentFor(inDoc));

		Element eleIpRoot = inDoc.getDocumentElement();
		String sRetentionDays = eleIpRoot.getAttribute(TelstraConstants.RETENTION_DAYS);

		Document docGetTransportMatrixListOp = getTransportMatrixListOp(env, sRetentionDays);
		List<Document> jobList = prepareReturnList(docGetTransportMatrixListOp);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", inDoc);
		return jobList;

	}

	/**
	 * 
	 * @param env
	 * @param sRetentionDays
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private Document getTransportMatrixListOp(YFSEnvironment env, String sRetentionDays) throws YFSException, RemoteException, YIFClientCreationException {
		
		String sToModifyTS = getToModifyTS(Integer.parseInt("-" + sRetentionDays));

		Document docGetTransportMatrixListIp = (YFCDocument.createDocument(TelstraConstants.GPS_TRANSPORT_MATRIX)).getDocument();
		docGetTransportMatrixListIp.getDocumentElement().setAttribute(TelstraConstants.TO_MODIFY_TS, sToModifyTS);
		docGetTransportMatrixListIp.getDocumentElement().setAttribute(TelstraConstants.MODIFY_TS_QRY_TYPE,
				TelstraConstants.DATE_RANGE);
		docGetTransportMatrixListIp.getDocumentElement().setAttribute(TelstraConstants.TO_TO_DATE, getCurrentTS());
		docGetTransportMatrixListIp.getDocumentElement().setAttribute(TelstraConstants.TO_DATE_QRY_TYPE,
				TelstraConstants.DATE_RANGE);
		LoggerUtil.verboseLog("GpsTransportMatrixPurgeAgent::getJobs::docGetTransportMatrixListIp\n", logger,
				YFCDocument.getDocumentFor(docGetTransportMatrixListIp));
		Document docGetTransportMatrixListOp = YIFClientFactory.getInstance().getApi().executeFlow(env,
				TelstraConstants.GPS_GET_TRANSPORT_MATRIX_LIST, docGetTransportMatrixListIp);
		LoggerUtil.verboseLog("GpsTransportMatrixPurgeAgent::getJobs::docGetTransportMatrixListOp\n", logger,
				YFCDocument.getDocumentFor(docGetTransportMatrixListOp));
		return docGetTransportMatrixListOp;
	}

	/**
	 * 
	 * @param docGetTransportMatrixListOp
	 * @return
	 */
	private List<Document> prepareReturnList(Document docGetTransportMatrixListOp) {
		Element eleRoot = docGetTransportMatrixListOp.getDocumentElement();
		NodeList nlChildNode = eleRoot.getChildNodes();

		List<Document> jobList = new ArrayList<>();
		for (int i = 0; i < nlChildNode.getLength(); i++) {

			Element eleChild = (Element) nlChildNode.item(i);
			Document docExecuteJobsIp = (YFCDocument.createDocument()).getDocument();
			Element eleImportedElement = (Element) docExecuteJobsIp.importNode(eleChild, true);
			docExecuteJobsIp.appendChild(eleImportedElement);

			jobList.add(docExecuteJobsIp);
		}
		return jobList;
	}
	
	/**
	 * 
	 * @param iRetentionDays
	 * @return
	 */
	private String getToModifyTS(int iRetentionDays) {

		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(TelstraConstants.DATE_TIME_FORMAT_2);
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, iRetentionDays); // number of days to add
		String sToModifyTS = sdf.format(c.getTime());
		LoggerUtil.verboseLog("GpsTransportMatrixPurgeAgent::getToCreateTS::sToModifyTS\n", logger, sToModifyTS);

		return sToModifyTS;
	}

	/**
	 * 
	 * @return
	 */
	private String getCurrentTS(){
		
		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(TelstraConstants.DATE_TIME_FORMAT_2);
		return sdf.format(currentDate);
		
	}
}
