package com.gps.hubble.async.agent;

import java.rmi.RemoteException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.interop.japi.YIFClientFactory;
import com.yantra.ycp.agent.server.YCPAbstractAgent;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSEnvironment;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsFileUploadPurgeAgent extends YCPAbstractAgent {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsFileUploadPurgeAgent.class);

	/**
	 * 
	 */
	@Override
	public void executeJob(YFSEnvironment env, Document inDoc) throws Exception {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "executeJob", inDoc);
		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent::executeJob::inDoc\n", logger,
				YFCDocument.getDocumentFor(inDoc));
		// invoke delete api
		YIFClientFactory.getInstance().getApi().executeFlow(env, TelstraConstants.GPS_DELETE_FILE_UPLOAD, inDoc);
		// delete corresponding record from file upload row table
		Document docFileUploadRowList = getFileUploadRowList(env, inDoc);
		deleteRecordFromFileUploadRowTable(env, docFileUploadRowList);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "executeJob", inDoc);

	}

	/**
	 * 
	 */
	@Override
	public List<Document> getJobs(YFSEnvironment env, Document inDoc, Document docLastMsg) throws Exception {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", inDoc);
		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent::getJobs::InputDocument\n", logger,
				YFCDocument.getDocumentFor(inDoc));

		Element eleIpRoot = inDoc.getDocumentElement();
		String sRetentionDays = eleIpRoot.getAttribute(TelstraConstants.RETENTION_DAYS);

		Document docGetFileUploadListOp = getFileUploadListOp(env, sRetentionDays);
		List<Document> jobList = prepareReturnList(docGetFileUploadListOp);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", inDoc);
		return jobList;

	}

	/**
	 * 
	 * @param env
	 * @param inDoc
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private Document getFileUploadRowList(YFSEnvironment env, Document inDoc)
			throws YFSException, RemoteException, YIFClientCreationException {

		String sFileUploadKey = ((Element) inDoc.getElementsByTagName(TelstraConstants.FILE_UPLOAD).item(0))
				.getAttribute(TelstraConstants.FILE_UPLOAD_KEY);
		YFCDocument getFileUploadRowListIp = YFCDocument
				.getDocumentFor("<GPSFileUploadRow FileUploadKey='" + sFileUploadKey + "' />");
		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent:: getFileUploadRowList ::getFileUploadRowListIp\n", logger,
				getFileUploadRowListIp);
		Document docGetFileUploadRowListOp = YIFClientFactory.getInstance().getApi().executeFlow(env,
				TelstraConstants.GPS_GET_FILE_UPLOAD_ROW_LIST, getFileUploadRowListIp.getDocument());
		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent:: getFileUploadRowList ::docGetFileUploadRowListOp\n", logger,
				docGetFileUploadRowListOp);
		return docGetFileUploadRowListOp;
	}

	/**
	 * 
	 * @param env
	 * @param docFileUploadRowList
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */

	private void deleteRecordFromFileUploadRowTable(YFSEnvironment env, Document docFileUploadRowList)
			throws YFSException, RemoteException, YIFClientCreationException {

		NodeList nlFileUploadRow = docFileUploadRowList.getElementsByTagName(TelstraConstants.FILE_UPLOAD_ROW);
		for (int i = 0; i < nlFileUploadRow.getLength(); i++) {

			Element eleFileUploadRow = (Element) nlFileUploadRow.item(i);
			Document docDeleteFileUploadRowIp = YFCDocument.createDocument().getDocument();
			Element eleDeleteFileUploadRowIp = (Element) docDeleteFileUploadRowIp.importNode(eleFileUploadRow, true);
			docDeleteFileUploadRowIp.appendChild(eleDeleteFileUploadRowIp);
			LoggerUtil.verboseLog(
					"GpsFileUploadPurgeAgent:: deleteRecordFromFileUploadRowTable ::docDeleteFileUploadRowIp\n", logger,
					docDeleteFileUploadRowIp);
			YIFClientFactory.getInstance().getApi().executeFlow(env, TelstraConstants.GPS_DELETE_FILE_UPLOAD_ROW,
					docDeleteFileUploadRowIp);
		}
	}

	/**
	 * 
	 * @param docGetFileUploadListOp
	 * @return
	 */
	private List<Document> prepareReturnList(Document docGetFileUploadListOp) {
		Element eleRoot = docGetFileUploadListOp.getDocumentElement();
		NodeList nlChildNode = eleRoot.getChildNodes();

		List<Document> jobList = new ArrayList<>();
		for (int i = 0; i < nlChildNode.getLength(); i++) {

			Element eleChild = (Element) nlChildNode.item(i);
			Document docExecuteJobsIp = (YFCDocument.createDocument()).getDocument();
			Element eleImportedElement = (Element) docExecuteJobsIp.importNode(eleChild, true);
			docExecuteJobsIp.appendChild(eleImportedElement);

			jobList.add(docExecuteJobsIp);
		}
		return jobList;
	}

	/**
	 * 
	 * @param env
	 * @param sRetentionDays
	 * @return
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private Document getFileUploadListOp(YFSEnvironment env, String sRetentionDays)
			throws YFSException, RemoteException, YIFClientCreationException {

		String sToModifyTS = getToModifyTS(Integer.parseInt("-" + sRetentionDays));

		Document docGetFileUploadListIp = (YFCDocument.createDocument(TelstraConstants.GPS_FILE_UPLOAD)).getDocument();
		docGetFileUploadListIp.getDocumentElement().setAttribute(TelstraConstants.TO_MODIFY_TS, sToModifyTS);
		docGetFileUploadListIp.getDocumentElement().setAttribute(TelstraConstants.MODIFY_TS_QRY_TYPE,
				TelstraConstants.DATE_RANGE);
		docGetFileUploadListIp.getDocumentElement().setAttribute(TelstraConstants.PROCESSED_FLAG, TelstraConstants.YES);

		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent::getJobs::docGetFileUploadListIp\n", logger,
				YFCDocument.getDocumentFor(docGetFileUploadListIp));

		Document docGetFileUploadListOp = YIFClientFactory.getInstance().getApi().executeFlow(env,
				TelstraConstants.GPS_GET_FILE_UPLOAD_LIST, docGetFileUploadListIp);
		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent::getJobs::docGetFileUploadListOp\n", logger,
				YFCDocument.getDocumentFor(docGetFileUploadListOp));

		return docGetFileUploadListOp;
	}

	/**
	 * 
	 * @param iRetentionDays
	 * @return
	 */
	private String getToModifyTS(int iRetentionDays) {

		Date currentDate = new Date();
		SimpleDateFormat sdf = new SimpleDateFormat(TelstraConstants.DATE_TIME_FORMAT_2);
		Calendar c = Calendar.getInstance();
		c.setTime(currentDate);
		c.add(Calendar.DATE, iRetentionDays); // number of days to add
		String sToModifyTS = sdf.format(c.getTime());
		LoggerUtil.verboseLog("GpsFileUploadPurgeAgent::getToCreateTS::sToCreateTS\n", logger, sToModifyTS);
		return sToModifyTS;
	}

}
