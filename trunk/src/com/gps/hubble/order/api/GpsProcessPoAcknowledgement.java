package com.gps.hubble.order.api;

import org.apache.commons.lang.StringUtils;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * 
 *
 */
public class GpsProcessPoAcknowledgement extends AbstractCustomApi{


	private static YFCLogCategory logger = YFCLogCategory.instance(GpsProcessPoAcknowledgement.class);

	/**
	 * 
	 */
	@Override
	public YFCDocument invoke(YFCDocument inDoc) throws YFSException {

		String sOrderHeaderKey = getOrderHeaderKeyFromOrderName(inDoc);
		if(StringUtils.isNotBlank(sOrderHeaderKey)){
			YFCElement eleOrderStatusChange = inDoc.getDocumentElement();
			eleOrderStatusChange.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);			
			invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, inDoc);
		}
		return inDoc;
	}

	/**
	 * 
	 * @param inDoc
	 * @return
	 */
	private String getOrderHeaderKeyFromOrderName(YFCDocument inDoc) {

		String sOrderHeaderKey = TelstraConstants.BLANK;

		String sOrderName = inDoc.getDocumentElement().getAttribute(TelstraConstants.ORDER_NAME);
		if(StringUtils.isNotBlank(sOrderName)){
			YFCDocument docGetOrderListIp = YFCDocument.createDocument(TelstraConstants.ORDER);
			docGetOrderListIp.getDocumentElement().setAttribute(TelstraConstants.ORDER_NAME, sOrderName);	
			docGetOrderListIp.getDocumentElement().setAttribute(TelstraConstants.DOCUMENT_TYPE, TelstraConstants.DOCUMENT_TYPE_0005);
			YFCDocument docGetOrderListTemp = YFCDocument.getDocumentFor("<OrderList><Order OrderHeaderKey='' OrderName='' MinOrderStatus=''/></OrderList>");


			YFCDocument docGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST, docGetOrderListIp, docGetOrderListTemp);

			YFCElement eleOrderOp = docGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER).item(0);

			if(YFCCommon.isVoid(eleOrderOp)){
				LoggerUtil.verboseLog("Invalid Order Name ", logger, docGetOrderListOp);
				//YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument(TelstraErrorCodeConstants.PO_ACKN_INVALID_ORDER);
				//throw new YFSException(erroDoc.toString());
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.PO_ACKN_INVALID_ORDER, new YFSException());
			}

			String sMinOrderStatus = eleOrderOp.getAttribute(TelstraConstants.MIN_ORDER_STATUS);
			sMinOrderStatus = sMinOrderStatus+".00";
			String [] arrayMaxOrderStatus = sMinOrderStatus.split(TelstraConstants.DOT_SEPARATOR);
			sMinOrderStatus = arrayMaxOrderStatus[0]+"."+arrayMaxOrderStatus[1];
			if(Double.parseDouble(TelstraConstants.PO_SENT_TO_VENDOR_STATUS)>Double.parseDouble(sMinOrderStatus)){
				sOrderHeaderKey = eleOrderOp.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
			}			
		}		

		return sOrderHeaderKey;
	}

}
