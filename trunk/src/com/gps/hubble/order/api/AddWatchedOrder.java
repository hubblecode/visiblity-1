package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Bridge
 * This class validates the number of Orders added to the watchlist for the user. If the Watched Order Count exceeds the configured limit, an exception is thrown.
 */
public class AddWatchedOrder extends AbstractCustomApi {
	
	private static 	YFCLogCategory logger = YFCLogCategory.instance(AddWatchedOrder.class); 
	@Override
	public YFCDocument invoke(YFCDocument inDoc) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		int maxWatchedOrders = Integer.parseInt(getProperty("MaxWatchlistPerUser",TelstraConstants.DEF_MAX_WATCHLIST));
		logger.debug("Max Watched Orders from Property "+maxWatchedOrders);
		int totalWatchedOrders = 0;
		YFCDocument erroDoc;		
		YFCDocument commonCodeDoc = YFCDocument.createDocument("CommonCode");
		commonCodeDoc.getDocumentElement().setAttribute("CodeType", "MAX_USER_WATCHLIST");
		commonCodeDoc.getDocumentElement().setAttribute("OrganizationCode",TelstraConstants.DEFAULT);
		YFCDocument commonCodeListOutDoc = invokeYantraApi("getCommonCodeList", commonCodeDoc);
		YFCElement commonCodeOutElem = commonCodeListOutDoc.getDocumentElement().getFirstChildElement();
		if(commonCodeOutElem != null) {
			maxWatchedOrders = Integer.parseInt(commonCodeOutElem.getAttribute("CodeValue",TelstraConstants.DEF_MAX_WATCHLIST));
		}
		logger.debug("Max Watched Orders from Common Code "+maxWatchedOrders);
		String watchedOrderServiceName = getProperty("WatchedOrderService","GPSGetWatchedOrderList");
		String createWatchedOrderServiceName= getProperty("CreateWatchedOrderService","GPSCreateWatchedOrder");
		YFCElement inElem = inDoc.getDocumentElement();
		String loginId = inElem.getAttribute("Loginid");
		YFCDocument watchedOrderInDoc = YFCDocument.createDocument("WatchedOrder");
		YFCElement watchedOrderInElem = watchedOrderInDoc.getDocumentElement();
		watchedOrderInElem.setAttribute("Loginid",loginId);
		YFCDocument watchedOrderListOutDoc = invokeYantraService(watchedOrderServiceName, watchedOrderInDoc);
		YFCElement watchedOrderOutElem = watchedOrderListOutDoc.getDocumentElement();
		logger.debug("Total Watched Order Length from API "+watchedOrderOutElem.getElementsByTagName("WatchedOrder").getLength());
		YFCNodeList<YFCElement> watchedOrderList = watchedOrderOutElem.getElementsByTagName("WatchedOrder");
		if(watchedOrderList!=null) {
			totalWatchedOrders = watchedOrderList.getLength();
		}
		
		//Incrementing the Watched Order count to include the current order
		totalWatchedOrders++;
		logger.debug("Total Watched Orders "+totalWatchedOrders);
		if(totalWatchedOrders > maxWatchedOrders) {
			erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_1161_005","You Have Exceeded The Limit Of Orders That Can Be Added To Your Watchlist. Please Remove An Existing Order From Watchlist And Try Again.");
			throw new YFSException(erroDoc.toString());	
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inDoc);
		return invokeYantraService(createWatchedOrderServiceName,inDoc);
	}

}
