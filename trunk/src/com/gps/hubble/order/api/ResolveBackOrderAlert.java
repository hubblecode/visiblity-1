/***********************************************************************************************
 * File	Name		: ResolveBackOrderAlert.java *
 * Description		: This class is called from On Release event on Schedule Order Transaction.
 * 						Will get all open backorder alerts of the order and resolve them.
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		Aug 4,2016	  	Tushar Dhaka 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 **********************************************************************************************/

package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.gps.hubble.constants.TelstraConstants;


/**
 * Custom code to call resolveException API
 * 
 * @author Tushar Dhaka
 * @version 1.0
 * 
 * Extends AbstractCustomApi class
 */
public class ResolveBackOrderAlert extends AbstractCustomApi{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(ResolveBackOrderAlert.class);
	
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		/*
		 * Input XML:-
			<OrderRelease CarrierAccountNo=" " CarrierServiceCode=" " Currency=" " CustomerPoNo=" " DeliveryCode=" " Division=" " EnterpriseCode="TELSTRA_SCLSA" FOB=" " HoldFlag="N" HoldReasonCode=" " MaxOrderReleaseStatus="3200" MaxOrderReleaseStatusDesc="Released" MinOrderReleaseStatus="3200" MinOrderReleaseStatusDesc="Released" NotifyAfterShipmentFlag="N" OrderDate="2016-08-04T12:28:24-04:00" OrderHeaderKey="2016080412282362033" OrderName=" " OrderReleaseKey="2016080414362462892" OrderType=" " OtherCharges="0.00" PacklistType=" " PersonalizeCode=" " PriorityCode=" " Purpose=" " ReleaseNo="2" ReleaseSeqNo="0" ReqCancelDate="2500-01-01T00:00:00-05:00" ReqDeliveryDate="2016-08-04T14:36:24-04:00" ReqShipDate="2016-08-04T14:36:24-04:00" SCAC=" " SalesOrderNo="Y100000598" ShipAdviceNo="Y100000598_2" ShipCompleteFlag="N" ShipLineComplete=" " ShipNode="TestOrg4" ShipNodeClass=" " ShipOrderComplete=" " ShipToKey="201606281800089819" Status="Released" SupplierCode="TestOrg4" SupplierName="TestOrg4" TaxpayerId=" " isHistory="N">
				<Order DocumentType="0001" EnterpriseCode="TELSTRA_SCLSA" OrderHeaderKey="2016080412282362033" OrderNo="Y100000598"/>
				<PersonInfoShipTo AddressLine1="" AddressLine2="" AddressLine3="" AddressLine4="" AddressLine5="" AddressLine6="" AlternateEmailID="" Beeper="" City="" Company="" Country="AU" DayFaxNo="" DayPhone="" Department="" EMailID="" EveningFaxNo="" EveningPhone="" FirstName="" JobTitle="" LastName="" MiddleName="" MobilePhone="" OtherPhone="" PersonID="" PersonInfoKey="201606281800089819" State="" Suffix="" Title="" ZipCode=""/>
				<PackListPriceInfo Currency="AUD" HeaderCharges="0.00" HeaderDiscount="0.00" HeaderTax="0.00" LineSubTotal="0.00" TotalAmount="0.00" TotalCharges="0.00" TotalDiscountAmount="0.00" TotalTax="0.00"/>
				<OrderLine PrimeLineNo="1" SubLineNo="1">
					<Item CostCurrency="" CountryOfOrigin="" CustomerItem="" CustomerItemDesc="" ECCNNo="" HarmonizedCode="" ISBN="" ItemDesc="" ItemID="Item1" ItemShortDesc="Item1" ItemWeight="0.00" ItemWeightUOM="KG" ManufacturerItem="" ManufacturerItemDesc="" ManufacturerName="" NMFCClass="" NMFCCode="" NMFCDescription="" ProductClass="" ProductLine="" ScheduleBCode="" SupplierItem="" SupplierItemDesc="" TaxProductCode="" UPCCode="" UnitCost="0.00" UnitOfMeasure="EACH"/>
					<OrderStatuses>
						<OrderStatus OrderHeaderKey="2016080412282362033" OrderLineKey="2016080413020862234" OrderLineScheduleKey="201608041494362462893" OrderReleaseKey="2016080414362462892" OrderReleaseStatusKey="201608041437362462894" PipelineKey="2016070816001313133" Status="3200" StatusDate="2016-08-04T14:36:24-04:00" StatusDescription="Released" StatusQty="10.00" TotalQuantity="10.00"/>
					</OrderStatuses>
					<LinePackListPriceInfo Charges="0.00" ExtendedPrice="0.00" LineTotal="0.00" ListPrice="0.00" RetailPrice="0.00" Tax="0.00" UnitPrice="0.00"/>
				</OrderLine>
				<OrderLine PrimeLineNo="2" SubLineNo="1">
					<Item CostCurrency="" CountryOfOrigin="" CustomerItem="" CustomerItemDesc="" ECCNNo="" HarmonizedCode="" ISBN="" ItemDesc="" ItemID="Item2" ItemShortDesc="Item2" ItemWeight="0.00" ItemWeightUOM="KG" ManufacturerItem="" ManufacturerItemDesc="" ManufacturerName="" NMFCClass="" NMFCCode="" NMFCDescription="" ProductClass="" ProductLine="" ScheduleBCode="" SupplierItem="" SupplierItemDesc="" TaxProductCode="" UPCCode="" UnitCost="0.00" UnitOfMeasure="EACH"/>
					<OrderStatuses>
						<OrderStatus OrderHeaderKey="2016080412282362033" OrderLineKey="2016080413020862235" OrderLineScheduleKey="201608041460362462895" OrderReleaseKey="2016080414362462892" OrderReleaseStatusKey="201608041476362462896" PipelineKey="2016070816001313133" Status="3200" StatusDate="2016-08-04T14:36:24-04:00" StatusDescription="Released" StatusQty="10.00" TotalQuantity="10.00"/>
					</OrderStatuses>
					<LinePackListPriceInfo Charges="0.00" ExtendedPrice="0.00" LineTotal="0.00" ListPrice="0.00" RetailPrice="0.00" Tax="0.00" UnitPrice="0.00"/>
				</OrderLine>
			</OrderRelease>

		 */
				
		//call getExceptionList to get all open backorder exceptions
		//input = <Inbox ActiveFlag='Y' Status='OPEN' OrderHeaderKey='2016080412282362033' ExceptionType='backorder'/>
		//template = <InboxList><Inbox InboxKey='' EnterpriseKey=''/></InboxList>
				
		String sOrderHeaderKey = inXml.getElementsByTagName(TelstraConstants.ORDER).item(0).getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		String sDocumentType = inXml.getElementsByTagName(TelstraConstants.ORDER).item(0).getAttribute(TelstraConstants.DOCUMENT_TYPE);
		
		String sExceptionType = getProperty(sDocumentType+".BackOrderExceptionType", true);

        	YFCDocument docGetExceptionListInput = YFCDocument.getDocumentFor("<Inbox ActiveFlag='Y' Status='OPEN' "
    				+ "OrderHeaderKey='"+sOrderHeaderKey+"' ExceptionType='"+sExceptionType+"'/>");
    		YFCDocument docTemplate = YFCDocument.getDocumentFor("<InboxList><Inbox InboxKey='' EnterpriseKey=''/></InboxList>");
    		
    		YFCDocument docGetExceptionListOutput = invokeYantraApi("getExceptionList", docGetExceptionListInput, docTemplate);
    		
    		for(YFCElement eleInbox : docGetExceptionListOutput.getElementsByTagName("Inbox")) {
    			String sInboxKey = eleInbox.getAttribute("InboxKey");
    			
    			String sAlertResolveComments = getProperty("AlertResolveComments", true);
    			//first call changeException to add comments and then add resolveException
    			//changeException Input = <Inbox InboxKey='2016080415180863371'><InboxReferencesList><InboxReferences ReferenceType="COMMENT" Value="Auto Closure" Name="Auto Closure3" CreateUserId="system"/>	</InboxReferencesList></Inbox>
    			YFCDocument docChangeExceptionInput = YFCDocument.getDocumentFor("<Inbox InboxKey='"+sInboxKey+"'>"
    					+ "<InboxReferencesList><InboxReferences ReferenceType='COMMENT' Value='"+sAlertResolveComments+"' "
    							+ "Name='"+sInboxKey+"'/>	</InboxReferencesList></Inbox>");
    			invokeYantraApi("changeException", docChangeExceptionInput);
    			
    			YFCDocument docResolveException = YFCDocument.getDocumentFor("<ResolutionDetails><Inbox InboxKey='"+sInboxKey+"'"
    					+ " ActiveFlag='Y' Status='OPEN' OrderHeaderKey='"+sOrderHeaderKey+"' ExceptionType='"+sExceptionType+"'></Inbox></ResolutionDetails>");
    			
    			invokeYantraApi("resolveException", docResolveException);
    		}

				
		
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", inXml);
		return inXml;
	}
}