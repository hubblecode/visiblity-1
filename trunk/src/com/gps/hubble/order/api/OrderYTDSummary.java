package com.gps.hubble.order.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.Calendar;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;


/**
 * Custom API for Year Till Date Order Summary for Sterling Portal Dashboard. This api summarizes the orders created till date for the year.
 * 
 * @author Vinay Bhanu
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */

public class OrderYTDSummary extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(OrderYTDSummary.class);
	@Override
	public YFCDocument invoke(YFCDocument inXml) {
	
	LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		Map<String, String> attributeColumnMap = getAttributeColumnMap();
		Set<String> selectAttributes = new HashSet<>();
		String attri = getProperty("GroupByAttributes",true);
		int inputYear = Calendar.getInstance().get(Calendar.YEAR);
		String enterpriseCode="";
		String documentType="";
		String orderType="";
		YFCElement rootElem = (inXml== null)? null:inXml.getDocumentElement();
		if(rootElem != null) {
			inputYear = rootElem.hasAttribute("Year")?rootElem.getIntAttribute("Year"):inputYear;
			enterpriseCode=rootElem.hasAttribute("EnterpriseCode")?rootElem.getAttribute("EnterpriseCode"):"";
			documentType=rootElem.hasAttribute("DocumentType")?rootElem.getAttribute("DocumentType"):"";
			orderType=rootElem.hasAttribute("OrderType")?rootElem.getAttribute("OrderType"):"";
		}
		
		if(!XmlUtils.isVoid(attri)){
			if(!attri.contains("NoOfOrders") && !attri.contains("TotalAmount")){
				YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_13061_001","Aggregate Attribute Not Provided.");
				throw new YFSException(erroDoc.toString());	
			}
			String[] s = attri.split(",");
			for (String string : s) {
				selectAttributes.add(string);
			}
		}
		if(!selectAttributes.isEmpty()){
			
			Connection connection = getDBConnection();
			YFCDocument docOutput = null;
			try (PreparedStatement ps = getQuery(connection,selectAttributes,attributeColumnMap,enterpriseCode,documentType,orderType,inputYear);
					ResultSet rs = ps.executeQuery()){
				docOutput =  constructDocument(rs,selectAttributes, attributeColumnMap);
			}catch(SQLException sqlEx){
				logger.error("SqlException while executing query ", sqlEx);
				throw new YFCException(sqlEx);
			}
			try (PreparedStatement psdate=getLastUpdatedQuery(connection,"GPS_YTD_ORDER_SUMMARY");
					ResultSet rsdate = psdate.executeQuery()){
				if(docOutput!=null) {
					if(rsdate!=null && rsdate.next()) {
						Timestamp updatedSqlDate = rsdate.getTimestamp("UPDATED_DATE");
						YTimestamp updatedDate= YTimestamp.newTimestamp(updatedSqlDate);
						docOutput.getDocumentElement().setDateTimeAttribute("UpdatedDate", updatedDate);
					}
				}
			}catch(SQLException sqlEx){
				logger.error("SqlException while executing query ", sqlEx);
				throw new YFCException(sqlEx);
			}			

			LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docOutput);
			return  docOutput;
		}else{
			throw new YFSException("Invalid Input");
		}

}

private PreparedStatement getLastUpdatedQuery(Connection connection,String tableName) throws SQLException {
	StringBuilder query = new StringBuilder();
	query.append("SELECT REFRESH_TIME AS UPDATED_DATE");
	query.append(" FROM SYSCAT.TABLES ");
	query.append(" WHERE TABNAME= ? ");
	query.append(" AND TYPE = 'S'");
	PreparedStatement ps = connection.prepareStatement(query.toString());
	ps.setString(1, tableName);
	return ps;
	
}
private Set<String> getSelectAttributes(YFCDocument inXml,Map<String, String> attributeColumnMap){
	Set<String> selectAttributes = new HashSet<>();
	if(inXml.getDocumentElement() == null || inXml.getDocumentElement().getChildElement("Attributes") == null){
		return selectAttributes;
	}
	YFCElement elemSelect = inXml.getDocumentElement().getChildElement("Attributes");
	if(elemSelect.getChildren("Attribute") == null){
		return selectAttributes;
	}
	for(Iterator<YFCElement> itr = elemSelect.getChildren("Attribute").iterator();itr.hasNext();){
		YFCElement elemAttribute = itr.next();
		String attribute = elemAttribute.getAttribute("Name");
		if(!attributeColumnMap.containsKey(attribute)){
			throw new YFCException(); 
		}
		selectAttributes.add(attribute);
	}
	return selectAttributes;
}

private PreparedStatement getQuery(Connection connection,Set<String> selectAttributes,Map<String, String> attributeColumnMap, String enterpriseCode,String documentType,String orderType, int inYear) throws SQLException {
	int psIndex = 1;
	StringBuilder builder = new StringBuilder();
	builder.append(getSelect(selectAttributes, attributeColumnMap));
	builder.append(" FROM GPS_YTD_ORDER_SUMMARY ");
	builder.append("  WHERE ");
	if(!XmlUtils.isVoid(enterpriseCode)) {
		builder.append(" ENTERPRISE_CODE = ? ");
		builder.append(" AND ");
	}
	if(!XmlUtils.isVoid(orderType)) {
		builder.append(" ORDER_TYPE = ? ");
		builder.append(" AND ");
	}	
	
	if(!XmlUtils.isVoid(documentType)) {
		builder.append(" DOCUMENT_TYPE = ? ");
		builder.append(" AND ");
	}	
	builder.append(" YEAR = ? ");
	builder.append(getGroupBy(selectAttributes, attributeColumnMap));
	String query = builder.toString();
	logger.debug("query is "+query);
	PreparedStatement ps = connection.prepareStatement(query);
	if(!XmlUtils.isVoid(enterpriseCode)) {
		ps.setString(psIndex, enterpriseCode);
		psIndex++;
	}
	if(!XmlUtils.isVoid(orderType)) {
		ps.setString(psIndex, orderType);
		psIndex++;
	}	
	if(!XmlUtils.isVoid(documentType)) {
		ps.setString(psIndex, documentType);
		psIndex++;
	}
		ps.setInt(psIndex, inYear);
	return ps;
}

private String getSelect(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
	StringBuilder builder = new StringBuilder();
	builder.append("SELECT ");
	boolean isFirst = true;
	for(String attribute : selectAttributes){
		if(!isFirst){
			builder.append(",");
		}
		String columnName = attributeColumnMap.get(attribute);
		if(attribute.equals("NoOfOrders")){
			builder.append("SUM(NO_OF_ORDERS) AS NO_OF_ORDERS");
		}else if(attribute.equals("TotalAmount")){
			builder.append("SUM(TOTAL_AMOUNT) AS TOTAL_AMOUNT");
		}else{
			builder.append(columnName);
		}
		isFirst = false;
	}
	return builder.toString();
}

private String getGroupBy(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
	StringBuilder builder = new StringBuilder();
	builder.append("GROUP BY ");
	boolean isFirst = true;
	for(String attribute : selectAttributes){
		String columnName = attributeColumnMap.get(attribute);
		if(!attribute.equals("NoOfOrders") && !attribute.equals("TotalAmount")){
			if(!isFirst){
				builder.append(",");
			}
			builder.append(columnName);
			isFirst = false;
		}

	}
	return builder.toString();
}

private Map<String, String> getAttributeColumnMap(){
	Map<String, String> attributeMap = new HashMap<String, String>();
	attributeMap.put("EnterpriseCode", "ENTERPRISE_CODE");
	attributeMap.put("OrderType", "ORDER_TYPE");
	attributeMap.put("DocumentType", "DOCUMENT_TYPE");
	attributeMap.put("Year", "YEAR");
	attributeMap.put("Month", "MONTH");
	attributeMap.put("NoOfOrders", "NO_OF_ORDERS");
	attributeMap.put("TotalAmount", "TOTAL_AMOUNT");
	return attributeMap;
}


private YFCDocument constructDocument(ResultSet rs, Set<String> attributes, Map<String, String> attributeColumnMap) throws SQLException{ 
	YFCDocument docYtdOrderSummary = YFCDocument.createDocument("OrderSummaryList");
	YFCElement elemYtdSummaryList = docYtdOrderSummary.getDocumentElement();
	while(rs.next()){
		YFCElement elemOrderSummary = elemYtdSummaryList.createChild("OrderSummary");
		if(attributes.contains("OrderType")){
			String orderType = rs.getString("ORDER_TYPE");
			elemOrderSummary.setAttribute("OrderType", orderType);
		}
		if(attributes.contains("DocumentType")){
			String documentType = rs.getString("DOCUMENT_TYPE");
			elemOrderSummary.setAttribute("DocumentType", documentType);
		}
		if(attributes.contains("Year")){
			Integer year = rs.getInt("YEAR");
			elemOrderSummary.setAttribute("Year", year);
		}
		if(attributes.contains("Month")){
			Integer month = rs.getInt("MONTH");
			elemOrderSummary.setAttribute("Month", month);
		}
		
		if(attributes.contains("NoOfOrders")){
			long noOfOrders = rs.getLong("NO_OF_ORDERS");
			elemOrderSummary.setAttribute("NoOfOrders", noOfOrders);
		}
		if(attributes.contains("TotalAmount")){
			double totalAmount = rs.getDouble("TOTAL_AMOUNT");
			elemOrderSummary.setAttribute("TotalAmount", totalAmount);
		}
	}
	return docYtdOrderSummary;
}


}



