package com.gps.hubble.order.api;

import javax.xml.transform.TransformerException;

import org.apache.commons.lang.StringUtils;
import org.apache.xpath.XPathAPI;
import org.w3c.dom.Element;

import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * This is a Javadoc comment
 */

public class GPSCopyAttributesToChainedOrderAPI {

	private static YFCLogCategory logger = YFCLogCategory.instance(GPSCopyAttributesToChainedOrderAPI.class);
		
	
	
	/**
	 * 
	 * @param inDoc
	 * @param docGetOrderListOp
	 * @param inDoc
	 * @throws TransformerException 
	 * @throws Exception
	 */

	public void copyAttributes(YFCDocument inDoc, YFCDocument docGetOrderListOp) throws TransformerException {

		logger.beginTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyAttributes\n");

		YFCElement eleOrderIp = inDoc.getElementsByTagName(TelstraConstants.ORDER).item(0);
		YFCElement eleOrderOp = docGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER).item(0);

		eleOrderIp.setAttribute(TelstraConstants.ORDER_NAME, eleOrderOp.getAttribute(TelstraConstants.ORDER_NAME));
		eleOrderIp.setAttribute(TelstraConstants.PRIORITY_CODE,
				eleOrderOp.getAttribute(TelstraConstants.PRIORITY_CODE));
		eleOrderIp.setAttribute(TelstraConstants.DIVISION, eleOrderOp.getAttribute(TelstraConstants.DIVISION));
		eleOrderIp.setAttribute(TelstraConstants.BILL_TO_ID, eleOrderOp.getAttribute(TelstraConstants.BILL_TO_ID));
		eleOrderIp.setAttribute(TelstraConstants.SHIP_TO_ID, eleOrderOp.getAttribute(TelstraConstants.SHIP_TO_ID));
		eleOrderIp.setAttribute(TelstraConstants.DEPARTMENT_CODE,
				eleOrderOp.getAttribute(TelstraConstants.DEPARTMENT_CODE));
		eleOrderIp.setAttribute(TelstraConstants.ENTRY_TYPE, eleOrderOp.getAttribute(TelstraConstants.ENTRY_TYPE));
		eleOrderIp.setAttribute(TelstraConstants.ORDER_DATE, eleOrderOp.getAttribute(TelstraConstants.ORDER_DATE));
		eleOrderIp.setAttribute(TelstraConstants.ORDER_TYPE, eleOrderOp.getAttribute(TelstraConstants.ORDER_TYPE));
		eleOrderIp.setAttribute(TelstraConstants.SEARCH_CRITERIA_1,
				eleOrderOp.getAttribute(TelstraConstants.SEARCH_CRITERIA_1));
		eleOrderIp.setAttribute(TelstraConstants.SEARCH_CRITERIA_2,
				eleOrderOp.getAttribute(TelstraConstants.SEARCH_CRITERIA_2));
		eleOrderIp.setAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE,
				eleOrderOp.getAttribute(TelstraConstants.REQUIRED_DELIVERY_DATE));
		eleOrderIp.setAttribute(TelstraConstants.REQUIRED_SHIP_DATE,
				eleOrderOp.getAttribute(TelstraConstants.REQUIRED_SHIP_DATE));

		//copy ship to address
		YFCElement elePersonInfoShipToOrderList = docGetOrderListOp
				.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);
		if (!YFCCommon.isVoid(elePersonInfoShipToOrderList)) {

			YFCElement elePersonInfoShipTo = eleOrderIp.getChildElement(TelstraConstants.PERSON_INFO_SHIP_TO);		
			if(!YFCCommon.isVoid(elePersonInfoShipTo)){
				inDoc.removeChild(elePersonInfoShipTo);
			}
			elePersonInfoShipTo = inDoc.importNode(elePersonInfoShipToOrderList, true);
			eleOrderIp.appendChild(elePersonInfoShipTo);		
		}
		
		//copy bill to address
		YFCElement elePersonInfoBillToOrderList = docGetOrderListOp
				.getElementsByTagName(TelstraConstants.PERSON_INFO_BILL_TO).item(0);

		if (!YFCCommon.isVoid(elePersonInfoBillToOrderList)) {
			
			YFCElement elePersonInfoBillTo = eleOrderIp.getChildElement(TelstraConstants.PERSON_INFO_BILL_TO);		
			if(!YFCCommon.isVoid(elePersonInfoBillTo)){
				inDoc.removeChild(elePersonInfoBillTo);
			}
			elePersonInfoBillTo = inDoc.importNode(elePersonInfoBillToOrderList, true);
			eleOrderIp.appendChild(elePersonInfoBillTo);				
		}		
		//copy additional addresses
		YFCElement eleAdditionalAddressesOrderList =  
				docGetOrderListOp.getElementsByTagName(TelstraConstants.ADDITIONAL_ADDRESSES).item(0);
		if(!YFCCommon.isVoid(eleAdditionalAddressesOrderList)){
			YFCElement eleAdditionalAddresses = inDoc.importNode(eleAdditionalAddressesOrderList, true);
			eleOrderIp.appendChild(eleAdditionalAddresses);
		}
		
		copyShipNodeInOrderLine(inDoc, docGetOrderListOp);
		
		logger.debug("\nGPSCopyAttributesToChainedOrderAPI :: copyAttributes :: return doc :- \n"
				+ inDoc);
		
		logger.endTimer("\nGPSCopyAttributesToChainedOrderAPI :: callChangeOrderApi\n");

	}

	/**
	 * This method copy ship node from sales order to chained PO.
	 * @param inDoc 
	 * 
	 * @param docChangeOrderIp
	 * @param docGetOrderListOp
	 * @throws TransformerException 
	 */
	private void copyShipNodeInOrderLine(YFCDocument inDoc, YFCDocument docGetOrderListOp) throws TransformerException {

		logger.beginTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyShipNodeInOrderLine\n");

		YFCNodeList<YFCElement> nlOrderLineIp = inDoc
				.getElementsByTagName(TelstraConstants.ORDER_LINE);
				
		for (YFCElement eleOrderLineIp : nlOrderLineIp) {

			String sChaninedFromOrderLineKey = eleOrderLineIp.getAttribute(TelstraConstants.CHAINED_FROM_ORDER_LINE_KEY);
			if(StringUtils.isNotBlank(sChaninedFromOrderLineKey)){
				Element eleOrderLineOp = (Element) XPathAPI.selectSingleNode(docGetOrderListOp.getDocument(), "//OrderList/Order/OrderLines/OrderLine[@OrderLineKey='"+sChaninedFromOrderLineKey+"']");
				if(!YFCCommon.isVoid(eleOrderLineOp)){
					eleOrderLineIp.setAttribute(TelstraConstants.SHIP_NODE, eleOrderLineOp.getAttribute(TelstraConstants.SHIP_NODE));
				}
			}
		}		
		logger.endTimer("\nGPSCopyAttributesToChainedOrderAPI :: copyShipNodeInOrderLine\n");
	}

}
