/***********************************************************************************************
 * File	Name		: GpsManageVectorOrder.java
 *
 * Description		: This class is called from ManageOrder.java
 * 					The purpose of this class is to create and manage Vector order
 * 
 * Modification	Log	:
 * -------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * 
 * 
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		Mar 27,2017	  	Prateek Kumar 		   	Initial	Version 
 * 1.1		May 02,2017		Prateek Kumar			HUB-8731: If MR is in released status, back ordering the order before updating the address and then moving it to created status
 * 1.2		May 10,2017		Prateek Kumar			Zip code validation for Standard vector order
 * 1.3		May 16,2017		Prateek Kumar			HUB-9069 throwing a valid exception if the cancellation comes for the new line and consuming the cancellation message if the line is already cancelled
 * 1.4		May 18,2017		Prateek Kumar			HUB-9067 Taking into account multiple release scenario while updating MR addresses
 * -------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.gps.hubble.utils.GpsManageOrderUtil;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * Sample xml
 * 
 * @author Prateek
 *
 */
public class GpsManageVectorOrder {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsManageVectorOrder.class);

	enum ProductType{
		COMBINATION,CUSTOMIZED,STANDARD;
	}

	private ServiceInvoker serviceInvoker;
	private YFCDocument yfcInDoc;
	private String sOrderHeaderKey = "";

	/**
	 * 
	 * @param yfcInDoc
	 * @param serviceInvoker
	 */

	public YFCDocument manageVectorOrder(YFCDocument yfcInDoc, ServiceInvoker serviceInvoker) {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		this.serviceInvoker = serviceInvoker;
		this.yfcInDoc = yfcInDoc;
		boolean isOrderHasItem = false;

		/*
		 * move the lra details from order extn to order line hangoff
		 */
		moveLraToOrderLineHangoff();
		/*
		 * This method update the order with primeLineNo
		 */
		YFCDocument yfcDocGetOrderListOp = updateOrderWithLineNo();
		/*
		 * Based on the product type, below method will update order with the change status Indicator
		 */
		updateOrderWithChangeStatusIndicator();
		YFCElement yfcEleItem = XPathUtil.getXPathElement(this.yfcInDoc, "//OrderLine/Item");
		if (!YFCCommon.isVoid(yfcEleItem)) {

			/*
			 * Below method will replace the Vector Item ID and UOM with the
			 * Integral Plus
			 */
			updateOrderWithIpItemID();
			isOrderHasItem = true;
		}		

		YFCElement yfcEleOrderRoot = this.yfcInDoc.getDocumentElement();
		String sOrderName = yfcEleOrderRoot.getAttribute(TelstraConstants.ORDER_NAME);
		/*
		 * call get order list to get order header key from the order
		 */

		if (!YFCCommon.isStringVoid(sOrderName)) {
			YFCDocument yfcDocGetOrderListOpWithOrderName = callGetOrderListWithOrderName(sOrderName);
			sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderListOpWithOrderName, "//Order/@OrderHeaderKey");
			if(!YFCCommon.isStringVoid(sOrderHeaderKey)){
				/*
				 * Append action = "Create" for the new line. It is required for date computation in before create order ue
				 */
				appendActionCreateForNewLine(yfcDocGetOrderListOpWithOrderName);
				yfcEleOrderRoot.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
			}
		}

		String sIncludeReservation = yfcEleOrderRoot.getAttribute(TelstraConstants.INCLUDE_RESERVATIONS);
		if (TelstraConstants.YES.equalsIgnoreCase(sIncludeReservation)) {
			//			processReservations(yfcEleOrderRoot);
			serviceInvoker.invokeYantraService(TelstraConstants.GPS_UPDATE_VECTOR_LRA_DROP_Q, YFCDocument.getDocumentFor(yfcEleOrderRoot.toString()));
		}
		/*
		 * If change status is not cancel, then only proceed forward. In case change status is cancel, 
		 * no further processing is required as order has already been cancelled. 
		 */
		if(isOrderHasItem){

			/*
			 * Below method will replace the Vector Item ID and UOM with the
			 * Integral Plus
			 */
			updateOrderWithIpItemID();

			if (YFCCommon.isStringVoid(sOrderHeaderKey)) {
				/*
				 * Order creation 
				 */
				yfcInDoc.getDocumentElement().setAttribute(TelstraConstants.ACTION, "Create");
				LoggerUtil.verboseLog("GpsManageVectorOrder :: manageVectorOrder :: Create order Ip \n", logger,
						yfcInDoc);

				serviceInvoker.invokeYantraApi(TelstraConstants.API_CREATE_ORDER, yfcInDoc);
			} else {
				processChangeOrder(yfcDocGetOrderListOp);
			}
		}
		GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
		obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />"),serviceInvoker);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}

	/**
	 * This method move the lra details from Order/Extn to //OrderLine/LRAList/LRA element
	 */
	private void moveLraToOrderLineHangoff() {

		YFCElement yfcEleOrderLine =  yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		YFCElement yfcEleOLExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
		if(YFCCommon.isVoid(yfcEleOLExtn)){
			yfcEleOLExtn = yfcEleOrderLine.createChild(TelstraConstants.EXTN);
		}
		YFCElement LraList = yfcEleOLExtn.createChild(TelstraConstants.LRA_LIST);
		YFCElement Lra = LraList.createChild(TelstraConstants.LRA);
		YFCElement yfcEleExtn = yfcInDoc.getDocumentElement().getChildElement(TelstraConstants.EXTN);

		Lra.setAttribute(TelstraConstants.LRA_ID, yfcEleExtn.getAttribute(TelstraConstants.LRA));
		Lra.setAttribute(TelstraConstants.LRA_SERVICE, yfcEleExtn.getAttribute(TelstraConstants.SERVICE));
		Lra.setAttribute(TelstraConstants.PRODUCT_TYPE, yfcEleExtn.getAttribute(TelstraConstants.PRODUCT_TYPE));
		
		yfcEleExtn.getParentElement().removeChild(yfcEleExtn);
		
	}

	/**
	 * 
	 */
	private void updateOrderWithChangeStatusIndicator() {

		String sProductType = XPathUtil.getXpathAttribute(yfcInDoc, "//OrderLine/Extn/LRAList/LRA/@ProductType");
		String sChangeStatus = XPathUtil.getXpathAttribute(yfcInDoc, "//Order/OrderLines/OrderLine/@ChangeStatus");
		if ((ProductType.COMBINATION.toString().equals(sProductType)
				|| ProductType.STANDARD.toString().equals(sProductType))
				&& TelstraConstants.CHANGE_STATUS_CANCELLED.equalsIgnoreCase(sChangeStatus)) {
			yfcInDoc.getDocumentElement().setAttribute(TelstraConstants.CHANGE_STATUS, TelstraConstants.CHANGE_STATUS_CANCELLED);
		}		
	}

	/**
	 * This method update the order with the order line no 
	 */
	private YFCDocument updateOrderWithLineNo() {

		List<String> listVectorId = new ArrayList<>();
		/*
		 * preparing the set of the vector id
		 */
		for(YFCElement yfcEleOrderLine : yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCCommon.isVoid(yfcEleExtn)){

				String sVectorId = yfcEleExtn.getAttribute(TelstraConstants.VECTOR_ID);
				if(!YFCCommon.isStringVoid(sVectorId)){
					listVectorId.add(sVectorId);
				}
			}
		}
		/*
		 * fetch the map with vector id as key and prime line no as value
		 */
		GpsManageOrderUtil obj = new GpsManageOrderUtil();
		Map<String, String> mapVectorIdPrimeLineNo = obj.getVectorIdLineNoMap(listVectorId, serviceInvoker);
		YFCDocument yfcDocGetOrderListOp = obj.getYfcDocGetOrderListOp();

		/*
		 * update order with the prime line no from the map
		 */
		for(YFCElement yfcEleOrderLine : yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
			if(!YFCCommon.isVoid(yfcEleExtn)){

				String sVectorId = yfcEleExtn.getAttribute(TelstraConstants.VECTOR_ID);
				if(!YFCCommon.isStringVoid(sVectorId)){

					String sPrimeLineNo = mapVectorIdPrimeLineNo.get(sVectorId);
					if(!YFCCommon.isStringVoid(sPrimeLineNo)){
						yfcEleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
						yfcEleOrderLine.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
					}
				}
			}
		}
		return yfcDocGetOrderListOp;
	}

	/**
	 * This method update the new order line with action = "CREATE"
	 * @param yfcDocGetOrderListOp
	 */
	private void appendActionCreateForNewLine(YFCDocument yfcDocGetOrderListOp) {

		YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for(YFCElement yfcEleOrderLine : yfcNlOrderLine){

			String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
			YFCElement yfcEleOrderLineInOrderList = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			if(YFCCommon.isVoid(yfcEleOrderLineInOrderList)){
				yfcEleOrderLine.setAttribute(TelstraConstants.ACTION, "CREATE");
			}			
		}		
	}

	/**
	 * If change status is D, cancel the incoming order and blank out LRA level
	 * details in all the MR sharing same LRA This method update the go dac for
	 * all the MR sharing the same project number
	 * 
	 * @param yfcEleOrderRoot
	 * @param sOrderChangeStatus 
	 * 
	 *//*
	private void processReservations(YFCElement yfcEleOrderRoot) {


		String sOrderChangeStatus = yfcEleOrderRoot.getAttribute(TelstraConstants.CHANGE_STATUS);
		if (TelstraConstants.CHANGE_STATUS_CANCELLED.equalsIgnoreCase(sOrderChangeStatus) && !YFCCommon.isStringVoid(sOrderHeaderKey)) {


	  * Call get order list with the passed LRA, blank out all the LRA
	  * details in the MR returned

			blankOutLRADetailsInMR();

		} else {


	  * Get all the orders having the above project number

			Map<String,Map <String, String>> mapOHKeyLineNoReleasedQty = new HashMap<>();

			YFCDocument yfcDocGetOrderListOp = callGetOrderListWithProjectNo(yfcEleOrderRoot.getAttribute(TelstraConstants.SEARCH_CRITERIA_1));
			moveReleasedLineToBO(yfcDocGetOrderListOp, mapOHKeyLineNoReleasedQty);
			updateOrderLineWithAddress(yfcDocGetOrderListOp, mapOHKeyLineNoReleasedQty);
			if(!mapOHKeyLineNoReleasedQty.isEmpty()){
				moveBOLineToCreated(yfcDocGetOrderListOp, mapOHKeyLineNoReleasedQty);
			}
		}

	}*/

	/**
	 * 
	 * @param yfcDocGetOrderListOp
	 * @param mapOHKeyLineNoReleasedQty
	 *//*
	private void moveBOLineToCreated(YFCDocument yfcDocGetOrderListOp, Map<String, Map<String, String>> mapOHKeyLineNoReleasedQty) {


		for (Entry<String, Map<String, String>> entryMap1 : mapOHKeyLineNoReleasedQty.entrySet()){

			String sOrderHeaderKey = entryMap1.getKey();

			String sDocumentType = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order[@OrderHeaderKey='"+sOrderHeaderKey+"']/@DocumentType");
			String sTransactionType = "CHANGE_BO_STATUS."+sDocumentType+".ex";
			YFCDocument docChnageOrderStatusIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"' TransactionId='"+sTransactionType+"'><OrderLines/></Order>");
			YFCElement yfcEleOrderLines = docChnageOrderStatusIp.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);

			for (Map.Entry<String, String> entry : entryMap1.getValue().entrySet()){

				String sLineNo = entry.getKey();
				String sQty = entry.getValue();

				YFCElement yfcEleOrderLine = yfcEleOrderLines.createChild(TelstraConstants.ORDER_LINE);
				yfcEleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sLineNo);
				yfcEleOrderLine.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
				yfcEleOrderLine.setAttribute(TelstraConstants.QUANTITY, sQty);
				yfcEleOrderLine.setAttribute("BaseDropStatus", "1100");
			}

			LoggerUtil.verboseLog("GpsManageVectorOrder :: moveBOLineToCreated :: changeOrderStatus \n", logger,
					docChnageOrderStatusIp);
			serviceInvoker.invokeYantraApi("changeOrderStatus", docChnageOrderStatusIp);
		}
	}

	  *//**
	  * This method will move the released qty to backorder
	  * @param yfcDocGetOrderListOp
	  * @param mapOHKeyLineNoReleasedQty
	  *//*
	private void moveReleasedLineToBO(YFCDocument yfcDocGetOrderListOp, Map<String, Map<String, String>> mapOHKeyLineNoReleasedQty) {

		List<String> lOrderReleaseKey = new ArrayList<>();
		for(YFCElement yfcEleOrderLine : yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleStatus = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcEleOrderLine.toString()),
					"//OrderStatus[@Status='3200']");
			if(!YFCCommon.isVoid(yfcEleStatus)){

				String sOrderHeaderKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

				Map<String, String> mapLineNoReleasedQty;
				if(mapOHKeyLineNoReleasedQty.containsKey(sOrderHeaderKey)){
					mapLineNoReleasedQty = mapOHKeyLineNoReleasedQty.get(sOrderHeaderKey);
				}
				else{
					mapLineNoReleasedQty = new HashMap<>();
					mapOHKeyLineNoReleasedQty.put(sOrderHeaderKey, mapLineNoReleasedQty);
				}

				String sStatusQty = yfcEleStatus.getAttribute(TelstraConstants.STATUS_QTY);
				mapLineNoReleasedQty.put(yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO), sStatusQty);

				String sOrderReleaseKey = yfcEleStatus.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
				if(!lOrderReleaseKey.contains(sOrderReleaseKey)){
					lOrderReleaseKey.add(sOrderReleaseKey);
				}
			}			
		}


	   * prepare change release input to back order complete release

		LoggerUtil.verboseLog("GpsManageVectorOrder :: moveReleasedLineToBO :: lOrderReleaseKey \n", logger,
				lOrderReleaseKey);
		for(String sOrderReleaseKey : lOrderReleaseKey){
			YFCDocument docChangeReleaseIp = YFCDocument.getDocumentFor("<OrderRelease Action='BACKORDER' OrderReleaseKey='"+sOrderReleaseKey+"'/>");
			LoggerUtil.verboseLog("GpsManageVectorOrder :: moveReleasedLineToBO :: docChangeReleaseIp \n", logger,
					docChangeReleaseIp);
			serviceInvoker.invokeYantraApi("changeRelease", docChangeReleaseIp);
		}
	}

	   *//**
	   * 
	   * @param sLRA
	   *//*
	private void blankOutLRADetailsInMR() {

		String sLRA = XPathUtil.getXpathAttribute(yfcInDoc, "//Order/Extn/@LRA");

	    * calling get order list with the LRA

		YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineListWithLRA(sLRA);

		YFCNodeList<YFCElement> yfcNlOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE);

		for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {

			YFCElement yfcEleOrder = yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER).item(0);
			String sEntryType = yfcEleOrder.getAttribute(TelstraConstants.ENTRY_TYPE);

	    * Not blanking out for vector orders as it would be cancelled separately

			if(TelstraConstants.VECTOR.equalsIgnoreCase(sEntryType)){
				continue;
			}

			boolean bRemoveLRADetail = true;
			YFCNodeList<YFCElement> yfcNlOrderStatus = yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUS);

			for(YFCElement yfcEleOrderStatus : yfcNlOrderStatus){
				double dStatus = yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
				if(dStatus>3200){
					bRemoveLRADetail = false;
					break;
				}
			}

			if(bRemoveLRADetail){

				YFCDocument yfcDocGetLRAListIp = YFCDocument.getDocumentFor(
						"<LRA OrderLineKey='" + yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY) + "'/>");
				LoggerUtil.verboseLog("GpsManageVectorOrder :: blankOutLRADetailsInMR :: yfcDocGetLRAListIp \n", logger,
						yfcDocGetLRAListIp);

				YFCDocument yfcDocGetLRAListOp = serviceInvoker.invokeYantraService(TelstraConstants.SERVICE_GPS_GET_LRA_LIST, yfcDocGetLRAListIp);
				LoggerUtil.verboseLog("GpsManageVectorOrder :: blankOutLRADetailsInMR :: yfcDocGetLRAListOp \n", logger,
						yfcDocGetLRAListOp);

				for(YFCElement yfcEleLra : yfcDocGetLRAListOp.getElementsByTagName(TelstraConstants.LRA)){
					serviceInvoker.invokeYantraService(TelstraConstants.SERVICE_GPS_DELETE_LRA, YFCDocument.getDocumentFor(yfcEleLra.toString()));					
				}
			}
		}
	}*/
	/*
	 *//**
	 * 
	 * @param sLRA
	 * @return
	 *//*
	private YFCDocument callGetOrderListWithLRA(String sLRA) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument
				.getDocumentFor("<Order><Extn LRA='" + sLRA + "'/></Order>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithLRA :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderListIp);
		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList TotalNumberOfRecords=''><Order DocumentType='' OrderNo='' OrderHeaderKey=''><Extn/></Order></OrderList>");

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithLRA :: change order Ip \n", logger,
				yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithLRA :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;

	}*/

	/*	private YFCDocument callGetOrderLineListWithLRA(String sLRA){

		YFCDocument yfcDocGetOrderLineListIp = YFCDocument
				.getDocumentFor("<OrderLine><Extn> <LRAList> <LRA LRAID='"+sLRA+"'/> </LRAList></Extn></OrderLine>");

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderLineListWithLRA :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderLineListIp);
		YFCDocument yfcDocGetOrderLineListTemp = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderLineKey='' PrimeLineNo='' OrderHeaderKey=''> <Order EntryType=''/> <OrderStatuses> <OrderStatus Status=''/>  </OrderStatuses> </OrderLine></OrderLineList>");


		YFCDocument yfcDocGetOrderLineListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LINE_LIST,
				yfcDocGetOrderLineListIp, yfcDocGetOrderLineListTemp);

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderLineListWithLRA :: yfcDocGetOrderLineListOp\n", logger,
				yfcDocGetOrderLineListOp);

		return yfcDocGetOrderLineListOp;

	}
	 */
	/**
	 * This method replace the item id and UOM in the incoming message with the
	 * Integral Plus Item and UOM
	 */
	private void updateOrderWithIpItemID() {

		Map<String, String> mapItemIDUom = new HashMap<>();
		YFCNodeList<YFCElement> yfcNlItem = yfcInDoc.getElementsByTagName(TelstraConstants.ITEM);

		for (YFCElement yfcEleItem : yfcNlItem) {
			String sItemID = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			String sIpItemIDUom = "";
			if (mapItemIDUom.containsKey(sItemID)) {
				sIpItemIDUom = mapItemIDUom.get(sItemID);
			} else {
				String sUom = yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
				sIpItemIDUom = getIntegralPlusItemIDAndUOM(sItemID, sUom);
				if (!YFCCommon.isStringVoid(sIpItemIDUom)) {
					mapItemIDUom.put(sItemID, sIpItemIDUom);
				}
			}
			if (!YFCCommon.isStringVoid(sIpItemIDUom)) {
				String[] tokens = sIpItemIDUom.split("\\*");
				String sIpItemID = tokens[0];
				yfcEleItem.setAttribute(TelstraConstants.ITEM_ID, sIpItemID);
				String sUOM = tokens[1];
				yfcEleItem.setAttribute(TelstraConstants.UNIT_OF_MEASURE, sUOM);
			}
		}
	}

	/**
	 * 
	 * @param sItemID
	 * @param sUom
	 * @return ItemIDUom combination
	 */
	private String getIntegralPlusItemIDAndUOM(String sItemID, String sUom) {

		/*
		 * call get item list with the incoming item id to fetch unit of measure
		 */
		YFCDocument yfcDocGetItemListIp = YFCDocument
				.getDocumentFor("<Item ItemID='" + sItemID + "' OrganizationCode='TELSTRA_SCLSA'/>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: getIntegralPlusItemIDAndUOM :: yfcDocGetItemListIp 1 \n", logger,
				yfcDocGetItemListIp);

		YFCDocument yfcDocGetItemListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
				yfcDocGetItemListIp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: getIntegralPlusItemIDAndUOM :: yfcDocGetItemListOp 1 \n", logger,
				yfcDocGetItemListOp);

		YFCElement yfcEleItem = yfcDocGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		if (YFCCommon.isVoid(yfcEleItem)) {
			/*
			 * If incoming item id does not exist in Sterling, call get item
			 * list with incoming item id as Vector item alias
			 */
			yfcDocGetItemListIp = YFCDocument.getDocumentFor(
					"<Item OrganizationCode='TELSTRA_SCLSA'><ItemAliasList><ItemAlias AliasName='VECTOR_ITEM' AliasValue='"
							+ sItemID + "'/></ItemAliasList></Item>");
			LoggerUtil.verboseLog("GpsManageVectorOrder :: getIntegralPlusItemIDAndUOM :: yfcDocGetItemListIp 2 \n",
					logger, yfcDocGetItemListIp);
			yfcDocGetItemListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ITEM_LIST,
					yfcDocGetItemListIp);
			LoggerUtil.verboseLog("GpsManageVectorOrder :: getIntegralPlusItemIDAndUOM :: yfcDocGetItemListOp 2 \n",
					logger, yfcDocGetItemListOp);

			yfcEleItem = yfcDocGetItemListOp.getElementsByTagName(TelstraConstants.ITEM).item(0);
		}
		String sReturn = "";
		if (YFCCommon.isVoid(yfcEleItem)) {
			sReturn = sItemID + "*" + sUom;
		} else {
			String sIpItemID = yfcEleItem.getAttribute(TelstraConstants.ITEM_ID);
			String sIpUom = yfcEleItem.getAttribute(TelstraConstants.UNIT_OF_MEASURE);
			sReturn = sIpItemID + "*" + sIpUom;
		}

		return sReturn;
	}

	/** 
	 * 
	 * @param yfcDocGetOrderListOp 
	 * @param sIncludeReservation
	 * 
	 */
	private void processChangeOrder(YFCDocument yfcDocGetOrderListOp) {

		List<String> listPrimeLineNo = new ArrayList<>();
		YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {
			String sOrderLineChangeStatus = yfcEleOrderLine.getAttribute(TelstraConstants.CHANGE_STATUS);
			if (TelstraConstants.CHANGE_STATUS_CANCELLED.equalsIgnoreCase(sOrderLineChangeStatus)) {

				String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);	

				if(YFCCommon.isStringVoid(sPrimeLineNo)){
					throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.VECTOR_ID_INVALID_ERROR_CODE,
							new YFSException());
				}

				YFCElement yfcEleOrderStatus = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']/OrderStatuses/OrderStatus[@Status='9000']");
				if(YFCElement.isVoid(yfcEleOrderStatus)){
					yfcEleOrderLine.setAttribute(TelstraConstants.ACTION, TelstraConstants.CANCEL);
				}
				else{
					listPrimeLineNo.add(sPrimeLineNo);
				}				
			}
		}

		for(String sPrimeLineNo : listPrimeLineNo){
			YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcInDoc, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
		}

		YFCNodeList<YFCElement> yfcNlOrderLineChangeOrder = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);

		LoggerUtil.verboseLog("GpsManageVectorOrder :: processChangeOrder :: Change Order Ip \n", logger, yfcInDoc);

		if(yfcNlOrderLineChangeOrder.getLength()>0){

			serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcInDoc);
		}
	}

	/**
	 * This method copy the dac address and order extn fields in the
	 * 
	 * @param yfcDocGetOrderListOp
	 * @param mapOHKeyLineNoReleasedQty 
	 *//*
	private void updateOrderLineWithAddress(YFCDocument yfcDocGetOrderListOp, Map<String, Map<String, String>> mapOHKeyLineNoReleasedQty) {

		YFCElement yfcEleLRAFrmIp = XPathUtil.getXPathElement(yfcInDoc, "//OrderLine/Extn/LRAList/LRA");
		Map<String, String> mapOrderLineExtnLRAAttris = yfcEleLRAFrmIp.getAttributes();

		YFCElement yfcElePersonInfoShipTo = yfcInDoc.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);

		for (YFCElement yfcOrderEle : yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER)) {


			String sEntryType = yfcOrderEle.getAttribute(TelstraConstants.ENTRY_TYPE);

	  * skip the vector order

			if(TelstraConstants.VECTOR.equalsIgnoreCase(sEntryType)){
				continue;
			}

			YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor(yfcOrderEle.toString());
			for(YFCElement yfcEleOrderLine : yfcDocChangeOrderIp.getElementsByTagName(TelstraConstants.ORDER_LINE)){

				boolean bAddressUpdateReq = true;
				boolean bLRAUpdateReq = false;
				for (YFCElement yfcEleOrderStatus : yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUS)){

					double dStatus = yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
					if(dStatus>3200){
						bAddressUpdateReq = false;
					}
					if(dStatus<=3200 && dStatus!=1300 && dStatus!=1400){
						bLRAUpdateReq =true;
					}
				}

				yfcEleOrderLine.removeChild(yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUSES).item(0));

				if(bLRAUpdateReq){
					YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
					if(!YFCCommon.isVoid(yfcEleOrderLine)){
						yfcEleExtn = yfcEleOrderLine.createChild(TelstraConstants.EXTN);
					}					
					YFCElement yfcEleLRAList = yfcEleExtn.createChild(TelstraConstants.LRA_LIST);
					YFCElement yfcEleLRA = yfcEleLRAList.createChild(TelstraConstants.LRA);
					yfcEleLRA.setAttributes(mapOrderLineExtnLRAAttris);

					if(bAddressUpdateReq){
						yfcEleOrderLine.removeChild(yfcEleOrderLine.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0));
						yfcEleOrderLine.importNode(yfcElePersonInfoShipTo);
					}
				}
			}

			LoggerUtil.verboseLog("GpsManageVectorOrder :: updateOrderWithGoDAC :: GpsUpdateVectorLra\n", logger,
					yfcDocChangeOrderIp);

			serviceInvoker.invokeYantraService(TelstraConstants.GPS_UPDATE_VECTOR_LRA_DROP_Q,yfcDocChangeOrderIp);

		}
	}

	  *//**
	  * This method calls get order list api with SearchCriteria1 as the input
	  * 
	  * @param sProjectNumber
	  * @return yfcDocGetOrderListOp
	  *//*
	private YFCDocument callGetOrderListWithProjectNo(String sProjectNumber) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument
				.getDocumentFor("<Order SearchCriteria1='" + sProjectNumber + "' OrderType='MATERIAL_RESERVATION'/>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithProjectNo :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderListIp);
		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList TotalNumberOfRecords=''><Order DocumentType='' OrderNo='' OrderHeaderKey='' EntryType=''><OrderLines><OrderLine OrderHeaderKey='' PrimeLineNo='' "
						+ "SubLineNo=''><OrderStatuses> <OrderStatus Status='' StatusQty='' OrderReleaseKey=''/></OrderStatuses> <PersonInfoShipTo/> <Extn/> </OrderLine></OrderLines></Order></OrderList>");

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithProjectNo :: change order Ip \n", logger,
				yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithProjectNo :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		YFCNodeList<YFCNode> yfcNlVectorOrder = XPathUtil.getXpathNodeList(yfcDocGetOrderListOp, "//Order[@EntryType='VECTOR']");

		List<String> listVectorOrderHeaderKey = new ArrayList<>();

	   * preparing the list of vector order header key so that it is not processed by removing it from the order list

		for(YFCNode yfcNodeOrderLine : yfcNlVectorOrder){			
			YFCElement yfcEleOrderLine = (YFCElement) yfcNodeOrderLine;
			listVectorOrderHeaderKey.add(yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY));			
		}

		for(String sOrderHeaderKey : listVectorOrderHeaderKey){

			YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//Order[@OrderHeaderKey='"+sOrderHeaderKey+"']");
			yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
		}

		return yfcDocGetOrderListOp;
	}
	   */
	/**
	 * 
	 * @param sOrderName
	 * @return
	 */
	private YFCDocument callGetOrderListWithOrderName(String sOrderName) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "'/>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderList :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderListIp);
		YFCDocument yfcDocGetOrderListTemp = YFCDocument
				.getDocumentFor("<OrderList TotalNumberOfRecords=''><Order OrderHeaderKey=''><OrderLines><OrderLine PrimeLineNo=''/></OrderLines></Order></OrderList>");

		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithOrderName :: get order list Ip \n", logger,
				yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderList :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		return yfcDocGetOrderListOp;
	}
}
