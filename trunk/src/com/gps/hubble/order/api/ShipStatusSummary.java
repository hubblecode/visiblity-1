package com.gps.hubble.order.api;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.sterlingcommerce.tools.datavalidator.XmlUtils;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.date.YTimestamp;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for Shipment Status Summary for Vendor Portal Dashboard. This api summarizes the shipments for a vendor by status.
 * 
 * @author Vinay Bhanu
 * @version 1.0
 *
 * Extends AbstractCustomApi Class
 * 
 */
public class ShipStatusSummary extends AbstractCustomApi{
	private static YFCLogCategory logger = YFCLogCategory.instance(ShipStatusSummary.class);
	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		Map<String, String> attributeColumnMap = getAttributeColumnMap();
		Set<String> selectAttributes = new HashSet<>();
		String attri = getProperty("GroupByAttributes",true);
		Set<String> allowedStatuses = new HashSet<>();
		String allowedStatus1 = getProperty("AllowedStatus1",true);
		allowedStatuses.add(allowedStatus1);
		String allowedStatus2 = getProperty("AllowedStatus2",true);
		allowedStatuses.add(allowedStatus2);
		String allowedStatus3 = getProperty("AllowedStatus3",true);
		allowedStatuses.add(allowedStatus3);		
		YFCElement rootElem = (inXml==null)? null:inXml.getDocumentElement();
		String sellerOrg = rootElem.getAttribute("SellerOrganizationCode");
		String processType=rootElem.getAttribute("ProcessType","PO_SHIPMENT");
		String documentType=rootElem.getAttribute("DocumentType","0005");
		if(XmlUtils.isVoid(sellerOrg)) {
			YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_13061_002","Seller Code is Mandatory.");
			throw new YFSException(erroDoc.toString());	
			
		}
		if(!XmlUtils.isVoid(attri)){
			if(!attri.contains("NoOfShipments")){
				YFCDocument erroDoc = ExceptionUtil.getYFSExceptionDocument("TEL_ERR_13061_001","Aggregate Attribute Not Provided.");
				throw new YFSException(erroDoc.toString());	
			}
			String[] s = attri.split(",");
			for (String string : s) {
				selectAttributes.add(string);
			}
		}
			if(selectAttributes != null && !selectAttributes.isEmpty()){
				String query = getQuery(selectAttributes,attributeColumnMap,sellerOrg);
				logger.debug("query is "+query);
				Connection connection = getDBConnection();
				YFCDocument docOutput = null;
				try (PreparedStatement ps = createPS(connection,query,sellerOrg,processType,documentType);ResultSet rs = ps.executeQuery()){
					docOutput =  constructDocument(rs,selectAttributes, attributeColumnMap,allowedStatuses);
				}catch(SQLException sqlEx){
					logger.error("SqlException while executing query ", sqlEx);
					throw new YFCException(sqlEx);
				}
				
				try (PreparedStatement psdate=getLastUpdatedQuery(connection,"GPS_SELLER_SHP_SUMMARY");
						ResultSet rsdate = psdate.executeQuery()){
					if(docOutput!=null) {
						if(rsdate!=null && rsdate.next()) {
							Timestamp updatedSqlDate = rsdate.getTimestamp("UPDATED_DATE");
							YTimestamp updatedDate= YTimestamp.newTimestamp(updatedSqlDate);
							docOutput.getDocumentElement().setDateTimeAttribute("UpdatedDate", updatedDate);
						}
					}
				}catch(SQLException sqlEx){
					logger.error("SqlException while executing query ", sqlEx);
					throw new YFCException(sqlEx);
				}	

				LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", docOutput);
				return  docOutput;
			}else{
				throw new YFSException("Invalid Input");
			}


	}
	
	private PreparedStatement createPS(Connection connection,String query,String sellerOrganizationCode,String processType,String documentType) throws SQLException {
		PreparedStatement ps = connection.prepareStatement(query);
		if(!YFCObject.isVoid(sellerOrganizationCode)){
			ps.setString(1, sellerOrganizationCode);
		}
		//This will work only for Purchase Orders
		ps.setString(2,processType);
		ps.setString(3, documentType);
		return ps;
				
	}

	//TODO This is not being used
	private Set<String> getSelectAttributes(YFCDocument inXml,Map<String, String> attributeColumnMap){
		Set<String> selectAttributes = new HashSet<>();
		if(inXml.getDocumentElement() == null || inXml.getDocumentElement().getChildElement("Attributes") == null){
			return selectAttributes;
		}
		YFCElement elemSelect = inXml.getDocumentElement().getChildElement("Attributes");
		if(elemSelect.getChildren("Attribute") == null){
			return selectAttributes;
		}
		for(Iterator<YFCElement> itr = elemSelect.getChildren("Attribute").iterator();itr.hasNext();){
			YFCElement elemAttribute = itr.next();
			String attribute = elemAttribute.getAttribute("Name");
			if(!attributeColumnMap.containsKey(attribute)){
				throw new YFCException(); 
			}
			selectAttributes.add(attribute);
		}
		return selectAttributes;
	}

	private String getQuery(Set<String> selectAttributes,Map<String, String> attributeColumnMap,String sellerOrg){
		StringBuilder builder = new StringBuilder();
		//builder.append(getSelect(selectAttributes, attributeColumnMap));
		builder.append(" SELECT GPS_SELLER_SHP_SUMMARY.SELLER_CODE AS SELLER_CODE,GPS_SELLER_SHP_SUMMARY.STATUS AS STATUS,YFS_STATUS.DESCRIPTION AS STATUS_DESCRIPTION,SUM(SHIPMENT_COUNT) AS NO_OF_SHIPMENTS " );
		builder.append(" FROM GPS_SELLER_SHP_SUMMARY GPS_SELLER_SHP_SUMMARY, YFS_STATUS YFS_STATUS, YFS_PROCESS_TYPE YFS_PROCESS_TYPE ");
		builder.append(" WHERE ");
		if(!XmlUtils.isVoid(sellerOrg)) {
			builder.append(" GPS_SELLER_SHP_SUMMARY.SELLER_CODE = ?");
			builder.append(" AND ");
		}
		builder.append(" YFS_STATUS.STATUS = GPS_SELLER_SHP_SUMMARY.STATUS");
		builder.append(" AND YFS_STATUS.PROCESS_TYPE_KEY = ?");
		builder.append(" AND YFS_STATUS.PROCESS_TYPE_KEY=YFS_PROCESS_TYPE.PROCESS_TYPE_KEY ");
		builder.append(" AND YFS_PROCESS_TYPE.BASE_PROCESS_TYPE='ORDER_DELIVERY' ");
		builder.append(" AND YFS_PROCESS_TYPE.DOCUMENT_TYPE= ? ");
		
		
	//	builder.append(getGroupBy(selectAttributes, attributeColumnMap));
		builder.append(" GROUP BY GPS_SELLER_SHP_SUMMARY.SELLER_CODE,GPS_SELLER_SHP_SUMMARY.STATUS,YFS_STATUS.DESCRIPTION");
		return builder.toString();
	}

	private String getSelect(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append("SELECT ");
		boolean isFirst = true;
		for(String attribute : selectAttributes){
			if(!isFirst){
				builder.append(",");
			}
			String columnName = attributeColumnMap.get(attribute);
			if(attribute.equals("NoOfShipments")){
				builder.append("SUM(SHIPMENT_COUNT) AS NO_OF_SHIPMENTS");
			}else{
				builder.append(columnName);
			}
			isFirst = false;
		}
		return builder.toString();
	}

	private String getGroupBy(Set<String> selectAttributes,Map<String, String> attributeColumnMap){
		StringBuilder builder = new StringBuilder();
		builder.append("GROUP BY ");
		boolean isFirst = true;
		for(String attribute : selectAttributes){
			String columnName = attributeColumnMap.get(attribute);
			if(!"NoOfShipments".equals(attribute)){
				if(!isFirst){
					builder.append(",");
				}
				builder.append(columnName);
				isFirst = false;
			}

		}
		return builder.toString();
	}

	private Map<String, String> getAttributeColumnMap(){
		Map<String, String> attributeMap = new HashMap<String, String>();
		attributeMap.put("Status", "STATUS");
		attributeMap.put("StatusDescription", "STATUS_DESCRIPTION");
		attributeMap.put("SellerOrganizationCode", "SELLER_CODE");
		attributeMap.put("NoOfShipments", "NO_OF_SHIPMENTS");
		return attributeMap;
	}

	private PreparedStatement getLastUpdatedQuery(Connection connection,String tableName) throws SQLException {
		StringBuilder query = new StringBuilder();
		query.append("SELECT REFRESH_TIME AS UPDATED_DATE");
		query.append(" FROM SYSCAT.TABLES ");
		query.append(" WHERE TABNAME= ? ");
		query.append(" AND TYPE = 'S'");
		PreparedStatement ps = connection.prepareStatement(query.toString());
		ps.setString(1, tableName);
		return ps;
		
	}	

	private YFCDocument constructDocument(ResultSet rs, Set<String> attributes, Map<String, String> attributeColumnMap,Set<String> allowedStatuses) throws SQLException{ 
		YFCDocument docShipSummaryList = YFCDocument.createDocument("ShipmentStatusSummaryList");
		YFCElement elemShipSummaryList = docShipSummaryList.getDocumentElement();
		 Set<String> missingStatuses = new HashSet<String>();
		 missingStatuses.addAll(allowedStatuses);	
		 String sellerOrgCode = null; 
		 
		 while(rs.next()){
			String status = rs.getString("STATUS").trim();
			if(allowedStatuses.contains(status.trim())) {
				YFCElement elemShipSummary = elemShipSummaryList.createChild("ShipmentStatusSummary");
				if(attributes.contains("DocumentType")){
					String documentType = rs.getString("DOCUMENT_TYPE");
					elemShipSummary.setAttribute("DocumentType", documentType);
				}
				if(attributes.contains("SellerOrganizationCode")){
					sellerOrgCode = rs.getString("SELLER_CODE");
					elemShipSummary.setAttribute("SellerOrganizationCode", sellerOrgCode);
				}			
				if(attributes.contains("Status")){
					
					elemShipSummary.setAttribute("Status", status);
				}
				if(attributes.contains("StatusDescription")){
					String statusDesc = rs.getString("STATUS_DESCRIPTION");
					elemShipSummary.setAttribute("StatusDescription", statusDesc);
				}			
				if(attributes.contains("NoOfShipments")){
					long noOfShipments = rs.getLong("NO_OF_SHIPMENTS");
					elemShipSummary.setAttribute("NoOfShipments", noOfShipments);
				}
				missingStatuses.remove(status);
			} 
		}
		if(!missingStatuses.isEmpty()) {
			for(Iterator<String> itr=missingStatuses.iterator();itr.hasNext();) {
				String missingStatus = itr.next();
				YFCElement elemShipSummary = elemShipSummaryList.createChild("ShipmentStatusSummary");
				elemShipSummary.setAttribute("Status", missingStatus); 
				elemShipSummary.setAttribute("SellerOrganizationCode", sellerOrgCode);
				
				String sStatusInput= "<Status Status= '"+missingStatus+"' ProcessTypeKey='PO_SHIPMENT'/>";
				YFCDocument statusInput = YFCDocument.getDocumentFor(sStatusInput);
				YFCDocument statusListDoc = invokeYantraApi("getStatusList", statusInput);
				if(statusListDoc!= null) {
					YFCElement statusElem = statusListDoc.getDocumentElement().getFirstChildElement();
					if(statusElem!=null) {
						elemShipSummary.setAttribute("StatusDescription",statusElem.getAttribute("Description"));
					}
				}
				elemShipSummary.setAttribute("NoOfShipments", 0);
			}
		}
		return docShipSummaryList;
	}

}
