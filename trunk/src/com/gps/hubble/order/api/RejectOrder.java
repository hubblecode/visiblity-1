/***********************************************************************************************
 * File Name        : RejectOrder.java
 *
 * Description      : Custom API for processing rejection from WMS
 * 
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date			Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		18 May, 2017	Prateek Kumar		Initial Version
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2017. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.order.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfs.japi.YFSException;

/**
 * Custom API for WMS status update for rejection of an order
 * 
 * @author Prateek Kumar
 * @version 1.0
 *
 *          Extends AbstractCustomApi Class
 * 
 *          sample input
 * 
	<Shipment ServiceName="Service Rejected/Cancellation Accepted" OrderName="Vector Id">
		<ShipmentLines>
			<ShipmentLine PrimeLineNo="1(use the vector id to identify the line number)" SubLineNo="1" ItemID="ignore the item id passed here" 
			UnitOfMeasure="ignore the uom passed here" Quantity="only cancel the qty passed here">
			</ShipmentLine>
		</ShipmentLines>
	</Shipment>
 * 
 */

public class RejectOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ApplyDuplicateOrderHold.class);

	@Override
	public YFCDocument invoke(YFCDocument inXml) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		YFCElement yfcEleInRoot = inXml.getDocumentElement();
		String sServiceName = yfcEleInRoot.getAttribute("ServiceName");

		if ("Service Rejected".equalsIgnoreCase(sServiceName)
				|| "Cancellation Accepted".equalsIgnoreCase(sServiceName)) {

			String sVectorID = yfcEleInRoot.getAttribute("OrderName");

			YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineList(sVectorID);

			int iOrderLineLength = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)
					.getLength();
			if (iOrderLineLength == 0) {
				throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.REJECT_ORDER_NO_ORDER_LINE_FOR_VECTOR_ID,
						new YFSException());
			} else if (iOrderLineLength > 1) {
				throw ExceptionUtil.getYFSException(
						TelstraErrorCodeConstants.REJECT_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID,
						new YFSException());
			}
			/*
			 * Cancel the reject line
			 */
			double dCancelledQuantity = yfcEleInRoot.getElementsByTagName(TelstraConstants.SHIPMENT_LINE).item(0)
					.getDoubleAttribute(TelstraConstants.QUANTITY, 0.00);
			cancelOrderLine(yfcDocGetOrderLineListOp, dCancelledQuantity);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", inXml);
		return inXml;
	}

	/**
	 * 
	 * @param sQuantity
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 */
	private void cancelOrderLine(YFCDocument yfcDocGetOrderLineListOp, double dCancelledQuantity) {

		YFCElement yfcEleOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
		String sOrderLineKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY);
		YFCElement yfcEleOrder = yfcEleOrderLine.getChildElement(TelstraConstants.ORDER);
		String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		double dOrderedQty = yfcEleOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY);

		double dNewOrderedQty = dOrderedQty - dCancelledQuantity;
		logger.verbose("RejectOrder::cancelOrderLine:: dNewOrderedQty\n" + dNewOrderedQty);

		if (dNewOrderedQty < 0) {
			logger.error("RejectOrder::cancelOrderLine:: Cancellation qty can not be greater than the ordered qty"
					+ dNewOrderedQty);
			throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.REJECT_ORDER_CANCELLED_QTY_MORE,
					new YFSException());
		}

		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor(
				"<Order OrderHeaderKey='" + sOrderHeaderKey + "'> <OrderLines> <OrderLine Override='Y' OrderLineKey='"
						+ sOrderLineKey + "' OrderedQty='" + dNewOrderedQty + "'/> </OrderLines> </Order>");
		logger.verbose("RejectOrder::cancelOrderLine:: yfcDocChangeOrderIp\n" + yfcDocChangeOrderIp);

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);
	}

	/**
	 * 
	 * @return
	 */
	private YFCDocument callGetOrderLineList(String sVectorID) {

		YFCDocument inDocgetOrdLineList = YFCDocument.getDocumentFor(
				"<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='" + sVectorID + "' /></OrderLine>");
		YFCDocument templateForGetOrdLineList = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' OrderLineKey='' OrderedQty=''><Order OrderName='' OrderHeaderKey=''/></OrderLine>"
				+ "</OrderLineList>");
		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", inDocgetOrdLineList,
				templateForGetOrdLineList);

		logger.verbose("RejectOrder::callGetOrderLineList:: yfcDocGetOrderLineListOp\n" + yfcDocGetOrderLineListOp);

		return yfcDocGetOrderLineListOp;
	}

}
