/***********************************************************************************************
 * File	Name		: GpsSterlingOrderUpdate.java
 *
 * Description		: This class is called from ManageOrder.java. The objective of this class is to do the order update
 * 					for the orders coming from Sterling portal
 * 
 * Modification	Log	:
 * ---------------------------------------------------------------------------------------------
 * Ver #	Date				Author					Modification


 * ---------------------------------------------------------------------------------------------
 * 1.0		March 13,2016	  	Prateek Kumar 		   	Initial	Version 
 * ---------------------------------------------------------------------------------------------
 * 
 */

package com.gps.hubble.order.api;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;

/**
 * 
 * @author Prateek
 *
 */
public class GpsSterlingOrderUpdate {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsSterlingOrderUpdate.class);
	private ServiceInvoker serviceInvoker;
	
	/**
	 * 
	 * @param yfcInDoc
	 * @param yfcGetOrderListOp
	 * @param serviceInvoker
	 */
	public void updateSterlingOrder(YFCDocument yfcInDoc, YFCDocument yfcGetOrderListOp, ServiceInvoker serviceInvoker)  {
		
		LoggerUtil.startComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);		

		YFCNodeList<YFCElement> yfcNlOrderLineChangeOrderIp = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		/*
		 * If there is no order line present in change order Ip, then return back
		 */
		if(yfcNlOrderLineChangeOrderIp.getLength() == 0){
			LoggerUtil.verboseLog("Returning back from GpsSterlingOrderUpdate as there is no order line present to change", logger, yfcInDoc);
			return;
		}
		
		this.serviceInvoker = serviceInvoker;
		
		Map<String, Map<String,Double>> mapOrderReleaseKeyLineNoQty = new HashMap<>();
		YFCNodeList<YFCElement> yfcNlOrderLine = yfcInDoc.getElementsByTagName(TelstraConstants.ORDER_LINE);
		
		for(YFCElement yfcEleOrderLine : yfcNlOrderLine){
			
			String sPrimeLineNo = yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO);
			double dOrderedQtyInIp = yfcEleOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY);
			
			YFCElement yfcEleOrderListOrderLine = XPathUtil.getXPathElement(yfcGetOrderListOp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
			
			YFCElement yfcEleOrderStatus = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcEleOrderListOrderLine.toString()), "//OrderStatus[@Status='3200']");
			if(!YFCCommon.isVoid(yfcEleOrderStatus) && 0.00!=yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS_QTY)){
				
				String sOrderReleaseKey = yfcEleOrderStatus.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
				double dOrderedQtyInOrderList =  yfcEleOrderListOrderLine.getDoubleAttribute(TelstraConstants.ORDERED_QTY);
				double dDeltaOrderedQty = dOrderedQtyInIp - dOrderedQtyInOrderList;
				if(dDeltaOrderedQty>0){

					if(mapOrderReleaseKeyLineNoQty.containsKey(sOrderReleaseKey)){
						Map<String, Double> mapLineNoQty = mapOrderReleaseKeyLineNoQty.get(sOrderReleaseKey);
						mapLineNoQty.put(sPrimeLineNo, dDeltaOrderedQty);
					}
					else{
						Map<String, Double> mapLineNoQty = new HashMap<>();
						mapLineNoQty.put(sPrimeLineNo, dDeltaOrderedQty);
						mapOrderReleaseKeyLineNoQty.put(sOrderReleaseKey, mapLineNoQty);
					}
				}
			}
		}		
		
		/**
		 * calling change release to add the positive delta qty in order line if it is in released status 
		 */
		callChangeReleaseApi(mapOrderReleaseKeyLineNoQty);
		LoggerUtil.verboseLog("GpsSterlingOrderUpdate :: updateSterlingOrder :: changeOrderIp", logger, yfcInDoc);

		serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcInDoc);
		
		/*// HUB-6859 - [Start]
		GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
		String sOrderName = XPathUtil.getXpathAttribute(yfcGetOrderListOp, "//Order/@OrderName");
		if(!YFCCommon.isStringVoid(sOrderName)){
			obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderName='" + sOrderName + "' />"),
					serviceInvoker);
			// HUB-6859 - [End]
		}*/
		LoggerUtil.endComponentLog(logger, this.getClass().getName(),"invoke", yfcInDoc);

	}
	
	/**
	 * This method calls the change release to add the quantity in the existing release
	 * @param mapOrderReleaseKeyLineNoQty
	 */
	private void callChangeReleaseApi(Map<String, Map<String, Double>> mapOrderReleaseKeyLineNoQty) {
		
		for (Entry<String, Map<String, Double>> entry : mapOrderReleaseKeyLineNoQty.entrySet()){
		    String sOrderReleaseKey =  entry.getKey();
		 
		    YFCDocument docOrderReleaseinXml = YFCDocument.getDocumentFor("<OrderRelease Action='MODIFY' ModificationReasonText='Added to a release at "+ getDateStamp() + "' OrderReleaseKey='" + sOrderReleaseKey + "'><OrderLines></OrderLines></OrderRelease>");
		    YFCElement sOrderLines = docOrderReleaseinXml.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);
		    Map<String,Double> mapLineNoQty = entry.getValue();
		   
		    for(Entry<String, Double> entry1 : mapLineNoQty.entrySet()){
		    	String sPrimeLineNo = entry1.getKey();
		    	double sChangeInQty = entry1.getValue();
		    	
		    	YFCElement yfcEleOrderLine = docOrderReleaseinXml.createElement(TelstraConstants.ORDER_LINE);
		    	sOrderLines.appendChild(yfcEleOrderLine);
		    	yfcEleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sPrimeLineNo);
		    	yfcEleOrderLine.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
		    	yfcEleOrderLine.setAttribute(TelstraConstants.CHANGE_IN_QUANTITY, sChangeInQty);
		    }
		    LoggerUtil.verboseLog("GpsSterlingOrderUpdate :: updateSterlingOrder :: changeReleaseIp", logger, docOrderReleaseinXml);
		    serviceInvoker.invokeYantraApi("changeRelease", docOrderReleaseinXml);
		    
		}		
	}

	/**
	 * 
	 * @return
	 */
	private String getDateStamp() {
		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		// get current date time with Date()
		Date date = new Date();
		return dateFormat.format(date);
	}
}
