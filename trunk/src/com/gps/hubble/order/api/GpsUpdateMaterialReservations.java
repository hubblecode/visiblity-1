/***********************************************************************************************
 * File	Name		: GpsUpdateMaterialReservations.java
 *
 * Description		: 
 * 
 * Modification	Log	:
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * Ver #	Date			Author					Modification
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 1.0		May 25,2017	  	Prateek Kumar 		   	Initial	Version 
 *  
 * --------------------------------------------------------------------------------------------------------------------------------------------------------------
 * 
 */
package com.gps.hubble.order.api;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNode;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsUpdateMaterialReservations extends AbstractCustomApi {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsUpdateMaterialReservations.class);


	@Override
	public YFCDocument invoke(YFCDocument yfcInDoc) throws YFSException {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);

		updateMarterialResevations(yfcInDoc);
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke", yfcInDoc);
		return yfcInDoc;
	}


	/**
	 * If change status is D, cancel the incoming order and blank out LRA level
	 * details in all the MR sharing same LRA This method update the go dac for
	 * all the MR sharing the same project number
	 * 
	 * @param yfcInDoc
	 *
	 * 
	 */
	private void updateMarterialResevations(YFCDocument yfcInDoc) {

		YFCElement yfcEleOrderRoot = yfcInDoc.getDocumentElement();
		String sOrderChangeStatus = yfcEleOrderRoot.getAttribute(TelstraConstants.CHANGE_STATUS);
		if (TelstraConstants.CHANGE_STATUS_CANCELLED.equalsIgnoreCase(sOrderChangeStatus)) {

			/*
			 * Call get order list with the passed LRA, blank out all the LRA
			 * details in the MR returned
			 */
			blankOutLRADetailsInMR(yfcInDoc);

		} else {

			/*
			 * Get all the orders having the above project number
			 */
			Map<String,Map <String, String>> mapOHKeyLineNoReleasedQty = new HashMap<>();

			YFCDocument yfcDocGetOrderListOp = callGetOrderListWithProjectNo(yfcEleOrderRoot.getAttribute(TelstraConstants.SEARCH_CRITERIA_1));
			moveReleasedLineToBO(yfcDocGetOrderListOp, mapOHKeyLineNoReleasedQty);
			updateLRAAndAddress(yfcInDoc, yfcDocGetOrderListOp, mapOHKeyLineNoReleasedQty);
			if(!mapOHKeyLineNoReleasedQty.isEmpty()){
				moveBOLineToCreated(yfcDocGetOrderListOp, mapOHKeyLineNoReleasedQty);
			}			
		}
	}



	/**
	 * 
	 * @param yfcDocGetOrderListOp
	 * @param mapOHKeyLineNoReleasedQty
	 */
	private void moveBOLineToCreated(YFCDocument yfcDocGetOrderListOp, Map<String, Map<String, String>> mapOHKeyLineNoReleasedQty) {


		for (Entry<String, Map<String, String>> entryMap1 : mapOHKeyLineNoReleasedQty.entrySet()){

			String sOrderHeaderKey = entryMap1.getKey();

			String sDocumentType = XPathUtil.getXpathAttribute(yfcDocGetOrderListOp, "//Order[@OrderHeaderKey='"+sOrderHeaderKey+"']/@DocumentType");
			String sTransactionType = "CHANGE_BO_STATUS."+sDocumentType+".ex";
			YFCDocument docChnageOrderStatusIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"' TransactionId='"+sTransactionType+"'><OrderLines/></Order>");
			YFCElement yfcEleOrderLines = docChnageOrderStatusIp.getElementsByTagName(TelstraConstants.ORDER_LINES).item(0);

			for (Map.Entry<String, String> entry : entryMap1.getValue().entrySet()){

				String sLineNo = entry.getKey();
				String sQty = entry.getValue();

				YFCElement yfcEleOrderLine = yfcEleOrderLines.createChild(TelstraConstants.ORDER_LINE);
				yfcEleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, sLineNo);
				yfcEleOrderLine.setAttribute(TelstraConstants.SUB_LINE_NO, "1");
				yfcEleOrderLine.setAttribute(TelstraConstants.QUANTITY, sQty);
				yfcEleOrderLine.setAttribute("BaseDropStatus", "1100");
			}

			LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: moveBOLineToCreated :: changeOrderStatus \n", logger,
					docChnageOrderStatusIp);
			invokeYantraApi("changeOrderStatus", docChnageOrderStatusIp);
		}
	}

	/**
	 * This method copy the dac address and order extn fields in the
	 * 
	 * @param yfcDocGetOrderListOp
	 * @param yfcDocGetOrderListOp2 
	 * @param mapOHKeyLineNoReleasedQty 
	 */
	private void updateLRAAndAddress(YFCDocument yfcInDoc, YFCDocument yfcDocGetOrderListOp,  Map<String, Map<String, String>> mapOHKeyLineNoReleasedQty) {

		YFCElement yfcEleLRAFrmIp = XPathUtil.getXPathElement(yfcInDoc, "//OrderLine/Extn/LRAList/LRA");
		Map<String, String> mapOrderLineExtnLRAAttris = yfcEleLRAFrmIp.getAttributes();

		YFCElement yfcElePersonInfoShipTo = yfcInDoc.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0);

		for (YFCElement yfcOrderEle : yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER)) {


			String sEntryType = yfcOrderEle.getAttribute(TelstraConstants.ENTRY_TYPE);
			/*
			 * skip the vector order
			 */
			if(TelstraConstants.VECTOR.equalsIgnoreCase(sEntryType)){
				continue;
			}

			List<String> lLineNoToRemoveFrmCO = new ArrayList<>();

			YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor(yfcOrderEle.toString());

			for(YFCElement yfcEleOrderLine : yfcDocChangeOrderIp.getElementsByTagName(TelstraConstants.ORDER_LINE)){

				boolean bAddressUpdateReq = true;
				boolean bLRAUpdateReq = false;
				for (YFCElement yfcEleOrderStatus : yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUS)){

					double dStatus = yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
					if(dStatus>3200){
						bAddressUpdateReq = false;
					}
					if(dStatus<=3200 && dStatus!=1300 && dStatus!=1400){
						bLRAUpdateReq =true;
					}
				}

				yfcEleOrderLine.removeChild(yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUSES).item(0));

				if(bLRAUpdateReq){
					YFCElement yfcEleExtn = yfcEleOrderLine.getChildElement(TelstraConstants.EXTN);
					if(YFCCommon.isVoid(yfcEleExtn)){
						yfcEleExtn = yfcEleOrderLine.createChild(TelstraConstants.EXTN);
					}					
					YFCElement yfcEleLRAList = yfcEleExtn.createChild(TelstraConstants.LRA_LIST);
					YFCElement yfcEleLRA = yfcEleLRAList.createChild(TelstraConstants.LRA);
					yfcEleLRA.setAttributes(mapOrderLineExtnLRAAttris);

					if(bAddressUpdateReq){
						yfcEleOrderLine.removeChild(yfcEleOrderLine.getElementsByTagName(TelstraConstants.PERSON_INFO_SHIP_TO).item(0));
						yfcEleOrderLine.importNode(yfcElePersonInfoShipTo);
					}
				}
				else{
					lLineNoToRemoveFrmCO.add(yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO));
				}
			}

			for(String sPrimeLineNo : lLineNoToRemoveFrmCO){
				YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcDocChangeOrderIp, "//OrderLine[@PrimeLineNo='"+sPrimeLineNo+"']");
				yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
			}			
			/*
			 * 
			 */
			String sLraIp = yfcEleLRAFrmIp.getAttribute(TelstraConstants.LRA_ID);
			updateLRAListAtHeaderLevel(yfcDocChangeOrderIp, sLraIp);

			LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: updateOrderWithGoDAC :: GpsUpdateVectorLra\n", logger,
					yfcDocChangeOrderIp);
			invokeYantraApi(TelstraConstants.API_CHANGE_ORDER,yfcDocChangeOrderIp);

			/*
			 * for updating boolean
			 */
			GpsUpdateOrderExtnFields obj = new GpsUpdateOrderExtnFields();
			obj.updateOrderExtnFields(YFCDocument.getDocumentFor("<Order OrderName='" + yfcOrderEle.getAttribute(TelstraConstants.ORDER_NAME) + "' />"),
					getServiceInvoker());

		}
	}
	
	/**
	 * This method will append the new lra to existing lra comma separated value
	 * @param yfcDocChangeOrderIp
	 * @param sLraId
	 */

	private void updateLRAListAtHeaderLevel(YFCDocument yfcDocChangeOrderIp, String sLraId) {

		YFCElement yfcEleOrderExtn = yfcDocChangeOrderIp.getDocumentElement().getChildElement(TelstraConstants.EXTN);
		String sExtnLra = yfcEleOrderExtn.getAttribute(TelstraConstants.LRA);
		if(YFCCommon.isStringVoid(sExtnLra)){
			yfcEleOrderExtn.setAttribute(TelstraConstants.LRA, sLraId);
		}
		else if(!sExtnLra.contains(sLraId)){

			sExtnLra = sExtnLra+","+sLraId;
			int len = sExtnLra.length();
			String sLraMaxLen = getProperty("EXTN_LRA_MAX_LEN", false);
			int iLraMaxLen = 200;
			if(!YFCCommon.isStringVoid(sLraMaxLen) && StringUtils.isNumeric(sLraMaxLen)){
				iLraMaxLen = Integer.parseInt(sLraMaxLen);
			}
			while(len>iLraMaxLen){				
				int iIndex = sExtnLra.indexOf(",");
				sExtnLra = sExtnLra.substring(iIndex+1);				
				len = sExtnLra.length();				
			}
			yfcEleOrderExtn.setAttribute(TelstraConstants.LRA, sExtnLra);
		}
	}


	/**
	 * This method will move the released qty to backorder
	 * @param yfcDocGetOrderListOp
	 * @param mapOHKeyLineNoReleasedQty
	 */
	private void moveReleasedLineToBO(YFCDocument yfcDocGetOrderListOp, Map<String, Map<String, String>> mapOHKeyLineNoReleasedQty) {

		List<String> lOrderReleaseKey = new ArrayList<>();
		for(YFCElement yfcEleOrderLine : yfcDocGetOrderListOp.getElementsByTagName(TelstraConstants.ORDER_LINE)){

			YFCElement yfcEleStatus = XPathUtil.getXPathElement(YFCDocument.getDocumentFor(yfcEleOrderLine.toString()),
					"//OrderStatus[@Status='3200']");
			if(!YFCCommon.isVoid(yfcEleStatus)){

				String sOrderHeaderKey = yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY);

				Map<String, String> mapLineNoReleasedQty;
				if(mapOHKeyLineNoReleasedQty.containsKey(sOrderHeaderKey)){
					mapLineNoReleasedQty = mapOHKeyLineNoReleasedQty.get(sOrderHeaderKey);
				}
				else{
					mapLineNoReleasedQty = new HashMap<>();
					mapOHKeyLineNoReleasedQty.put(sOrderHeaderKey, mapLineNoReleasedQty);
				}

				String sStatusQty = yfcEleStatus.getAttribute(TelstraConstants.STATUS_QTY);
				mapLineNoReleasedQty.put(yfcEleOrderLine.getAttribute(TelstraConstants.PRIME_LINE_NO), sStatusQty);

				String sOrderReleaseKey = yfcEleStatus.getAttribute(TelstraConstants.ORDER_RELEASE_KEY);
				if(!lOrderReleaseKey.contains(sOrderReleaseKey)){
					lOrderReleaseKey.add(sOrderReleaseKey);
				}
			}			
		}

		/*
		 * prepare change release input to back order complete release
		 */		
		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: moveReleasedLineToBO :: lOrderReleaseKey \n", logger,
				lOrderReleaseKey);
		for(String sOrderReleaseKey : lOrderReleaseKey){
			YFCDocument docChangeReleaseIp = YFCDocument.getDocumentFor("<OrderRelease Action='BACKORDER' OrderReleaseKey='"+sOrderReleaseKey+"'/>");
			LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: moveReleasedLineToBO :: docChangeReleaseIp \n", logger,
					docChangeReleaseIp);
			invokeYantraApi("changeRelease", docChangeReleaseIp);
		}
	}

	/**
	 * This method calls get order list api with SearchCriteria1 as the input
	 * 
	 * @param sProjectNumber
	 * @return yfcDocGetOrderListOp
	 */
	private YFCDocument callGetOrderListWithProjectNo(String sProjectNumber) {

		YFCDocument yfcDocGetOrderListIp = YFCDocument
				.getDocumentFor("<Order SearchCriteria1='" + sProjectNumber + "' OrderType='MATERIAL_RESERVATION'/>");
		LoggerUtil.verboseLog("GpsManageVectorOrder :: callGetOrderListWithProjectNo :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderListIp);
		YFCDocument yfcDocGetOrderListTemp = YFCDocument.getDocumentFor(
				"<OrderList TotalNumberOfRecords=''><Order DocumentType='' OrderNo='' OrderName='' OrderHeaderKey='' EntryType=''> <Extn/> <OrderLines><OrderLine OrderHeaderKey='' PrimeLineNo='' "
						+ "SubLineNo=''><OrderStatuses> <OrderStatus Status='' StatusQty='' OrderReleaseKey=''/></OrderStatuses> <PersonInfoShipTo /> <Extn/> </OrderLine></OrderLines></Order></OrderList>");

		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: callGetOrderListWithProjectNo :: change order Ip \n", logger,
				yfcDocGetOrderListIp);

		YFCDocument yfcDocGetOrderListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LIST,
				yfcDocGetOrderListIp, yfcDocGetOrderListTemp);
		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: callGetOrderListWithProjectNo :: yfcDocGetOrderListOp\n", logger,
				yfcDocGetOrderListOp);

		YFCNodeList<YFCNode> yfcNlVectorOrder = XPathUtil.getXpathNodeList(yfcDocGetOrderListOp, "//Order[@EntryType='VECTOR']");

		List<String> listVectorOrderHeaderKey = new ArrayList<>();
		/*
		 * preparing the list of vector order header key so that it is not processed by removing it from the order list
		 */
		for(YFCNode yfcNodeOrderLine : yfcNlVectorOrder){			
			YFCElement yfcEleOrderLine = (YFCElement) yfcNodeOrderLine;
			listVectorOrderHeaderKey.add(yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_HEADER_KEY));			
		}

		for(String sOrderHeaderKey : listVectorOrderHeaderKey){

			YFCElement yfcEleOrderLine = XPathUtil.getXPathElement(yfcDocGetOrderListOp, "//Order[@OrderHeaderKey='"+sOrderHeaderKey+"']");
			yfcEleOrderLine.getParentElement().removeChild(yfcEleOrderLine);
		}

		return yfcDocGetOrderListOp;
	}


	private void blankOutLRADetailsInMR(YFCDocument yfcInDoc) {

		String sLRAID = XPathUtil.getXpathAttribute(yfcInDoc, "//LRAList/LRA/@LRAID");
		/*
		 * calling get order list with the LRA
		 */
		YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineListWithLRA(sLRAID);

		YFCNodeList<YFCElement> yfcNlOrderLine = yfcDocGetOrderLineListOp.getElementsByTagName(TelstraConstants.ORDER_LINE);

		for (YFCElement yfcEleOrderLine : yfcNlOrderLine) {

			YFCElement yfcEleOrder = yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER).item(0);
			String sEntryType = yfcEleOrder.getAttribute(TelstraConstants.ENTRY_TYPE);
			/*
			 * Not blanking out for vector orders as it would be cancelled separately
			 */
			if(TelstraConstants.VECTOR.equalsIgnoreCase(sEntryType)){
				continue;
			}

			boolean bRemoveLRADetail = true;
			YFCNodeList<YFCElement> yfcNlOrderStatus = yfcEleOrderLine.getElementsByTagName(TelstraConstants.ORDER_STATUS);

			for(YFCElement yfcEleOrderStatus : yfcNlOrderStatus){
				double dStatus = yfcEleOrderStatus.getDoubleAttribute(TelstraConstants.STATUS);
				if(dStatus>3200){
					bRemoveLRADetail = false;
					break;
				}
			}

			if(bRemoveLRADetail){

				YFCDocument yfcDocGetLRAListIp = YFCDocument.getDocumentFor(
						"<LRA OrderLineKey='" + yfcEleOrderLine.getAttribute(TelstraConstants.ORDER_LINE_KEY) + "'/>");
				LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: blankOutLRADetailsInMR :: yfcDocGetLRAListIp \n", logger,
						yfcDocGetLRAListIp);

				YFCDocument yfcDocGetLRAListOp = invokeYantraService(TelstraConstants.SERVICE_GPS_GET_LRA_LIST, yfcDocGetLRAListIp);
				LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: blankOutLRADetailsInMR :: yfcDocGetLRAListOp \n", logger,
						yfcDocGetLRAListOp);

				for(YFCElement yfcEleLra : yfcDocGetLRAListOp.getElementsByTagName(TelstraConstants.LRA)){
					invokeYantraService(TelstraConstants.SERVICE_GPS_DELETE_LRA, YFCDocument.getDocumentFor(yfcEleLra.toString()));					
				}

				removeLRAFromHeaderExtn(yfcEleOrder, sLRAID);
			}
		}
	}


	private void removeLRAFromHeaderExtn(YFCElement yfcEleOrder, String sLRAID) {

		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: removeLRAFromHeaderExtn :: LRA id to delete \n", logger,
				sLRAID);
		YFCElement yfcEleExtn = yfcEleOrder.getChildElement(TelstraConstants.EXTN);
		String sLRA = yfcEleExtn.getAttribute(TelstraConstants.LRA);
		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: removeLRAFromHeaderExtn :: LRA id comma separated \n", logger,
				sLRA);
		List<String> lLra = new ArrayList<String>(Arrays.asList(sLRA.split(",")));
		//remove the lra from lra comma separated value
		lLra.remove(sLRAID);

		String sNewLRA = lLra.toString().replaceAll("[\\s\\[\\]]", "");
		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: removeLRAFromHeaderExtn :: sNewLRA id comma separated \n", logger,
				sNewLRA);

		String sOrderHeaderKey = yfcEleOrder.getAttribute(TelstraConstants.ORDER_HEADER_KEY);
		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor("<Order OrderHeaderKey='"+sOrderHeaderKey+"'> <Extn LRA='"+sNewLRA+"'/> </Order>");

		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: removeLRAFromHeaderExtn :: yfcDocChangeOrderIp \n", logger,
				yfcDocChangeOrderIp);
		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);

	}


	private YFCDocument callGetOrderLineListWithLRA(String sLRA) {

		YFCDocument yfcDocGetOrderLineListIp = YFCDocument
				.getDocumentFor("<OrderLine><Extn> <LRAList> <LRA LRAID='"+sLRA+"'/> </LRAList></Extn></OrderLine>");

		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: callGetOrderLineListWithLRA :: yfcDocGetOrderListIp\n", logger,
				yfcDocGetOrderLineListIp);
		YFCDocument yfcDocGetOrderLineListTemp = YFCDocument.getDocumentFor("<OrderLineList><OrderLine OrderLineKey='' PrimeLineNo='' OrderHeaderKey=''> <Order EntryType='' OrderHeaderKey=''> <Extn LRA=''/> </Order> <OrderStatuses> <OrderStatus Status=''/>  </OrderStatuses> </OrderLine></OrderLineList>");


		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi(TelstraConstants.API_GET_ORDER_LINE_LIST,
				yfcDocGetOrderLineListIp, yfcDocGetOrderLineListTemp);

		LoggerUtil.verboseLog("GpsUpdateMaterialReservations :: callGetOrderLineListWithLRA :: yfcDocGetOrderLineListOp\n", logger,
				yfcDocGetOrderLineListOp);

		return yfcDocGetOrderLineListOp;

	}

}
