package com.gps.hubble.order.api;

import java.rmi.RemoteException;

import com.bridge.sterling.framework.service.ServiceInvoker;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.interop.japi.YIFClientCreationException;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfs.japi.YFSException;

/**
 * 
 * @author Prateek
 *
 */
public class GpsChangeOrderStatusForPO {

	private static YFCLogCategory logger = YFCLogCategory.instance(GpsChangeOrderStatusForPO.class);
	private ServiceInvoker serviceInvoker;
	private String sOrderLineKey;
	private String sOrderHeaderKey;
	private String sChainedFromOrderLineKey;
	private String sChainedFromOrderHeaderKey;

	/**
	 * 
	 * @param sOrderLineKey
	 * @param serviceInvoker
	 * @throws YFSException
	 */
	public void changeOrderStatusForPO(String sOrderLineKey, ServiceInvoker serviceInvoker) throws YFSException {

		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "changeOrderStatusForPO", sOrderLineKey);
		this.serviceInvoker = serviceInvoker;
		this.sOrderLineKey = sOrderLineKey;
		YFCDocument docGetOrderLineListIp = YFCDocument
				.getDocumentFor("<OrderLine OrderLineKey='" + sOrderLineKey + "'/>");
		YFCDocument docGetOrderLineListTemp = YFCDocument.getDocumentFor(
				"<OrderLineList><OrderLine OrderLineKey='' OrderHeaderKey='' MinLineStatus='' ChainedFromOrderHeaderKey='' ChainedFromOrderLineKey=''/></OrderLineList>");

		YFCDocument docGetOrderLineListOp = serviceInvoker.invokeYantraApi(TelstraConstants.API_GET_ORDER_LINE_LIST,
				docGetOrderLineListIp, docGetOrderLineListTemp);

		LoggerUtil.verboseLog("GpsChangeOrderStatusForPO::docGetOrderLineListOp", logger, docGetOrderLineListOp);

		String sMinLineStatus = XPathUtil.getXpathAttribute(docGetOrderLineListOp, "//OrderLine/@MinLineStatus");
		sOrderHeaderKey = XPathUtil.getXpathAttribute(docGetOrderLineListOp, "//OrderLine/@OrderHeaderKey");
		sChainedFromOrderLineKey = XPathUtil.getXpathAttribute(docGetOrderLineListOp,
				"//OrderLine/@ChainedFromOrderLineKey");
		sChainedFromOrderHeaderKey = XPathUtil.getXpathAttribute(docGetOrderLineListOp,
				"//OrderLine/@ChainedFromOrderHeaderKey");

		if (TelstraConstants.STATUS_PO_NOT_SENT_TO_VENDOR.equals(sMinLineStatus)) {
			movePOLineToPOSentToVendorStatus();
		} else if (TelstraConstants.STATUS_PO_SENT_TO_VENDOR.equals(sMinLineStatus)) {
			movePOLineToCommittedStatus();
		} else if (TelstraConstants.STATUS_COMMITTED.equals(sMinLineStatus)
				|| TelstraConstants.STATUS_INCLUDED_IN_SHIPMENT.equals(sMinLineStatus)) {
			movePOLineToShippedStatus();
		} else if (TelstraConstants.STATUS_SHIPPED.equals(sMinLineStatus)) {
			movePOLineToIntransitStatus();
		}

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "changeOrderStatusForPO", sOrderLineKey);
	}

	/**
	 * This method will move all the available quantity of an order line from PO
	 * not sent to vendor to PO sent to vendor status
	 * 
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 * @param sChainedFromOrderHeaderKey
	 * @param sChainedFromOrderLineKey
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private void movePOLineToPOSentToVendorStatus() throws YFSException {

		YFCDocument changeOrderStatusIp = YFCDocument.getDocumentFor("<OrderStatusChange OrderHeaderKey='"
				+ sOrderHeaderKey + "' TransactionId='" + TelstraConstants.PO_TECH_CONFIRM_TRANSACTION_ID
				+ "'> <OrderLines> <OrderLine BaseDropStatus='" + TelstraConstants.STATUS_PO_SENT_TO_VENDOR
				+ "'  OrderLineKey='" + sOrderLineKey
				+ "' ChangeForAllAvailableQty='Y'> </OrderLine> </OrderLines> </OrderStatusChange>");

		LoggerUtil.verboseLog("GpsChangeOrderStatusForPO :: movePOLineToPOSentToVendorStatus :: changeOrderStatusIp\n",
				logger, changeOrderStatusIp);

		serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, changeOrderStatusIp);
		movePOLineToCommittedStatus();

	}

	/**
	 * This method will move all the available quantity of an order line from PO
	 * sent to vendor to Committed status.
	 * 
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 * @param sChainedFromOrderHeaderKey
	 * @param sChainedFromOrderLineKey
	 * @throws YIFClientCreationException
	 * @throws RemoteException
	 * @throws YFSException
	 */

	private void movePOLineToCommittedStatus() throws YFSException {

		YFCDocument changeOrderStatusIp = YFCDocument
				.getDocumentFor("<OrderStatusChange OrderHeaderKey='" + sOrderHeaderKey + "' TransactionId='"
						+ TelstraConstants.TRANSACTION_VENDOR_UPDATE + "'> <OrderLines> <OrderLine BaseDropStatus='"
						+ TelstraConstants.STATUS_COMMITTED + "'  OrderLineKey='" + sOrderLineKey
						+ "' ChangeForAllAvailableQty='Y'> </OrderLine> </OrderLines> </OrderStatusChange>");

		LoggerUtil.verboseLog("GpsChangeOrderStatusForPO :: movePOLineToCommittedStatus :: changeOrderStatusIp\n",
				logger, changeOrderStatusIp);

		serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, changeOrderStatusIp);

		movePOLineToShippedStatus();

	}

	/**
	 * This method will move all the available quantity of an order line from
	 * Committed/Included In Shipment to Shipped status by changing its status.
	 * 
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 * @param sChainedFromOrderHeaderKey
	 * @param sChainedFromOrderLineKey
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private void movePOLineToShippedStatus() throws YFSException {

		YFCDocument changeOrderStatusIp = YFCDocument.getDocumentFor("<OrderStatusChange OrderHeaderKey='"
				+ sOrderHeaderKey + "' TransactionId='" + TelstraConstants.TRANSACTION_SHIP_PURCHASE_ORDER
				+ "'> <OrderLines> <OrderLine BaseDropStatus='" + TelstraConstants.STATUS_SHIPPED + "'  OrderLineKey='"
				+ sOrderLineKey + "' ChangeForAllAvailableQty='Y'> </OrderLine> </OrderLines> </OrderStatusChange>");

		LoggerUtil.verboseLog("GpsChangeOrderStatusForPO :: movePOLineToCommittedStatus :: changeOrderStatusIp\n",
				logger, changeOrderStatusIp);

		serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, changeOrderStatusIp);

		if (!(YFCCommon.isStringVoid(sChainedFromOrderHeaderKey) || YFCCommon.isStringVoid(sChainedFromOrderLineKey))) {

			YFCDocument changeSalesOrderStatusIp = YFCDocument.getDocumentFor(
					"<OrderStatusChange OrderHeaderKey='" + sChainedFromOrderHeaderKey + "' TransactionId='"
							+ TelstraConstants.TRANSACTION_SHIP_ORDER + "'> <OrderLines> <OrderLine BaseDropStatus='"
							+ TelstraConstants.STATUS_SHIPPED + "'  OrderLineKey='" + sChainedFromOrderLineKey
							+ "' ChangeForAllAvailableQty='Y'> </OrderLine> </OrderLines> </OrderStatusChange>");

			LoggerUtil.verboseLog("GpsChangeOrderStatusForPO :: movePOLineToCommittedStatus :: changeOrderStatusIp\n",
					logger, changeSalesOrderStatusIp);

			serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, changeSalesOrderStatusIp);
		}

		movePOLineToIntransitStatus();

	}

	/**
	 * This method will move all the available quantity of an order line from
	 * Shipped to Intransit status by changing its status.
	 * 
	 * @param sOrderHeaderKey
	 * @param sOrderLineKey
	 * @throws YFSException
	 * @throws RemoteException
	 * @throws YIFClientCreationException
	 */
	private void movePOLineToIntransitStatus() throws YFSException {

		YFCDocument changeOrderStatusIp = YFCDocument
				.getDocumentFor("<OrderStatusChange OrderHeaderKey='" + sOrderHeaderKey + "' TransactionId='"
						+ TelstraConstants.TRANSACTION_PO_SHIP_LISTENER + "'> <OrderLines> <OrderLine BaseDropStatus='"
						+ TelstraConstants.STATUS_INTRANSIT + "'  OrderLineKey='" + sOrderLineKey
						+ "' ChangeForAllAvailableQty='Y'> </OrderLine> </OrderLines> </OrderStatusChange>");

		LoggerUtil.verboseLog("GpsChangeOrderStatusForPO :: movePOLineToCommittedStatus :: changeOrderStatusIp\n",
				logger, changeOrderStatusIp);

		serviceInvoker.invokeYantraApi(TelstraConstants.API_CHANGE_ORDER_STATUS, changeOrderStatusIp);

	}

}
