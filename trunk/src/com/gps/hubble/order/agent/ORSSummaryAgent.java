package com.gps.hubble.order.agent;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import com.bridge.sterling.framework.agent.AbstractCustomBaseAgent;
import com.bridge.sterling.utils.LoggerUtil;
import com.yantra.yfc.date.YDate;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCException;

public class ORSSummaryAgent extends AbstractCustomBaseAgent{
	
	private static YFCLogCategory logger = YFCLogCategory.instance(ORSSummaryAgent.class);

	@Override
	public List<YFCDocument> getJobs(YFCDocument msgXml, YFCDocument lastMsgXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "getJobs", msgXml);
		boolean getJobsRequired = true;
		int refreshInterval = msgXml.getDocumentElement().getIntAttribute("RefreshInterval",24);
		//We do not want to call getJobs twice in a run
		if(lastMsgXml != null){
			getJobsRequired = false;
		}
		// Check if the summary table was refreshed before the last interval and if so no need to proceed 
		if(hasLatestRecords(refreshInterval)){
			getJobsRequired = false;
		}
		
		List<YFCDocument> list = new ArrayList<>();
		if(!getJobsRequired){
			logger.debug("returning as latest records are present");
			return list;
		}
		refreshTable();
		deleteOldRecords();
		logger.debug("GPS_ORS_MQT is refreshed");
		String query = createQuery();
		Connection connection = getServiceInvoker().getDBConnection();

		try (PreparedStatement ps = connection.prepareStatement(query);
				ResultSet rs = ps.executeQuery()) {
			YFCDocument doc = constructDocument(rs);
			list.add(doc);
		}catch(SQLException sqlEx){
			logger.error("SQLException while executing query ",sqlEx);
			throw new YFCException(sqlEx);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "getJobs", list);
		return list;
	}

	@Override
	public void executeJob(YFCDocument executeJobXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "executeJob", executeJobXml);
		if(executeJobXml.getDocumentElement() == null || executeJobXml.getDocumentElement().getChildren("OrsSummary") == null){
			return;
		}
		//deleteOldRecords();
		for(Iterator<YFCElement> itr = executeJobXml.getDocumentElement().getChildren("OrsSummary").iterator();itr.hasNext();){
			YFCElement elemOrsVendorSummary = itr.next();
			YFCDocument docOrsVendorSummary = YFCDocument.getDocumentFor(elemOrsVendorSummary.getString());
			logger.debug("creating records for ors summary");
			invokeYantraService("GpsCreateOrsSummary", docOrsVendorSummary);
		}
		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "executeJob", executeJobXml);
	}
	
	private String createQuery(){
		return "SELECT OH.ORDER_TYPE AS ORDER_TYPE, OH.DOCUMENT_TYPE AS DOCUMENT_TYPE, ORS.STATUS AS STATUS, YFS_STATUS.DESCRIPTION AS STATUS_DESCRIPTION, "
				+ " COUNT(DISTINCT OH.ORDER_HEADER_KEY) AS NO_OF_ORDERS, SUM(OH.TOTAL_AMOUNT) AS TOTAL_AMOUNT "
				+ " FROM YFS_ORDER_HEADER OH, GPS_ORS_MQT ORS, YFS_STATUS YFS_STATUS, YFS_PROCESS_TYPE YFS_PROCESS_TYPE "
				+ " WHERE ORS.ORDER_HEADER_KEY = OH.ORDER_HEADER_KEY "
				+ " AND OH.ORDER_CLOSED <> 'Y'"
				+ " AND ORS.STATUS= YFS_STATUS.STATUS "
				+ " AND YFS_STATUS.PROCESS_TYPE_KEY=YFS_PROCESS_TYPE.PROCESS_TYPE_KEY "
				+ " AND YFS_PROCESS_TYPE.BASE_PROCESS_TYPE='ORDER_FULFILLMENT' "
				+ " AND YFS_PROCESS_TYPE.DOCUMENT_TYPE=OH.DOCUMENT_TYPE "
				+ " GROUP BY OH.ORDER_TYPE, OH.DOCUMENT_TYPE, ORS.STATUS, YFS_STATUS.DESCRIPTION";
	}
	
	/*
	 * This method will check whether records inserted in GPR_ORS_VENDOR_SUMMARY is latest, i.e. inserted withing last one hour 
	 */
	private boolean hasLatestRecords(int refreshInterval){
		try{
			YFCDocument inDoc = YFCDocument.getDocumentFor("<OrsSummary MaximumRecords='1' />");
			//YFCDocument template = YFCDocument.getDocumentFor("<OrsVendorSummaryList><OrsVendorSummary  /></OrsVendorSummaryList>");
			YFCDocument outDoc = invokeYantraService("GpsGetOrsSummaryList", inDoc);
			if(outDoc == null || outDoc.getDocumentElement() == null || outDoc.getDocumentElement().getChildren() == null){
				logger.debug("No latest record found");
				return false;
			}
			YFCElement orsSummary = outDoc.getDocumentElement().getChildElement("OrsSummary");
			if(orsSummary == null){
				logger.debug("No latest record found");
				return false;
			}
			YDate createTs = orsSummary.getYDateAttribute("CreateTS");
			YDate currentTime = new YDate(false);
			long hour = refreshInterval * 60 * 60 * 1000;
			if(createTs == null || currentTime.getTime() - createTs.getTime() > hour){
				logger.debug("createTs is not latest "+createTs);
				return false;
			}
			return true;
		}catch(Exception e){
			logger.error("Exception while determining latest records", e);
			throw new YFCException(e);
		}
	}
	
	private YFCDocument constructDocument(ResultSet rs) throws SQLException{
		YFCDocument doc = YFCDocument.getDocumentFor("<OrsSummaryList />");
		YFCElement elem = doc.getDocumentElement();
		while(rs.next()){
			YFCElement elemOrsSummary = elem.createChild("OrsSummary");
			String orderType = rs.getString("ORDER_TYPE");
			elemOrsSummary.setAttribute("OrderType", orderType);
			String sellerOrganizationCode = rs.getString("DOCUMENT_TYPE");
			elemOrsSummary.setAttribute("DocumentType", sellerOrganizationCode);
			String status = rs.getString("STATUS");
			elemOrsSummary.setAttribute("Status", status);
			String statusDescription = rs.getString("STATUS_DESCRIPTION");
			elemOrsSummary.setAttribute("StatusDescription", statusDescription);			
			long noOfOrders = rs.getLong("NO_OF_ORDERS");
			elemOrsSummary.setAttribute("NoOfOrders", noOfOrders);
			Double totalAmount = rs.getDouble("TOTAL_AMOUNT");
			elemOrsSummary.setAttribute("TotalAmount", totalAmount);
		}
		return doc;
	}
	
	private void refreshTable(){
		String refreshQuery = "REFRESH TABLE GPS_ORS_MQT";
		Connection connection = getServiceInvoker().getDBConnection();
		try (PreparedStatement ps = connection.prepareStatement(refreshQuery)) {
			ps.execute();
		}catch(SQLException sqlEx){
			sqlEx.printStackTrace();
			throw new YFCException(sqlEx);
		}
	}
	
	private void deleteOldRecords(){
			String deleteQuery = "DELETE FROM GPS_ORS_SUMMARY";
		Connection connection = getServiceInvoker().getDBConnection();

		try (PreparedStatement ps = connection.prepareStatement(deleteQuery)) {
			ps.execute();
		}catch(SQLException sqlEx){
			throw new YFCException(sqlEx);
		}
	}

}
