/***********************************************************************************************
 * File Name        : ManageLogisticsResponse.java
 *
 * Description      : A TRA_ODR_1 message is received in Sterling, an integration server will update the shipment for the order, 
 *                    showing the status of the logistics request. It can either by Accepted/Rejected. If multiple shipments or no 
 *                    Shipments are found passed with the input data Exception will be thrown.
 *                                       
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #    Date              Author				Modification
 * --------------------------------------------------------------------------------------------
 * 1.0      12-01-2017      Keerthi Yadav			Initial Version 
 * 1.1		12-05-2017		Prateek Kumar			HUB-9036 - 3B12C message for vector order
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/

package com.gps.hubble.logistics.api;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.ExceptionUtil;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XPathUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.gps.hubble.constants.TelstraErrorCodeConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.dom.YFCNodeList;
import com.yantra.yfc.log.YFCLogCategory;
import com.yantra.yfc.util.YFCCommon;
import com.yantra.yfc.util.YFCException;
import com.yantra.yfs.japi.YFSException;


public class ManageLogisticsResponse extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(ManageLogisticsResponse.class);

	/**
	 * This method will change the Shipment Status to Accept/Reject as responded by the logistics
	 * @param inXml
	 * @return outXml
	 */

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		LoggerUtil.startComponentLog(logger, this.getClass().getName(), "invoke", inXml);

		// Sample inXml to this class:-

		/*<Shipment LogisticsStatus="Accepted/Rejected" ShipmentNo="" OrderNo="OrderNo3" TrackingNo="123" /> */

		/*
		 * HUB-9036 :Begin
		 */
		YFCElement yfcEleInRoot = inXml.getDocumentElement();
		String sLogisticsStatus = yfcEleInRoot.getAttribute(TelstraConstants.LOGISTICS_STATUS);

		String sOrderNo = inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO);
		if(!YFCCommon.isStringVoid(sOrderNo)){
			YFCDocument yfcDocGetOrderLineListOp = callGetOrderLineList(sOrderNo);

			double dTotalLineList = yfcDocGetOrderLineListOp.getDocumentElement().getDoubleAttribute("TotalLineList");
			if(dTotalLineList > 0) {
				if("Rejected".equalsIgnoreCase(sLogisticsStatus)){
					if(dTotalLineList > 1) {
						throw ExceptionUtil.getYFSException(TelstraErrorCodeConstants.SHIP_ORDER_MULTIPLE_ORDER_LINES_WITH_SAME_VECTOR_ID, new YFSException());
					}
					cancelRejectedVectorOrderLine(yfcDocGetOrderLineListOp);
				}
				return inXml;
			}			
		}
		/*
		 * HUB-9036 :End
		 */
		//Change Shipment Status to Adhoc Accepted or Rejected
		YFCDocument outXml = changeShipmentStatus(inXml);

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return outXml;
	}

	/*
	 * This method will cancel the line having the incoming vector ID.
	 */
	private void cancelRejectedVectorOrderLine(YFCDocument yfcDocGetOrderLineListOp) {

		String sOrderLineKey = XPathUtil.getXpathAttribute(yfcDocGetOrderLineListOp, "//OrderLine/@OrderLineKey");
		String sOrderHeaderKey = XPathUtil.getXpathAttribute(yfcDocGetOrderLineListOp, "//OrderLine/Order/@OrderHeaderKey");

		YFCDocument yfcDocChangeOrderIp = YFCDocument.getDocumentFor(
				"<Order OrderHeaderKey='" + sOrderHeaderKey + "'><OrderLines><OrderLine Action='CANCEL' OrderLineKey='"
						+ sOrderLineKey + "'/> </OrderLines></Order>");
		LoggerUtil.verboseLog("ManageLogisticsResponse :: cancelRejectedVectorOrderLine :: yfcDocChangeOrderIp", logger, yfcDocChangeOrderIp);

		invokeYantraApi(TelstraConstants.API_CHANGE_ORDER, yfcDocChangeOrderIp);
	}

	/*
	 * call get order line list with incoming order no as vector id
	 */
	private YFCDocument callGetOrderLineList(String sOrderNo) {

		YFCDocument yfcDocGetOrderLineListIp = YFCDocument.getDocumentFor("<OrderLine><Order EntryType='VECTOR' /><Extn VectorID='"+sOrderNo+"' /></OrderLine>");
		LoggerUtil.verboseLog("ManageLogisticsResponse :: callGetOrderLineList :: yfcDocGetOrderLineListIp", logger, yfcDocGetOrderLineListIp);

		YFCDocument yfcDocGetOrderLineListTemp = YFCDocument.getDocumentFor("<OrderLineList TotalLineList=''>"
				+ "<OrderLine PrimeLineNo='' OrderLineKey=''><Order OrderHeaderKey='' OrderName='' OrderNo='' DocumentType='' /></OrderLine>"
				+ "</OrderLineList>");
		YFCDocument yfcDocGetOrderLineListOp = invokeYantraApi("getOrderLineList", yfcDocGetOrderLineListIp, yfcDocGetOrderLineListTemp);
		LoggerUtil.verboseLog("ManageLogisticsResponse :: callGetOrderLineList :: yfcDocGetOrderLineListOp", logger, yfcDocGetOrderLineListOp);
		return yfcDocGetOrderLineListOp;
	}

	/**
	 * 
	 * @param inXml
	 * @return outXml of changeShipment Api
	 */
	private YFCDocument changeShipmentStatus(YFCDocument inXml) {

		YFCDocument docChangeShipmentOutXml=inXml;

		String sShipmentNo=inXml.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO,"");
		String sTrackingNo=inXml.getDocumentElement().getAttribute(TelstraConstants.TRACKING_NO,"");
		String sOrderNo=inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO,"");

		/*Validate if Shipment is present with the passed input if multiple shipments or no shipments 
		are found passed with the input data Exception will be thrown. */

		if(!YFCObject.isNull(sOrderNo) || !YFCObject.isNull(sTrackingNo) || !YFCObject.isNull(sShipmentNo)){
			//Checking Shipment for Shipment Shipped Status
			YFCDocument docGetShipmentListInput=YFCDocument.getDocumentFor("<Shipment OrderNo='"+ sOrderNo+ "' ShipmentNo='"+ sShipmentNo + "' TrackingNo='"+ sTrackingNo +"' Status='1400'/>");
			YFCDocument docGetShipmentListTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentNo='' ShipmentKey='' Status=''/></Shipments>");
			YFCDocument docGetShipmentListOutput = invokeYantraApi("getShipmentList", docGetShipmentListInput,docGetShipmentListTemplate);	
			YFCNodeList<YFCElement> nlShipment = docGetShipmentListOutput.getElementsByTagName(TelstraConstants.SHIPMENT);

			if(nlShipment.getLength() ==  1 ) {
				String sShipmentKey = docGetShipmentListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.SHIPMENT).item(0).getAttribute(TelstraConstants.SHIPMENT_KEY,"");
				if(!YFCObject.isNull(sShipmentKey)){
					//changing the Shipment Status as responded by logistics

					String sLogisticsStatus=inXml.getDocumentElement().getAttribute("LogisticsStatus","");
					String sRejectionReason=inXml.getDocumentElement().getAttribute("RejectionReason","");
					docChangeShipmentOutXml=invokeYantraApi("changeShipment",YFCDocument.getDocumentFor("<Shipment ShipmentKey='"+ sShipmentKey + "' "
							+ "Action='Modify'><Extn LogisticsStatus='" + sLogisticsStatus + "'  RejectionReason='" + sRejectionReason + "' /></Shipment>"));
				}
			}else{
				LoggerUtil.verboseLog("Shipment was not Found", logger, " throwing exception");
				String strErrorCode = getProperty("ShipmentSearchFailureCode", true);
				throw  ExceptionUtil.getYFSException(strErrorCode,new YFCException());	
			}
		}else{
			LoggerUtil.verboseLog("Shipment was not Found", logger, " throwing exception");
			String strErrorCode = getProperty("ShipmentSearchFailureCode", true);
			throw  ExceptionUtil.getYFSException(strErrorCode,new YFCException());	
		}
		return docChangeShipmentOutXml;
	}
}
