/***********************************************************************************************
 * File Name        : CreateAdhocLogisticsOrder.java
 *
 * Description      : Ad-hoc Logistics will be created in Sterling by IBM to move boxes from one address to another. 
 *                    The logistics request created by the vendor will be against an ASN. The request will be captured 
 *                    as sales order in Sterling. This order will be created for a dummy item. The system will also prepare the 
 *                    Adhoc logistics request STR_ODR_1 (3B12) message and drop it to an MQ.
 *
 * Modification Log :
 * ---------------------------------------------------------------------------------------------
 * Ver #  	Date              Author				Modification
 * ---------------------------------------------------------------------------------------------
 * 1.0		10-01-2017      Keerthi Yadav           Initial Version 
 * 1.1		25-04-2017		Prateek Kumar			HUB-8803: Append sequence number in the outbound message
 * ---------------------------------------------------------------------------------------------
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
 **********************************************************************************************/
package com.gps.hubble.logistics.api;

import org.w3c.dom.Document;

import com.bridge.sterling.framework.api.AbstractCustomApi;
import com.bridge.sterling.utils.LoggerUtil;
import com.bridge.sterling.utils.XMLUtil;
import com.gps.hubble.constants.TelstraConstants;
import com.yantra.yfc.core.YFCObject;
import com.yantra.yfc.dom.YFCDocument;
import com.yantra.yfc.dom.YFCElement;
import com.yantra.yfc.log.YFCLogCategory;

// Sample inXml to this class:-

/*<Order EnterpriseCode='TELSTRA_SCLSA' BuyerOrganizationCode='TELSTRA' SellerOrganizationCode='TELSTRA' PriorityCode='STANDARD' OrderType='ADHOC_LOGISTICS' OrderNo=''
  EntryType='STERLING' DocumentType='0001' >
   <References>
     <Reference Name='TRACKING_NO' Value='446257'/>
   </References>
   <PersonInfoBillTo FirstName='Keerthi' DayPhone='32321' EMailID='k@123' AddressLine1='1' AddressLine2='1' AddressLine3='1' AddressLine4='1' AddressLine5='1' City='Sydney' 
     Country='AU' State='' ZipCode=''/>
   <AdditionalAddresses>
   <AdditionalAddress AddressType='SHIP_FROM'>
     <PersonInfo FirstName='Yadav' DayPhone='2786' EMailID='' AddressLine1='2' AddressLine2='2' AddressLine3='2' AddressLine4='2' AddressLine5='' City='SYdney' 
      Country='AU' State='' ZipCode=''/>
    </AdditionalAddress>
   </AdditionalAddresses>
   <OrderLines>
    <OrderLine ShipNode='TestOrg9' >
   </OrderLine>
  </OrderLines>
  <Containers>
   <Container ContainerType='TYPE1' ContainerNo='2' ContainerHeightUOM='EACH' ContainerHeight='2' ContainerLengthUOM='EACH' ContainerLength='2' ContainerWidthUOM='EACH' 
  ContainerWidth='2' ContainerVolumeUOM='EACH' ContainerVolume='2' ContainerGrossWeightUOM='' ContainerGrossWeight='2'/>
  </Containers>
 </Order>	
 */

public class CreateAdhocLogisticsOrder extends AbstractCustomApi {
	private static YFCLogCategory logger = YFCLogCategory.instance(CreateAdhocLogisticsOrder.class);
	private YFCDocument outconfirmShipmentxml=null;

	@Override
	public YFCDocument invoke(YFCDocument inXml) {
		//HUB 7273- [START]


		outconfirmShipmentxml = inXml;

		//HUB 8118- [START]
		/* Removing the shipment check and delete/update container logic as BookFreight class has been implemented to perform this function */

		//boolean bNoShipment=validateShipmentforOrder(inXml);

		//Renaming Node to 'Order' as JSON from the UI makes the root element as the service name

		Document doc = inXml.getDocument();
		doc.renameNode(doc.getDocumentElement(), doc.getNamespaceURI(), TelstraConstants.ORDER);
		YFCDocument inputDoc = YFCDocument.getDocumentFor(doc);

		//Create sales order with the dummy Item if no shipments associated with the order
		//if(bNoShipment){
		outconfirmShipmentxml=createOrder(inputDoc);
		//}

		//HUB 8118- [END]

		LoggerUtil.endComponentLog(logger, this.getClass().getName(), "invoke",inXml);
		return outconfirmShipmentxml;

		//HUB 7273- [END]
	}

	// Creating a dummy sales Order with a dummy ShipNode and Item.
	private YFCDocument createOrder(YFCDocument inXml) {
		if(YFCObject.equals(inXml.getDocumentElement().getAttribute(TelstraConstants.ORDER_TYPE),"ADHOC_LOGISTICS")){
			YFCElement eleinXmlContainers=inXml.getDocumentElement().getElementsByTagName(TelstraConstants.CONTAINERS).item(0);
			YFCElement eleCreateOrder=inXml.getDocumentElement();
			eleCreateOrder.removeChild(eleinXmlContainers);
			YFCElement eleOrderLine=inXml.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER_LINE).item(0);
			if(!YFCObject.isNull(eleOrderLine)){
				String sShipNode=eleOrderLine.getAttribute(TelstraConstants.SHIP_NODE,"");
				if(YFCObject.isNull(sShipNode)){
					sShipNode = getProperty("ADHOC_LOGISTICS_NODE", true);
					eleOrderLine.setAttribute(TelstraConstants.SHIP_NODE,sShipNode);
				}
				eleOrderLine.setAttribute(TelstraConstants.ORDERED_QTY, "1");
				eleOrderLine.setAttribute(TelstraConstants.PRIME_LINE_NO, "1");
				YFCElement eleOrderItemLine= eleOrderLine.createChild(TelstraConstants.ITEM);
				eleOrderItemLine.setAttribute(TelstraConstants.ITEM_ID, "ADHOC_LOGISTICS");
				eleOrderItemLine.setAttribute("ItemDesc", "Item For Adhoc Logistics Order");
				eleOrderItemLine.setAttribute(TelstraConstants.UNIT_OF_MEASURE, "EA");
				YFCDocument doctempOrderListinXml = YFCDocument.getDocumentFor("<Order EnterpriseCode='' OrderNo='' DocumentType='' OrderHeaderKey='' ><OrderLines><OrderLine Status='' OrderLineKey='' PrimeLineNo='' /></OrderLines></Order>");
				YFCDocument outxml = invokeYantraApi("createOrder", inXml,doctempOrderListinXml);

				String sOrderHeaderKey=outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_HEADER_KEY,"");
				String sOrderNo=outxml.getDocumentElement().getAttribute(TelstraConstants.ORDER_NO,"");

				//Schedule and Release the Order
				String sOrderReleaseKey =scheduleAndReleaseOrder(sOrderHeaderKey);

				// Method to Create and Confirm Shipment with the container details and drop to Queue
				outconfirmShipmentxml= createAndConfirmShipment(inXml,eleinXmlContainers,sOrderNo,sOrderHeaderKey,sOrderReleaseKey);

			}

		}
		return outconfirmShipmentxml;
	}

	//Scheduling and releasing the created Order
	private String scheduleAndReleaseOrder(String sOrderHeaderKey) {
		String sOrderReleaseKey=null;
		YFCDocument docInputscheduleXml=YFCDocument.getDocumentFor("<ScheduleOrder ScheduleAndRelease='Y' CheckInventory='N' IgnoreReleaseDate='Y' OrderHeaderKey='" + sOrderHeaderKey + "' />");
		invokeYantraApi("scheduleOrder", docInputscheduleXml);
		YFCDocument docOrderReleaseListoutXml = getOrderReleaseList(sOrderHeaderKey);
		YFCElement eleReleaseorderlist=docOrderReleaseListoutXml.getDocumentElement();
		if(!YFCObject.isNull(eleReleaseorderlist.getElementsByTagName(TelstraConstants.ORDER_RELEASE).item(0).getAttribute(TelstraConstants.ORDER_HEADER_KEY))){
			sOrderReleaseKey=eleReleaseorderlist.getAttribute("LastOrderReleaseKey");
		}
		return sOrderReleaseKey;
	}

	//Retrieving the releaseKey for the Order
	private YFCDocument getOrderReleaseList(String sOrderHeaderKey) {
		YFCDocument docOrderListinXml = YFCDocument.getDocumentFor("<OrderRelease OrderHeaderKey='"+ sOrderHeaderKey+ "' />");
		YFCDocument doctempgetOrderListinXml = YFCDocument.getDocumentFor("<OrderReleaseList LastOrderReleaseKey='' TotalNumberOfRecords=''><OrderRelease  "
				+ "EnterpriseCode='' DocumentType='' OrderHeaderKey='' OrderNo='' /></OrderReleaseList>");
		return invokeYantraApi("getOrderReleaseList", docOrderListinXml,doctempgetOrderListinXml);
	}

	//Confirming the Shipment with the Container Details(Dimensions) of the box to track it at the shipment level with the total No of containers in the extended column
	//and then a message is formed for STR_ODR_1 (3B12) for the logistics request 

	private YFCDocument createAndConfirmShipment(YFCDocument inXml, YFCElement eleinXmlContainers, String sOrderNo, String sOrderHeaderKey, String sOrderReleaseKey) {
		String sPriorityCode=inXml.getDocumentElement().getAttribute("PriorityCode","");
		//HUB-8104 [START]
		//Initially reference/@name was being read for the tracking Number this has been changed to reference/@value.

		String sTrackingNo=inXml.getElementsByTagName(TelstraConstants.REFERENCE).item(0).getAttribute(TelstraConstants.VALUE,"");
		//long lNextOrderNo = getServiceInvoker().getNextSequenceNumber("GPS_TRACKING_NO_SEQ");
		if(YFCObject.isVoid(sTrackingNo)) {
			long lNextOrderNo = getServiceInvoker().getNextSequenceNumber("GPS_TRACKING_NO_SEQ");
			sTrackingNo = "IBMS"+lNextOrderNo;
		}

		//HUB-8104 [END]
		String sRequestedShipmentDate=inXml.getDocumentElement().getAttribute(TelstraConstants.REQUESTED_SHIPMENT_DATE,"");
		String sCarrier="";
		String sService="";

		//Priority based  SCAC and service is read from the common Code List
		YFCDocument docGetCommonCodeListOutput = invokeYantraApi(TelstraConstants.API_GET_COMMON_CODE_LIST,YFCDocument.getDocumentFor("<CommonCode CodeType=\"LOGISTICS_PRIORITY\" CodeValue='" + sPriorityCode + "'/>"));
		if(!YFCObject.isNull(docGetCommonCodeListOutput.getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute(TelstraConstants.CODE_VALUE))){
			sCarrier=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeShortDescription","");
			sService=docGetCommonCodeListOutput.getDocumentElement().getElementsByTagName(TelstraConstants.COMMON_CODE).item(0).getAttribute("CodeLongDescription","");
		}	    

		YFCDocument docCreateShipment = YFCDocument.getDocumentFor("<Shipment />");
		YFCElement eleShipment=docCreateShipment.getDocumentElement();
		eleShipment.setAttribute(TelstraConstants.ORDER_NO, sOrderNo);
		eleShipment.setAttribute(TelstraConstants.ORDER_HEADER_KEY, sOrderHeaderKey);
		eleShipment.setAttribute(TelstraConstants.TRACKING_NO, sTrackingNo);
		eleShipment.setAttribute(TelstraConstants.REQUESTED_SHIPMENT_DATE, sRequestedShipmentDate);
		eleShipment.setAttribute("SCAC", sCarrier);
		eleShipment.setAttribute("CarrierServiceCode", sService);

		eleShipment.createChild(TelstraConstants.ORDER_RELEASES).createChild(TelstraConstants.ORDER_RELEASE).setAttribute(TelstraConstants.ORDER_RELEASE_KEY, sOrderReleaseKey);
		eleShipment.createChild(TelstraConstants.EXTN).setAttribute("LogisticsStatus", "PENDING");

		//Stamping the SHIP_TO Address
		YFCElement eleBillingPersonInfo = inXml.getDocumentElement().getChildElement(TelstraConstants.PERSON_INFO_BILL_TO);
		eleShipment.appendChild(docCreateShipment.importNode(eleBillingPersonInfo,true));
		docCreateShipment.getDocument().renameNode(eleShipment.getChildElement(TelstraConstants.PERSON_INFO_BILL_TO).getDOMNode(), null, "ToAddress");

		//Stamping the SHIP_FROM Address
		YFCElement elePersonInfo = inXml.getElementsByTagName(TelstraConstants.PERSON_INFO).item(0);
		eleShipment.appendChild(docCreateShipment.importNode(elePersonInfo,true));
		docCreateShipment.getDocument().renameNode(eleShipment.getChildElement(TelstraConstants.PERSON_INFO).getDOMNode(), null, "FromAddress");

		YFCDocument docDroptoLogisticsQueue= XMLUtil.getDocumentFor(docCreateShipment
				.getDocumentElement().cloneNode(true));
		YFCElement eleShipmentContainers =eleShipment.createChild(TelstraConstants.CONTAINERS);
		YFCElement eleLogisticsContainers =docDroptoLogisticsQueue.getDocumentElement().createChild(TelstraConstants.CONTAINERS);


		//Adding the Containers(Boxes) to the Shipment
		for (YFCElement eleinXmlContainer : eleinXmlContainers.getElementsByTagName(TelstraConstants.CONTAINER)){
			int sContainerNo=eleinXmlContainer.getIntAttribute(TelstraConstants.CONTAINER_NO);
			if(sContainerNo > 0){
				eleinXmlContainer.removeAttribute(TelstraConstants.CONTAINER_NO);
				eleinXmlContainer.createChild(TelstraConstants.EXTN).setAttribute("NoOfContainer", sContainerNo);
				eleShipmentContainers.appendChild(docCreateShipment.importNode(eleinXmlContainer,true));
				eleinXmlContainer.removeChild(eleinXmlContainer.getElementsByTagName(TelstraConstants.EXTN).item(0));
				for(int iterator=1; iterator<=sContainerNo; iterator++) {	
					eleLogisticsContainers.appendChild(docDroptoLogisticsQueue.importNode(eleinXmlContainer, true));
				}
			}
		}

		YFCDocument outcreateShipmentxml = invokeYantraApi("createShipment", docCreateShipment, getCreateShipmentTemplate());
		outconfirmShipmentxml = invokeYantraApi("confirmShipment", outcreateShipmentxml);
		String sShipmentNo=outconfirmShipmentxml.getDocumentElement().getAttribute(TelstraConstants.SHIPMENT_NO,"");
		if(!YFCObject.isNull(sShipmentNo)){			
			docDroptoLogisticsQueue.getDocumentElement().setAttribute(TelstraConstants.SHIPMENT_NO, sShipmentNo);
			YFCElement eleinXmlReleases=docDroptoLogisticsQueue.getDocumentElement().getElementsByTagName(TelstraConstants.ORDER_RELEASES).item(0);
			docDroptoLogisticsQueue.getDocumentElement().removeChild(eleinXmlReleases);
			//HUB-8803 - Begin
			long lNextSequenceNo = getServiceInvoker().getNextSequenceNumber(TelstraConstants.GPS_SEQUENCE_ADHOC);
			String sSequenceNo = String.valueOf(lNextSequenceNo);
			docDroptoLogisticsQueue.getDocumentElement().setAttribute(TelstraConstants.SEQUENCE_NO, sSequenceNo);
			//HUB-8803 - End
			//Dropping the confirm shipment message to DropLogisticQueue
			String sDroptoLogisticsQueue = getProperty("SERVICE_DropToLogisticsRequestQ", true);
			invokeYantraService(sDroptoLogisticsQueue, docDroptoLogisticsQueue);

		}
		return outcreateShipmentxml;
	}

	private YFCDocument getCreateShipmentTemplate() {
		YFCDocument createShipmentTemplate = YFCDocument.getDocumentFor("<Shipment SellerOrganizationCode='' ShipNode='' ShipmentKey='' ShipmentNo='' TrackingNo='' />");
		return createShipmentTemplate;
	}

	//HUB 7273- [START]

	/**
	 * This method returns boolean if it has shipments. If there are existing containers for the shipment it will delete them and add the new containers 
	 * passed in the input.
	 * Method name: validateShipmentforOrder
	 * @param inXml
	 * @return boolean
	 */

	/*	private boolean validateShipmentforOrder(YFCDocument inXml){

		String sOrderNo =inXml.getDocumentElement().getAttribute("OrderNo","");
		if(!YFCObject.isNull(sOrderNo)){
			YFCDocument docGetShipmentListOutput = getShipmentList(sOrderNo);
			YFCElement elelShipment = docGetShipmentListOutput.getDocumentElement().getChildElement("Shipment");
			if(!YFCObject.isNull(elelShipment)){
				//Delete ShipmentContainers if present
				YFCDocument docchangeShipmentOutput = deleteShipmentContainers(docGetShipmentListOutput);
				//Create ShipmentContainers from inXml
				createShipmentContainers(docchangeShipmentOutput,inXml);
				return false;
			}else{
				return true;
			}
		}else{
			return true;
		}
	}*/

	/**
	 * This method returns getShipmentList document
	 * Method name: getShipmentList
	 * @param sShipmentKey
	 * @return docGetShipmentListOutput
	 */

	/*	private YFCDocument getShipmentList(String sOrderNo){
		YFCDocument docGetShipmentListInput=YFCDocument.getDocumentFor("<Shipment OrderNo='"+ sOrderNo+ "' Status='1400' />");
		YFCDocument docGetShipmentListTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentNo='' ShipmentKey='' Status=''><Containers><Container ContainerNo='' /></Containers></Shipment></Shipments>");
		return invokeYantraApi("getShipmentList", docGetShipmentListInput,docGetShipmentListTemplate);
	}*/

	/**
	 * This method deletes all the shipment containers to insert the new one in the inXml
	 * Method name: deleteShipmentContainers
	 * @param docGetShipmentListOutput
	 * @return docchangeShipment
	 */

	/*private YFCDocument deleteShipmentContainers(YFCDocument docGetShipmentListOutput) {
		logger.verbose("getShipmentList Output : " + docGetShipmentListOutput.toString());
		YFCElement eleShipmentList=docGetShipmentListOutput.getDocumentElement().getElementsByTagName("Shipment").item(0);
		YFCDocument docchangeShipment= XMLUtil.getDocumentFor(eleShipmentList.cloneNode(true));
		YFCElement eleShipment=docchangeShipment.getDocumentElement();
		YFCElement elegetShipmentListContainers=eleShipment.getElementsByTagName(TelstraConstants.CONTAINERS).item(0);
		YFCElement elegetShipmentListContainer=elegetShipmentListContainers.getElementsByTagName(TelstraConstants.CONTAINER).item(0);
		if(!YFCObject.isNull(elegetShipmentListContainer)){
			Map<String, String> mapToattibutessize = elegetShipmentListContainer.getAttributes();
			if(mapToattibutessize.size()>0){
				for (YFCElement elegetShipmentContainer : elegetShipmentListContainers.getElementsByTagName(TelstraConstants.CONTAINER)){
					elegetShipmentContainer.setAttribute("Action", "Delete");
				}
				YFCDocument docChangeShipmentTemplate = YFCDocument.getDocumentFor("<Shipments><Shipment ShipmentNo='' ShipmentKey='' Status=''><Containers><Container ContainerNo='' /></Containers></Shipment></Shipments>");
				invokeYantraApi("changeShipment", docchangeShipment,docChangeShipmentTemplate);
			}
		}
		return docchangeShipment;
	}*/

	/**
	 * This method deletes all the shipment containers to insert the new one in the inXml
	 * Method name: deleteShipmentContainers
	 * @param docGetShipmentListOutput
	 */

	/*changeShipment is called for the Shipment update the Container Details(Dimensions) of the box to track it at 
	  the shipment level with the total No of containers in the extended column and then a message is formed for STR_ODR_1 (3B12) 
	  for the logistics request */

	/*	private void createShipmentContainers(YFCDocument docChangeShipment,YFCDocument inXml) {
		logger.verbose("changeShipment Output from changeShipment DeleteContainers : " + docChangeShipment.toString());
		YFCElement eleShipment=docChangeShipment.getDocumentElement();
		YFCElement elechangeShipmentContainer=eleShipment.getElementsByTagName(TelstraConstants.CONTAINERS).item(0);
		eleShipment.removeChild(elechangeShipmentContainer);
		YFCDocument docDroptoLogisticsQueue= XMLUtil.getDocumentFor(docChangeShipment
				.getDocumentElement().cloneNode(true));
		YFCElement eleShipmentContainers =eleShipment.createChild(TelstraConstants.CONTAINERS);
		YFCElement eleLogisticsContainers =docDroptoLogisticsQueue.getDocumentElement().createChild(TelstraConstants.CONTAINERS);

		//Adding the Containers(Boxes) to the Shipment
		for (YFCElement eleinXmlContainer : inXml.getElementsByTagName(TelstraConstants.CONTAINER)){
			int sContainerNo=eleinXmlContainer.getIntAttribute(TelstraConstants.CONTAINER_NO);
			if(sContainerNo > 0){
				eleinXmlContainer.removeAttribute(TelstraConstants.CONTAINER_NO);
				eleinXmlContainer.createChild(TelstraConstants.EXTN).setAttribute("NoOfContainer", sContainerNo);
				eleShipmentContainers.appendChild(docChangeShipment.importNode(eleinXmlContainer,true));
				eleinXmlContainer.removeChild(eleinXmlContainer.getElementsByTagName(TelstraConstants.EXTN).item(0));
				for(int iterator=1; iterator<=sContainerNo; iterator++) {	
					eleLogisticsContainers.appendChild(docDroptoLogisticsQueue.importNode(eleinXmlContainer, true));
				}
			}
		}

		outconfirmShipmentxml = invokeYantraApi("changeShipment", docChangeShipment);
		logger.verbose("changeShipment Output : " + docChangeShipment.toString());

		String sDroptoLogisticsQueue = getProperty("SERVICE_DropToLogisticsRequestQ", true);
		invokeYantraService(sDroptoLogisticsQueue, docDroptoLogisticsQueue);
		logger.verbose("DroptoLogisticRequest : " + docDroptoLogisticsQueue.toString());
	}
	 */

	//HUB 7273- [END]
}	