<!-- Copyright (c) IBM Australia Limited 2016. All rights reserved.  -->
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
 <meta charset="UTF-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="./dist/app.js"></script>
    <script src="./dist/vendor.js"></script>
<title>Login</title>
</head>

<body class="bodybg">
    <header class=" top-bar">
        <div class="">
            <div class="row top-bar-border no-gutters nav-down">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-5 col-xs-5">
                            <div class="navbar-brand p-l-0">
                              <a style="
    display: block;
    max-width: 68px;
    overflow: hidden;" href=""><img src="./images/ibm-logo.png" class="pull-left" alt=""></a>
                            
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <nav class="navbar navbar-default cusBottom">
                <div class="row-border-bottom no-gutters">
                    <div class="container-fluid">
                        <div class="navbar-header">

                            <div class="navbar-brand" ><h4>Sterling Portal</h4></div>
                        </div>

                    </div>
                </div>
            </nav>
        </div>

    </header>
    <div class="container">
    	<div class="row loginContainer">
            <div class="col-md-8"></div>
			<div class="col-md-4">
                 <div class="logo"><img src="./images/telstralogo.png"></div>
				<div class="panel panel-login">
                      
					<div class="panel-heading">
						<div class="row">
							<div class="col-xs-12">
                             
                                
								<a href="#" class="active" id="login-form-link">Login</a>
							</div>
							
						</div>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="col-lg-12">
								<form id="login-form" action="./login" method="post" role="form" style="display: block;">
									<div class="form-group">
										<input autocomplete="off" type="text" name="LoginID" id="LoginID" tabindex="1" class="form-control" placeholder="Login ID" >
									</div>
									<div class="form-group">
										<input autocomplete="off" type="password" name="Password" id="Password" tabindex="2" class="form-control" placeholder="Password">
									</div>
									
									<div class="form-group">
										<div class="row">
											<div class="col-sm-6 col-sm-offset-3">
											<!--	<a href="sterling_portal/index.html"><input name="login-submit" id="login-submit" tabindex="4" class="form-control btn btn-login" value="Log In"></a>-->
											<button type="submit" name="login-submit" id="login-submit" tabindex="3" class="form-control btn btn-login">Login</button>
											</div>
										</div>
									</div>
									<div class="form-group">
										<div class="row">
											<div class="col-lg-12">
                                                <br>
												<div class="text-center">
													<b>Forgot Password?</b> Please Contact The Help Desk. 
												</div>
											</div>
										</div>
									</div>
								</form>
							
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

    <footer class="footer LoginFooter">
        <div class="container">
            <div class="text-center">© Copyright IBM Corporation 2016</div>
        </div>
    </footer>

</body> 
</html>