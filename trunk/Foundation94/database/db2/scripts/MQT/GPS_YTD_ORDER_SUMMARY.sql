--Copyright © IBM Australia Limited 2016. All rights reserved.

DROP TABLE GPS_YTD_ORDER_SUMMARY;

DROP TABLE GPS_YTD_ORDER_SUMMARY_S;

CREATE TABLE GPS_YTD_ORDER_SUMMARY as (
SELECT OH.ENTERPRISE_KEY ENTERPRISE_CODE,YEAR(OH.ORDER_DATE) AS YEAR,MONTH(OH.ORDER_DATE) AS MONTH,OH.ORDER_TYPE ORDER_TYPE,OH.DOCUMENT_TYPE DOCUMENT_TYPE,COUNT(OH.ORDER_HEADER_KEY) AS NO_OF_ORDERS,SUM(OH.TOTAL_AMOUNT) AS TOTAL_AMOUNT
  FROM YFS_ORDER_HEADER OH
  WHERE OH.ORDER_CLOSED <> 'Y'
  GROUP BY OH.ENTERPRISE_KEY,YEAR(OH.ORDER_DATE),MONTH(OH.ORDER_DATE),OH.ORDER_TYPE,OH.DOCUMENT_TYPE)
  DATA INITIALLY DEFERRED REFRESH DEFERRED;
  
CREATE TABLE GPS_YTD_ORDER_SUMMARY_S for GPS_YTD_ORDER_SUMMARY propagate IMMEDIATE;

SET integrity for  GPS_YTD_ORDER_SUMMARY materialized query IMMEDIATE unchecked;

SET integrity for GPS_YTD_ORDER_SUMMARY_S staging IMMEDIATE unchecked;