--Copyright © IBM Australia Limited 2016. All rights reserved.

DROP TABLE GPS_ORS_MQT;

DROP TABLE GPS_ORS_MQT_S;

CREATE TABLE GPS_ORS_MQT as (
SELECT ORS.ORDER_HEADER_KEY AS ORDER_HEADER_KEY, ORS.STATUS AS STATUS, COUNT(*) AS COUNT
FROM YFS_ORDER_RELEASE_STATUS ORS
  WHERE ORS.STATUS_QUANTITY > 0
  AND ORS.STATUS <> '1400'
  GROUP BY ORS.ORDER_HEADER_KEY,ORS.STATUS)   
  data initially deferred refresh deferred;
  
SET integrity for  GPS_ORS_MQT materialized query immediate unchecked;

-- Initial refresh of the MQT
refresh table GPS_ORS_MQT;

CREATE TABLE GPS_ORS_MQT_S for GPS_ORS_MQT propagate immediate;

set integrity for  GPS_ORS_MQT_S staging immediate unchecked;



