<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:lxslt="http://xml.apache.org/xslt"
                version="1.0">
<xsl:output method="text"/>
<xsl:template match="/">Amendment Requested For PO <xsl:value-of select="/GpsPOAmmendment/@OrderNo"/>  to be recieved at  <xsl:value-of select="/GpsPOAmmendment/@ReceivingNode"/> by user : <xsl:value-of select="/GpsPOAmmendment/@ReqUserId"/>:Comments:<xsl:value-of select="/GpsPOAmmendment/@Details"/>
