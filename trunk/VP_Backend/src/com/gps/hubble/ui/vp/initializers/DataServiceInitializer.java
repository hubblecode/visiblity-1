/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.vp.initializers;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.dataService.DataServiceManager;

/**
 * Servlet implementation class MashupInitializer
 */
@WebServlet(urlPatterns = {"/init/dataserviceinitializer"}, loadOnStartup = 50)
public final class DataServiceInitializer extends HttpServlet {
	
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataServiceInitializer() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.info("Data Service Initialization Called");
		DataServiceManager.registerDataServiceFile("/resources/vp_dataservices.json");
		DataServiceManager.registerDataServiceTemplateFile("/resources/vp_templates.xml");
		DataServiceManager.loadDataservices();
	}

}
