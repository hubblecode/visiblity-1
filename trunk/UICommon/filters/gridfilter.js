/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('gridfilter', function (rows, search, columns,dateColumns,exactMatch) {

	var requiresFilter = function(rows, search, columns) {
		if (!rows || rows.length == 0) {
			return false
		}
		for (var i = 0, l = columns.length; i < l; i++) {
			if(Utils.getValue(search, columns[i])) {
				return true
			}
		}
		return false
	}

 	if (!requiresFilter(rows, search, columns)) {
 		return rows
 	}
	var res = []
 	for (var i = 0, l = rows.length; i < l; i++) {
 		var canBeAdded = true
 		for (var c = 0, cn = columns.length; c < cn; c++) {
                        if(dateColumns && dateColumns.indexOf(columns[c]) >= 0){     
                                var searchData =Utils.getValue(search, columns[c]) 
                                if(!searchData) {
                                        continue
                                    }
                                 searchData = searchData.toString().toLowerCase()
                                 var actualData = Utils.formatForDisplay(Utils.getValue(rows[i], columns[c]),"filter") 
                                 actualData = actualData.toString().toLowerCase()
                                 if(!actualData) {
                                     canBeAdded = false
                                     break
                                 }
                                var newactual=actualData
                                var newsearch=searchData
                                 if(actualData.indexOf(searchData) == -1) {
                                    canBeAdded = false
                                    break
                                } 
                        } 
                else 
                        {
                            var searchData = Utils.getValue(search, columns[c])

                            if(!searchData) {
                                    continue
                                }
                            searchData = searchData.toString().toLowerCase()
                            var actualData = Utils.getValue(rows[i], columns[c])
                            if(!actualData) {
                                canBeAdded = false
                                break
                            }
                            if(exactMatch) {
                                if(actualData.toString().toLowerCase() != searchData) {
                                    canBeAdded = false
                                    break
                                }
                            } else {
                                if(actualData.toString().toLowerCase().indexOf(searchData) == -1) {
                                    canBeAdded = false
                                    break
                                }
                                
                            }
                        }
		}
		if(canBeAdded) {
			res.push(rows[i])
		}
 	}

  return res
})

