/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var BridgeDateUtils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('datefilter', {
  // model -> view
  // formats the value when updating the input element.
  read: function(val) {
    return BridgeDateUtils.formatForDisplay(val,"filter")
  },
  // view -> model
  // formats the value when writing to the data.
  write: function(val, oldVal) {
    var valInserverFormat = "";
    try {
      valInserverFormat =  BridgeDateUtils.formatForServer(val,"filter")
      BridgeDateUtils.formatForDisplay(valInserverFormat,"filter")
    } catch(error) {
      valInserverFormat =  BridgeDateUtils.formatForServer(oldVal,"filter")
    }
    return valInserverFormat
  }
})