/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('nodetypefilter', function (rows, DistributionRuled) {

    if (!rows || rows.length == 0) {
        return rows
    }
    
    var res = []
    var nodeTypList = function(row) {
		 
		if(DistributionRuled == '_NT') {
			if(row.DistributionRuleId.indexOf('_NT') > -1) {
				return true
			} else {
				return false
			}
		}
		else if(DistributionRuled == '_RG'){
			if(row.DistributionRuleId.indexOf('_RG') > -1 ) {
				return true
			} else {
				return false
			}
		}
		else if(DistributionRuled == 'Others'){
			if((row.DistributionRuleId.indexOf('_NT') < 0) && (row.DistributionRuleId.indexOf('_RG')< 0)
					&& row.DistributionRuleId != 'ALL' ) {
				return true
			} else {
				return false
			}
		}
		else{
			return false
		}
		
    }
    res = rows.filter(nodeTypList)

  return res
})

