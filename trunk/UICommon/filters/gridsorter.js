/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
/* Vue Filter to sort the grid data based on chosen column*/
var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('gridsorter', function (rows, key, order, numeric) {

	var requiresFilter = function(rows, key) {
		if (!rows || rows.length == 0) {
			return false
		}
		if (!key) {
			return false
		}
		return true
	}

 	if (!requiresFilter(rows, key)) {
 		return rows
 	}

 	function compare(row1, row2) {
 		var val1 = Utils.getValue(row1, key)
 		var val2 = Utils.getValue(row2, key)
        if(!val1) {
            val1=""
        }
        if(!val2) {
            val2=""
        }        
 		if(numeric) {
 			val1 = Utils.getNumber(val1)
 			val2 = Utils.getNumber(val2)
 		}

		return val1 === val2 ? 0 : val1 > val2 ? order : -order
 	}
  return rows.slice().sort(compare)
})

