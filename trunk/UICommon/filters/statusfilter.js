/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('statusfilter', function (rows, fromstatus) {

	var requiresFilter = function(rows) {
		if (!rows || rows.length == 0) {
			return false
		} else {
            return true
        }
	}

 	if (!requiresFilter(rows)) {
 		return rows
 	}
	var res = []
    var greaterStatus = function(row) {
        if(row.Status>= fromstatus) {
            return true
        } else {
            return false
        }
    }
    res = rows.filter(greaterStatus)

  return res
})

