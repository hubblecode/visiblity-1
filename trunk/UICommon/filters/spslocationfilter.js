/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var Utils = require('./../utils/utils.js')
var Vue = require('vue')
Vue.filter('spslocfilter', function (rows, locationid) {

	var res = []
    var locList = function(row) {
		 
        if(row.CodeValue != locationid) {
            return true
        } else {
            return false
        }
    }
    res = rows.filter(locList)

  return res
})

