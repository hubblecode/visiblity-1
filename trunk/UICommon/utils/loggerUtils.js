/* 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/	
var BridgeLogger = new Object();
BridgeLogger.loggerEntries = [];
BridgeLogger.enableLog = 'N';

    BridgeLogger.getInstance = function(ObjId){
        if(window.AppInfo.traceenabled ) {
            BridgeLogger.enableLog = window.AppInfo.traceenabled
        }
        return {
                log : customizedLogger('log', ObjId),
                info : customizedLogger('info', ObjId),
                warn : customizedLogger('warn', ObjId),
                debug : customizedLogger('debug', ObjId),
                error : customizedLogger('error', ObjId),
            }
        
    };

    BridgeLogger.getloggedEntries = function(){
		   return BridgeLogger.loggerEntries;  
	};
	   
    BridgeLogger.ClearloggedEntries = function(){
		   	 BridgeLogger.loggerEntries = [];  
     };

    function customizedLogger(method, ObjId){
      return function(){
      
        var $log = window.console;
        var contextEnabled = BridgeLogger.enableLog;
        if(contextEnabled === 'Y'){
            
       var timestamp = new Date()
       
        //    var timestamp = '';
            var entries = BridgeLogger.loggerEntries[ObjId];
            var logArguments = [].slice.call(arguments);
           /* if(entries == null)
               BridgeLogger.loggerEntries[ObjId] = '\n' + $.datepicker.formatDate('dd/mm/yy', new Date()) +" ::: " + ObjId + ' ::: '+ method+' ::: '+logArguments[0];
            else
               BridgeLogger.loggerEntries[ObjId] = entries +'\n'+ $.datepicker.formatDate('dd/mm/yy', new Date()) +" ::: " + ObjId+ ' ::: ' + method +' ::: '+logArguments[0];
            logArguments[0] = $.datepicker.formatDate('dd/mm/yy', new Date()) +" ::: "+ ObjId + " ::: " + logArguments[0];*/
            if(entries == null)
               BridgeLogger.loggerEntries[ObjId] = ObjId + ' ::: '+logArguments[0];
            else
               BridgeLogger.loggerEntries[ObjId] = entries +'\n'+ ObjId+ ' ::: '+logArguments[0];
            logArguments[0] =  ObjId + " ::: " + logArguments[0];
            //$log[method].apply(null, logArguments);
            $log[method](logArguments[0]);
        }
            
        }
        
    }


	module.exports = BridgeLogger

