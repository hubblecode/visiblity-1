package com.gps.hubble.ui;

import com.gps.hubble.ui.utilities.Utils;

public class AppInfo {

	private static String loginpage = null;
	private static String homepage = null;
	private static String remoteserverurl = null;
	private static String index=null;
	private static String loginid=null;
	private static String identifier = null;
	private static boolean ssomode = false;
	
	public static String getLoginpage() {
		return loginpage;
	}

	public static void setLoginpage(String loginpage) {
		AppInfo.loginpage = loginpage;
	}
	
	public static String getHomepage() {
		return homepage;
	}
	
	public static void setHomepage(String homepage) {
		AppInfo.homepage = homepage;
	}
	
	public static String getRemoteserverurl() {
		return remoteserverurl;
	}
	
	public static void setRemoteserverurl(String remoteserverurl) {
		AppInfo.remoteserverurl = remoteserverurl;
	}

	public static String getIndex() {
		return index;
	}

	public static void setIndex(String index) {
		AppInfo.index = index;
	}

    public static String getLoginid() {
      return loginid;
    }
  
    public static void setLoginid(String loginid) {
      AppInfo.loginid = loginid;
    }

	public static String getIdentifier() {
		
		if (identifier != null)
			return identifier;
		else
		    return "";
	}

	public static void setIdentifier(String identifier) {
		AppInfo.identifier = identifier;
	}

	public static boolean isSsomode() {
		return ssomode;
	}

	public static void setSsomode(String ssomode) {
		AppInfo.ssomode = Utils.getBooleanValue(ssomode);
	}
	
}
