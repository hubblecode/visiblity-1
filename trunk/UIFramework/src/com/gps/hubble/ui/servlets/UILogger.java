/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Enumeration;
import java.util.StringTokenizer;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * 
 * @author Bridge
 *  This class persists the logs from UI and stores them on the server
 */
@WebServlet("/api/uilogger")
public class UILogger extends HttpServlet {

  private static final long serialVersionUID = 1L;
  private static final Logger logger = LogManager.getLogger(UILogger.class);
  private static final Level uiTraceLevel = Level.forName("UITRACE", 350);
  /**
   * 
   */
  public UILogger() {
    super();
}
  /**
   * Method to get the log content from the Servlet request
   * 
   * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
   */
  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	Enumeration params = request.getParameterNames();
    while(params.hasMoreElements()){
        String paramName = (String)params.nextElement();
        logger.info("Parameter Passed "+paramName + " Param Value "+request.getParameter(paramName));
        wrtielogContent(request.getParameter(paramName),paramName);
    }
  }
  /**
   * Method to write logs into the log file in system
   * @param logText
   * @throws IOException
   */
  static void wrtielogContent(String logText, String portal) throws IOException{
    String date = new SimpleDateFormat("yyyyMMdd").format(new Date());
    String fileName = "SPPortal";
    if("LogVP".equals(portal)) {
      fileName = "VPPortal";
    }
	String usrHome = System.getProperty("user.home");
	
	//TODO externalize this
    //File logFile = new File("/"+usrHome+"/work/hubble/logs/portal/"+fileName+"_"+date+".log");
    Logger uiLogger = LogManager.getLogger("uiLogger");
    if(logText.length() > 0) {
    	logger.info("Logging in UI Logs");
    	uiLogger.info("Logging in UI Logs");
    	String [] logTextArr = logText.split("\n");
    	for(int i=0;i<logTextArr.length;i++) {
    		uiLogger.log(uiTraceLevel, logTextArr[i]);
    	}
    	//uiLogger.log(uiTraceLevel, logText);
    }
    /*if(logText.length()>0){
    	if(!logFile.exists()  ) {
    		logFile.createNewFile();
    	}
    }
    
    FileOutputStream fos = new FileOutputStream(logFile, true);
    fos.write(logText.getBytes());
    fos.close(); */
  }
}
