/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.servlets;

import java.io.IOException;
import java.net.HttpURLConnection;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.gps.hubble.ui.dataService.DataServiceDef;
import com.gps.hubble.ui.dataService.DataServiceManager;
import com.gps.hubble.ui.utilities.DataServiceUtils;
import com.gps.hubble.ui.utilities.UIContext;

/**
 * Servlet implementation class APIServlet
 */
@WebServlet("/api/*")
public class APIServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();
	private static final String jsonContentType = "application/json";
	
    /**
     * Default constructor. 
     */
    public APIServlet() {
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO remove doGet once in production
		doPost(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		long beginTime = System.currentTimeMillis();
		try {
			logger.info("Starting Execution of the API Servlet "+beginTime);
			UIContext context = UIContext.getUIContext(request, response);
			boolean tracing = context.isTracing();
			String dataServiceId = DataServiceUtils.getDataServiceId(context);
			
			if (tracing) {
				logger.info("DataService being invoked: {}.",  dataServiceId);
			}
			DataServiceDef dataServiceDef = DataServiceManager.getDataServiceDef(dataServiceId);
			if (dataServiceDef != null && dataServiceDef.isInternal()) {
				logger.error("Internal dataService {} being invoked from the API Servlet by loginid {}", dataServiceDef.getName(), context.getLoginid());
				context.setRemoteServerErrorStatusCode(HttpURLConnection.HTTP_NOT_FOUND);
				response.setStatus(context.getRemoteServerErrorStatusCode());
				return;
			}
			
			String jsonInput = DataServiceUtils.getDataServiceInput(context, dataServiceId);
			if (tracing) {
				logger.info("DataService input: {}", jsonInput);
			}
			JSONObject output = DataServiceUtils.invokeDataService(dataServiceId, DataServiceUtils.getJSONObject(jsonInput), context);
			if (context.isRemoteServerUnreachable()) {
				if (tracing) {
					logger.info("Remote server unreachable");
				}
				// TODO handle errors
				response.setStatus(0);
			} else if (context.getRemoteServerErrorStatusCode() != -1) {
				if (tracing) {
					logger.info("Remote server error code: {}", context.getRemoteServerErrorStatusCode());
				}
				// TODO handle errors
				response.setStatus(context.getRemoteServerErrorStatusCode());
				String errStr= context.getRemoteServerErrorDescription();
				response.setContentType(jsonContentType);
				//Adding Secure Response Headers
				response.addHeader("X-XSS-Protection", "1;mode=block");
				response.addHeader("X-Frame-Options", "sameorigin");
				response.addHeader("X-Content-Type-Options", "nosniff");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().append(errStr);
			} else {
			    
				String outputStr = "";
				if(output !=null) {
				  outputStr= output.toJSONString();
				} else {
				  output = new JSONObject();
				  output.put("Result", "ApiSuccess");
				  outputStr= output.toJSONString();
				}
				if (tracing) {
					logger.info("Output of DataService: {}", outputStr);
				}
				response.setContentType(jsonContentType);
				//Adding Secure Response Headers
				response.addHeader("X-XSS-Protection", "1;mode=block");
				response.addHeader("X-Frame-Options", "sameorigin");
				response.addHeader("X-Content-Type-Options", "nosniff");
				response.setCharacterEncoding("UTF-8");
				response.getWriter().append(outputStr);
			}
		} finally {
			long endTime = System.currentTimeMillis();
			logger.info("The Response time of the api servlet is:"+(endTime-beginTime));		
			
		}
	}

}
