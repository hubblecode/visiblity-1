/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.initializers;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.dataService.DataServiceManager;

/**
 * 
 * @author Bridge
 * Init Servlet to load the DataServices from the metadata
 */
@WebServlet(urlPatterns = {"/init/dataServiceLoader"}, loadOnStartup = 100)
public class DataServiceLoader extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private static final Logger logger = LogManager.getLogger();

    /**
     * @see HttpServlet#HttpServlet()
     */
    public DataServiceLoader() {
        super();
    }

	/**
	 * @see Servlet#init(ServletConfig)
	 */
	public void init(ServletConfig config) throws ServletException {
		logger.info("DataService loading started.");
		DataServiceManager.loadDataservices();
		
	}
}
