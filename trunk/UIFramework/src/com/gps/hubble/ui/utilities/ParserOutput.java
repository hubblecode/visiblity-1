/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.utilities;

/**
 * 
 * @author Bridge
 *  Bean class for Output of the JSON parser. This is used to indicate if the Input Validation was successful.
 */
public class ParserOutput {
	
	private boolean bSuccess;
	private String path;
	private String value;
	
	public boolean isSuccess() {
		return bSuccess;
	}
	public void setSuccess(boolean bSuccess) {
		this.bSuccess = bSuccess;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getValue() {
		return value;
	}
	public void setValue(String value) {
		this.value = value;
	}
	
	
}
