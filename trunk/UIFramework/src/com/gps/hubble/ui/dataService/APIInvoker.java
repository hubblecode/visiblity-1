/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.gps.hubble.ui.AppInfo;
import com.gps.hubble.ui.Constants;
import com.gps.hubble.ui.utilities.UIContext;

/*
 * This class Invokes Sterling REST Apis and returns the output in JSON format.
 */

public class APIInvoker {

	private static final String restUrl = AppInfo.getRemoteserverurl();
	private static final String restApiUrl = restUrl+"/invoke";
	private static final String loginApiUrl = restUrl+"/invoke/login";
	private static final String restServiceUrl = restUrl+"/executeFlow";
	private static final int CONNECT_TIMEOUT = 2000;
	private static final int REQUEST_TIMEOUT = 60000;
	private static final String MAX_CONNECTIONS = "10";
	private static final Logger logger = LogManager.getLogger(APIInvoker.class);

	static {
		System.setProperty("http.maxConnections", MAX_CONNECTIONS);
	}

	public static String invoke(UIContext context, DataServiceDef dataServiceDef, String input, String apiName, boolean isService, boolean useJson) {
		long beginTime = System.currentTimeMillis();
		HttpURLConnection connection = getURLConnection(context, dataServiceDef, apiName, isService, useJson);
		postData(connection, input, context);
		if (context.isRemoteServerUnreachable()) {
			return null;
		}
		String output = readData(connection, context);
		long endTime = System.currentTimeMillis();
		logger.info("The Response time of the api "+apiName+" is :"+(endTime-beginTime));
		return output;
	}

	private static String readData(HttpURLConnection connection, UIContext context) {
		int respCode = -1;
		try {
			BufferedReader in = new BufferedReader(new InputStreamReader(connection.getInputStream()));
			respCode = connection.getResponseCode();
			logger.info("The Response Code From Remote API is "+respCode);
			if(respCode == HttpURLConnection.HTTP_INTERNAL_ERROR) {
				context.setRemoteServerErrorStatusCode(respCode);
				return null;
			}
			if(respCode == HttpURLConnection.HTTP_FORBIDDEN || respCode == HttpURLConnection.HTTP_UNAUTHORIZED) {
				context.setRemoteServerErrorStatusCode(respCode);
				return null;
			}			
			String decodedString;
			StringBuilder outputBuilder = new StringBuilder();
			while ((decodedString = in.readLine()) != null) {
				outputBuilder.append(decodedString);
			}
			in.close();
			String output = outputBuilder.toString();
			return output;
		} catch (IOException e) {
			try {
				respCode = connection.getResponseCode();
				context.setRemoteServerErrorStatusCode(respCode);
				StringBuilder errorBuilder = new StringBuilder();
				InputStream es = connection.getErrorStream();
				// read the response body
				
				if(es!=null) {
					BufferedReader errReader = new BufferedReader(new InputStreamReader(es));
					String errorText;
					while ((errorText= errReader.readLine()) != null ) {
						errorBuilder.append(errorText);	
						//logger.error(errorText);
					}
					es.close();
				}	
				
				context.setRemoteServerErrorDescription(errorBuilder.toString());
				logger.error("Error Thrown from API" + errorBuilder.toString());
				
			} catch(IOException ex) {
			}
		}
		return null;
	}

	private static void postData(HttpURLConnection connection, String input, UIContext context) {
		try {
			BufferedOutputStream oStream = new BufferedOutputStream(connection.getOutputStream());
			oStream.write(input.getBytes("UTF-8"));
			oStream.flush();
			oStream.close();
		} catch (IOException e) {
			logger.catching(e);
			e.printStackTrace();
			context.setRemoteServerUnreachable(true);
		}
	}

	private static HttpURLConnection getURLConnection(UIContext context, DataServiceDef dataServiceDef, String apiName, boolean isService, boolean useJson) {
		try {
			HttpURLConnection connection = null;
			URL url = null;
			if ("login".equalsIgnoreCase(apiName)) {
				url = new URL(loginApiUrl);
			} else if (isService) {
				String finalUrl = createUrl(restServiceUrl, dataServiceDef, apiName, context);
				logger.debug("Service Url is "+finalUrl);
				url = new URL(finalUrl);
			} else {
				String finalUrl = createUrl(restApiUrl, dataServiceDef, apiName, context);
				logger.debug("API Url is "+finalUrl);
				url = new URL(finalUrl);
			}
			connection = (HttpURLConnection) url.openConnection();
			setupConnection(connection, context, useJson);
			return connection;
		}catch(IOException ioe) {
			logger.catching(ioe);
			ioe.printStackTrace();
			context.setRemoteServerUnreachable(true);
		}catch(Exception e) {
			logger.catching(e);
		}
		return null;
	}

	private static void setupConnection(HttpURLConnection connection, UIContext context, boolean useJson) throws Exception {
		String LtpaCookie = null;
		connection.setConnectTimeout(CONNECT_TIMEOUT);
		connection.setReadTimeout(REQUEST_TIMEOUT);
		if (useJson) {
			connection.addRequestProperty("Accept", Constants.jsonContentType);
			connection.addRequestProperty("Content-Type", Constants.jsonContentType);
		} else {
			connection.addRequestProperty("Accept", Constants.xmlContentType);
			connection.addRequestProperty("Content-Type", Constants.xmlContentType);
		}
		if(AppInfo.isSsomode()){
			LtpaCookie = Constants.LTPA_COOKIE_NAME + "=" + context.getLtpaToken();
			if(context.isTracing()) {
				logger.debug("Setting LTPA Token Before Calling API "+LtpaCookie);
			}
			connection.addRequestProperty("Cookie", LtpaCookie);
		} else {
			if(context.isTracing()) {
				logger.debug("Non SSO Mode.. So No Cookies set ");
			}
		}
  		connection.setRequestMethod("POST");
		connection.setDoInput(true);
		connection.setDoOutput(true);
		connection.setUseCaches(false);
	}

	private static String createUrl(String url, DataServiceDef dataServiceDef, String apiName, UIContext context) throws UnsupportedEncodingException {
		String token = context.getToken();
		String loginid = context.getLoginid();
		StringBuilder builder = new StringBuilder(url);
		builder.append("/");
		builder.append(apiName);
		builder.append("?_loginid=");
		builder.append(loginid);
		if(!AppInfo.isSsomode()) {
			builder.append("&_token=");
			builder.append(token);
		}

		if (dataServiceDef != null && dataServiceDef.getTemplateId() != null && dataServiceDef.requiresTemplate()) {
				builder.append("&_templateId=");
				builder.append(URLEncoder.encode(dataServiceDef.getTemplateId(), "UTF-8"));
			}
		return builder.toString();
	}

}
