/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

/**
 * This class maintains the definition of a data service. \
 * The DataServices are defined in JSON format and have specific attributes which are captured by this class.
 */
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gps.hubble.ui.utilities.JsonUtils;
import com.gps.hubble.ui.utilities.Utils;

public class DataServiceDef {

	private static final String CONSTANT_DATA = "const:";
	private static final String DYNAMIC_DATA = "data:";
	private static final String HEADER_DATA = "header:";
	private static final String ATTR_SEQUENCE = "sequence";
	private static final String PROTOTYPE_ATTR = "isprototype";
	private static final String INTERNAL_ATTR = "isinternal";
	private static final String IS_COMPOSITE_ATTR = "iscomposite";
	private static final String NAME_ATTR = "name";
	private static final String INPUT_ATTR = "input";
	private static final String IS_EXTENDED_ATTR = "isextended";
	private static final String API_NAME_ATTR = "apiname";
	private static final String CLASS_NAME_ATTR = "classname";
	private static final String HOOK_NAME_ATTR = "hookclass";
	private static final String RETAIN_EMPTY_ATTR = "retainemptyattrs";
	private static final String COMPOSITE = "composite";
	private static final String VALIDATION_REQUIRED = "validate";
	private static final String TEMPLATE_REQUIRED = "isTemplated";
	
	private String name = null;
	private String className = null;
	private String hookClassName = null;
	private boolean isExtended = false;
	private String apiName = null;
	private Document input = null;
	private HashMap<String, String> inputDataDef = null;
	private HashMap<String, String> constDataDef = null;
	private HashMap<String, String> headerDataDef = null;
	private IDataService dataService = null;
	private IDataServiceHook dataServiceHook = null;
	private ArrayList<JSONObject> dataServiceRefs = null;
	private boolean composite = false;
	private String templateId = null;
	private boolean isPrototype = false;
	private boolean isInternal = false;
	private boolean retainEmptyAttributes = false;
	private String appshortcode = "";
	private boolean isTemplate = true;
	private boolean validate = true;
	
	private static final Logger logger = LogManager.getLogger();
	
	public DataServiceDef(JSONObject dataService,String appShortCode) {
		
		this.name = (String) dataService.get(NAME_ATTR);
		logger.info("Parsing data service def for name: {}", this.name);
		this.composite = Utils.getBooleanValue((String)dataService.get(IS_COMPOSITE_ATTR));
		this.isPrototype = JsonUtils.getBooleanAttribute(dataService, PROTOTYPE_ATTR, false);
		this.isInternal = JsonUtils.getBooleanAttribute(dataService, INTERNAL_ATTR, false);
		this.retainEmptyAttributes = JsonUtils.getBooleanAttribute(dataService, RETAIN_EMPTY_ATTR, false);
		this.validate = JsonUtils.getBooleanAttribute(dataService,VALIDATION_REQUIRED,true);
		this.isTemplate=JsonUtils.getBooleanAttribute(dataService,TEMPLATE_REQUIRED,true);
		this.appshortcode=appShortCode;

		//this.className = (String) dataService.get(CLASS_NAME_ATTR);
		this.hookClassName= (String) dataService.get(HOOK_NAME_ATTR);
		if(this.hookClassName != null) {
			loadHookClass();
		}
		
		if (composite) {
			readCompositeDataServiceDef(dataService);
		} else {
			readDataServiceDef(dataService);
		}
	}
	public boolean requiresTemplate() {
		return isTemplate;
	}
	
	public boolean isRetainEmptyAttributes() {
		return retainEmptyAttributes;
	}

	@SuppressWarnings("unchecked")
	private void readCompositeDataServiceDef(JSONObject dataService) {
		JSONArray refList = (JSONArray) dataService.get(COMPOSITE);
		Collections.sort(refList, new Comparator<JSONObject>() {
			@Override
			public int compare(JSONObject ref1, JSONObject ref2) {
				int sequence1 = JsonUtils.getIntAttribute(ref1, ATTR_SEQUENCE, -1);
				int sequence2 = JsonUtils.getIntAttribute(ref2, ATTR_SEQUENCE, -1);
				if (sequence1 < sequence2) {
					return -1;
				} else if (sequence1 == sequence2) {
					return 0;
				}
				return 1;
			}
		});
		dataServiceRefs = refList;
	}

	private void readDataServiceDef(JSONObject dataService) {
				
		this.apiName = (String) dataService.get(API_NAME_ATTR) ;
		this.isExtended = Utils.getBooleanValue((String)dataService.get(IS_EXTENDED_ATTR));
		
		
		JSONObject inputObj = (JSONObject) dataService.get(INPUT_ATTR);
		if (!JsonUtils.isVoid(inputObj)) {
			//since xsl gives root element as one of the key. so, we can't pass input as it is
			//consistency should be there for newly added json also
			Set<String> keys = inputObj.keySet();
			String entrykey = null;
			int i = 0;
			for (String key : keys) {
				entrykey = key;
				i++;
			}
			if(entrykey != null || i != 1){
				Object rootObj = inputObj.get(entrykey);
				parseInputForData(rootObj,"");
			}else{
				logger.debug("input should have only one key");
			}
			
		}
		this.templateId = this.appshortcode+this.name;
	}

	private void parseInputForData(Object object,String path) {
		
		if(object instanceof JSONObject && !JsonUtils.isVoid((JSONObject)object)){
			
			JSONObject inputObj = (JSONObject) object;
			Set<String> attrs = inputObj.keySet();
			
			for (String attr : attrs) {
				
				Object value = inputObj.get(attr);
				
				if(value instanceof String){
					handleJsonString(value,manipulatePath(path, attr));
				}else if (value instanceof JSONObject) {
					parseInputForData((JSONObject)value,manipulatePath(path, attr));
				}else if (value instanceof JSONArray) {
					handleJsonArray(value,path,attr);
				}
			}
			
		}
	}

	
	private void handleJsonArray(Object value, String path, String attr) {
		
		JSONArray array = (JSONArray) value;
		int count = 0;
		for (Iterator iterator = array.iterator(); iterator.hasNext();count++) {
			JSONObject object = (JSONObject) iterator.next();
			String pathSuffix = attr+"["+count+"]";
			parseInputForData(object, path+"."+pathSuffix);
		}
		
	}

	private void handleJsonString(Object value, String path) {
		
		String strValue = (String) value;
		if (strValue != null && strValue != "") {
			int index = strValue.indexOf(DYNAMIC_DATA);
			if (index != -1) {
				String dataKey = strValue.substring(DYNAMIC_DATA.length());
				if (inputDataDef == null) {
					inputDataDef = new HashMap<String, String>();
				}
				inputDataDef.put(path, dataKey);
				return;
			}
			index = strValue.indexOf(CONSTANT_DATA);
			if (index != -1) {
				String dataKey = strValue.substring(CONSTANT_DATA.length());
				if (constDataDef == null) {
					constDataDef = new HashMap<String, String>();
				}
				constDataDef.put(path, dataKey);
				return;
			}
			index = strValue.indexOf(HEADER_DATA);
			if (index != -1) {
				String dataKey = strValue.substring(HEADER_DATA.length());
				if (headerDataDef == null) {
					headerDataDef = new HashMap<String, String>();
				}
				headerDataDef.put(path, dataKey);
				return;
			}
		}
	}

	private String manipulatePath(String path, String attr) {
		
		if(path == null || path.length() == 0){
			return attr;
		}
		return path+"."+attr;
	}

	public void createDataServiceImpl() {
		try {
			Class<?> implClass = Class.forName(className);
			dataService = (IDataService) implClass.newInstance();
			dataService.setDataServiceDef(this);
		} catch (Exception e) {
			logger.error("Exception while creating data service class: Data Service: {} classname: {}.", name, className);
			logger.catching(e);
		}
	}

	public void loadHookClass() {
		try {
			Class<?> implClass = Class.forName(hookClassName);
			dataServiceHook = (IDataServiceHook) implClass.newInstance();
		} catch (Exception e) {
			logger.error("Exception while creating data service hook class: Data Service Hook: {} classname: {}.", name, hookClassName);
			logger.catching(e);
		}

		
	}
	public IDataService getDataService() {
		return dataService;
	}
	
	public IDataServiceHook getDataServiceHook() {
		return dataServiceHook;
	}	

	public Document getInput() {
		return input;
	}
	
	public String getHookClass() {
		return hookClassName;
	}

	public void setHookClass(String hookClassName) {
		this.hookClassName = hookClassName;
	}
	/*public String getClassName() {
		return className;
	}

	public void setClassName(String className) {
		this.className = className;
	}*/

	public boolean isExtended() {
		return isExtended;
	}

	public boolean isPrototype() {
		return isPrototype;
	}

	public boolean isInternal() {
		return isInternal;
	}

	public String getAPIName() {
		return apiName;
	}

	public HashMap<String, String> getInputDataDef() {
		return inputDataDef;
	}

	public HashMap<String, String> getConstDataDef() {
		return constDataDef;
	}

	public HashMap<String, String> getHeaderDataDef() {
		return headerDataDef;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean isComposite() {
		return composite;
	}

	public void setComposite(boolean composite) {
		this.composite = composite;
	}

	public ArrayList<JSONObject> getDataServiceRefs() {
		return dataServiceRefs;
	}

	public String getTemplateId() {
		return templateId;
	}
	
	public boolean isValidationRequired() {
	      return validate;
	  }
	
}
