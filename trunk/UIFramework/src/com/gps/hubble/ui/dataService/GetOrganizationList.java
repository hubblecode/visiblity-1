/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import java.net.HttpURLConnection;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.gps.hubble.ui.utilities.UIContext;

/**
 * 
 * @author Bridge
 * This class invokes getOrganizationList
 *
 */
public class GetOrganizationList {

	
	public static String getOrganization( UIContext context,String input) throws ParseException {
		JSONObject orgInputObj = new JSONObject();
		orgInputObj.put("OrganizationCode",input);
		DataServiceDef orgDataServiceDef= DataServiceManager.getDataServiceDef("getOrganizationList");
		JSONParser orgParser = new JSONParser();	
		JSONObject orgOutput = (JSONObject) orgParser.parse( APIInvoker.invoke(context,orgDataServiceDef, orgInputObj.toJSONString(), orgDataServiceDef.getAPIName(), orgDataServiceDef.isExtended(), true))  ;
		if(orgOutput !=null){
			JSONArray organizationList=(JSONArray)orgOutput.get("Organization");
			if(organizationList!=null){
		         JSONObject organization = (JSONObject)organizationList.get(0);
		    	   String organizationName  = (String)organization.get("OrganizationName");
		    	   if(organizationName != null) {
		    		  //  massagedOutput.put("OrganizationName",organizationName);
		    		   return organizationName;
		    	       }
		      
		   }
		}

		return input;
	}

}
