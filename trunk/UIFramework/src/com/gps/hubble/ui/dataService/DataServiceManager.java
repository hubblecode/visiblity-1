/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.dataService;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import com.gps.hubble.ui.utilities.JsonUtils;
import com.gps.hubble.ui.utilities.UIContext;
import com.gps.hubble.ui.utilities.XMLUtils;

/**
 * 
 * @author Bridge
 * This class loads the DataServices JSON file and maintains the metadata
 *
 */
public class DataServiceManager {

	private static final String DATA_SERVICE_ID_MANAGE_TEMPLATE = "manageApiTemplate";
	private static final String APPLICATION_CODE = "AppCode";
	private static final String DATA_SERVICE_ELEM_NAME = "dataservice";
	private static final String APP_SHORT_CODE = "appshortcode";
	private static int counter = 0;
	private static int counterForTemplate = 0;
	private static Map<Integer, String> registeredFiles = new HashMap<>();
	private static Map<Integer, String> registeredTemplateFiles = new HashMap<>();
	private static Map<String, DataServiceDef> dataservices = new HashMap<>();
	private static Map<String, DataServiceDef> dataserviceTemplates = new HashMap<>();
	private static final Logger logger = LogManager.getLogger();
	public static final String defaultOrg = "DEFAULT";

	public static void registerDataServiceFile(String dataServiceFile) {
		registeredFiles.put(counter, dataServiceFile);
		counter++;
	}

	public static void registerDataServiceTemplateFile(String dataServiceTemplateFile) {
		registeredTemplateFiles.put(counterForTemplate, dataServiceTemplateFile);
		counterForTemplate++;
	}

	public static void loadDataservices() {
		for (int i = 0; i < counter; i++) {
			loadDataService(registeredFiles.get(i));
		}
		logger.info("Total DataServices loaded: {}", dataservices.size());
	}

	private static void loadDataService(String file) {

		JSONObject jsonObject = JsonUtils.createJsonObjectFromFile(file);
		String appshortcode = (String) jsonObject.get(APP_SHORT_CODE);
		JSONArray dataServiceList = (JSONArray) jsonObject.get(DATA_SERVICE_ELEM_NAME);
		for (Object object : dataServiceList) {
			JSONObject dataServiceObj = (JSONObject) object;
			try {
				DataServiceDef dataServiceDef = new DataServiceDef(dataServiceObj, appshortcode);
				//dataServiceDef.createDataServiceImpl();
				dataservices.put(dataServiceDef.getName(), dataServiceDef);
			} catch (Exception e) {
				logger.catching(e);
			}
		}
	}

	public static DataServiceDef getDataServiceDef(String dataServiceId) {
		return dataservices.get(dataServiceId);
	}

	// What's the purpose
	public static void syncDataServiceTemplates(UIContext context) {

		for (int i = 0; i < counterForTemplate; i++) {
			syncDataServiceTemplate(context, registeredTemplateFiles.get(i));
		}
	}

	private static void syncDataServiceTemplate(UIContext context, String file) {

		Document dsTempDoc = XMLUtils.createDocumentFromFile(file);
		Element dsTempElem = dsTempDoc.getDocumentElement();
		String appCode = dsTempElem.getAttribute(APPLICATION_CODE);
		List<Element> dsTempElemList = XMLUtils.getChildElements("DataServiceTemplate", dsTempElem);
		for (Element element : dsTempElemList) {
			try {
				String dataServiceId = element.getAttribute("DataServiceId");
				String apiName = element.getAttribute("APIName");
				Element tempElem = XMLUtils.getChildElement("Template", element);
				if (tempElem != null) {
					List<Element> childElemList = XMLUtils.getChildElements(tempElem);
					if (childElemList != null && childElemList.size() > 0) {
						registerTemplate(context,appCode+dataServiceId, apiName, childElemList.get(0));
					}
				}
			} catch (Exception e) {
				logger.catching(e);	
			}
		}

	}

	private static void registerTemplate(UIContext context, String templateId, String sApiName, Element element) {

		DataServiceDef manageDataServiceDef = getDataServiceDef(DATA_SERVICE_ID_MANAGE_TEMPLATE);
		
		JSONObject jsonObject = getManageTemplateJsonObject(templateId,sApiName);
		
		Document document = XMLUtils.createDocumentFromElement(element);
		jsonObject.put("TemplateData", XMLUtils.getString(document));
		APIInvoker.invoke(context, null, jsonObject.toJSONString(), manageDataServiceDef.getAPIName(),
				manageDataServiceDef.isExtended(), true);
	}

	private static JSONObject getManageTemplateJsonObject(String templateId,String sApiName) {
		
		JSONObject jsonObject = new JSONObject();
		jsonObject.put("Action", "Manage");
		jsonObject.put("TemplateType", "00");
		jsonObject.put("OrganizationCode", defaultOrg);
		jsonObject.put("TemplateId", templateId);
		jsonObject.put("ApiName", sApiName);
		return jsonObject;
	}
}
