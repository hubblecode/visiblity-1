/** 
 * Copyright (c) IBM Australia Limited 2016. All rights reserved.
*/
package com.gps.hubble.ui.listener;

import java.util.EnumSet;

import javax.servlet.DispatcherType;
import javax.servlet.FilterRegistration;
import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import com.gps.hubble.ui.utilities.Utils;


/**
 * 
 * @author Bridge
 * Context Listener 
 */
@WebListener
public class GpsUIContextListener implements ServletContextListener {
	
	private final String SSO_MODE = "SSO_MODE";

    /**
     * Default constructor. 
     */
    public GpsUIContextListener() {
        // TODO Auto-generated constructor stub
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent arg) {
    	ServletContext ctx = arg.getServletContext();
    	FilterRegistration fr = null;
    	String sAuthmode = ctx.getInitParameter(SSO_MODE);

    	if(sAuthmode != null && !sAuthmode.isEmpty() && Utils.getBooleanValue(sAuthmode)){
      	    fr = ctx.addFilter("SSO Authorization Filter", com.gps.hubble.ui.filters.SSOAuthorizationFilter.class);
    	}else {
    		fr = ctx.addFilter("Authorization Filter", com.gps.hubble.ui.filters.AuthorizationFilter.class);
    	}
    	fr.addMappingForUrlPatterns(EnumSet.of(DispatcherType.REQUEST), true, new String[]{ "/api/*", "/init/*"});
    }

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent arg0) {
        // TODO Auto-generated method stub
    }
	
}
